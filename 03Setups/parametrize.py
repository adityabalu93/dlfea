import os

possible_keys = ['Time_Period', 'Delt', 'Nstep', 'ifq','ifq_tq','rhoinf', 'Mode',
    'T_START','InitPress','DENS_AIR','VISC_AIR','WBC_TauNor','WBC_TauTan',
    'Adap_Int_Level','NS_hessian','NS_GMRES_tol','NS_GMRES_itermax',
    'NS_GMRES_itermin','NS_NL_Utol','NS_NL_Ptol','NS_NL_itermax',
    'Mesh_GMRES_tol','Mesh_GMRES_itermax','Mesh_GMRES_itermin',
    'NPatch','NBC_set','Density_Shell','E_Shell','nu_Shell',
    'Thickness_Shell','Material_Shell','Coef_a','Coef_b','Coef_c',
    'MtCoef_1','MtCoef_2','MtCoef_3','MtCoef_4','MtCoef_5','fib_ang',
    'Density_Stent','E_Stent','nu_Stent','Thickness_Stent','Material_Stent',
    'NGauss_TH_Shell','Shell_CG_Tol','contact_k1','contact_k2','contact_k3',
    'contact_pow','contact_cutoff','contact_lox','contact_loy','contact_loz',
    'contact_hix','contact_hiy','contact_hiz','contact_nx','contact_ny',
    'contact_nz','Damping_a','Damping_b']

int_keys = ['Nstep', 'ifq','ifq_tq', 'Mode', 'Adap_Int_Level', 'NS_hessian',
    'NS_GMRES_itermax', 'NS_GMRES_itermin', 'NS_NL_itermax', 'Mesh_GMRES_itermax',
    'Mesh_GMRES_itermin', 'NPatch', 'NBC_set', 'Material_Shell', 'NGauss_TH_Shell',
    'contact_pow', 'contact_nx', 'contact_ny', 'contact_nz', 'Material_Stent']

float_keys = ['T_START','MtCoef_1','MtCoef_2','MtCoef_3','fib_ang']

double_keys = ['Time_Period','Delt','rhoinf','InitPress','DENS_AIR',
    'VISC_AIR','WBC_TauNor','WBC_TauTan','NS_GMRES_tol','NS_NL_Utol',
    'NS_NL_Ptol','Mesh_GMRES_tol','Density_Shell','E_Shell','nu_Shell',
    'Thickness_Shell','Coef_a','Coef_b','Coef_c','MtCoef_4','MtCoef_5',
    'Density_Stent','E_Stent','nu_Stent','Thickness_Stent','Shell_CG_Tol',
    'contact_k1','contact_k2','contact_k3','contact_cutoff','contact_lox',
    'contact_loy','contact_loz','contact_hix','contact_hiy','contact_hiz',
    'Damping_a','Damping_b']



def ReadParam(filename):
    Param = {}
    with open(filename, 'r') as f:
        for line in f:
            if line.startswith('!') or line.startswith('#'):
                continue
            if len(line.strip().split('=')) == 2:
                key, val = line.strip().split()[0], line.strip().split()[2]
                assert key in possible_keys
                if key in int_keys:
                    Param[key] = int(val)
                elif key in float_keys:
                    Param[key] = float(val)
                elif key in double_keys:
                    Param[key] = float(val.replace('d', 'e'))
    return Param

def WriteParam(Param, filename):
    with open(filename, 'w') as f:
        for key in possible_keys:
            if key in int_keys:
                f.write('%s = %s\n'%(key, Param[key]))
            elif key in float_keys:
                f.write('%s = %.10e\n'%(key, Param[key]))
            elif key in double_keys:
                valstring = ('%.10e'%Param[key]).replace('e','d')
                f.write('%s = %s\n'%(key, valstring))




def GenParam(Foldername):
    thicknesses = [0.01, 0.0142, 0.0186, 0.0231, 0.0327, 0.0427, 0.05]
    pressures = [float(press) for press in range(70, 92, 2)]
    MtCoef_1 = 6.760546875e+05
    MtCoef_2 = 1.328477454e+05
    MtCoef_3 = 0.381877971e+02

    MtCoefs_1 = [(1 + 0.2*(i-1))*MtCoef_1 for i in range(3)]
    MtCoefs_2 = [(1 + 0.2*(i-1))*MtCoef_2 for i in range(3)]
    MtCoefs_3 = [(1 + 0.2*(i-1))*MtCoef_3 for i in range(3)]

    print(MtCoefs_1)
    print(MtCoefs_2)
    print(MtCoefs_3)

    if not os.path.isdir(Foldername):
        os.makedirs(Foldername)

    numparams = 0
    for thickness in thicknesses:
        for pressure in pressures:
            for MtCoef_1 in MtCoefs_1:
                for MtCoef_2 in MtCoefs_2:
                    for MtCoef_3 in MtCoefs_3:
                        Param = ReadParam('Template.dat')
                        Param['Thickness_Shell'] = thickness
                        Param['InitPress'] = pressure
                        Param['MtCoef_1'] = MtCoef_1
                        Param['MtCoef_2'] = MtCoef_2
                        Param['MtCoef_3'] = MtCoef_3
                        Param['Nstep'] = 160
                        Param['ifq'] = 20
                        Param['Damping_a'] = 200.0
                        WriteParam(Param, os.path.join(Foldername, str(numparams)+'.dat'))
                        numparams += 1


def main():
    GenParam('Params')

if __name__ == '__main__':
    main()

