! subroutines to compute contact forces at gauss points of a shell structure

! subroutine to get the velocity of a paramteric point
subroutine getVelocityOfParametricPoint(tsp,bez,nsd,iel,xi,ushAlpha,v)

  use types_shell
  use commonvar_shell
  use commonvar_contact

  implicit none

  type(mesh), intent(in) :: tsp,bez

  integer, intent(in) :: nsd, iel
  real(8), intent(in) :: xi(2)
  real(8), intent(out) :: v(nsd)
  real(8), intent(inout) :: ushAlpha(tsp%NNODE,nsd)

  !  Local variables
  integer :: p, q, nshl, nuk, nvk, ptype, igauss, jgauss, &
             i, j, ni, nj, aa, bb, igp, s,nshb

  real(8) :: gp(tsp%NGAUSS), gw(tsp%NGAUSS), gwt, da, VVal, &
             DetJb_SH, nor(NSD), &
             xu(NSD), xd(NSD), dxdxi(NSD,2), ddxddxi(nsd,3), DetJb_r

  integer, allocatable :: lIEN(:)
  real(8), allocatable :: shl(:), shgradl(:,:), shhessl(:,:)

  ! initialize v to zero
  v = 0.0d0

  ! get NURB coordinates
!  ni = NRB%INN(iel,1); nj = NRB%INN(iel,2)
    
  ! Check to see if current element has nonzero area, 
  ! skip if it doesn't
!  if ((NRB%U_KNOT(iel,ni) /= NRB%U_KNOT(iel,ni+1)) .and. &
!       (NRB%V_KNOT(iel,nj) /= NRB%V_KNOT(iel,nj+1))   ) then
         
     ! used in calculating quadrature points. The factor of 4.0d0
     ! comes from mapping from the [-1,1] line onto a real segment...
!     da = (NRB%U_KNOT(iel,ni+1)-NRB%U_KNOT(iel,ni))*  &
!          (NRB%V_KNOT(iel,nj+1)-NRB%V_KNOT(iel,nj))/4.0d0

!     p = NRB%P(iel); nuk = NRB%NUK(iel); nshl = NRB%NSHL(iel)
!     q = NRB%Q(iel); nvk = NRB%NVK(iel); ptype = NRB%PTYPE(iel)

  p = BEZ%P(iel); nshl = TSP%NSHL(iel); ptype = TSP%PTYPE(iel)
  q = BEZ%Q(iel); nshb = BEZ%NSHL(iel)
                    
  allocate(shl(nshl), shgradl(nshl,2), &
       shhessl(nshl,3), lIEN(nshl))

  lIEN = -1
  do i = 1, nshl
     lIEN(i) = tsp%IEN(iel,i)
  end do

     ! Get Element Shape functions and their gradients
     shl = 0.0d0; shgradl = 0.0d0; shhessl = 0.0d0
     xu = 0.0d0; xd = 0.0d0; dxdxi = 0.0d0; ddxddxi = 0.0d0
     nor = 0.0d0

!     call eval_SHAPE_shell(xi(1), xi(2),  &
!          shl, shgradl, shhessl, nor,  &
!          xu, xd, dxdxi, ddxddxi,  &
!          p, q, nsd, nshl, &
!          lIEN, NRB%NNODE, &
!          NRB%B_NET_U, NRB%B_NET_D, DetJb_SH, &
!          ni, nj, nuk, nvk, &
!          NRB%U_KNOT(iel,1:nuk), &
!          NRB%V_KNOT(iel,1:nvk))

     call eval_SHAPE_bez_sh(xi(1), xi(2), &
          shl, shgradl, shhessl, nor, &
          xu, xd, dxdxi, ddxddxi,  &
          p, q, nsd, nshl, nshb,  &
          lien, TSP%NNODE, &
          TSP%B_NET_U, TSP%B_NET_D, DetJb_r, DetJb_sh, &
          BEZ%Ext(iel,1:nshl,1:nshb))

     ! get velocity of point from nodal velocities at alpha level
     do s = 1,nshl
        v(:) = v(:) + shl(s)*ushAlpha(lien(s),:)
     enddo

     deallocate(shl, shgradl, shhessl, lIEN)

  !end if  ! end if nonzero areas elements

end subroutine getVelocityOfParametricPoint

! given a point, compute the force on it due to its closest neighbor
subroutine computeForceOnPoint(sur,bez,el,pt,shl,ushAlpha)

  use types_shell
  use commonvar_contact
  use commonvar
  implicit none

  type(mesh), intent(inout) :: sur,bez
  integer, intent(in) :: el,pt
  type(shell), intent(inout) :: SHL
  real(8), intent(inout) :: ushAlpha(sur%NNODE,nsd)
  
  real(8) :: dx(3), dxdotn, dxn(3), f(3), fmag, dist, cnor(3), du(3), dudotn,&
       dadotn, da(3), lhsContrib(9), fdir(3), vf(3), lhsContribVel(9), a, w,&
       lhsContribPos(9)
  integer :: cel, cpt

  real(8) :: tp(2),txi(3), tnor(3), tdist, &
       guess(2,1), v(3), vdiff(3),ux,cauchyux,zeroguess(2,8), &
       cauchyderiv, vm(3), ndotn
  integer :: tel, tnewt, P_Flag
  integer :: i,j,gpindex,nshl1,nshl2

  zeroguess = 0.0

  ! an element always "contacts itself", albeit without forces; this is 
  ! really just a trick to make sure that the IEN array for the shell
  ! is built once to handle both contact and elasticity
  gpindex = (el-1)*(sur%ngauss**2) + pt
  nshl1 = sur%nshl(el)
  sur%gp%cien(gpindex,1:nshl1) = sur%ien(el,1:nshl1)
  sur%gp%cnshl(gpindex) = nshl1
  ! leave sgn as zero; this way no forces (even if nonzero by some accident)
  ! can be added

  ! get index of closest point info
  cel = sur%gp%closestPoint(el,pt,1)


  !!!!! DEBUG !!!!
  !cel = 0
  !!!!!!!!!!!!!!!!



  ! if we aren't contacting anything, leave now
  if(cel > 0) then

  cpt = sur%gp%closestPoint(el,pt,2)
  cnor(:) = sur%gp%nor(cel,cpt,:)
  guess(:,1) = sur%gp%param(cel,cpt,:)

  ! find the actual parametric point on that element that is closest,
  ! using a newton iteration.  first search the suspected element.  then, if
  ! p_flag is zero, search neighbors
  call find_point_s2_interior_tsp(sur,bez, 3, sur%gp%xg(el,pt,:), &
       1, (/cel/), &
       guess, tel, tp, &
       tdist, txi, tnor, tnewt, P_Flag)


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!   Since there's no immediate way to get element neighbors for the t-spline
!   case, let's just leave this out and hope for the best.  
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  if(P_Flag == 0) then
!     call find_point_s2_interior(sur, 3, sur%gp%xg(el,pt,:), &
!          8, sur%elementNeighbors(cel,1:8), &
!          zeroguess, tel, tp, &
!          tdist, txi, tnor, tnewt, P_Flag)
!  endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  ! it is still possible that no point was found on the interior of an element,
  ! specifically at the edges.
  if(P_Flag > 0) then

     ! look at the displacement between the points and its relation to the
     ! surface normal of the contacting (slave) surface
     dx = txi(:) - sur%gp%xg(el,pt,:) ! (from master pt to slave pt)
     dxdotn = -sum(dx*tnor)
     dist = sqrt(sum(dx**2))

     ! check the absolute value of the dot product of the normals; if it is
     ! too small, disregard the potential contact
     ndotn = sum(tnor*sur%gp%nor(el,pt,:))

     ! ignore points that are too far away; these are to be considered
     ! false positives.  also, avoid building extra matrix terms unless 
     ! one of the conditions for them being nonzero is actually met
     if(dist < contact_cutoff .and. &

          (dxdotn >= 0.0 .or. abs(ndotn) > 0.7) .and. & !!!!!!!!!!!!!!!!!!!

          (dxdotn < 0.0 .or. dist < contact_k3)) then

        ! get the velocity of the slave point
        call getVelocityOfParametricPoint(sur,bez,nsd,tel,tp,ushAlpha,v)

        ! if penetration is worsening, penalize normal component of velocity 
        ! difference
        vdiff = v - sur%gp%ush(el,pt,:) ! (vel of slave pt relative to master)

     ! update the list of nodes contacting this element and the list of signs
     ! for the associated shape functions
     sur%gp%celem(gpindex) = tel
     sur%gp%cparam(el,pt,:) = tp
     nshl2 = sur%nshl(tel)
     sur%gp%cnshl(gpindex) = nshl1 + nshl2
     do i=1,nshl1
        sur%gp%cien(gpindex,i) = sur%ien(el,i)
     enddo
     do i=1,nshl2
        j = i + nshl1
        sur%gp%cien(gpindex,j) = sur%ien(tel,i)
     enddo

     ! initialize forces and lhs contribs to zero
     f = 0.0
     vf = 0.0
     lhsContribPos = 0.0
     lhsContribVel = 0.0

     ! if slave pt is on wrong side of master pt, apply linear penalty
     if(dxdotn < 0.0) then

        fdir = dx/dist
        f = contact_k1*dx + 0.5*contact_k3*contact_k1*fdir

        ! linearize allowing master point to slide perpendicular to contact 
        ! force from slave surface (assume force is normal to slave, but
        ! do not actually use slave normal, to naturally allow edge forces)
        lhsContrib(1) = fdir(1)*fdir(1)
        lhsContrib(5) = fdir(2)*fdir(2)
        lhsContrib(9) = fdir(3)*fdir(3)
        lhsContrib(2) = fdir(1)*fdir(2)
        lhsContrib(4) = fdir(2)*fdir(1)
        lhsContrib(3) = fdir(1)*fdir(3)
        lhsContrib(7) = fdir(3)*fdir(1)
        lhsContrib(6) = fdir(2)*fdir(3)
        lhsContrib(8) = fdir(3)*fdir(2)
        
        ! if penetration is worsening, penalize normal component of velocity 
        ! difference
        !vdiff = v - sur%gp%ush(el,pt,:) ! (vel of slave pt relative to master)
        
        vf = 0.0
        lhsContribVel = 0.0
        ! if slave point is moving away from master, penalize velocity
        if(sum(vdiff*dx) > 0.0) then
           vf = contact_k2*sum(vdiff*fdir)*fdir
           ! velocity contribution is in same normal direction, but
           ! scaled by k2
           lhsContribVel = contact_k2*lhsContrib
        endif
        
        ! scale lhs contribution from position penalty
        lhsContribPos = contact_k1*lhsContrib
        
     
     elseif(dist < contact_k3) then ! maybe apply reuplsive force
        fdir = dx/dist
        a = contact_k1/(2.0*contact_k3)
        ! quadratic force
        f = -a*((contact_k3 - dist)**2)*fdir

        ! linearize allowing master point to slide perpendicular to contact 
        ! force from slave surface (assume force is normal to slave, but
        ! do not actually use slave normal, to naturally allow edge forces)
        lhsContrib(1) = fdir(1)*fdir(1)
        lhsContrib(5) = fdir(2)*fdir(2)
        lhsContrib(9) = fdir(3)*fdir(3)
        lhsContrib(2) = fdir(1)*fdir(2)
        lhsContrib(4) = fdir(2)*fdir(1)
        lhsContrib(3) = fdir(1)*fdir(3)
        lhsContrib(7) = fdir(3)*fdir(1)
        lhsContrib(6) = fdir(2)*fdir(3)
        lhsContrib(8) = fdir(3)*fdir(2)

        ! get the velocity of the slave point
        !call getVelocityOfParametricPoint(sur,nsd,tel,tp,ushAlpha,v)
        
        vf = 0.0
        lhsContribVel = 0.0
        lhsContribPos = 0.0
        ! if slave point is moving toward master, penalize velocity
        if(sum(vdiff*dx) < 0.0) then
          ux = dist/contact_k3
           cauchyux = exp(-((ux/(1.0-ux))**2))
           cauchyderiv = cauchyux*(-2.0*ux/(1.0-ux))*(1.0/(1.0-ux) - &
                ux/((1.0-ux)**2))/contact_k3
           vf = contact_k2*cauchyux*sum(vdiff*fdir)*fdir
           ! velocity contribution is in same normal direction, but
           ! scaled by k2
           lhsContribVel = contact_k2*cauchyux*lhsContrib
           ! need position derivative as well (what sign?)
           !lhsContribPos = contact_k2*cauchyderiv*lhsContrib*sum(vdiff*fdir)
        endif

        ! TODO: add position derivative contributions from velocity penalty
        ! to position LHS

        ! scale position penalty lhs for quadratic force
        lhsContribPos = lhsContribPos + 2.0*a*(contact_k3 - dist)*lhsContrib

     endif
     
     ! apply a force normal to this tangent plane to stop penetration
     w = sur%gp%gw(el,pt)*sur%gp%detJ(el,pt)
     sur%gp%contactForce(el,pt,:) = f*w
     sur%gp%contactVelForce(el,pt,:) = vf*w
     sur%gp%contactLhs(el,pt,:) = lhsContribPos*w
     sur%gp%contactLhsVel(el,pt,:) = lhsContribVel*w

  endif ! end if close point was close enough

  endif ! end if found close point

endif ! end if possibility of contact


end subroutine computeForceOnPoint

! check whether points are closest to each other
subroutine comparePoints(sur,el1,pt1,el2,pt2)

  use types_shell
  use commonvar_contact
  implicit none

  type(mesh), intent(inout) :: sur
  integer, intent(in) :: el1,el2,pt1,pt2

  real(8) :: dist, dx(3), dotn1, dotn2, dotu, fmag, f(3), s, ndotn, &
       tp(2), tdist, txi(3)
  integer :: tnewt, P_Flag, tel


  ! exclude points in the same patch
  if( .not.(sur%elementPatchNumbers(el1) == &
       sur%elementPatchNumbers(el2)) ) then

     dx(:) = sur%gp%xg(el2,pt2,:) - sur%gp%xg(el1,pt1,:) ! (from 1 to 2)
     dist = sqrt(sum(dx**2))

     ! check relative orientations of normals
     ndotn = sum(sur%gp%nor(el1,pt1,:)*sur%gp%nor(el2,pt2,:))

     ! try just searching for close gauss points with a threshold
     ! that is implicitly determined by the contact grid; use 
     ! contact_cutoff on the ACTUAL close points found through Newton
     ! iteration

     !if(ndotn < 0.0) then

        ! consider for contacting point, based on whether normals detect
        ! penetration
        !dotn1 = sum(dx(:)*sur%gp%nor(el1,pt1,:))
        
        ! master/slave relationships based on patch numbers (smaller means
        ! master to larger)

        !if(sur%elementPatchNumbers(el1) > sur%elementPatchNumbers(el2)) then

        ! try reversed...
        !if(sur%elementPatchNumbers(el1) < sur%elementPatchNumbers(el2)) then

           if(sur%gp%closestPoint(el2,pt2,1) == 0 .or. &
                dist < sur%gp%closestDist(el2,pt2)) then
              sur%gp%closestPoint(el2,pt2,1) = el1
              sur%gp%closestPoint(el2,pt2,2) = pt1
              sur%gp%closestDist(el2,pt2) = dist
           endif
        !else
           !dotn2 = sum(-dx(:)*sur%gp%nor(el2,pt2,:))
           if(sur%gp%closestPoint(el1,pt1,1) == 0 .or. &
                dist < sur%gp%closestDist(el1,pt1)) then
              sur%gp%closestPoint(el1,pt1,1) = el2
              sur%gp%closestPoint(el1,pt1,2) = pt2
              sur%gp%closestDist(el1,pt1) = dist
           endif
        !endif

     !endif

        endif
end subroutine comparePoints

! check for closest points within one cell
subroutine compareCellToSelf(sur,cells,c)

  use types_shell
  implicit none

  type(contactCells), intent(inout) :: cells
  type(mesh), intent(inout) :: sur
  integer, intent(in) :: c

  integer :: i1,i2,j1,j2,ni1,ni2,nj1,nj2

  ! iterate over the gauss points in c1
  i1 = cells%hashTable(c,1)
  i2 = cells%hashTable(c,2)
  do while(i1 > 0 .and. i2 > 0)
     ni1 = cells%gpLinkedList(i1,i2,1)
     ni2 = cells%gpLinkedList(i1,i2,2)
     j1 = ni1
     j2 = ni2
     do while(j1 > 0 .and. j2 > 0)
        
        ! compare points pairwise
        call comparePoints(sur, i1,i2, j1,j2)

        ! move to the next element in the inner loop
        nj1 = cells%gpLinkedList(j1,j2,1)
        nj2 = cells%gpLinkedList(j1,j2,2)
        j1 = nj1
        j2 = nj2
     enddo

     ! move to the next element in the outer loop
     i1 = ni1
     i2 = ni2
  enddo
  
end subroutine compareCellToSelf

! compare points in two cells, searching for closest points
subroutine compareCells(sur,cells,c1,c2)

  use types_shell
  use commonvar
  implicit none

  type(contactCells), intent(inout) :: cells
  type(mesh), intent(inout) :: sur
  integer, intent(in) :: c1,c2

  integer :: i1,i2,j1,j2, patchCounter, i, ni1,ni2,nj1,nj2

  ! if c1 and c2 are the same cell, use the previous subroutine to avoid
  ! double-counting
  if(c1 == c2) then
     call compareCellToSelf(sur,cells,c1)
     
     ! otherwise, just compare every point in c1 to every point in c2
  else
     
     ! iterate over the gauss points in c1
     i1 = cells%hashTable(c1,1)
     i2 = cells%hashTable(c1,2)
     do while(i1 > 0 .and. i2 > 0)
        
        ! iterate over the gauss points in c2
        j1 = cells%hashTable(c2,1)
        j2 = cells%hashTable(c2,2)
        do while(j1 > 0 .and. j2 > 0)
           
           ! compute and add pairwise forces
           call comparePoints(sur, i1,i2, j1,j2)
           
           ! advance the iteraction of the inner loop over pts in c2
           nj1 = cells%gpLinkedList(j1,j2,1)
           nj2 = cells%gpLinkedList(j1,j2,2)
           j1 = nj1
           j2 = nj2
        enddo
        
        ! advance the iteration of the outer loop over pts in c1
        ni1 = cells%gpLinkedList(i1,i2,1)
        ni2 = cells%gpLinkedList(i1,i2,2)
        i1 = ni1
        i2 = ni2
        
     enddo ! end loop over point in c1
     
  endif ! end if c1 == c2
  
end subroutine compareCells

! for each gp, find its closest contacting point
subroutine findNeighbors(sur,cells)
  
  use mpi
  use types_shell
  implicit none

  type(contactCells), intent(inout) :: cells
  type(mesh), intent(inout) :: sur

  integer :: M,N,O,i,j,k,c1,c2,pi,pj,pk,si,sj,sk,c1i,ei

  ! build the hash table for cells, assuming that the gptype in sur is
  ! currently up-to-date

   if (ismaster)   write(*,*) "Building contact hash table..."
     call flush(6)

  call contactCells_buildHash(cells,sur)

  ! zero out the closest points array
  sur%gp%closestPoint = 0

  M = cells%nCellsDir(1)
  N = cells%nCellsDir(2)
  O = cells%nCellsDir(3)


  if (ismaster)    write(*,*) "Checking occupied cells..."
     call flush(6)


  ! iterate over occupied cells
  do c1i=1,cells%numOccupied

     ! get the index of the occupied cell
     c1 = cells%occupiedCells(c1i)

     !get the i,j,k coordinates of the cell
     call contactCells_cell2ijk(cells,c1,i,j,k)

     ! if we aren't at the outer edge in a positive direction,
     ! do comparisons with neighbors in positive quadrant; otherwise only
     ! with self
     si = 0
     sj = 0
     sk = 0
     if(i<M) then
        si = 1
     endif
     if(j<N) then
        sj = 1
     endif
     if(k<O) then
        sk = 1
     endif

     ! iterate over positive quadrant neighbors
     do pi=0,si
        do pj=0,sj
           do pk=0,sk

              ! get 2nd index of pair
              call contactCells_ijk2cell(cells,i+pi,j+pj,k+pk,c2)

              ! compare the current cell with its neighbor (if occupied)
              if(cells%isOccupied(c2)) then
                  call compareCells(sur,cells,c1,c2)
              endif

           enddo
        enddo
     enddo ! end iteration over neighbors
	 
     ! check up to three more neighbors
     if(i>1) then
        if(j<N) then
           call contactCells_ijk2cell(cells,i-1,j+1,k,c2)
           if(cells%isOccupied(c2)) then
              call compareCells(sur,cells,c1,c2)
           endif ! c2 occupied
           if(k<O) then
              call contactCells_ijk2cell(cells,i-1,j+1,k+1,c2)
              if(cells%isOccupied(c2)) then
                 call compareCells(sur,cells,c1,c2)
              end if ! c2 occupied
              call contactCells_ijk2cell(cells,i-1,j,k+1,c2)
              if(cells%isOccupied(c2)) then
                 call compareCells(sur,cells,c1,c2)
              end if ! c2 occupied
           end if ! k
        end if ! j
     end if ! i

     ! check even more neighbors
     if(k<O .and. j>1) then
        si=i
        ei=i
        if(i<M) ei=i+1
        if(i>1) si=i-1
        do pi=si,ei
           call contactCells_ijk2cell(cells,pi,j-1,k+1,c2)
           if(cells%isOccupied(c2)) then
              call compareCells(sur,cells,c1,c2)
           end if ! c2 occupied
        end do ! pi
     end if ! k,j

  enddo ! end loop over occupied cells


  if (ismaster)    write(*,*) "Finished locating nearest Gauss points."
     call flush(6)


end subroutine findNeighbors





! compute contact
subroutine computeContactForces(sur,bez, cells, shl, ushAlpha)
  
  use mpi
  use types_shell
  use commonvar
  implicit none

  type(contactCells), intent(inout) :: cells
  type(mesh), intent(inout) :: sur,bez
  type(shell), intent(inout) :: SHL
  real(8), intent(inout) :: ushAlpha(sur%NNODE,nsd)

  integer :: el, pt

  ! first do searching for neighbors
  call findNeighbors(sur,cells)

  ! zero out the forces
  sur%gp%contactForce = 0.0
  sur%gp%contactVelForce = 0.0
  sur%gp%contactLhs = 0.0
  sur%gp%contactLhsVel = 0.0

  ! clear out contact element book-keeping info
  sur%gp%cien = -1
  sur%gp%cnshl = 0
  sur%gp%cparam = 0.0
  sur%gp%celem = 0

  if (ismaster) write(*,*) "Searching for closest points..."
  
     call flush(6)

  ! now loop through all points, adding contact forces to points with
  ! contacting neighbors, while updating contact element information
  do el = 1,sur%nel
     do pt = 1,(sur%ngauss)**2
        call computeForceOnPoint(sur,bez,el,pt,shl,ushAlpha)
     enddo
  enddo

  !write(*,*) sum(sur%gp%contactForce), sum(sur%gp%contactVelForce)

  !call mpi_barrier(mpi_comm_world)
  !stop


  if (ismaster) write(*,*) "Finished contact computations."
  call flush(6)

end subroutine computeContactForces

