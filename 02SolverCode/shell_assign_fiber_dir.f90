subroutine assign_fiber_dir(TSP, BEZ, nsd)
    use types_shell
    use commonpar
    use commonvar_shell
    use mpi
    
    implicit none

    type(mesh), intent(inout) :: TSP, BEZ
    integer, intent(in) :: nsd
        
    integer :: p, q, nshl, nshb, nuk, nvk, ptype, iel, igauss, jgauss, &
               i, j, ni, nj, aa, bb, igp    
               
    real(8) :: gp(TSP%NGAUSS), gw(TSP%NGAUSS), gwt,&
               DetJb_r, DetJb, nor(NSD), &
               xu(NSD), xd(NSD), dxdxi(NSD,2), ddxddxi(nsd,3)

    integer, allocatable :: lIEN(:)
    real(8), allocatable :: shl(:), shgradl(:,:), shhessl(:,:)
    
    real(8) :: rvec(3), zvec(3), m0(3), m(3), Rmat(3,3), normN(3), norxy(3), m2(3)
    real(8) :: G_r_con(nsd,3)
    integer :: nE, nN, ngsq, NEL_Leaflet
    
    real(8) :: gpth(NGauss_TH_Shell), gwth(NGauss_TH_Shell), zeta
    integer :: iz
    
    logical :: ifwrite
    
    ngsq = TSP%NGAUSS**2
    
    allocate(m_contrav(TSP%NEL*ngsq, NGauss_TH_Shell, 2))
  
    ifwrite = .true. .and. ismaster
    
    
    NEL_Leaflet = 0
    do iel = 1, TSP%NEL
        if (TSP%PTYPE(iel) > 3) CYCLE ! 3 leaflets only
        NEL_Leaflet = NEL_Leaflet + 1
    end do
    

    nE = NEL_Leaflet*TSP%NGAUSS*TSP%NGAUSS 
    nN = nE * 3
    
    if (ifwrite) then
          open(unit=1, file='direction.dat', status='replace')

          write(1,*) 'VARIABLES = "X" "Y" "Z"'
          write(1,*) 'ZONE N=' ,nN, ', E=' ,nE
          write(1,*) 'DATAPACKING=POINT, ZONETYPE=FETRIANGLE'
          
    end if
    
    m_contrav = 0.0d0
    
    ! get Gaussian points and weights
    gp = 0.0d0; gw = 0.0d0
    call genGPandGW_shell(gp, gw, TSP%NGAUSS) 
    
    call genGPandGW_shell(gpth, gwth, NGauss_TH_Shell) 
    
    ! loop over elements
    do iel = 1, TSP%NEL
        
        
        if (TSP%PTYPE(iel) > 3) CYCLE ! 3 leaflets only
        
        
        p = BEZ%P(iel); nshl = TSP%NSHL(iel); ptype = TSP%PTYPE(iel)
        q = BEZ%Q(iel); nshb = BEZ%NSHL(iel)
        
        allocate(shl(nshl), shgradl(nshl,2), &
                 shhessl(nshl,3), lIEN(nshl))
                 
        lIEN = -1
        do i = 1, nshl
          lIEN(i) = TSP%IEN(iel,i)
        end do      
        
        ! counter for gauss points in this element
        igp = 1
	  
        ! Loop over integration points (NGAUSS in each direction) 
        do jgauss = 1, TSP%NGAUSS
          do igauss = 1, TSP%NGAUSS
              
              
              ! Get Element Shape functions and their gradients
              shl = 0.0d0; shgradl = 0.0d0; shhessl = 0.0d0
              xu = 0.0d0; xd = 0.0d0; dxdxi = 0.0d0; ddxddxi = 0.0d0
              nor = 0.0d0
              
              
              call eval_SHAPE_bez_sh(gp(igauss), gp(jgauss), &
                                     shl, shgradl, shhessl, nor, &
                                     xu, xd, dxdxi, ddxddxi,  &
                                     p, q, nsd, nshl, nshb,  &
                                     lIEN, TSP%NNODE, &
                                     TSP%B_NET_U, TSP%B_NET_D, DetJb_r, DetJb, &
                                     BEZ%Ext(iel,1:nshl,1:nshb))
                                    
              
              nor = nor/sqrt(sum(nor*nor))
              norxy = (/nor(1), nor(2), 0.0d0/)
              normN = norxy/sqrt(sum(norxy*norxy))
              rvec = xu
              zvec = (/0.0d0, 0.0d0, -1.0d0/)
              call cross(rvec, zvec, m0)
              
              m0 = m0/sqrt(sum(m0*m0))
              
              m = m0 - sum(m0 * nor)/sum(nor*normN) * normN
              
              m = m/sqrt(sum(m*m))
              
              call Rmat3D(nor, fib_ang*PI/180.0d0, Rmat)
              
              !write(*,*) m, sum(m*nor)
              !m2 = m
              
              m = MATMUL(TRANSPOSE(Rmat), m)
              
              
              do iz = 1, NGauss_TH_Shell
                  
                  zeta = 0.5d0*Thickness_Shell*gpth(iz)
                  !zeta = 0.0d0
                  call calc_G_r_con_for_fiber(shgradl, shhessl, nshl, NSD, TSP%nnode, lIEN, &
                                            TSP%B_NET_U, zeta, G_r_con)
                  
                
                  m_contrav((iel-1)*ngsq+igp, iz, 1) = sum(m*G_r_con(:,1))
                  m_contrav((iel-1)*ngsq+igp, iz, 2) = sum(m*G_r_con(:,2))
              end do
              
             
              !call calc_G_r_con_for_fiber(shgradl, shhessl, nshl, NSD, TSP%nnode, lIEN, &
              !                          TSP%B_NET_U, G_r_con)
              
              !m_contrav((iel-1)*ngsq+igp, 1) = sum(m*G_r_con(:,1))
              !m_contrav((iel-1)*ngsq+igp, 2) = sum(m*G_r_con(:,2))
              
              
              !nor = nor/sqrt(sum(nor*nor))
              
              if (ifwrite) then
                  write(1,*) xu - m * 20.0d-3
                  write(1,*) xu
                  write(1,*) xu + m * 20.0d-3
                  
!                   write(1,*) xu
!                   write(1,*) xu + nor * 2.0d-3
!                   write(1,*) xu + nor * 16.0d-3
!
                  !write(1,*) xu
                  !write(1,*) xu + m2 * 2.0d-3
                  !write(1,*) xu + m2 * 20.0d-3
              end if
              
              ! increment 1d gauss point counter
              igp = igp+1

            end do
          end do  ! end loop gauss points
        
        deallocate(shl, shgradl, shhessl, lIEN)
        
    end do ! end loop over elements
    
    
    if (ifwrite) then
        do i = 1, nE
            write(1,*) ((i-1)*3+j, j = 1, 3)
        end do
    end if
    
    
    if (ifwrite) then
        close(1)
    end if
    
end subroutine assign_fiber_dir


subroutine calc_G_r_con_for_fiber(shgradl, shhessl, nshl, NSD, nnode, lIEN, &
                          B_NET_SH, zeta, G_r_con)
    
    implicit none  

    integer, intent(in) :: nshl, NSD, lIEN(nshl), nnode
    real(8), intent(in) :: shgradl(NSHL,2), shhessl(NSHL,3), B_NET_SH(nnode,nsd+1), zeta
    real(8), intent(out) :: G_r_con(NSD,2)
    
    integer :: i
    real(8) :: dR(NSHL,2), ddR(NSHL,3)
    real(8) :: G_r(3,2), Gab_r(3), Bv_r(3), Nor_r(3), dNor_r(3,2)
    real(8) :: tmp33(3,3), tmp3(3), tmp

    
    do i = 1, NSHL
      dR(i,:)  = shgradl(i,:)
      ddR(i,1) = shhessl(i,1)
      ddR(i,2) = shhessl(i,3)
      ddR(i,3) = shhessl(i,2)
    end do
  
    ! reference configuration
    call shell_geo(nshl, nsd, nnode, dR, ddR, lIEN, B_NET_SH, &
                   G_r, tmp33, tmp3, tmp, Nor_r, Bv_r, Gab_r,  &
                   dNor_r)
    
    
    G_r(1:3,1:2) = G_r(1:3,1:2) + zeta*dNor_r(1:3,1:2)
    
    call cov2contra(G_r_con, G_r)
    
    
end subroutine calc_G_r_con_for_fiber

! ! covariant -> contravariant
! subroutine cov2contra(g_contra, g_cov)
!     implicit none
!     real(8), intent(in) :: g_cov(3,2)
!     real(8), intent(out) :: g_contra(3,2)
!
!     real(8) :: GG(3), invdetGab, GG_con(3)
!
!     GG(1) = sum(g_cov(:,1)*g_cov(:,1))
!     GG(2) = sum(g_cov(:,2)*g_cov(:,2))
!     GG(3) = sum(g_cov(:,1)*g_cov(:,2))
!
!     ! contravariant metric Gab_con and base vectors G_con
!     invdetGab = 1.0d0/(GG(1)*GG(2)-GG(3)*GG(3))
!     GG_con(1) =  invdetgab*GG(2)
!     GG_con(3) = -invdetgab*GG(3)
!     GG_con(2) =  invdetgab*GG(1)
!
!     ! reference contravariant base vector
!     g_contra(:,1) = g_cov(:,1) * GG_con(1) + g_cov(:,2) * GG_con(3)
!     g_contra(:,2) = g_cov(:,1) * GG_con(3) + g_cov(:,2) * GG_con(2)
!
!
! end subroutine cov2contra


