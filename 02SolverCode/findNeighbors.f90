! for each gp, find its closest contacting point
subroutine findNeighbors(sur,cells)
  
  use mpi
  use types_shell
  implicit none

  type(contactCells), intent(inout) :: cells
  type(mesh), intent(inout) :: sur

  integer :: M,N,O,i,j,k,c1,c2,pi,pj,pk,si,sj,sk,c1i,ei

  ! build the hash table for cells, assuming that the gptype in sur is
  ! currently up-to-date

   if (ismaster)   write(*,*) "Building contact hash table..."
     call flush(6)

  call contactCells_buildHash(cells,sur)

  ! zero out the closest points array
  sur%gp%closestPoint = 0

  M = cells%nCellsDir(1)
  N = cells%nCellsDir(2)
  O = cells%nCellsDir(3)


  if (ismaster)    write(*,*) "Checking occupied cells..."
     call flush(6)


  ! iterate over occupied cells
  do c1i=1,cells%numOccupied

     ! get the index of the occupied cell
     c1 = cells%occupiedCells(c1i)

     !get the i,j,k coordinates of the cell
     call contactCells_cell2ijk(cells,c1,i,j,k)

     ! if we aren't at the outer edge in a positive direction,
     ! do comparisons with neighbors in positive quadrant; otherwise only
     ! with self
     si = 0
     sj = 0
     sk = 0
     if(i<M) then
        si = 1
     endif
     if(j<N) then
        sj = 1
     endif
     if(k<O) then
        sk = 1
     endif

     ! iterate over positive quadrant neighbors
     do pi=0,si
        do pj=0,sj
           do pk=0,sk

              ! get 2nd index of pair
              call contactCells_ijk2cell(cells,i+pi,j+pj,k+pk,c2)

              ! compare the current cell with its neighbor (if occupied)
              if(cells%isOccupied(c2)) then
                  call compareCells(sur,cells,c1,c2)
              endif

           enddo
        enddo
     enddo ! end iteration over neighbors
	 
     ! check up to three more neighbors
     if(i>1) then
        if(j<N) then
           call contactCells_ijk2cell(cells,i-1,j+1,k,c2)
           if(cells%isOccupied(c2)) then
              call compareCells(sur,cells,c1,c2)
           endif ! c2 occupied
           if(k<O) then
              call contactCells_ijk2cell(cells,i-1,j+1,k+1,c2)
              if(cells%isOccupied(c2)) then
                 call compareCells(sur,cells,c1,c2)
              end if ! c2 occupied
              call contactCells_ijk2cell(cells,i-1,j,k+1,c2)
              if(cells%isOccupied(c2)) then
                 call compareCells(sur,cells,c1,c2)
              end if ! c2 occupied
           end if ! k
        end if ! j
     end if ! i

     ! check even more neighbors
     if(k<O .and. j>1) then
        si=i
        ei=i
        if(i<M) ei=i+1
        if(i>1) si=i-1
        do pi=si,ei
           call contactCells_ijk2cell(cells,pi,j-1,k+1,c2)
           if(cells%isOccupied(c2)) then
              call compareCells(sur,cells,c1,c2)
           end if ! c2 occupied
        end do ! pi
     end if ! k,j

  enddo ! end loop over occupied cells


  if (ismaster)    write(*,*) "Finished locating nearest Gauss points."
     call flush(6)


end subroutine findNeighbors
