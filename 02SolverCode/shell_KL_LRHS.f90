subroutine INFO_Rhs_KLShell(shgradl, shhessl, nshl, NSD, nnode, lIEN, &
                          MM_Flag, B_NET_SH, B_NET_SH_D, mu, kappa, &
                          thi, ngauss, MtCoefs, m, n_info, infos)

    implicit none  
    integer, intent(in) :: nshl, NSD, lIEN(nshl), nnode, MM_Flag, ngauss, n_info 
    real(8), intent(in) :: shgradl(NSHL,2), shhessl(NSHL,3), &
                           mu, kappa, thi, &
                           B_NET_SH(nnode,nsd+1), &
                           B_NET_SH_D(nnode,nsd+1)
						 
    real(8), intent(in) :: MtCoefs(100), m(2)
  
    real(8), intent(out):: infos(n_info)
    
    integer :: ur, kr, dirr, us, ks, dirs, dirt, ddir, i, j
    real(8) :: dR(NSHL,2), ddR(NSHL,3)

    real(8) :: G_r(3,2), Gab_r(3), Bv_r(3), Nor_r(3), dNor_r(3,2)

    real(8) :: g(3,2), g3(3), lg3, nor(3), dnor(3,2), gab(3), h(3,3), &
               bv(3), lg3_3, lg3_5

    real(8) :: dg(3,2,3*NSHL), dg3(3,3*NSHL), g3dg3(3*NSHL), &
               g3dg3lg3_3(3*NSHL), dn(3,3*NSHL), &
               ddg3(3), tmp1, tmp2, ddn(3), &
               E_cu(3), K_cu(3), &
               dE_cu(3,3*NSHL), dK_cu(3,3*NSHL), &
               ddE_cu(3,3*NSHL,3*NSHL), ddK_cu(3,3*NSHL,3*NSHL), &
               N_cu(3), M_cu(3), dN_cu(3,3*NSHL), dM_cu(3,3*NSHL)

    real(8) :: kem(3*NSHL,3*NSHL), keb(3*NSHL,3*NSHL), ke(3*NSHL,3*NSHL)

    real(8) :: fiem(3*NSHL), fieb(3*NSHL), fie(3*NSHL)

    real(8) :: tmp33(3,3), tmp32(3,2), tmp3(3), tmp22(2,2), tmp
    
    real(8) :: MIPE(3)
    
    real(8) :: zeta
    real(8) :: GGv_r(3), ggv_c(3), GG_r(2,2), gg_c(2,2), &
               detGG, sdetgg, C33, Gi_r(3,3), gi_c(3,3)
    
    real(8) :: I1, I4, NH
    real(8) :: I1m3, I4m1, I1m3sq, I4m1sq, dt, idt, e1, e4

    infos = 0.0d0
    
    ! initialization
    dg = 0.0d0; dg3 = 0.0d0; g3dg3 = 0.0d0
    g3dg3lg3_3 = 0.0d0; ddg3 = 0.0d0
    tmp1 = 0.0d0; tmp2 = 0.0d0
    dn = 0.0d0; ddn = 0.0d0
    E_cu = 0.0d0; K_cu = 0.0d0
    dE_cu = 0.0d0; dK_cu = 0.0d0
    ddE_cu = 0.0d0; ddK_cu = 0.0d0
    N_cu = 0.0d0; M_cu = 0.0d0; dN_cu = 0.0d0; dM_cu = 0.0d0
    kem = 0.0d0; keb = 0.0d0; fiem = 0.0d0; fieb = 0.0d0

    tmp33 = 0.0d0; tmp32 = 0.0d0; tmp3  = 0.0d0; tmp22 = 0.0d0
    
    ! Get basis functions (Josef ordered the derivatives differently)
    do i = 1, NSHL
      dR(i,:)  = shgradl(i,:)
      ddR(i,1) = shhessl(i,1)
      ddR(i,2) = shhessl(i,3)
      ddR(i,3) = shhessl(i,2)
    end do
  
    ! reference configuration
    call shell_geo(nshl, nsd, nnode, dR, ddR, lIEN, B_NET_SH, &
                   G_r, tmp33, tmp3, tmp, Nor_r, Bv_r, Gab_r,  &
                   dNor_r)

    ! actual configuration
    call shell_geo(nshl, nsd, nnode, dR, ddR, lIEN, B_NET_SH_D, &
                   g, h, g3, lg3, nor, bv, gab, dNor)
    
    
    lg3_3 = lg3**3
    lg3_5 = lg3**5
    
    ! strain vector [E11,E22,E12] referred to curvilinear coor sys
    E_cu = 0.5d0*(gab - Gab_r);

    ! curvature vector [K11,K22,K12] referred to curvilinear coor sys
    K_cu = -(bv - Bv_r)
    
    ! Voigt notation
    E_cu(3)       = E_cu(3)*2.0d0
    K_cu(3)       = K_cu(3)*2.0d0
    dE_cu(3,:)    = dE_cu(3,:)*2.0d0
    dK_cu(3,:)    = dK_cu(3,:)*2.0d0
    ddE_cu(3,:,:) = ddE_cu(3,:,:)*2.0d0
    ddK_cu(3,:,:) = ddK_cu(3,:,:)*2.0d0
    
    
    ! shell bottom (top of BHV)
    zeta = -0.5d0*thi
    
    GGv_r = Gab_r - 2.0d0*zeta*Bv_r
    ggv_c = gab   - 2.0d0*zeta*bv
    
    detGG  = GGv_r(1)*GGv_r(2) - GGv_r(3)**2
    sdetgg = ggv_c(1)*ggv_c(2) - ggv_c(3)**2

    GG_r(1,:) = (/ GGv_r(1), GGv_r(3) /)
    GG_r(2,:) = (/ GGv_r(3), GGv_r(2) /)

    gg_c(1,:) = (/ ggv_c(1), ggv_c(3) /)
    gg_c(2,:) = (/ ggv_c(3), ggv_c(2) /)

    C33 = detGG/sdetgg
    
    Gi_r(1,:) = 1.0d0/detGG*(/  GG_r(2,2), -GG_r(1,2), 0.0d0 /)
    Gi_r(2,:) = 1.0d0/detGG*(/ -GG_r(2,1),  GG_r(1,1), 0.0d0 /)
    Gi_r(3,:) =             (/      0.0d0,      0.0d0, 1.0d0 /)
    
    gi_c(1,:) = 1.0d0/sdetgg*(/  gg_c(2,2), -gg_c(1,2), 0.0d0     /)
    gi_c(2,:) = 1.0d0/sdetgg*(/ -gg_c(2,1),  gg_c(1,1), 0.0d0     /)
    gi_c(3,:) =              (/      0.0d0,      0.0d0, 1.0d0/C33 /)
    
    ! Lee-Sacks
    
    I1 = Gi_r(1,1)*gg_c(1,1)+Gi_r(2,2)*gg_c(2,2)+2.0d0*Gi_r(1,2)*gg_c(1,2)+C33
    I4 = m(1)*m(1)*gg_c(1,1) + m(2)*m(2)*gg_c(2,2) + 2.0d0*m(1)*m(2)*gg_c(1,2)
  
    I1m3 = I1 - 3.0d0
    I4m1 = I4 - 1.0d0
  
    I1m3sq = I1m3*I1m3
    I4m1sq = I4m1*I4m1
  
    e1 = exp(MtCoefs(3)*I1m3sq)
    e4 = exp(MtCoefs(4)*I4m1sq)
    
    NH = 0.5d0*MtCoefs(1)*I1m3
    
    ! dt must be 0 < dt < 1
    dt = MtCoefs(5)
    idt = 1.0d0 - dt
    
    infos(1) = I1m3sq
    infos(2) = I4m1sq
    infos(3) = dt*e1
    infos(4) = idt*e4*0.0d0
    infos(5) = NH * 1.0d-4 ! kPa = 10^3Pa = 10^4dyn/cm^2
    infos(6) = 0.5d0*MtCoefs(2)*(dt*e1+idt*e4-1.0d0) * 1.0d-4 ! kPa = 10^3Pa = 10^4dyn/cm^2
    
!     if (idt*e4 > dt*e1) then
!        write(*,*) I1m3, I4m1, dt*e1, idt*e4
!     end if
    
    
!     MIPE(1) = 0.5d0*(E_zeta(1)+E_zeta(2)) &
!               + sqrt((0.5d0*(E_zeta(1)-E_zeta(2)))**2+E_zeta(3)**2)
!
!     MIPE(2) = 0.5d0*(E_zeta(1)+E_zeta(2)) &
!              - sqrt((0.5d0*(E_zeta(1)-E_zeta(2)))**2+E_zeta(3)**2)
!
!     MIPE(3) = 0.0d0
    
    
end subroutine INFO_Rhs_KLShell

!======================================================================
! Evaluates stiffness matrix and residual vector of an element 
! at an integration point
!======================================================================
subroutine e3LRhs_KLShell(shgradl, shhessl, nshl, NSD, nnode, lIEN, &
                          MM_Flag, B_NET_SH, B_NET_SH_D, mu, kappa, &
                          thi, ngauss, xKebe, Rhs, dA_r, &
						  Coef_a, Coef_b, Coef_c, MtCoefs, m_contrav)

  implicit none  

  integer, intent(in) :: nshl, NSD, lIEN(nshl), nnode, MM_Flag, ngauss  
  real(8), intent(in) :: shgradl(NSHL,2), shhessl(NSHL,3), &
                         mu, kappa, thi, &
                         B_NET_SH(nnode,nsd+1), &
                         B_NET_SH_D(nnode,nsd+1)
						 
  real(8), intent(in) :: Coef_a, Coef_b, Coef_c, MtCoefs(100), m_contrav(ngauss,2)
  
  real(8), intent(out):: xKebe(NSD*NSD,NSHL,NSHL), Rhs(NSD,NSHL), dA_r

  integer :: ur, kr, dirr, us, ks, dirs, dirt, ddir, i, j
  real(8) :: dR(NSHL,2), ddR(NSHL,3)

  real(8) :: G_r(3,2), Gab_r(3), Bv_r(3), Nor_r(3), dNor_r(3,2)

  real(8) :: g(3,2), g3(3), lg3, nor(3), dnor(3,2), gab(3), h(3,3), &
             bv(3), lg3_3, lg3_5

  real(8) :: dg(3,2,3*NSHL), dg3(3,3*NSHL), g3dg3(3*NSHL), &
             g3dg3lg3_3(3*NSHL), dn(3,3*NSHL), &
             ddg3(3), tmp1, tmp2, ddn(3), &
             E_cu(3), K_cu(3), &
             dE_cu(3,3*NSHL), dK_cu(3,3*NSHL), &
             ddE_cu(3,3*NSHL,3*NSHL), ddK_cu(3,3*NSHL,3*NSHL), &
             N_cu(3), M_cu(3), dN_cu(3,3*NSHL), dM_cu(3,3*NSHL)

  real(8) :: kem(3*NSHL,3*NSHL), keb(3*NSHL,3*NSHL), ke(3*NSHL,3*NSHL)

  real(8) :: fiem(3*NSHL), fieb(3*NSHL), fie(3*NSHL)

  real(8) :: tmp33(3,3), tmp32(3,2), tmp3(3), tmp22(2,2)

  real(8) :: Fung_c, Fung_A(6)
  
  integer :: ifwrite
  
  real(8) :: SdEr(3*NSHL), dSdEr(3*NSHL, 3*NSHL), SddEr(3*NSHL,3*NSHL)
  
  ifwrite = 0 ! if you want to output the points
  
  ! initialization
  dg = 0.0d0; dg3 = 0.0d0; g3dg3 = 0.0d0
  g3dg3lg3_3 = 0.0d0; ddg3 = 0.0d0
  tmp1 = 0.0d0; tmp2 = 0.0d0
  dn = 0.0d0; ddn = 0.0d0
  E_cu = 0.0d0; K_cu = 0.0d0
  dE_cu = 0.0d0; dK_cu = 0.0d0
  ddE_cu = 0.0d0; ddK_cu = 0.0d0
  N_cu = 0.0d0; M_cu = 0.0d0; dN_cu = 0.0d0; dM_cu = 0.0d0
  kem = 0.0d0; keb = 0.0d0; fiem = 0.0d0; fieb = 0.0d0

  tmp33 = 0.0d0; tmp32 = 0.0d0; tmp3  = 0.0d0; tmp22 = 0.0d0

  ! Get basis functions (Josef ordered the derivatives differently)
  do i = 1, NSHL
    dR(i,:)  = shgradl(i,:)
    ddR(i,1) = shhessl(i,1)
    ddR(i,2) = shhessl(i,3)
    ddR(i,3) = shhessl(i,2)
  end do
  
  ! reference configuration
  call shell_geo(nshl, nsd, nnode, dR, ddR, lIEN, B_NET_SH, &
                 G_r, tmp33, tmp3, dA_r, Nor_r, Bv_r, Gab_r,  &
                 dNor_r)

  ! actual configuration
  call shell_geo(nshl, nsd, nnode, dR, ddR, lIEN, B_NET_SH_D, &
                 g, h, g3, lg3, nor, bv, gab, dNor)

  
!   if (ifwrite == 1) then
!       write(1,*) xd(1:3)
!       write(1,*) xd(1:3) + G_r(1:3,2)*0.2
!       write(1,*) xd(1:3) + G_r(1:3,2)*0.4
!   end if
   
  lg3_3 = lg3**3
  lg3_5 = lg3**5

  ! strain vector [E11,E22,E12] referred to curvilinear coor sys
  E_cu = 0.5d0*(gab - Gab_r);

  ! curvature vector [K11,K22,K12] referred to curvilinear coor sys
  K_cu = -(bv - Bv_r)

  ! first variation of strain and curvature w.r.t. dof
  do ur = 1, 3*NSHL
    ! local node number kr and dof direction dirr
    kr = (ur+2)/3
    dirr = ur-3*(kr-1)

    dg(dirr,1,ur) = dR(kr,1)
    dg(dirr,2,ur) = dR(kr,2)
  
    ! strain
    dE_cu(1,ur) = dR(kr,1)*g(dirr,1)
    dE_cu(2,ur) = dR(kr,2)*g(dirr,2)
    dE_cu(3,ur) = 0.5d0*(dR(kr,1)*g(dirr,2) + g(dirr,1)*dR(kr,2))

    ! curvature
    dg3(1,ur) = dg(2,1,ur)*g(3,2)-dg(3,1,ur)*g(2,2) &
              + g(2,1)*dg(3,2,ur)-g(3,1)*dg(2,2,ur)
    dg3(2,ur) = dg(3,1,ur)*g(1,2)-dg(1,1,ur)*g(3,2) &
              + g(3,1)*dg(1,2,ur)-g(1,1)*dg(3,2,ur)
    dg3(3,ur) = dg(1,1,ur)*g(2,2)-dg(2,1,ur)*g(1,2) &
              + g(1,1)*dg(2,2,ur)-g(2,1)*dg(1,2,ur)
    
    g3dg3(ur) = g3(1)*dg3(1,ur)+g3(2)*dg3(2,ur)+g3(3)*dg3(3,ur)
    g3dg3lg3_3(ur) = g3dg3(ur)/lg3_3
  
    dn(1,ur) = dg3(1,ur)/lg3 - g3(1)*g3dg3lg3_3(ur)
    dn(2,ur) = dg3(2,ur)/lg3 - g3(2)*g3dg3lg3_3(ur)
    dn(3,ur) = dg3(3,ur)/lg3 - g3(3)*g3dg3lg3_3(ur)
    
    dK_cu(1,ur) = -(ddR(kr,1)*nor(dirr) + sum(h(:,1)*dn(:,ur)))
    dK_cu(2,ur) = -(ddR(kr,2)*nor(dirr) + sum(h(:,2)*dn(:,ur)))
    dK_cu(3,ur) = -(ddR(kr,3)*nor(dirr) + sum(h(:,3)*dn(:,ur)))
  end do

  ! second variation of strain and curvature w.r.t. dofs
  do ur = 1, 3*NSHL
    kr = (ur+2)/3
    dirr = ur-3*(kr-1)
    do us = 1, ur
      ks = (us+2)/3
      dirs = us-3*(ks-1)

      ! strain
      if (dirr == dirs) then
        ddE_cu(1,ur,us) = dR(kr,1)*dR(ks,1)
        ddE_cu(2,ur,us) = dR(kr,2)*dR(ks,2)
        ddE_cu(3,ur,us) = 0.5d0*(dR(kr,1)*dR(ks,2)+dR(kr,2)*dR(ks,1))
      end if

      ! curvature
      ddg3 = 0.0d0

      dirt = 6-dirr-dirs
      ddir = dirr-dirs      

      if (ddir == -1) then
        ddg3(dirt) =  dR(kr,1)*dR(ks,2)-dR(ks,1)*dR(kr,2)
      else if (ddir ==  2) then
        ddg3(dirt) =  dR(kr,1)*dR(ks,2)-dR(ks,1)*dR(kr,2)
      else if (ddir ==  1) then
        ddg3(dirt) = -dR(kr,1)*dR(ks,2)+dR(ks,1)*dR(kr,2)
      else if (ddir == -2) then
        ddg3(dirt) = -dR(kr,1)*dR(ks,2)+dR(ks,1)*dR(kr,2)
      end if
    
      tmp1 = -(sum(ddg3(:)*g3(:)) + sum(dg3(:,ur)*dg3(:,us)))/lg3_3
      tmp2 = 3.0d0*g3dg3(ur)*g3dg3(us)/lg3_5
    
      ddn(1) = ddg3(1)/lg3 - g3dg3lg3_3(us)*dg3(1,ur) &
               - g3dg3lg3_3(ur)*dg3(1,us) + tmp1*g3(1) + tmp2*g3(1)
      ddn(2) = ddg3(2)/lg3 - g3dg3lg3_3(us)*dg3(2,ur) &
               - g3dg3lg3_3(ur)*dg3(2,us) + tmp1*g3(2) + tmp2*g3(2)
      ddn(3) = ddg3(3)/lg3 - g3dg3lg3_3(us)*dg3(3,ur) &
               - g3dg3lg3_3(ur)*dg3(3,us) + tmp1*g3(3) + tmp2*g3(3)
    
      ddK_cu(1,ur,us) = -(ddR(kr,1)*dn(dirr,us) &
                        + ddR(ks,1)*dn(dirs,ur) &
                        + sum(h(:,1)*ddn(:)))
      ddK_cu(2,ur,us) = -(ddR(kr,2)*dn(dirr,us) &
                        + ddR(ks,2)*dn(dirs,ur) &
                        + sum(h(:,2)*ddn(:)))
      ddK_cu(3,ur,us) = -(ddR(kr,3)*dn(dirr,us) &
                        + ddR(ks,3)*dn(dirs,ur) &
                        + sum(h(:,3)*ddn(:)))
    end do
  end do
  
  ! Voigt notation
  E_cu(3)       = E_cu(3)*2.0d0
  K_cu(3)       = K_cu(3)*2.0d0
  dE_cu(3,:)    = dE_cu(3,:)*2.0d0
  dK_cu(3,:)    = dK_cu(3,:)*2.0d0
  ddE_cu(3,:,:) = ddE_cu(3,:,:)*2.0d0
  ddK_cu(3,:,:) = ddK_cu(3,:,:)*2.0d0

  ! choose linear or nonlinear material
  if (MM_Flag == 0) then
    call shell_mat_stVK(nshl, G_r, Nor_r, kappa, mu, thi, ngauss, E_cu, dE_cu, &
                            K_cu, dK_cu, N_cu, dN_cu, M_cu, dM_cu) 
							
  else if (MM_Flag == 1) then
    call shell_mat_exp(nshl, Gab_r, gab, Bv_r, bv, &
                       G_r, Nor_r, dNor_r, g, nor, dnor, &
							    mu, thi, ngauss, dE_cu, dK_cu, &
								N_cu, dN_cu, M_cu, dM_cu, 1, Coef_a, Coef_b, Coef_c, MtCoefs, m_contrav)	
  else if (MM_Flag == 2) then  
      
    call shell_mat_exp(nshl, Gab_r, gab, Bv_r, bv, &
                       G_r, Nor_r, dNor_r, g, nor, dnor, &
							    mu, thi, ngauss, dE_cu, dK_cu, &
								N_cu, dN_cu, M_cu, dM_cu, 2, Coef_a, Coef_b, Coef_c, MtCoefs, m_contrav)	
                                
  else if (MM_Flag == 3) then
      call shell_mat_exp(nshl, Gab_r, gab, Bv_r, bv, &
                         G_r, Nor_r, dNor_r, g, nor, dnor, &
  							    mu, thi, ngauss, dE_cu, dK_cu, &
  								N_cu, dN_cu, M_cu, dM_cu, 3, Coef_a, Coef_b, Coef_c, MtCoefs, m_contrav)	
  ! Lee and Sacks
  else if (MM_Flag == 4) then
   call shell_mat_exp(nshl, Gab_r, gab, Bv_r, bv, &
                      G_r, Nor_r, dNor_r, g, nor, dnor, &
                                mu, thi, ngauss, dE_cu, dK_cu, &
                                N_cu, dN_cu, M_cu, dM_cu, 4, Coef_a, Coef_b, Coef_c, MtCoefs, m_contrav)
      
    
!     call shell_mat_exp_SE(nshl, Gab_r, gab, Bv_r, bv, &
!                        G_r, Nor_r, dNor_r, g, nor, dnor, &
!                                 mu, thi, ngauss, dE_cu, dK_cu, ddE_cu, ddK_cu, &
!                                 SdEr, dSdEr, SddEr, 4, Coef_a, Coef_b, Coef_c, MtCoefs, m_contrav)
    
    
    
    ! call shell_mat_exp_T(nshl, G_r, dNor_r, Gab_r, gab, Bv_r, bv, &
!                                 mu, thi, ngauss, dE_cu, dK_cu, &
!                                 N_cu, dN_cu, M_cu, dM_cu, 2, Coef_a, Coef_b, Coef_c)	
	

!   else if (MM_Flag == 1) then
!     call shell_mat_neoHook(nshl, G_r, Nor_r, dNor_r, g, nor, dnor, &
!                             kappa, mu, thi, ngauss, dE_cu, dK_cu, &
!                             N_cu, dN_cu, M_cu, dM_cu)
!
!   else if (MM_Flag == 2) then
!   	call shell_mat_in_neoHook(nshl, G_r, Nor_r, dNor_r, g, nor, dnor, &
!                               mu, thi, ngauss, dE_cu, dK_cu, &
!                               N_cu, dN_cu, M_cu, dM_cu)
!
!   else if (MM_Flag == 3) then
!     call shell_mat_exp(nshl, G_r, Nor_r, dNor_r, g, nor, dnor, &
! 							    mu, thi, ngauss, dE_cu, dK_cu, &
! 								N_cu, dN_cu, M_cu, dM_cu)
!     else if (MM_Flag == 3) then
!        call shell_mat_exp_new(nshl, Gab_r, gab, Bv_r, bv, &
! 						      mu, thi, ngauss, dE_cu, dK_cu, &
! 				              N_cu, dN_cu, M_cu, dM_cu, Coef_a, Coef_b, Coef_c)
	
													  
!   else if (MM_Flag == 4) then
!     call shell_mat_exp_2(nshl, G_r, Nor_r, dNor_r, g, nor, dnor, &
! 							  	  mu, thi, ngauss, dE_cu, dK_cu, &
! 							  	  N_cu, dN_cu, M_cu, dM_cu)
    else
       write(*,*) "ERROR: Wrong MM_Flag in shell_KL_LRHS.f90"
       stop
  end if

  ! loop over dofs ur and us
  do ur = 1,3*NSHL
    do us = 1, ur
      ! membrane stiffness
      kem(ur,us) = sum(dE_cu(:,ur)*dN_cu(:,us)) &
                 + sum(N_cu(:)*ddE_cu(:,ur,us))

      ! bending stiffness
      keb(ur,us) = sum(dK_cu(:,ur)*dM_cu(:,us)) &
                 + sum(M_cu(:)*ddK_cu(:,ur,us))
    end do
    ! residual
    fiem(ur) = -(sum(N_cu(:)*dE_cu(:,ur)))
    fieb(ur) = -(sum(M_cu(:)*dK_cu(:,ur)))
  end do
  
  ke  = ( kem +  keb)*dA_r
  fie = (fiem + fieb)*dA_r
  
!   if (MM_Flag == 4) then
!       ke  = (dSdEr + SddEr) * dA_r
!       fie = -SdEr * dA_r
!   end if
 
  
  
  ! transform to our's format
  do ur = 1, 3*NSHL
    do us = 1, 3*NSHL
      if (us > ur) then
        ke(ur,us) = ke(us,ur)
      end if
    end do
  end do

  do ur = 1,3*NSHL
    kr = (ur+2)/3
    dirr = ur-3*(kr-1)
    do us = 1,3*NSHL
      ks = (us+2)/3
      dirs = us-3*(ks-1)
      i = (dirr-1)*3 + dirs

      xKebe(i,kr,ks) = ke(ur,us)

    end do
    Rhs(dirr,kr) = fie(ur)
  end do
      
end subroutine e3LRhs_KLShell




!======================================================================
!  
!
!
!======================================================================
subroutine e3LRhs_KLShell_old(shgradl, shhessl, Dm, Dc, Db, &
                          xKebe, Rhs, nshl, q, NSD, nor,  &
                          B_NET_SH, B_NET_SH_D, lIEN, nnode)

  implicit none  

  integer, intent(in) :: nshl, q, NSD, lIEN(nshl), nnode    
  real(8), intent(in) :: shgradl(NSHL,2), shhessl(NSHL,3), nor(3), &
                         Dm(3,3), Dc(3,3), Db(3,3), &
                         B_NET_SH(nnode,nsd+1), &
                         B_NET_SH_D(nnode,nsd+1)
  real(8), intent(out):: xKebe(NSD*NSD,NSHL,NSHL), Rhs(NSD,NSHL)

  integer :: ur, kr, dirr, us, ks, dirs, dirt, ddir, i, j
  real(8) :: dR(NSHL,2), ddR(NSHL,3)
  real(8) :: Gab_r(2,2), Bv_r(3), Tb(3,3), dA
  real(8) :: g(3,2), g3(3), lg3, n(3), gab(2,2), h(3,3), bv(3)
  real(8) :: lg3_3, lg3_5
  real(8) :: E_cu(3), E_ca(3), K_cu(3), K_ca(3)
  real(8) :: dg(3,2,3*NSHL), dE_cu(3), dE_ca(3,3*NSHL), &
             dg3(3,3*NSHL), g3dg3(3*NSHL), g3dg3lg3_3(3*NSHL), &
             dn(3,3*NSHL), dK_cu(3), dK_ca(3,3*NSHL)
  real(8) :: ddE_cu(3), ddE_ca(3,3*NSHL,3*NSHL), &
             ddg3(3), tmp1, tmp2, ddn(3), ddK_cu(3), &
             ddK_ca(3,3*NSHL,3*NSHL), N_ca(3), M_ca(3), &
             dN_ca(3), dM_ca(3)
  real(8) :: kem(3*NSHL,3*NSHL), keb(3*NSHL,3*NSHL), &
             ke(3*NSHL,3*NSHL)
  real(8) :: fiem(3*NSHL), fieb(3*NSHL), fie(3*NSHL)

  real(8) :: tmp33(3,3), tmp32(3,2), tmp3(3), tmp22(2,2)
  
  kem = 0d0
  keb = 0d0
  ke =  0d0

  fiem = 0d0
  fieb = 0d0
  fie  = 0d0

  do i = 1, NSHL
    dR(i,:)  = shgradl(i,:)
    ddR(i,1) = shhessl(i,1)
    ddR(i,2) = shhessl(i,3)
    ddR(i,3) = shhessl(i,2)
  end do
  
  ! reference configuration
  call shell_geo_old(nshl, nsd, nnode, q, dR, ddR, lIEN, B_NET_SH, nor, &
                 tmp32, tmp3, dA, tmp3, Gab_r, tmp22, tmp33, &
                 Bv_r, Tb, tmp33, tmp33)

  ! actual configuration
  call shell_geo_old(nshl, nsd, nnode, q, dR, ddR, lIEN, B_NET_SH_D, nor, &
                 g, g3, lg3, n, gab, tmp22, h, bv, tmp33, tmp33, &
                 tmp33)

  lg3_3 = lg3**3
  lg3_5 = lg3**5

  ! strain vector [E11,E22,E12] referred to curvilinear coor sys
  E_cu(1) = 0.5d0*(gab(1,1)-Gab_r(1,1))
  E_cu(2) = 0.5d0*(gab(2,2)-Gab_r(2,2))
  E_cu(3) = 0.5d0*(gab(1,2)-Gab_r(1,2))

  ! strain vector [E11,E22,2*E12] referred to cartesian coor sys
  ! E_ca = Tb*E_cu
  E_ca = MATMUL(Tb,E_cu)

  ! curvature vector [K11,K22,K12] referred to curvilinear coor sys
  K_cu = -(bv - Bv_r)

  ! curvature vector [K11,K22,2*K12] referred to cartesian coor sys
  ! K_ca = Tb*K_cu
  K_ca = MATMUL(Tb,K_cu)

  dg = 0.0d0
  ! first variation of strain and curvature w.r.t. dof
  do ur = 1, 3*NSHL
    ! local node number kr and dof direction dirr
    kr = (ur+2)/3
    dirr = ur-3*(kr-1)

    dg(dirr,1,ur) = dR(kr,1)
    dg(dirr,2,ur) = dR(kr,2)
  
    ! strain
    dE_cu(1) = dR(kr,1)*g(dirr,1)
    dE_cu(2) = dR(kr,2)*g(dirr,2)
    dE_cu(3) = 0.5d0*(dR(kr,1)*g(dirr,2) + g(dirr,1)*dR(kr,2))

    dE_ca(1,ur) = sum(Tb(1,:)*dE_cu(:))
    dE_ca(2,ur) = sum(Tb(2,:)*dE_cu(:))
    dE_ca(3,ur) = sum(Tb(3,:)*dE_cu(:))
  
    ! curvature
    dg3(1,ur) = dg(2,1,ur)*g(3,2)-dg(3,1,ur)*g(2,2) &
              + g(2,1)*dg(3,2,ur)-g(3,1)*dg(2,2,ur)
    dg3(2,ur) = dg(3,1,ur)*g(1,2)-dg(1,1,ur)*g(3,2) &
              + g(3,1)*dg(1,2,ur)-g(1,1)*dg(3,2,ur)
    dg3(3,ur) = dg(1,1,ur)*g(2,2)-dg(2,1,ur)*g(1,2) &
              + g(1,1)*dg(2,2,ur)-g(2,1)*dg(1,2,ur)
    
    g3dg3(ur) = g3(1)*dg3(1,ur)+g3(2)*dg3(2,ur)+g3(3)*dg3(3,ur)
    g3dg3lg3_3(ur) = g3dg3(ur)/lg3_3
  
    dn(1,ur) = dg3(1,ur)/lg3 - g3(1)*g3dg3lg3_3(ur)
    dn(2,ur) = dg3(2,ur)/lg3 - g3(2)*g3dg3lg3_3(ur)
    dn(3,ur) = dg3(3,ur)/lg3 - g3(3)*g3dg3lg3_3(ur)
    
    dK_cu(1) = -(ddR(kr,1)*n(dirr) + sum(h(:,1)*dn(:,ur)))
    dK_cu(2) = -(ddR(kr,2)*n(dirr) + sum(h(:,2)*dn(:,ur)))
    dK_cu(3) = -(ddR(kr,3)*n(dirr) + sum(h(:,3)*dn(:,ur)))
  
    dK_ca(1,ur) = sum(Tb(1,:)*dK_cu(:))
    dK_ca(2,ur) = sum(Tb(2,:)*dK_cu(:))
    dK_ca(3,ur) = sum(Tb(3,:)*dK_cu(:))
  end do

  ! second variation of strain and curvature w.r.t. dofs
  do ur = 1, 3*NSHL
    kr = (ur+2)/3
    dirr = ur-3*(kr-1)
    do us = 1, ur
      ks = (us+2)/3
      dirs = us-3*(ks-1)
      ! strain
      ddE_cu = 0.0d0
      if (dirr == dirs) then
        ddE_cu(1) = dR(kr,1)*dR(ks,1)
        ddE_cu(2) = dR(kr,2)*dR(ks,2)
        ddE_cu(3) = 0.5d0*(dR(kr,1)*dR(ks,2)+dR(kr,2)*dR(ks,1))
      end if
      
      ddE_ca(1,ur,us) = sum(Tb(1,:)*ddE_cu(:))
      ddE_ca(2,ur,us) = sum(Tb(2,:)*ddE_cu(:))
      ddE_ca(3,ur,us) = sum(Tb(3,:)*ddE_cu(:))
      
      ! curvature
      ddg3 = 0.0d0

      dirt = 6-dirr-dirs
      ddir = dirr-dirs      

      if (ddir == -1) then
        ddg3(dirt) =  dR(kr,1)*dR(ks,2)-dR(ks,1)*dR(kr,2)
      else if (ddir ==  2) then
        ddg3(dirt) =  dR(kr,1)*dR(ks,2)-dR(ks,1)*dR(kr,2)
      else if (ddir ==  1) then
        ddg3(dirt) = -dR(kr,1)*dR(ks,2)+dR(ks,1)*dR(kr,2)
      else if (ddir == -2) then
        ddg3(dirt) = -dR(kr,1)*dR(ks,2)+dR(ks,1)*dR(kr,2)
      end if
    
      tmp1 = -(sum(ddg3(:)*g3(:)) + sum(dg3(:,ur)*dg3(:,us)))/lg3_3
      tmp2 = 3.0d0*g3dg3(ur)*g3dg3(us)/lg3_5
    
      ddn(1) = ddg3(1)/lg3 - g3dg3lg3_3(us)*dg3(1,ur) &
             - g3dg3lg3_3(ur)*dg3(1,us) + tmp1*g3(1) + tmp2*g3(1)
      ddn(2) = ddg3(2)/lg3 - g3dg3lg3_3(us)*dg3(2,ur) &
             - g3dg3lg3_3(ur)*dg3(2,us) + tmp1*g3(2) + tmp2*g3(2)
      ddn(3) = ddg3(3)/lg3 - g3dg3lg3_3(us)*dg3(3,ur) &
             - g3dg3lg3_3(ur)*dg3(3,us) + tmp1*g3(3) + tmp2*g3(3)
    
      ddK_cu(1) = -(ddR(kr,1)*dn(dirr,us) + ddR(ks,1)*dn(dirs,ur) &
                    + sum(h(:,1)*ddn(:)))
      ddK_cu(2) = -(ddR(kr,2)*dn(dirr,us) + ddR(ks,2)*dn(dirs,ur) &
                    + sum(h(:,2)*ddn(:)))
      ddK_cu(3) = -(ddR(kr,3)*dn(dirr,us) + ddR(ks,3)*dn(dirs,ur) &
                    + sum(h(:,3)*ddn(:)))
      
      ddK_ca(1,ur,us) = sum(Tb(1,:)*ddK_cu(:))
      ddK_ca(2,ur,us) = sum(Tb(2,:)*ddK_cu(:))
      ddK_ca(3,ur,us) = sum(Tb(3,:)*ddK_cu(:))
    end do
  end do
  
  ! N_ca = Dm*E_ca + Dc*K_ca
  ! M_ca = Dc*E_ca + Db*K_ca
  ! "matmul" is very slow...
!  N_ca = matmul(Dm(1:3,1:3),E_ca(1:3)) + matmul(Dc(1:3,1:3),K_ca(1:3))
!  M_ca = matmul(Dc(1:3,1:3),E_ca(1:3)) + matmul(Db(1:3,1:3),K_ca(1:3))

  do i = 1, 3
    N_ca(i) = sum(Dm(i,:)*E_ca) + sum(Dc(i,:)*K_ca)
    M_ca(i) = sum(Dc(i,:)*E_ca) + sum(Db(i,:)*K_ca)
  end do

  kem = 0.0d0
  keb = 0.0d0
  ! loop over dofs ur and us
  do ur = 1,3*NSHL
    ! dN_ca = Dm*dE_ca(:,ur)
    ! dM_ca = Db*dK_ca(:,ur)
!    dN_ca = MATMUL(Dm,dE_ca(:,ur)) + MATMUL(Dc,dK_ca(:,ur))
!    dM_ca = MATMUL(Dc,dE_ca(:,ur)) + MATMUL(Db,dK_ca(:,ur))

    do i = 1, 3
      dN_ca(i) = sum(Dm(i,:)*dE_ca(:,ur)) + sum(Dc(i,:)*dK_ca(:,ur))
      dM_ca(i) = sum(Dc(i,:)*dE_ca(:,ur)) + sum(Db(i,:)*dK_ca(:,ur))
    end do

    do us = 1, ur
      ! membrane stiffness
      kem(ur,us) = sum(dN_ca(:)*dE_ca(:,us)) &
                 + sum(N_ca(:)*ddE_ca(:,ur,us))
      ! bending stiffness
      keb(ur,us) = sum(dM_ca(:)*dK_ca(:,us)) &
                 + sum(M_ca(:)*ddK_ca(:,ur,us))
    end do
    ! residual
    fiem(ur) = -(sum(N_ca(:)*dE_ca(:,ur)))
    fieb(ur) = -(sum(M_ca(:)*dK_ca(:,ur)))
  end do
  ke  = ( kem +  keb)*dA
  fie = (fiem + fieb)*dA
  
  ! transform to Yuri's format
  do ur = 1, 3*NSHL
    do us = 1, 3*NSHL
      if (us > ur) then
        ke(ur,us) = ke(us,ur)
      end if
    end do
  end do

  do ur = 1,3*NSHL
    kr = (ur+2)/3
    dirr = ur-3*(kr-1)
    do us = 1,3*NSHL
      ks = (us+2)/3
      dirs = us-3*(ks-1)
      i = (dirr-1)*3 + dirs

      xKebe(i,kr,ks) = ke(ur,us)

    end do
    Rhs(dirr,kr) = fie(ur)
  end do
      
end subroutine e3LRhs_KLShell_old
