!======================================================================
! returns entities of the reference geometry needed for nonlinear 
! stiffness matrix
!
! covariant base vectors G and hessian H
!   g1=dxdu    g2=dxdv
!   h1=dxdu^2  h2=dxdv^2  h3=dxdudv
!======================================================================
subroutine shell_geo(nshl, nsd, nnode, dR, ddR, lIEN, B_NET_SH, &
                     G, H, G3, dA, N, Bv, Gab, dN)
  implicit none      
  integer, intent(in) :: nshl, nnode, nsd, lIEN(nshl)
  real(8), intent(in) :: dR(NSHL,2), ddR(NSHL,3), B_NET_SH(nnode,nsd+1)

  real(8), intent(out) :: G(3,2), H(3,3), G3(3), dA, N(3), Bv(3), &
                          Gab(3), dN(3,2)

  real(8) :: dG3(3,2), Imat(3,3), Nmat(3,3), rtmp1(3), rtmp2(3)

  integer :: a, k, i, j, ii

  ! covariant base vectors G and hessian H
  G = 0.0d0  ! g1=dxdu; g2=dxdv
  H = 0.0d0  ! h1=dxdu^2; h2=dxdv^2; h3=dxdudv
  do ii = 1, nshl
    k = lIEN(ii)
    do a = 1, 3
      G(a,1:2) =  dR(ii,1:2)*B_NET_SH(k,a) + G(a,1:2)
      H(a,1:3) = ddR(ii,1:3)*B_NET_SH(k,a) + H(a,1:3)
    end do
  end do
     
  ! basis vector G3
  G3(1) = G(2,1)*G(3,2) - G(3,1)*G(2,2)
  G3(2) = G(3,1)*G(1,2) - G(1,1)*G(3,2)
  G3(3) = G(1,1)*G(2,2) - G(2,1)*G(1,2)

  ! differential area dA = length of G3
  dA = sqrt(sum(G3**2))

  ! normal vector N (a_3 in the paper)
  N = G3/dA
      
  ! curvature coefficients as vector
  Bv(:) = H(1,:)*N(1) + H(2,:)*N(2) + H(3,:)*N(3)

  ! covariant metric Gab
  Gab(1) = G(1,1)*G(1,1) + G(2,1)*G(2,1) + G(3,1)*G(3,1)
  Gab(2) = G(1,2)*G(1,2) + G(2,2)*G(2,2) + G(3,2)*G(3,2)
  Gab(3) = G(1,1)*G(1,2) + G(2,1)*G(2,2) + G(3,1)*G(3,2)

  ! cross product
  call cross(H(:,1), G(:,2), rtmp1)
  call cross(G(:,1), H(:,3), rtmp2)
  dG3(:,1) = rtmp1 + rtmp2

  call cross(H(:,3), G(:,2), rtmp1)
  call cross(G(:,1), H(:,2), rtmp2)
  dG3(:,2) = rtmp1 + rtmp2

  ! Identity matrix
  Imat = 0.0d0
  Imat(1,1) = 1.0d0
  Imat(2,2) = 1.0d0
  Imat(3,3) = 1.0d0

  ! N*N'
  do i = 1, 3
    do j = 1, 3
      Nmat(i,j) = N(i)*N(j)
    end do
  end do

  dN = 1.0d0/dA*matmul((Imat-Nmat),dG3)
  
  !dN(:,1) = dG3(:,1)/dA - N/dA*sum(N*dG3(:,1))
  !dN(:,2) = dG3(:,2)/dA - N/dA*sum(N*dG3(:,2))
  
  !Bv(1) = -sum(G(:,1)*dN(:,1))
  !Bv(2) = -sum(G(:,2)*dN(:,2))
  !Bv(3) = -sum(G(:,1)*dN(:,2))

end subroutine shell_geo




!======================================================================
!
!======================================================================
subroutine shell_geo_old(nshl, nsd, nnode, q, dR, ddR, lIEN, B_NET_SH, &
                     nor, g, g3, dA, n, Gab, Gab_con, H, Bv,       &
                     T_Gcon_E, T_E_G, T_G_E)
  implicit none      
  integer, intent(in) :: nshl, nnode, nsd, lIEN(nshl), q
  real(8), intent(in) :: dR(NSHL,2), ddR(NSHL,3), B_NET_SH(nnode,nsd+1), &
                         nor(3)

  real(8), intent(out) :: Bv(3), T_Gcon_E(3,3), dA,    & 
                          T_E_G(3,3), T_G_E(3,3),      &
                          G(3,2), G3(3), N(3), H(3,3), &
                          Gab(2,2), Gab_con(2,2)

  real(8) :: G_con(3,2), E(3,2), invdetGab, lg1, lg_con2, EG(2,2), &
             vtmp(3)
  integer :: k, c, b, a, ii
      
  ! covariant base vectors G and hessian H
  G = 0.0d0
  H = 0.0d0
  do ii = 1, nshl
    k = lIEN(ii)
    do a = 1,3
      G(a,1:2) =  dR(ii,1:2)*B_NET_SH(k,a) + G(a,1:2)
      H(a,1:3) = ddR(ii,1:3)*B_NET_SH(k,a) + H(a,1:3)
    end do
  end do
     
  ! basis vector G3
  G3(1) = G(2,1)*G(3,2) - G(3,1)*G(2,2)
  G3(2) = G(3,1)*G(1,2) - G(1,1)*G(3,2)
  G3(3) = G(1,1)*G(2,2) - G(2,1)*G(1,2)

  ! differential area dA = length of G3
  dA = sqrt(sum(G3*G3))

  ! normal vector N
  N = G3/dA
      
  ! curvature coefficients as vector
  Bv(:) = H(1,:)*N(1) + H(2,:)*N(2) + H(3,:)*N(3)

  ! covariant metric Gab
  Gab = matmul(transpose(G),G)

  ! contravariant metric Gab_con and base vectors G_con
  invdetGab = 1.0d0/(Gab(1,1)*Gab(2,2)-Gab(1,2)*Gab(1,2))
  Gab_con(1,1) =  invdetgab*Gab(2,2)
  Gab_con(1,2) = -invdetgab*Gab(1,2)
  Gab_con(2,2) =  invdetgab*Gab(1,1)
  Gab_con(2,1) =  Gab_con(1,2)

  G_con = matmul(G,transpose(Gab_con))

  ! local cartesian coordinates
  lg1 = sqrt(sum(G(:,1)**2))
  E(:,1) = G(:,1)/lg1

  lg_con2 = sqrt(sum(G_con(:,2)**2))
  E(:,2) = G_con(:,2)/lg_con2

!  !*****************************************************
!  ! local cartesian coordinates for wind turbine blade
!  !*****************************************************
!  vtmp = (/0.0, 1.0, 0.0/)
!  E(1,1) = vtmp(2)*nor(3) - vtmp(3)*nor(2)
!  E(2,1) = vtmp(3)*nor(1) - vtmp(1)*nor(3)
!  E(3,1) = vtmp(1)*nor(2) - vtmp(2)*nor(1)
!  E(:,1) = E(:,1)/sqrt(sum(E(:,1)**2))
!  E(1,2) = nor(2)*E(3,1) - nor(3)*E(2,1)
!  E(2,2) = nor(3)*E(1,1) - nor(1)*E(3,1)
!  E(3,2) = nor(1)*E(2,1) - nor(2)*E(1,1)
!  E(:,2) = E(:,2)/sqrt(sum(E(:,2)**2))

  ! quick fix for bending strip
  ! theta_1 need to coincide with the linear direction
  if (q == 1) then
    ! local cartesian coordinates
    lg1 = sqrt(sum(G(:,2)**2))
    E(:,1) = G(:,2)/lg1
    lg_con2 = sqrt(sum(G_con(:,1)**2))
    E(:,2) = G_con(:,1)/lg_con2
  end if

  ! Transformation matrix T_Gcon_E from G_con to E
  ! with *2 in last row (for strain in Voigt notation)
  EG = matmul(transpose(E),G_con)

  T_Gcon_E(1,1) = EG(1,1)**2
  T_Gcon_E(1,2) = EG(1,2)**2
  T_Gcon_E(1,3) = 2.0d0*EG(1,1)*EG(1,2)
  T_Gcon_E(2,1) = EG(2,1)**2
  T_Gcon_E(2,2) = EG(2,2)**2
  T_Gcon_E(2,3) = 2.0d0*EG(2,1)*EG(2,2)
  T_Gcon_E(3,1) = 2.0d0*EG(1,1)*EG(2,1)
  T_Gcon_E(3,2) = 2.0d0*EG(1,2)*EG(2,2)
  T_Gcon_E(3,3) = 2.0d0*EG(1,1)*EG(2,2)+EG(1,2)*EG(2,1)

  ! Transformation matrix T_E_G from E to G (for PK2 stress)
  T_E_G(1,1) = EG(1,1)**2
  T_E_G(1,2) = EG(2,1)**2             ! be carefure the difference
  T_E_G(1,3) = 2.0d0*EG(1,1)*EG(2,1)  ! be carefure the difference
  T_E_G(2,1) = EG(1,2)**2
  T_E_G(2,2) = EG(2,2)**2
  T_E_G(2,3) = 2.0d0*EG(1,2)*EG(2,2)
  T_E_G(3,1) = EG(1,1)*EG(1,2)
  T_E_G(3,2) = EG(2,1)*EG(2,2)
  T_E_G(3,3) = EG(1,1)*EG(2,2)+EG(2,1)*EG(1,2)

  ! Transformation matrix T_g_e from g to e (for Cauchy stress)
  EG = matmul(transpose(E),G)

  T_G_E(1,1) = EG(1,1)**2
  T_G_E(1,2) = EG(1,2)**2
  T_G_E(1,3) = 2.0d0*EG(1,1)*EG(1,2)
  T_G_E(2,1) = EG(2,1)**2
  T_G_E(2,2) = EG(2,2)**2
  T_G_E(2,3) = 2.0d0*EG(2,1)*EG(2,2)
  T_G_E(3,1) = EG(1,1)*EG(2,1)
  T_G_E(3,2) = EG(1,2)*EG(2,2)
  T_G_E(3,3) = EG(1,1)*EG(2,2)+EG(1,2)*EG(2,1)

end subroutine shell_geo_old
