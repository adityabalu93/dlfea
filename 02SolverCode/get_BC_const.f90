!======================================================================
! get the flowrate from input file. Linear intepolation
!======================================================================
subroutine get_BC_const(tim, Cmult)

  implicit none

  real(8), intent(in ) :: tim
  real(8), intent(out ):: Cmult

  integer :: i, j, k, l, nrec
  logical :: exts
  real(8) :: dist1, dist2
  real(8), allocatable :: tabl(:,:)

  inquire(file='flowrate.dat', exist=exts)

  if (exts) then

    open(99, file='flowrate.dat', status='old')
    read(99,*) nrec
    allocate(tabl(nrec,2))
    do i = 1, nrec
      read(99,*) tabl(i,1), tabl(i,2)
!!!      tabl(i,2) = tabl(i,2) + 5.1d0
    end do
    close(99)

    do i = 2, nrec
      dist2 = tim - tabl(i,1)
      if (dist2 <= 0.0d0) then
    dist1 = tim - tabl(i-1,1)

    Cmult = abs(dist1)/(abs(dist1)+abs(dist2))*tabl(i,2) + &
            abs(dist2)/(abs(dist1)+abs(dist2))*tabl(i-1,2)
    exit
      end if
    end do

    deallocate(tabl)

  else
    write(*,*) 'issue: missing flowrate file'
  end if
 
end subroutine get_BC_const
