subroutine shell_allocate_LRhs(FUN, NSD, SHL)

  use types_shell
  use commonvar_shell
  implicit none

  type(shell), intent(inout) :: SHL
  type(mesh),  intent(in)    :: FUN
  integer,     intent(in)    :: NSD
  integer :: ier

  allocate(SHL%col(FUN%NNODE+1), SHL%row(FUN%NNODE*overflow*FUN%maxNSHL), &
       stat=ier)
  if (ier /= 0) stop 'Allocation Error: col'    
  SHL%col = 0; SHL%row = 0
    
  call genSparStruc_shell(FUN%nel*((fun%ngauss)**2), &
       FUN%NNODE, FUN%maxNSHL, FUN%gp%cIEN, &
       FUN%gp%cNSHL, SHL%col, SHL%row, SHL%icnt)

!   call genSparStruc_shell(FUN%NEL, FUN%NNODE, FUN%maxNSHL, FUN%IEN, &
!                           FUN%NSHL, SHL%col, SHL%row, SHL%icnt)
            
  ! allocate the global RHS and LHS
  allocate(SHL%RHSG(FUN%NNODE,NSD), &
           SHL%RHSG_EXT(FUN%NNODE,NSD), &
           SHL%RHSG_GRA(FUN%NNODE,NSD))
  allocate(SHL%LHSK(NSD*NSD,SHL%icnt))
  SHL%RHSG     = 0.0d0
  SHL%RHSG_EXT = 0.0d0
  SHL%RHSG_GRA = 0.0d0
  SHL%LHSK     = 0.0d0

!  allocate(SHL%yg(FUN%NNODE,NSD), SHL%dg(FUN%NNODE,NSD),  &
!           SHL%tg(FUN%NNODE),     SHL%mg(FUN%NNODE),  &
!           SHL%dl(FUN%NNODE,NSD))
!  SHL%yg = 0.0d0; SHL%dg = 0.0d0; SHL%tg = 0.0d0; SHL%mg = 0.0d0
!  SHL%dl = 0.0d0

end subroutine shell_allocate_LRhs

subroutine shell_deallocate_LRhs(FUN, NSD, SHL)

  use types_shell
  implicit none

  type(shell), intent(inout) :: SHL
  type(mesh),  intent(in)    :: FUN
  integer,     intent(in)    :: NSD
  integer :: ier

  deallocate(SHL%col, SHL%row)
    
  ! allocate the global RHS and LHS
  deallocate(SHL%RHSG, &
           SHL%RHSG_EXT, &
           SHL%RHSG_GRA)
  deallocate(SHL%LHSK)

end subroutine shell_deallocate_LRhs
