!================================================================
! subroutine to fill in the displacements, weights, normals, and determinants
! for gauss points on a T-Spline surface
!================================================================
subroutine shell_gp_info_tsp(TSP, BEZ, ushAlpha, ashAlpha, nsd)
  
  use types_shell
  implicit none
  
  type(mesh), intent(inout) :: TSP, BEZ
  integer, intent(in) :: nsd
  real(8), intent(in) :: ushAlpha(TSP%NNODE,nsd), ashAlpha(TSP%NNODE,nsd)

  ! for testing purposes
!  real(8) :: testsum

  !  Local variables
  integer :: p, q, nshl, nshb, nuk, nvk, ptype, iel, igauss, jgauss, &
             i, j, ni, nj, aa, bb, igp, s

  real(8) :: gp(TSP%NGAUSS), gw(TSP%NGAUSS), gwt, da, VVal, &
             DetJb_SH, nor(NSD), thi, Dm(3,3), Dc(3,3), Db(3,3),&
             xu(NSD), xd(NSD), dxdxi(NSD,2), ddxddxi(nsd,3), &
             dens, bvec(NSD), bscale, DetJb_r

  ! TODO: move this to somewhere more appropriate
  real(8) :: samplingDistance

  integer, allocatable :: lIEN(:)
  real(8), allocatable :: shl(:), shgradl(:,:), shhessl(:,:)
  real(8) :: gpvelocity(3), gpaccel(3)

  ! get Gaussian points and weights
  gp = 0.0d0; gw = 0.0d0
  call genGPandGW_shell(gp, gw, TSP%NGAUSS) 

  ! loop over elements
  do iel = 1, TSP%NEL
    

    p = BEZ%P(iel); nshl = TSP%NSHL(iel)
!         ; ptype = TSP%PTYPE(iel)
    q = BEZ%Q(iel); nshb = BEZ%NSHL(iel)

              
      allocate(shl(nshl), shgradl(nshl,2), shhessl(nshl,3), lIEN(nshl))

      lIEN = -1
      do i = 1, nshl
        lIEN(i) = TSP%IEN(iel,i)
      end do    

      ! counter for gauss points within this element
      igp = 1

      ! Loop over integration points (NGAUSS in each direction) 
      do jgauss = 1, TSP%NGAUSS
        do igauss = 1, TSP%NGAUSS
        
           ! Get Element Shape functions and their gradients
           shl = 0.0d0; shgradl = 0.0d0; shhessl = 0.0d0
           xu = 0.0d0; xd = 0.0d0; dxdxi = 0.0d0; ddxddxi = 0.0d0
           nor = 0.0d0
                      
           call eval_SHAPE_bez_sh(gp(igauss), gp(jgauss), &
                                  shl, shgradl, shhessl, nor, &
                                  xu, xd, dxdxi, ddxddxi,  &
                                  p, q, nsd, nshl, nshb,  &
                                  lIEN, TSP%NNODE, &
                                  TSP%B_NET_U, TSP%B_NET_D, DetJb_r, DetJb_SH, &
                                  BEZ%Ext(iel,1:nshl,1:nshb))           
           
           gwt = gw(igauss)*gw(jgauss)

           ! compute velocity at gauss point from ushAlpha input array and
           ! shape function values
           gpvelocity = 0.0
           gpaccel = 0.0
           do s = 1,nshl
              gpvelocity(:) = gpvelocity(:) + shl(s)*ushAlpha(lIEN(s),:)
              gpaccel(:) = gpaccel(:) + shl(s)*ashAlpha(lIEN(s),:)
           enddo
           
           ! fill in the gp data structure entries for this gauss point
           TSP%gp%param(iel,igp,1) = gp(igauss)
           TSP%gp%param(iel,igp,2) = gp(jgauss)
           TSP%gp%xg(iel,igp,:) = xd(:)
           TSP%gp%nor(iel,igp,:) = &
                nor(:)/sqrt(nor(1)**2 + nor(2)**2 + nor(3)**2)
           TSP%gp%gw(iel,igp) = gwt
           TSP%gp%detJ(iel,igp) = DetJb_SH
           TSP%gp%ush(iel,igp,:) = gpvelocity(:)
           TSP%gp%ash(iel,igp,:) = gpaccel(:)
           TSP%gp%tv1( iel,igp,:) = dxdxi(:,1)/sqrt(sum(dxdxi(:,1)**2))
           TSP%gp%tv2( iel,igp,:) = dxdxi(:,2)/sqrt(sum(dxdxi(:,2)**2))

           ! (gauss point traction to be filled in weak BC routine)

           igp = igp+1
        enddo
     enddo

     
     deallocate(shl, shgradl, shhessl,lIEN)
       
end do    ! end loop elements

end subroutine shell_gp_info_tsp
