!======================================================================
!
!======================================================================
subroutine cross(a, b, c)
  implicit none
  real(8), intent(out) :: c(3)
  real(8), intent(in)  :: a(3), b(3)

  c(1) = a(2)*b(3) - a(3)*b(2)
  c(2) = a(3)*b(1) - a(1)*b(3)
  c(3) = a(1)*b(2) - a(2)*b(1)
end subroutine cross

!======================================================================
!
!======================================================================
      subroutine get_Rmat(theta, thetd, thedd, Rmat, Rdot, Rddt)

      implicit none

      real(8), intent(in ) :: theta, thetd, thedd
      real(8), intent(out) :: Rmat(3,3), Rdot(3,3), Rddt(3,3)

      Rmat      = 0.0d0
      Rmat(1,1) = cos(theta) - 1.0d0
      Rmat(1,2) =-sin(theta)
      Rmat(2,1) = sin(theta)
      Rmat(2,2) = cos(theta) - 1.0d0

      Rdot      = 0.0d0
      Rdot(1,1) =-sin(theta)*thetd
      Rdot(1,2) =-cos(theta)*thetd
      Rdot(2,1) = cos(theta)*thetd
      Rdot(2,2) =-sin(theta)*thetd

      Rddt      = 0.0d0
      Rddt(1,1) =-cos(theta)*thetd**2 - sin(theta)*thedd
      Rddt(1,2) = sin(theta)*thetd**2 - cos(theta)*thedd
      Rddt(2,1) =-sin(theta)*thetd**2 + cos(theta)*thedd
      Rddt(2,2) =-cos(theta)*thetd**2 - sin(theta)*thedd

      end subroutine get_Rmat
      
      subroutine get_Rmat_3D(r, theta, thetd, thedd, Rmat, Rdot, Rddt)

      implicit none

      real(8), intent(in ) :: theta, thetd, thedd, r(3)
      real(8), intent(out) :: Rmat(3,3), Rdot(3,3), Rddt(3,3)
	  
	  real(8) :: cosTH, sinTH, norm, x, y, z, c, dc, dcos, dsin, &
	             dc2, dcos2, dsin2 
	  
      norm = sqrt(sum(r*r))
	
      x = r(1)/norm
      y = r(2)/norm
      z = r(3)/norm
	  
	  cosTH = cos(theta)
	  sinTH = sin(theta)
	  c     = 1.0d0 - cosTH
	  
	  dc   =  sinTH * thetd 
	  dcos = -sinTH * thetd
	  dsin =  cosTH * thetd
	  
	  dc2   = cosTH * thetd * thetd + sinTH * thedd
	  dcos2 = -cosTH * thetd * thetd - sinTH * thedd
	  dsin2 = -sinTH * thetd * thetd + cosTH * thedd
	  
	  Rmat      = 0.0d0
	  Rmat(1,1) = x*x*c + cosTH - 1.0d0
	  Rmat(1,2) = x*y*c - z*sinTH
	  Rmat(1,3) = x*z*c + y*sinTH
	  
	  Rmat(2,1) = x*y*c + z*sinTH
	  Rmat(2,2) = y*y*c + cosTH - 1.0d0
	  Rmat(2,3) = y*z*c - x*sinTH
	  
	  Rmat(3,1) = x*z*c - y*sinTH
	  Rmat(3,2) = y*z*c + x*sinTH
	  Rmat(3,3) = z*z*c + cosTH - 1.0d0
	  
	  Rdot      = 0.0d0
	  Rdot(1,1) = x*x*dc + dcos
	  Rdot(1,2) = x*y*dc - z*dsin
	  Rdot(1,3) = x*z*dc + y*dsin
	  
	  Rdot(2,1) = x*y*dc + z*dsin
	  Rdot(2,2) = y*y*dc + dcos
	  Rdot(2,3) = y*z*dc - x*dsin
	  
	  Rdot(3,1) = x*z*dc - y*dsin
	  Rdot(3,2) = y*z*dc + x*dsin
	  Rdot(3,3) = z*z*dc + dcos
	  
	  Rddt      = 0.0d0
	  Rddt(1,1) = x*x*dc2 + dcos2
	  Rddt(1,2) = x*y*dc2 - z*dsin2
	  Rddt(1,3) = x*z*dc2 + y*dsin2
	  
	  Rddt(2,1) = x*y*dc2 + z*dsin2
	  Rddt(2,2) = y*y*dc2 + dcos2
	  Rddt(2,3) = y*z*dc2 - x*dsin2
	  
	  Rddt(3,1) = x*z*dc2 - y*dsin2
	  Rddt(3,2) = y*z*dc2 + x*dsin2
	  Rddt(3,3) = z*z*dc2 + dcos2

      end subroutine get_Rmat_3D
      
      subroutine Rmat3D(r, theta, Rmat)

      implicit none

      real(8), intent(in ) :: theta, r(3)
      real(8), intent(out) :: Rmat(3,3)
	  
	  real(8) :: cosTH, sinTH, norm, x, y, z, c, dc, dcos, dsin, &
	             dc2, dcos2, dsin2 
	  
      norm = sqrt(sum(r*r))
	
      x = r(1)/norm
      y = r(2)/norm
      z = r(3)/norm
	  
	  cosTH = cos(theta)
	  sinTH = sin(theta)
	  c     = 1.0d0 - cosTH 
	  
	  Rmat      = 0.0d0
	  Rmat(1,1) = x*x*c + cosTH 
	  Rmat(1,2) = x*y*c - z*sinTH
	  Rmat(1,3) = x*z*c + y*sinTH
	  
	  Rmat(2,1) = x*y*c + z*sinTH
	  Rmat(2,2) = y*y*c + cosTH 
	  Rmat(2,3) = y*z*c - x*sinTH
	  
	  Rmat(3,1) = x*z*c - y*sinTH
	  Rmat(3,2) = y*z*c + x*sinTH
	  Rmat(3,3) = z*z*c + cosTH 

      end subroutine Rmat3D
      
      
      
