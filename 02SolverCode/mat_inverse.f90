!======================================================================
! subroutine to invert an 3x3 matrix
!======================================================================
subroutine mat_inverse_3x3(Amat, Ainv, DetJ)
  implicit none
  real(8), intent(in)  :: Amat(3,3)
  real(8), intent(out) :: Ainv(3,3), DetJ
  real(8) :: tmp

  Ainv = 0.0d0

  Ainv(1,1) = Amat(2,2)*Amat(3,3) - Amat(3,2)*Amat(2,3)
  Ainv(1,2) = Amat(3,2)*Amat(1,3) - Amat(1,2)*Amat(3,3)
  Ainv(1,3) = Amat(1,2)*Amat(2,3) - Amat(1,3)*Amat(2,2)

  tmp = 1.0d0/(Ainv(1,1)*Amat(1,1) + Ainv(1,2)*Amat(2,1) + &
               Ainv(1,3)*Amat(3,1))

  Ainv(1,1) = Ainv(1,1)*tmp
  Ainv(1,2) = Ainv(1,2)*tmp
  Ainv(1,3) = Ainv(1,3)*tmp

  Ainv(2,1) = (Amat(2,3)*Amat(3,1) - Amat(2,1)*Amat(3,3)) * tmp
  Ainv(2,2) = (Amat(1,1)*Amat(3,3) - Amat(3,1)*Amat(1,3)) * tmp
  Ainv(2,3) = (Amat(2,1)*Amat(1,3) - Amat(1,1)*Amat(2,3)) * tmp
  Ainv(3,1) = (Amat(2,1)*Amat(3,2) - Amat(2,2)*Amat(3,1)) * tmp
  Ainv(3,2) = (Amat(3,1)*Amat(1,2) - Amat(1,1)*Amat(3,2)) * tmp
  Ainv(3,3) = (Amat(1,1)*Amat(2,2) - Amat(1,2)*Amat(2,1)) * tmp

  DetJ = 1.0d0/tmp
end subroutine mat_inverse_3x3
