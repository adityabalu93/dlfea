!======================================================================
! Read primal solution
!======================================================================
subroutine read_sol_tsp(istep, time, rtime, TSP)
  use aAdjKeep
  use commonvar
  use types_shell

  implicit none

  type(mesh),    intent(inout) :: TSP

  integer, intent(in)  :: istep
  real(8), intent(out) :: time, rtime

  integer :: nf1, i, j, itmp, ip, stat
  character(len=30) :: fname, fnum(3)

  write(*,*) "Reading step:", istep

  nf1 = 98

  write(fnum(2),'(I30)') istep
 !  fname = 'restart.'//trim(adjustl(fnum(2)))
!   open(nf1, file=fname, status='old')
!
!   read(nf1,*) itmp, time, rtime
!
!   do i = 1, NNODZu
!     read(nf1,*) (dgold(i,j), j = 1, 3)
!   end do
!
!   do i = 1, NNODZu
!     read(nf1,*) (ugold(i,j), j = 1, 3)
!   end do
!
!   do i = 1, NNODZu
!     read(nf1,*) (ugmold(i,j), j = 1, 3)
!   end do
!
!   do i = 1, NNODZu
!     read(nf1,*) pgold(i)
!   end do
!
!   do i = 1, NNODZu
!     read(nf1,*) (acgold(i,j), j = 1, 3)
!   end do
!
!   do i = 1, NNODZu
!     read(nf1,*) (acgmold(i,j), j = 1, 3)
!   end do
!
!   close(nf1)

  fname = 'sh.rest.tsp.'//trim(adjustl(fnum(2)))
  open(nf1, file=fname, status='old')             
                     
  do i = 1, TSP%NNODE
    read(nf1,*) (TSP%dshOld(i,j), j = 1, 3)
  end do
  do i = 1, TSP%NNODE
    read(nf1,*) (TSP%ushOld(i,j), j = 1, 3)
  end do
  do i = 1, TSP%NNODE
    read(nf1,*) (TSP%ashOld(i,j), j = 1, 3)
  end do   
  
  read(nf1,*, IOSTAT=stat) itmp, time, rtime
  
  close(nf1)  
  
end subroutine read_sol_tsp



!======================================================================
! Output primal solution
!======================================================================
subroutine write_sol_tsp(istep, time, rtime, TSP)
  use aAdjKeep
  use commonvar
  use types_shell
   
  implicit none
  
  type(mesh),    intent(in) :: TSP

  integer, intent(in) :: istep
  real(8), intent(in) :: time, rtime

  integer :: nf1, i, j, ip
  character(len=30) :: fname, fnum(3)

  nf1 = 99

  write(fnum(2),'(I30)') istep
  fname = 'restart.'//trim(adjustl(fnum(2)))
  ! open(nf1, file=fname, status='replace')
!
!   ! LAST RESTART
!   write(nf1,*) istep, time, rtime
!
!   do i = 1, NNODZu
!     write(nf1,*) (dg(i,j), j = 1, 3)
!   end do
!
!   do i = 1, NNODZu
!     write(nf1,*) (ug(i,j), j = 1, 3)
!   end do
!
!   do i = 1, NNODZu
!     write(nf1,*) (ugm(i,j), j = 1, 3)
!   end do
!
!   do i = 1, NNODZu
!     write(nf1,*) pg(i)
!   end do
!
!   do i = 1, NNODZu
!     write(nf1,*) (acg(i,j), j = 1, 3)
!   end do
!
!   do i = 1, NNODZu
!     write(nf1,*) (acgm(i,j), j = 1, 3)
!   end do
!
!   close(nf1)

  ! output shell displacement
    
    fname = 'sh.rest.tsp.'//trim(adjustl(fnum(2)))
    open(nf1, file=fname, status='replace')             
                       
    do i = 1, TSP%NNODE
      write(nf1,*) (TSP%dsh(i,j), j = 1, 3)
    end do
    do i = 1, TSP%NNODE
      write(nf1,*) (TSP%ush(i,j), j = 1, 3)
    end do
    do i = 1, TSP%NNODE
      write(nf1,*) (TSP%ash(i,j), j = 1, 3)
    end do    
    
    write(nf1,*) istep, time, rtime
    
    close(nf1)  
      

end subroutine write_sol_tsp

!======================================================================
! Output all patches data for tmesh.0.dat
!======================================================================
subroutine write_sol_tsp_full(NSD, TSP, BEZ)
  use aAdjKeep
  use commonvar_shell
  use types_shell
   
  implicit none
  
  type(mesh),    intent(in) :: TSP, BEZ
  integer,       intent(in) :: NSD  
  integer :: nf1, i, j, k
  character(len=30) :: fname
  
  nf1 = 12
  fname = 'tmesh.0.dat'
  open(nf1, file=fname, status='replace') 
  
  write(nf1,*) 'type surface'
  write(nf1,*) 'nodeN', TSP%NNODE
  write(nf1,*) 'elemN', TSP%NEL
  write(nf1,*) 'maxNSHL', TSP%maxNSHL
  do i = 1, TSP%NNODE
    write(nf1,*) 'node', (TSP%B_NET(i, j), j = 1, NSD+1)
  end do  
  
  ! Bezeir Elements, IEN need to minus 1
  do i = 1, TSP%NEL
    write(nf1,*) 'belem', TSP%NSHL(i), BEZ%P(i), BEZ%Q(i)
    write(nf1,*) (TSP%IEN(i,j)-1, j = 1, TSP%NSHL(i))
    do j = 1, TSP%NSHL(i)
      write(nf1,*) (BEZ%EXT(i,j,k), k = 1, BEZ%NSHL(i))
    end do
  end do

  ! Notice: no need to add BC temporary. Boundary needs to minus 1
  write(nf1,*) 'set', TSP%nIBC, 'node on boundary'
  write(nf1,*) (TSP%boundary-1)
  close(nf1)
  
!   ! Mapping between TSP and MTSP
!   nf2 = 13
!   fname = 'map.0.dat'
!   open(nf2, file=fname, status='replace')
!   do ip = 1, NPATCH
!     write(nf2,*) ip, mTSP%NNODE(ip)
!     do i = 1, mTSP%NNODE(ip)
!       write(nf2,*) i, mTSP%MAP(ip,i)
!     end do
!   end do
!
!   close(nf2)
  
end subroutine write_sol_tsp_full

subroutine write_shell_INFOs(NNODE, n_info, INFOS, istep)
    implicit none
    
    integer, intent(in) :: istep, n_info, NNODE
    real(8), intent(in) :: INFOS(NNODE, n_info)
    
    integer :: nf1, i, j
    character(len=30) :: fname, fnum
    character(LEN=30) FMT
    
    write(FMT,'("(", I0,"ES17.8)")') n_info

    nf1 = 99

    write(fnum,'(I30)') istep
    fname = 'sh.info.'//trim(adjustl(fnum))
    
    open(nf1, file=fname, status='replace')        
    
    do i = 1, NNODE
        write(nf1, FMT) (INFOS(i,j), j = 1, n_info)
    end do
    
    close(nf1)
    
end subroutine write_shell_INFOs
