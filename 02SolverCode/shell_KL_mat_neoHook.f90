!======================================================================
! this routine computes the shell stress resultants for a hyperelastic
! material. the specific material law is defined in a subroutine, such that
! different material laws can easily be substituted.
!----------------------
! denomination:
! N_cu,M_cu,dN_cu,dM_cu = normal forces, moments and derivatives
! G0, g0 = mid-plane base vectors
! G3,dG3, g3,dg3 = unit length directors and derivatives G3,alpha
! G.g = 3 base vectors in shell continuum
! mue, km = shear and bulk modulus
! dE_cu,dK_cu = membrane and curvature strain derivatives
! dE = Green-Lagrange strain derivatives
! C = left Cauchy-Green deformation tensor
! Ci = inverse of C
! J = determinant of deformation gradient
! Gij, Gconij = convariant and contravariant metric coefficients
! S,dS = PK2 stress and derivative as vectors
! St = PK2 stress tensor
! CC = material tensor
! D = material matrix
!======================================================================
subroutine shell_mat_neoHook(nshl, G0_r, G3_r, dG3_r, g0, g3, dg3, &
                              kappa, mu, thi, ngauss, dE_cu, dK_cu, &
                              N_cu, dN_cu, M_cu, dM_cu)
  implicit none      

  integer, intent(in) :: nshl, ngauss

  real(8), intent(in) :: G0_r(3,2), G3_r(3), dG3_r(3,2), &
                         g0(3,2), g3(3), dg3(3,2), &
                         kappa, mu, thi, dE_cu(3,3*NSHL), &
                         dK_cu(3,3*NSHL)

  real(8), intent(out) :: N_cu(3), M_cu(3), dN_cu(3,3*NSHL), &
                          dM_cu(3,3*NSHL)

  real(8) :: G_r(3,3), g(3,3), Gij(3,3), GC(3,3), Gconij(3,3),  &
             C(3,3), Ci(3,3), CC(3,3,3,3), Dtemp(4,4), D(3,3),  &
             St(3,3), S(3), gp(ngauss), gw(ngauss)
             
  real(8) :: zeta, detJ, rtmp, dC33, trC

  integer :: i, j, k, l, iz

  N_cu  = 0.0d0
  M_cu  = 0.0d0
  dN_cu = 0.0d0
  dM_cu = 0.0d0

  C   = 0.0d0
  Gij = 0.0d0 
  GC  = 0.0d0
  D   = 0.0d0

  ! thickness integration loop
  call genGPandGW_shell(gp, gw, ngauss) 

  do iz = 1, ngauss
  
    zeta = 0.5d0*thi*GP(iz)

    G_r(1:3,1:2) = G0_r(1:3,1:2) + zeta*dG3_r(1:3,1:2)
    G_r(1:3,3)   = G3_r(1:3)

    g(1:3,1:2) = g0(1:3,1:2) + zeta*dg3(1:3,1:2)
    g(1:3,3)   = g3(1:3)

    ! Metric and C tensor
!!!    Gij = matmul(transpose(G_r),G_r)

    ! keep the following line, but commented out. it's not clear yet if C
    ! should be computed like this or like in the next for-loops, both
    ! cases have justification. if you want, you can try both, in my
    ! tests it didn't change anything on the results.
!!!    C = matmul(transpose(g),g)

    ! linear strain distribution through thickness
    do i = 1, 2
      do j = 1, 2
        C(i,j) = sum(g0(:,i)*g0(:,j)) + 2.0d0*zeta*sum(g0(:,i)*dg3(:,j))
        Gij(i,j) = sum(G0_r(:,i)*G0_r(:,j)) + 2.0d0*zeta*sum(G0_r(:,i)*dG3_r(:,j))
      end do
    end do

    do i = 1, 2
      C(i,3) = sum((g0(:,i)+zeta*dg3(:,i)) * g3(:))   ! should be zero
      C(3,i) = C(i,3)                                 ! should be zero
	 
      Gij(i,3) = sum((G0_r(:,i)+zeta*dG3_r(:,i)) * G3_r(:))   ! should be zero
      Gij(3,i) = Gij(i,3)                                 ! should be zero
    end do
    C(3,3) = sum(g3*g3)
    Gij(3,3) = sum(G3_r*G3_r)
	
    ! contravariant metric
    call mat_inverse_3x3(Gij, Gconij, rtmp)

    ! Iterating S33=0
    dC33 = 1.0d0
    do while (abs(dC33) > 1.0d-9)
      trC = 0.0d0
      do i = 1, 3
        do j = 1, 3
          GC(i,j) = sum(Gconij(i,:)*C(:,j))
          trC = C(i,j)*Gconij(j,i) + trC
        end do
      end do

      ! determinant of GC
      detJ = GC(1,1)*(GC(2,2)*GC(3,3)-GC(2,3)*GC(3,2)) - &
             GC(1,2)*(GC(2,1)*GC(3,3)-GC(2,3)*GC(3,1)) + &
             GC(1,3)*(GC(2,1)*GC(3,2)-GC(2,2)*GC(3,1))

      if (detJ <= 0.0d0) then
        write(*,*) "ERROR: zero or negative detJ", detJ
      end if

      detJ = sqrt(detJ)

      call mat_inverse_3x3(C, Ci, rtmp)
    
      call shell_mat_neohook1(mu, kappa, Ci, Gconij, trC, detJ, &
                              St, CC)

      ! update C33
      dC33 = -2.0d0*St(3,3)/CC(3,3,3,3)
	  if (dC33+C(3,3) <= 0.0d0) dC33 = -0.9d0*C(3,3)/Gconij(3,3)
      C(3,3) = C(3,3) + dC33

!!!      write(*,*) C(3,3), St(3,3), dC33
    end do
 
    ! Stress vector and material matrix
    S = (/ St(1,1), St(2,2), St(1,2) /)

    Dtemp(1,:) = (/ CC(1,1,1,1),CC(1,1,2,2),CC(1,1,1,2),CC(1,1,3,3) /)
    Dtemp(2,:) = (/ CC(2,2,1,1),CC(2,2,2,2),CC(2,2,1,2),CC(2,2,3,3) /)
    Dtemp(3,:) = (/ CC(1,2,1,1),CC(1,2,2,2),CC(1,2,1,2),CC(1,2,3,3) /)
    Dtemp(4,:) = (/ CC(3,3,1,1),CC(3,3,2,2),CC(3,3,1,2),CC(3,3,3,3) /)

    ! static condensation of S33=0
    do i = 1, 3
      do j = 1, 3
        D(i,j) = Dtemp(i,j)-Dtemp(i,4)*Dtemp(4,j)/Dtemp(4,4)
      end do
    end do

    ! updated thickness due to plane stress
    N_cu  = 0.5d0*thi*S*GW(iz) + N_cu
    dN_cu = 0.5d0*thi*matmul(D,(dE_cu+zeta*dK_cu))*GW(iz) + dN_cu
    M_cu  = 0.5d0*thi*S*zeta*GW(iz) + M_cu
    dM_cu = 0.5d0*thi*matmul(D,(dE_cu+zeta*dK_cu))*zeta*GW(iz) + dM_cu
  end do

end subroutine shell_mat_neoHook



!======================================================================
! energy function from Holzapfel/Yuri
! pot = 0.5*mu*(J^(-2/3)*trC-3) + 0.5*kappa*(0.5*(J^2-1)-lnJ)
!======================================================================
subroutine shell_mat_neohook1(mu, kappa, Ci, Gconij, trC, detJ, &
                              St, CC)
  implicit none      

  real(8), intent(in)  :: mu, kappa, Ci(3,3), Gconij(3,3), trC, detJ
  real(8), intent(out) :: St(3,3), CC(3,3,3,3)
             
  real(8) :: CixCi(3,3,3,3), CipCi(3,3,3,3), IxCi(3,3,3,3), CixI(3,3,3,3)

  integer :: i, j, k, l

  ! 4th order tensor products for CC
  CixCi = 0.0d0
  CipCi = 0.0d0
  IxCi  = 0.0d0
  CixI  = 0.0d0

  do i = 1, 3
    do j = 1, 3
      do k = 1, 3
        do l = 1, 3
          CixCi(i,j,k,l) = Ci(i,j)*Ci(k,l)
          CipCi(i,j,k,l) = (Ci(i,k)*Ci(j,l)+Ci(i,l)*Ci(j,k))/2.0d0
          IxCi(i,j,k,l)  = Gconij(i,j)*Ci(k,l)
          CixI(i,j,k,l)  = Ci(i,j)*Gconij(k,l)
        end do
      end do
    end do
  end do

  St = mu*detJ**(-2.0d0/3.0d0)*(Gconij-1.0d0/3.0d0*trC*Ci) + &
       0.5d0*kappa*(detJ**2-1.0d0)*Ci

  CC = (2.0d0/9.0d0*mu*detJ**(-2.0d0/3.0d0)*trC+kappa*detJ**2)*CixCi &
     + (2.0d0/3.0d0*mu*detJ**(-2.0d0/3.0d0)*trC-kappa*(detJ**2-1.0d0))*CipCi &
      - 2.0d0/3.0d0*mu*detJ**(-2.0d0/3.0d0)*(IxCi+CixI)

end subroutine shell_mat_neohook1



!======================================================================
! energy function from Abaqus
! pot = 0.5*mu*(J^(-2/3)*trC-3) + 0.5*kappa*(J-1)^2
!======================================================================
subroutine shell_mat_neohook2(mu, kappa, Ci, Gconij, trC, detJ, &
                              St, CC)
  implicit none      

  real(8), intent(in)  :: mu, kappa, Ci(3,3), Gconij(3,3), trC, detJ
  real(8), intent(out) :: St(3,3), CC(3,3,3,3)
             
  real(8) :: CixCi(3,3,3,3), CipCi(3,3,3,3), IxCi(3,3,3,3), CixI(3,3,3,3)

  integer :: i, j, k, l

  ! 4th order tensor products for CC
  CixCi = 0.0d0
  CipCi = 0.0d0
  IxCi  = 0.0d0
  CixI  = 0.0d0

  do i = 1, 3
    do j = 1, 3
      do k = 1, 3
        do l = 1, 3
          CixCi(i,j,k,l) = Ci(i,j)*Ci(k,l)
          CipCi(i,j,k,l) = (Ci(i,k)*Ci(j,l)+Ci(i,l)*Ci(j,k))/2.0d0
          IxCi(i,j,k,l)  = Gconij(i,j)*Ci(k,l)
          CixI(i,j,k,l)  = Ci(i,j)*Gconij(k,l)
        end do
      end do
    end do
  end do

  St = mu*detJ**(-2.0d0/3.0d0)*(Gconij-1.0d0/3.0d0*trC*Ci) + &
       kappa*(detJ**2-detJ)*Ci

  CC = (2.0d0/9.0d0*mu*detJ**(-2.0d0/3.0d0)*trC + kappa*(2.0d0*detJ**2-detJ))*CixCi &
     + (2.0d0/3.0d0*mu*detJ**(-2.0d0/3.0d0)*trC-2.0d0*kappa*(detJ**2-detJ))*CipCi  &
      - 2.0d0/3.0d0*mu*detJ**(-2.0d0/3.0d0)*(IxCi+CixI)
end subroutine shell_mat_neohook2
