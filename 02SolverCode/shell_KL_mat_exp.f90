!======================================================================
! 
!======================================================================
subroutine shell_mat_exp_SE(nshl, Gab_r, gab_c, Bv_r, bv_c, &
                         G0_r, Nor_r, dNor_r, g0, nor, dnor, &
                         mu, thi, ngauss, dE_cu, dK_cu, ddE_cu, ddK_cu,  &
                         SdEr, dSdEr, SddEr, MM_Flag, &
								  Coef_a, Coef_b, Coef_c, MtCoefs, m_contrav)
  implicit none      

  integer, intent(in) :: nshl, ngauss, MM_Flag

  real(8), intent(in) :: G0_r(3,2), Nor_r(3), dNor_r(3,2), g0(3,2), nor(3), dnor(3,2)
  
  real(8), intent(in) :: Gab_r(3), gab_c(3), Bv_r(3), bv_c(3), &
                         mu, thi, dE_cu(3,3*NSHL), dK_cu(3,3*NSHL), &
                         ddE_cu(3,3*NSHL,3*NSHL), ddK_cu(3,3*NSHL,3*NSHL)
						 
  real(8), intent(in) :: Coef_a, Coef_b, Coef_c, MtCoefs(100), m_contrav(ngauss, 2)
  
  real(8), intent(out) :: SdEr(3*NSHL), dSdEr(3*NSHL, 3*NSHL), SddEr(3*NSHL,3*NSHL)
  
  !real(8), intent(out) :: N_cu(3), M_cu(3), dN_cu(3,3*NSHL), &
  !                        dM_cu(3,3*NSHL), ABD(3,3,3)

  real(8) :: GGv_r(3), ggv_c(3), GG_r(2,2), gg_c(2,2), &
             detGG, sdetgg, C33, Gi_r(3,3), gi_c(3,3), &
             dpdC(3,3), dpdCC(3,3,3,3), St(2,2), CC(2,2,2,2), &
             Svec(3), Dmat(3,3), Coeffa, Coeffb, Coeffc, zeta, dpdC33LHS

  real(8) :: gp(ngauss), gw(ngauss)
  
  real(8) :: Gij_r(3,3), gij(3,3), G_r(3,3), g(3,3)
  real(8) :: gg_c_full(2,2), GG_r_full(2,2), detGG_full, sdetgg_full, C33_full, Gi_r_full(3,3)
            
  integer :: i, j, k, l, iz, ur, us
  
  real(8) :: dEr_cu(3,3*NSHL), ddEr_cu(3,3*NSHL, 3*NSHL)
  !real(8) :: SdEr(3*NSHL), dSdEr(3*NSHL, 3*NSHL), SddEr(3*NSHL,3*NSHL), &
  !           dEr_cu(3,3*NSHL), ddEr_cu(3,3*NSHL, 3*NSHL)
  
  Coeffa  = Coef_a
  Coeffb  = Coef_b
  Coeffc  = Coef_c

  GGv_r = 0.0d0
  ggv_c = 0.0d0
  GG_r = 0.0d0
  gg_c = 0.0d0
  Gi_r = 0.0d0
  gi_c = 0.0d0
  dpdC = 0.0d0
  dpdCC = 0.0d0
  St = 0.0d0
  CC = 0.0d0
  Svec = 0.0d0
  Dmat = 0.0d0
  
  SdEr  = 0.0d0
  dSdEr = 0.0d0
  SddEr = 0.0d0
  
  ! thickness integration loop
  call genGPandGW_shell(gp, gw, ngauss) 

  do iz = 1, ngauss
      
      zeta = 0.5d0*thi*GP(iz)
    
      G_r(1:3,1:2) = G0_r(1:3,1:2) + zeta*dNor_r(1:3,1:2)
      G_r(1:3,3)   = Nor_r(1:3)
      Gij_r        = matmul(transpose(G_r),G_r)
    
      g(1:3,1:2)   = g0  (1:3,1:2) + zeta*dnor(1:3,1:2)
      g(1:3,3)     = nor(1:3)
      gij          = matmul(transpose(g),g)
      
      GGv_r = (/ Gij_r(1,1), Gij_r(2,2), Gij_r(1,2) /)
      ggv_c = (/   gij(1,1),   gij(2,2),   gij(1,2) /)

      detGG  = GGv_r(1)*GGv_r(2) - GGv_r(3)**2
      sdetgg = ggv_c(1)*ggv_c(2) - ggv_c(3)**2
      
      GG_r(1,:) = (/ GGv_r(1), GGv_r(3) /)
      GG_r(2,:) = (/ GGv_r(3), GGv_r(2) /)

      gg_c(1,:) = (/ ggv_c(1), ggv_c(3) /)
      gg_c(2,:) = (/ ggv_c(3), ggv_c(2) /)

      C33 = detGG/sdetgg
    
      Gi_r(1,:) = 1.0d0/detGG*(/  GG_r(2,2), -GG_r(1,2), 0.0d0 /)
      Gi_r(2,:) = 1.0d0/detGG*(/ -GG_r(2,1),  GG_r(1,1), 0.0d0 /)
      Gi_r(3,:) =             (/      0.0d0,      0.0d0, 1.0d0 /)
    
      gi_c(1,:) = 1.0d0/sdetgg*(/  gg_c(2,2), -gg_c(1,2), 0.0d0     /)
      gi_c(2,:) = 1.0d0/sdetgg*(/ -gg_c(2,1),  gg_c(1,1), 0.0d0     /)
      gi_c(3,:) =              (/      0.0d0,      0.0d0, 1.0d0/C33 /)
      
      

!       GG_r_full(1:2,1:2) = Gij_r(1:2,1:2)
!       gg_c_full(1:2,1:2) = gij  (1:2,1:2)
!
!       detGG_full  = GG_r_full(1,1)*GG_r_full(2,2) - GG_r_full(1,2)**2
!       sdetgg_full = gg_c_full(1,1)*gg_c_full(2,2) - gg_c_full(1,2)**2
!       C33_full    = detGG_full/sdetgg_full
!
!       Gi_r_full(1,:) = 1.0d0/detGG_full*(/  GG_r_full(2,2), -GG_r_full(1,2), 0.0d0 /)
!       Gi_r_full(2,:) = 1.0d0/detGG_full*(/ -GG_r_full(2,1),  GG_r_full(1,1), 0.0d0 /)
!       Gi_r_full(3,:) =                  (/           0.0d0,           0.0d0, 1.0d0 /)
!
!       gi_c(1,:) = 1.0d0/sdetgg_full*(/  gg_c_full(2,2), -gg_c_full(1,2), 0.0d0     /)
!       gi_c(2,:) = 1.0d0/sdetgg_full*(/ -gg_c_full(2,1),  gg_c_full(1,1), 0.0d0     /)
!       gi_c(3,:) =              (/      0.0d0,      0.0d0, 1.0d0/C33_full /)
!
!       Gi_r = Gi_r_full
!       C33  = C33_full
!       gg_c = gg_c_full
      
      if (MM_Flag < 3) then
          ! get the dphi/dC and d^2phi/dC^2
          call shell_mat_exp_S_C(Gi_r, gg_c, C33, Coeffa, Coeffb, Coeffc, &
                             dpdC, dpdCC, MM_Flag)
    
      else if (MM_Flag == 3) then
  	 ! HGO
          call shell_mat_HGO_S_C(Gi_r, gg_c, C33, m_contrav(iz,:), MtCoefs(1:4), &
                                       dpdC, dpdCC)
                                     
      else if (MM_Flag == 4) then
  	 ! Lee-Sacks
       !call shell_mat_Lee_Sacks_S_C(Gi_r, gg_c, gg_c, gab_c, C33, &
       !                           m_contrav(iz,:), MtCoefs(1:5), dpdC, dpdCC)
        
       call shell_mat_Lee_Sacks_hybrid_S_C(Gi_r, gg_c, gg_c, gab_c, C33, &
                                    m_contrav(iz,:), MtCoefs(1:5), dpdC, dpdCC)
      end if 
      
      St = 2.0d0*dpdC(1:2,1:2) - 2.0d0*dpdC(3,3)*C33*gi_c(1:2,1:2)
    
      CC = 0.0d0
    
      do i = 1, 2
        do j = 1, 2
          do k = 1, 2
            do l = 1, 2
              CC(i,j,k,l) = 4.0d0*dpdCC(i,j,k,l) &
               +4.0d0*dpdCC(3,3,3,3)*C33**2*gi_c(i,j)*gi_c(k,l) &
               -4.0d0*dpdCC(i,j,3,3)*C33*gi_c(k,l) &
               -4.0d0*dpdCC(k,l,3,3)*C33*gi_c(i,j) &
               +2.0d0*dpdC(3,3)*C33*(2.0d0*gi_c(i,j)*gi_c(k,l)+gi_c(i,k)*gi_c(j,l)+gi_c(i,l)*gi_c(j,k))
            end do
          end do
        end do
      end do
      
      
      ! Stress vector and material matrix
      Svec = (/ St(1,1), St(2,2), St(1,2) /)
      
      !write(*,*) dpdC(1,1), dpdC(2,2), dpdC(1,2)

      Dmat(1,:) = (/ CC(1,1,1,1),CC(1,1,2,2),CC(1,1,1,2) /)
      Dmat(2,:) = (/ CC(2,2,1,1),CC(2,2,2,2),CC(2,2,1,2) /)
      Dmat(3,:) = (/ CC(1,2,1,1),CC(1,2,2,2),CC(1,2,1,2) /)

      ! stress resultants
      !N_cu  = 0.5d0*thi*Svec*GW(iz) + N_cu
      !dN_cu = 0.5d0*thi*matmul(Dmat,(dE_cu+zeta*dK_cu))*GW(iz) + dN_cu
      !M_cu  = 0.5d0*thi*Svec*zeta*GW(iz) + M_cu
      !dM_cu = 0.5d0*thi*matmul(Dmat,(dE_cu+zeta*dK_cu))*zeta*GW(iz) + dM_cu
      
      dEr_cu  = dE_cu +zeta*dK_cu
      ddEr_cu = ddE_cu+zeta*ddK_cu
      
      !dEr_cu(3,:)    = dEr_cu(3,:)    * 2.0d0
      !ddEr_cu(3,:,:) = ddEr_cu(3,:,:) * 2.0d0
      
      do ur = 1,3*NSHL
          SdEr(ur) = sum(Svec(:) * dEr_cu(:,ur)) * GW(iz) + SdEr(ur)
               
          do us = 1, ur
              dSdEr(ur,us) = sum( matmul(Dmat,dEr_cu(:,ur)) * dEr_cu(:,us) ) * GW(iz) + dSdEr(ur,us)
              SddEr(ur,us) = sum(Svec(:) * ddEr_cu(:,ur, us))                * GW(iz) + SddEr(ur,us)
          end do
      end do
  end do
  
  SdEr  = 0.5d0*thi * SdEr
  dSdEr = 0.5d0*thi * dSdEr
  SddEr = 0.5d0*thi * SddEr
  
  
end subroutine shell_mat_exp_SE


!======================================================================
! 
!======================================================================
subroutine shell_mat_exp_temp(nshl, Gab_r, gab_c, Bv_r, bv_c, &
                         mu, thi, ngauss, dE_cu, dK_cu,  &
                         N_cu, dN_cu, M_cu, dM_cu, MM_Flag, &
								  Coef_a, Coef_b, Coef_c)
  implicit none      

  integer, intent(in) :: nshl, ngauss, MM_Flag

  real(8), intent(in) :: Gab_r(3), gab_c(3), Bv_r(3), bv_c(3), &
                         mu, thi, dE_cu(3,3*NSHL), dK_cu(3,3*NSHL)
						 
  real(8), intent(in) :: Coef_a, Coef_b, Coef_c
  
  real(8), intent(out) :: N_cu(3), M_cu(3), dN_cu(3,3*NSHL), &
                          dM_cu(3,3*NSHL)

  real(8) :: GGv_r(3), ggv_c(3), GG_r(2,2), gg_c(2,2), &
             detGG, sdetgg, C33, Gi_r(3,3), gi_c(3,3), &
             dpdC(3,3), dpdCC(3,3,3,3), St(2,2), CC(2,2,2,2), &
             Svec(3), Dmat(3,3), Coeffa, Coeffb, Coeffc, zeta

  real(8) :: gp(ngauss), gw(ngauss)
            
  integer :: i, j, k, l, iz

!   Coeffa = mu
!   Coeffb = 1.0d5
!   Coeffc = 100.0d0

  Coeffa  = Coef_a
  Coeffb  = Coef_b
  Coeffc  = Coef_c

  N_cu  = 0.0d0
  M_cu  = 0.0d0
  dN_cu = 0.0d0
  dM_cu = 0.0d0

  GGv_r = 0.0d0
  ggv_c = 0.0d0
  GG_r = 0.0d0
  gg_c = 0.0d0
  Gi_r = 0.0d0
  gi_c = 0.0d0
  dpdC = 0.0d0
  dpdCC = 0.0d0
  St = 0.0d0
  CC = 0.0d0
  Svec = 0.0d0
  Dmat = 0.0d0

  ! thickness integration loop
  call genGPandGW_shell(gp, gw, ngauss) 

  do iz = 1, ngauss
  
    zeta = 0.5d0*thi*GP(iz)

    GGv_r = Gab_r - 2.0d0*zeta*Bv_r
    ggv_c = gab_c - 2.0d0*zeta*bv_c

    detGG  = GGv_r(1)*GGv_r(2) - GGv_r(3)**2
    sdetgg = ggv_c(1)*ggv_c(2) - ggv_c(3)**2

    GG_r(1,:) = (/ GGv_r(1), GGv_r(3) /)
    GG_r(2,:) = (/ GGv_r(3), GGv_r(2) /)

    gg_c(1,:) = (/ ggv_c(1), ggv_c(3) /)
    gg_c(2,:) = (/ ggv_c(3), ggv_c(2) /)

    C33 = detGG/sdetgg
    
    Gi_r(1,:) = 1.0d0/detGG*(/  GG_r(2,2), -GG_r(1,2), 0.0d0 /)
    Gi_r(2,:) = 1.0d0/detGG*(/ -GG_r(2,1),  GG_r(1,1), 0.0d0 /)
    Gi_r(3,:) =             (/      0.0d0,      0.0d0, 1.0d0 /)
    
    gi_c(1,:) = 1.0d0/sdetgg*(/  gg_c(2,2), -gg_c(1,2), 0.0d0     /)
    gi_c(2,:) = 1.0d0/sdetgg*(/ -gg_c(2,1),  gg_c(1,1), 0.0d0     /)
    gi_c(3,:) =              (/      0.0d0,      0.0d0, 1.0d0/C33 /)
    
    ! get the dphi/dC and d^2phi/dC^2
    call shell_mat_exp_S_C(Gi_r, gg_c, C33, Coeffa, Coeffb, Coeffc, &
                           dpdC, dpdCC, MM_Flag)
    
    St = 2.0d0*dpdC(1:2,1:2) - 2.0d0*dpdC(3,3)*C33*gi_c(1:2,1:2)
    
    CC = 0.0d0
    
    do i = 1, 2
      do j = 1, 2
        do k = 1, 2
          do l = 1, 2
            CC(i,j,k,l) = 4.0d0*dpdCC(i,j,k,l) &
              +4.0d0*dpdCC(3,3,3,3)*C33**2*gi_c(i,j)*gi_c(k,l) &
              -4.0d0*dpdCC(i,j,3,3)*C33*gi_c(k,l) &
              -4.0d0*dpdCC(k,l,3,3)*C33*gi_c(i,j) &
              +2.0d0*dpdC(3,3)*C33*(2.0d0*gi_c(i,j)*gi_c(k,l)+gi_c(i,k)*gi_c(j,l)+gi_c(i,l)*gi_c(j,k))
          end do
        end do
      end do
    end do
    
    ! Stress vector and material matrix
    Svec = (/ St(1,1), St(2,2), St(1,2) /)

    Dmat(1,:) = (/ CC(1,1,1,1),CC(1,1,2,2),CC(1,1,1,2) /)
    Dmat(2,:) = (/ CC(2,2,1,1),CC(2,2,2,2),CC(2,2,1,2) /)
    Dmat(3,:) = (/ CC(1,2,1,1),CC(1,2,2,2),CC(1,2,1,2) /)

    ! stress resultants
    N_cu  = 0.5d0*thi*Svec*GW(iz) + N_cu
    dN_cu = 0.5d0*thi*matmul(Dmat,(dE_cu+zeta*dK_cu))*GW(iz) + dN_cu
    M_cu  = 0.5d0*thi*Svec*zeta*GW(iz) + M_cu
    dM_cu = 0.5d0*thi*matmul(Dmat,(dE_cu+zeta*dK_cu))*zeta*GW(iz) + dM_cu
  end do

end subroutine shell_mat_exp_temp

!======================================================================
! 
!======================================================================
subroutine shell_mat_exp(nshl, Gab_r, gab_c, Bv_r, bv_c, &
                         G0_r, Nor_r, dNor_r, g0, nor, dnor, &
                         mu, thi, ngauss, dE_cu, dK_cu,  &
                         N_cu, dN_cu, M_cu, dM_cu, MM_Flag, &
								  Coef_a, Coef_b, Coef_c, MtCoefs, m_contrav)
  implicit none      

  integer, intent(in) :: nshl, ngauss, MM_Flag
  
  real(8), intent(in) :: G0_r(3,2), Nor_r(3), dNor_r(3,2), g0(3,2), nor(3), dnor(3,2)

  real(8), intent(in) :: Gab_r(3), gab_c(3), Bv_r(3), bv_c(3), &
                         mu, thi, dE_cu(3,3*NSHL), dK_cu(3,3*NSHL)
						 
  real(8), intent(in) :: Coef_a, Coef_b, Coef_c, MtCoefs(100), m_contrav(ngauss,2)
  
  real(8), intent(out) :: N_cu(3), M_cu(3), dN_cu(3,3*NSHL), &
                          dM_cu(3,3*NSHL)

  real(8) :: GGv_r(3), ggv_c(3), GG_r(2,2), gg_c(2,2), &
             detGG, sdetgg, C33, Gi_r(3,3), gi_c(3,3), &
             dpdC(3,3), dpdCC(3,3,3,3), St(2,2), CC(2,2,2,2), &
             Svec(3), Dmat(3,3), Coeffa, Coeffb, Coeffc, zeta

  real(8) :: gp(ngauss), gw(ngauss)
  
  real(8) :: Gij_r(3,3), gij(3,3), G_r(3,3), g(3,3), gg_c_full(2,2)
            
  integer :: i, j, k, l, iz

!   Coeffa = mu
!   Coeffb = 1.0d5
!   Coeffc = 100.0d0

  Coeffa  = Coef_a
  Coeffb  = Coef_b
  Coeffc  = Coef_c

  N_cu  = 0.0d0
  M_cu  = 0.0d0
  dN_cu = 0.0d0
  dM_cu = 0.0d0

  GGv_r = 0.0d0
  ggv_c = 0.0d0
  GG_r = 0.0d0
  gg_c = 0.0d0
  Gi_r = 0.0d0
  gi_c = 0.0d0
  dpdC = 0.0d0
  dpdCC = 0.0d0
  St = 0.0d0
  CC = 0.0d0
  Svec = 0.0d0
  Dmat = 0.0d0

  ! thickness integration loop
  call genGPandGW_shell(gp, gw, ngauss) 

  do iz = 1, ngauss
  
    zeta = 0.5d0*thi*GP(iz)

    G_r(1:3,1:2) = G0_r(1:3,1:2) + zeta*dNor_r(1:3,1:2)
    G_r(1:3,3)   = Nor_r(1:3)
    Gij_r        = matmul(transpose(G_r),G_r)
    
    g(1:3,1:2)   = g0  (1:3,1:2) + zeta*dnor(1:3,1:2)
    g(1:3,3)     = nor(1:3)
    gij          = matmul(transpose(g),g)

    GGv_r = Gab_r - 2.0d0*zeta*Bv_r
    ggv_c = gab_c - 2.0d0*zeta*bv_c
    
    gg_c_full(1:2,1:2) = gij(1:2,1:2)
    
    !GGv_r = (/ Gij_r(1,1), Gij_r(2,2), Gij_r(1,2) /)
    !ggv_c = (/   gij(1,1),   gij(2,2),   gij(1,2) /)

    detGG  = GGv_r(1)*GGv_r(2) - GGv_r(3)**2
    sdetgg = ggv_c(1)*ggv_c(2) - ggv_c(3)**2

    GG_r(1,:) = (/ GGv_r(1), GGv_r(3) /)
    GG_r(2,:) = (/ GGv_r(3), GGv_r(2) /)

    gg_c(1,:) = (/ ggv_c(1), ggv_c(3) /)
    gg_c(2,:) = (/ ggv_c(3), ggv_c(2) /)

    C33 = detGG/sdetgg
    
    Gi_r(1,:) = 1.0d0/detGG*(/  GG_r(2,2), -GG_r(1,2), 0.0d0 /)
    Gi_r(2,:) = 1.0d0/detGG*(/ -GG_r(2,1),  GG_r(1,1), 0.0d0 /)
    Gi_r(3,:) =             (/      0.0d0,      0.0d0, 1.0d0 /)
    
    gi_c(1,:) = 1.0d0/sdetgg*(/  gg_c(2,2), -gg_c(1,2), 0.0d0     /)
    gi_c(2,:) = 1.0d0/sdetgg*(/ -gg_c(2,1),  gg_c(1,1), 0.0d0     /)
    gi_c(3,:) =              (/      0.0d0,      0.0d0, 1.0d0/C33 /)
    
    if (MM_Flag < 3) then
        ! get the dphi/dC and d^2phi/dC^2
        call shell_mat_exp_S_C(Gi_r, gg_c, C33, Coeffa, Coeffb, Coeffc, &
                           dpdC, dpdCC, MM_Flag)
    
    else if (MM_Flag == 3) then
	 ! HGO
        call shell_mat_HGO_S_C(Gi_r, gg_c, C33, m_contrav(iz,:), MtCoefs(1:4), &
                                     dpdC, dpdCC)
                                     
    else if (MM_Flag == 4) then
	 ! Lee-Sacks
     call shell_mat_Lee_Sacks_S_C(Gi_r, gg_c, gg_c_full, gab_c, C33, m_contrav(iz,:), MtCoefs(1:5), &
                                  dpdC, dpdCC)
     !call shell_mat_Lee_Sacks_S_C(Gi_r, gg_c, gg_c_full, gab_c, C33, m_contrav(iz,:), MtCoefs(1:5), &
     !                             dpdC, dpdCC)
    end if 
    
    St = 2.0d0*dpdC(1:2,1:2) - 2.0d0*dpdC(3,3)*C33*gi_c(1:2,1:2)
    !write(*,*) Svec
    CC = 0.0d0
    
    do i = 1, 2
      do j = 1, 2
        do k = 1, 2
          do l = 1, 2
            CC(i,j,k,l) = 4.0d0*dpdCC(i,j,k,l) &
              +4.0d0*dpdCC(3,3,3,3)*C33**2*gi_c(i,j)*gi_c(k,l) &
              -4.0d0*dpdCC(i,j,3,3)*C33*gi_c(k,l) &
              -4.0d0*dpdCC(k,l,3,3)*C33*gi_c(i,j) &
              +2.0d0*dpdC(3,3)*C33*(2.0d0*gi_c(i,j)*gi_c(k,l)+gi_c(i,k)*gi_c(j,l)+gi_c(i,l)*gi_c(j,k))
          end do
        end do
      end do
    end do
    
    ! Stress vector and material matrix
    Svec = (/ St(1,1), St(2,2), St(1,2) /)

    Dmat(1,:) = (/ CC(1,1,1,1),CC(1,1,2,2),CC(1,1,1,2) /)
    Dmat(2,:) = (/ CC(2,2,1,1),CC(2,2,2,2),CC(2,2,1,2) /)
    Dmat(3,:) = (/ CC(1,2,1,1),CC(1,2,2,2),CC(1,2,1,2) /)

    ! stress resultants
    N_cu  = 0.5d0*thi*Svec*GW(iz) + N_cu
    dN_cu = 0.5d0*thi*matmul(Dmat,(dE_cu+zeta*dK_cu))*GW(iz) + dN_cu
    M_cu  = 0.5d0*thi*Svec*zeta*GW(iz) + M_cu
    dM_cu = 0.5d0*thi*matmul(Dmat,(dE_cu+zeta*dK_cu))*zeta*GW(iz) + dM_cu
  end do

end subroutine shell_mat_exp

!======================================================================
! Performing base vector transformation when integration thru thinkness
! so that the integration is done in the same bases.
!======================================================================
subroutine shell_mat_exp_T(nshl, G_r, dG3_r, Gab_r, gab_c, Bv_r, bv_c, &
                         mu, thi, ngauss, dE_cu, dK_cu,  &
                         N_cu, dN_cu, M_cu, dM_cu, MM_Flag, &
								  Coeffa, Coeffb, Coeffc)
  implicit none      

  integer, intent(in) :: nshl, ngauss, MM_Flag

  real(8), intent(out) :: G_r(3,2), Gab_r(3), gab_c(3), Bv_r(3), bv_c(3), &
                         mu, thi, dE_cu(3,3*NSHL), dK_cu(3,3*NSHL), &
                         dG3_r(3,2)
						 
  real(8), intent(in) :: Coeffa, Coeffb, Coeffc
  
  real(8), intent(out) :: N_cu(3), M_cu(3), dN_cu(3,3*NSHL), &
                          dM_cu(3,3*NSHL)

  real(8) :: GGv_r(3), ggv_c(3), GG_r(2,2), gg_c(2,2), &
             detGG, sdetgg, C33, Gi_r(3,3), gi_c(3,3), &
             dpdC(3,3), dpdCC(3,3,3,3), St(2,2), CC(2,2,2,2), &
             Svec(3), Dmat(3,3), zeta

  real(8) :: gp(ngauss), gw(ngauss)
  
  real(8) :: GGv_r_con(3), GT_r_con(3,2), invdetGab, GT_r(3,2)
  
  real(8) :: temp(3)
            
  integer :: i, j, k, l, iz
  real(8) :: MN_Coefs(4), m(2), m_c(3), G_r_con(3,2), C_ca(3,3)
  
  ! fiber direction in cart.
  m_c = G_r(:,2)
  
  m_c = m_c/sqrt(sum(m_c*m_c))
  call cov2contra(G_r_con, G_r)
  ! fiber direction in contrav. 
  m(1) = sum(m_c*G_r_con(:,1))
  m(2) = sum(m_c*G_r_con(:,2))
  
  MN_Coefs = (/0.0d0, 5.0d4, 10.0d0, 20.0d0/)

  N_cu  = 0.0d0
  M_cu  = 0.0d0
  dN_cu = 0.0d0
  dM_cu = 0.0d0

  GGv_r = 0.0d0
  ggv_c = 0.0d0
  GG_r = 0.0d0
  gg_c = 0.0d0
  Gi_r = 0.0d0
  gi_c = 0.0d0
  dpdC = 0.0d0
  dpdCC = 0.0d0
  St = 0.0d0
  CC = 0.0d0
  Svec = 0.0d0
  Dmat = 0.0d0

  ! thickness integration loop
  call genGPandGW_shell(gp, gw, ngauss) 

  do iz = 1, ngauss
    
    !GT_r_con = G_r_con/(1-)
  
    zeta = 0.5d0*thi*GP(iz)
    
    ! covariant base vectors with dG3_r
    GT_r(1:3,1:2) = G_r(1:3,1:2) + zeta*dG3_r(1:3,1:2)  
    
    !temp(1) = sum(dG3_r(:,1)*dG3_r(:,1))
    !temp(2) = sum(dG3_r(:,2)*dG3_r(:,2))
    !temp(3) = sum(dG3_r(:,1)*dG3_r(:,2))

    ! metric coef. 
    GGv_r = Gab_r - 2.0d0*zeta*Bv_r !+ zeta**2 * temp
    ggv_c = gab_c - 2.0d0*zeta*bv_c 
    
    !write(*,*) iz
    !call eval_tensor(C_ca, GGv_r, GT_r)
    !write(*,*) C_ca(1,:)
    !write(*,*) C_ca(2,:)
    !write(*,*) C_ca(3,:)

    !call cov2contra(GT_r_con, GT_r)
    !---- convert g -> a ----
    ! g^i : GT_r_con
    ! a_i : G_r
    !call Trans_3x3_tensor_2D (GGv_r, G_r, GGv_r, GT_r_con)
    !call Trans_3x3_tensor_2D (ggv_c, G_r, ggv_c, GT_r_con)
    !---- convert g -> a ---- <end>
    
    !call eval_tensor(C_ca, GGv_r, G_r)
    !write(*,*) C_ca(1,:)
    !write(*,*) C_ca(2,:)
    !write(*,*) C_ca(3,:)
    !stop
    !call eval_tensor(C_ca, ggv_c, G_r)

    !temp(1) = sum(m_c*C_ca(:,1))
    !temp(2) = sum(m_c*C_ca(:,2))
    !temp(3) = sum(m_c*C_ca(:,3))
    !write(*,*) sum(temp*m_c)

    
    detGG  = GGv_r(1)*GGv_r(2) - GGv_r(3)**2
    sdetgg = ggv_c(1)*ggv_c(2) - ggv_c(3)**2

    GG_r(1,:) = (/ GGv_r(1), GGv_r(3) /)
    GG_r(2,:) = (/ GGv_r(3), GGv_r(2) /)

    gg_c(1,:) = (/ ggv_c(1), ggv_c(3) /)
    gg_c(2,:) = (/ ggv_c(3), ggv_c(2) /)

    C33 = detGG/sdetgg
    
    Gi_r(1,:) = 1.0d0/detGG*(/  GG_r(2,2), -GG_r(1,2), 0.0d0 /)
    Gi_r(2,:) = 1.0d0/detGG*(/ -GG_r(2,1),  GG_r(1,1), 0.0d0 /)
    Gi_r(3,:) =             (/      0.0d0,      0.0d0, 1.0d0 /)
    
    gi_c(1,:) = 1.0d0/sdetgg*(/  gg_c(2,2), -gg_c(1,2), 0.0d0     /)
    gi_c(2,:) = 1.0d0/sdetgg*(/ -gg_c(2,1),  gg_c(1,1), 0.0d0     /)
    gi_c(3,:) =              (/      0.0d0,      0.0d0, 1.0d0/C33 /)
    
    ! get the dphi/dC and d^2phi/dC^2
    call shell_mat_exp_S_C(Gi_r, gg_c, C33, Coeffa, Coeffb, Coeffc, &
                           dpdC, dpdCC, MM_Flag)
    
    
    
    
    !call shell_mat_MayNewman_S_C(Gi_r, gg_c, C33, m, MN_Coefs, &
    !                             dpdC, dpdCC)
    
    St = 2.0d0*dpdC(1:2,1:2) - 2.0d0*dpdC(3,3)*C33*gi_c(1:2,1:2)
    
    CC = 0.0d0
    
    do i = 1, 2
      do j = 1, 2
        do k = 1, 2
          do l = 1, 2
            CC(i,j,k,l) = 4.0d0*dpdCC(i,j,k,l) &
              +4.0d0*dpdCC(3,3,3,3)*C33**2*gi_c(i,j)*gi_c(k,l) &
              -4.0d0*dpdCC(i,j,3,3)*C33*gi_c(k,l) &
              -4.0d0*dpdCC(k,l,3,3)*C33*gi_c(i,j) &
              +2.0d0*dpdC(3,3)*C33*(2.0d0*gi_c(i,j)*gi_c(k,l)+gi_c(i,k)*gi_c(j,l)+gi_c(i,l)*gi_c(j,k))
          end do
        end do
      end do
    end do
    
    ! Stress vector and material matrix
    Svec = (/ St(1,1), St(2,2), St(1,2) /)
    
    Dmat(1,:) = (/ CC(1,1,1,1),CC(1,1,2,2),CC(1,1,1,2) /)
    Dmat(2,:) = (/ CC(2,2,1,1),CC(2,2,2,2),CC(2,2,1,2) /)
    Dmat(3,:) = (/ CC(1,2,1,1),CC(1,2,2,2),CC(1,2,1,2) /)

    ! stress resultants
    N_cu  = 0.5d0*thi*Svec*GW(iz) + N_cu
    dN_cu = 0.5d0*thi*matmul(Dmat,(dE_cu+zeta*dK_cu))*GW(iz) + dN_cu
    M_cu  = 0.5d0*thi*Svec*zeta*GW(iz) + M_cu
    dM_cu = 0.5d0*thi*matmul(Dmat,(dE_cu+zeta*dK_cu))*zeta*GW(iz) + dM_cu
  end do
  
  !stop

end subroutine shell_mat_exp_T



!======================================================================
! subroutine to calculate the S and C of Fung material models
!======================================================================
subroutine shell_mat_exp_S_C(Gi_r, gg_c, C33, Coeffa, Coeffb, Coeffc, &
                             dpdC, dpdCC, MM_Flag)
  implicit none      

  integer, intent(in)  :: MM_Flag
  real(8), intent(in)  :: Gi_r(3,3), gg_c(2,2), C33, Coeffa, Coeffb, Coeffc
  real(8), intent(out) :: dpdC(3,3), dpdCC(3,3,3,3)
  real(8) :: trC, trCm3
  integer :: i, j, k, l
     
  dpdC = 0.0d0; dpdCC = 0.0d0
             
  trC   = Gi_r(1,1)*gg_c(1,1)+Gi_r(2,2)*gg_c(2,2)+2.0d0*Gi_r(1,2)*gg_c(1,2)+C33
  trCm3 = trC - 3.0d0
    
  if (MM_Flag == 1) then 
    ! isotropic Fung (exp)
    dpdC  = 0.5d0*Coeffa*Gi_r + 0.5d0*Coeffb*Coeffc*exp(Coeffc*trCm3)*Gi_r

    do i = 1, 3
      do j = 1, 3
        do k = 1, 3
          do l = 1, 3
            dpdCC(i,j,k,l) = 0.5d0*Coeffb*Coeffc**2*exp(Coeffc*trCm3)*Gi_r(i,j)*Gi_r(k,l)
          end do
        end do
      end do
    end do
    
  else if (MM_Flag == 2) then 
    ! isotropic Fung (exp^2)
    dpdC  = 0.5d0*Coeffa*Gi_r + Coeffb*Coeffc*trCm3*exp(Coeffc*trCm3**2)*Gi_r

    do i = 1, 3
      do j = 1, 3
        do k = 1, 3
          do l = 1, 3
            dpdCC(i,j,k,l) = Coeffb*Coeffc*exp(Coeffc*trCm3**2)*(1.0d0+2.0d0*Coeffc*trCm3**2) &
                             *Gi_r(i,j)*Gi_r(k,l)
          end do
        end do
      end do
    end do
  
  else
    write(*,*) "ERROR: Undefined MM_Flag in 'shell_mat_exp_S_C'"
    stop
  end if  
end subroutine shell_mat_exp_S_C

subroutine shell_mat_HGO_S_C(Gi_r, gg_c, C33, m, Coef, &
                             dpdC, dpdCC)
  implicit none  
  
  real(8), intent(in)  :: Gi_r(3,3), gg_c(2,2), C33, m(2)
  real(8), intent(in)  :: Coef(4)
  real(8), intent(out) :: dpdC(3,3), dpdCC(3,3,3,3)
  real(8) :: I1, I4, gg_r(3,3), Q, mxm(3,3)
  real(8) :: tempI41, tempI42, tempI43, expQ, trCm3 
  real(8) :: tempII, tempIMM, tempMMI, tempMMMM
  integer :: i, j, k, l
  
  dpdC = 0.0d0; dpdCC = 0.0d0
  
  I1 = Gi_r(1,1)*gg_c(1,1)+Gi_r(2,2)*gg_c(2,2)+2.0d0*Gi_r(1,2)*gg_c(1,2)+C33
  I4 = m(1)*m(1)*gg_c(1,1) + m(2)*m(2)*gg_c(2,2) + 2.0d0*m(1)*m(2)*gg_c(1,2)
  

  Q = Coef(3)*(I1-3.0d0)**2 + Coef(4)*(I4-1.0d0)**2

  expQ = exp(Q)
  
  mxm = 0.0d0
  mxm(1,1) = m(1)*m(1)
  mxm(1,2) = m(1)*m(2)
  mxm(2,1) = m(2)*m(1)
  mxm(2,2) = m(2)*m(2)
  
  dpdC = 0.5d0*Coef(1)*Gi_r + Coef(2)*expQ*(Coef(3)*(I1-3.0d0)*Gi_r &
       + Coef(4)*(I4-1.0d0) * mxm)
  
  tempII   = 2.0d0*Coef(2)*expQ*(Coef(3)*(I1-3.0d0))**2 + Coef(2)*Coef(3)*expQ
  tempIMM  = 2.0d0*Coef(2)*Coef(3)*Coef(4)*(I1-3.0d0)*(I4-1.0d0)*expQ
  tempMMI  = tempIMM
  tempMMMM = Coef(2)*Coef(4)*(1.0d0+2.0d0*Coef(4)*(I4-1.0d0)**2)*expQ

  
!   dpdC = Coef(1)*Gi_r + 2.0d0*Coef(2)*expQ*(Coef(3)*(I1-3.0d0)*Gi_r &
!        + Coef(4)*(I4-1.0d0) * mxm)
!
!   tempII   = 4.0d0*Coef(2)*expQ*(Coef(3)*(I1-3.0d0))**2 + 2.0d0*Coef(2)*Coef(3)*expQ
!   tempIMM  = 4.0d0*Coef(2)*Coef(3)*Coef(4)*(I1-3.0d0)*(I4-1.0d0)*expQ
!   tempMMI  = tempIMM
!   tempMMMM = 2.0d0*Coef(2)*Coef(4)*(1.0d0+2.0d0*Coef(4)*(I4-1.0d0)**2)*expQ

  do i = 1, 3
    do j = 1, 3
      do k = 1, 3
        do l = 1, 3
		  dpdCC(i,j,k,l) = tempII  *Gi_r(i,j)*Gi_r(k,l) &
		                 + tempIMM *Gi_r(i,j)*mxm (k,l) &
						 + tempMMI *mxm (i,j)*Gi_r(k,l) &
						 + tempMMMM*mxm (i,j)*mxm (k,l)
        end do
      end do
    end do
  end do
  
end subroutine shell_mat_HGO_S_C

!======================================================================
! subroutine to calculate the S and C of May-Newman material models
! K. May-Newman, C. Lam, and F. C. P. Yin. A hyperelastic constitutive 
! law for aortic valve tissue. Journal of Biomechanical Engineering, 
! 131(8):081009–7, 2009. HGO
!======================================================================
subroutine shell_mat_MayNewman_S_C(Gi_r, gg_c, C33, m, MN_Coefs, &
                             dpdC, dpdCC)
  implicit none  
  
  real(8), intent(in)  :: Gi_r(3,3), gg_c(2,2), C33, m(2)
  real(8), intent(in)  :: MN_Coefs(4)
  real(8), intent(out) :: dpdC(3,3), dpdCC(3,3,3,3)
  real(8) :: I1, I4, gg_r(3,3), Q, mxm(3,3)
  real(8) :: tempI41, tempI42, tempI43, sqrtI4, expQ, trCm3 
  real(8) :: tempII, tempIMM, tempMMI, tempMMMM
  integer :: i, j, k, l
  
  dpdC = 0.0d0; dpdCC = 0.0d0
  
  I1 = Gi_r(1,1)*gg_c(1,1)+Gi_r(2,2)*gg_c(2,2)+2.0d0*Gi_r(1,2)*gg_c(1,2)+C33
  !I1 = I1 - 3.0d0
  
!   gg_r = 0.0d0
!   gg_r(1,1) = Gi_r(1,1)*gg_c(1,1) + Gi_r(2,1)*gg_c(2,1)
!   gg_r(1,2) = Gi_r(1,1)*gg_c(1,2) + Gi_r(2,1)*gg_c(2,2)
!   gg_r(2,1) = Gi_r(2,1)*gg_c(1,1) + Gi_r(2,2)*gg_c(2,1)
!   gg_r(2,2) = Gi_r(1,2)*gg_c(1,2) + Gi_r(2,2)*gg_c(2,2)
 !  gg_r(1,1) = Gi_r(1,1)*gg_c(1,1) + Gi_r(1,2)*gg_c(2,1)
!   gg_r(1,2) = Gi_r(1,1)*gg_c(1,2) + Gi_r(1,2)*gg_c(2,2)
!   gg_r(2,1) = Gi_r(2,1)*gg_c(1,1) + Gi_r(2,2)*gg_c(2,1)
!   gg_r(2,2) = Gi_r(2,1)*gg_c(1,2) + Gi_r(2,2)*gg_c(2,2)
  !gg_r(3,3) = C33
  
  !I1 = gg_r(1,1) + gg_r(2,2) + gg_r(3,3)
  
  !I4 = m(1)*(gg_r(1,1)*m(1) + gg_r(2,1)*m(2)) + m(2)*(gg_r(1,2)*m(1) + gg_r(2,2)*m(2))
  I4 = m(1)*m(1)*gg_c(1,1) + m(2)*m(2)*gg_c(2,2) + 2.0d0*m(1)*m(2)*gg_c(1,2)
  
  !sqrtI4 = sqrt(I4)

  !Q = MN_Coefs(3)*(I1-3.0d0)**2 + MN_Coefs(4)*(sqrtI4-1.0d0)**4
  
!   if (Q > 10) then
! 	  write(*,*) Q, I1-3.0d0
!   end if
  
  Q = MN_Coefs(3)*(I1-3.0d0)**2 + MN_Coefs(4)*(I4-1.0d0)**2
  
  expQ = exp(Q)
  
  mxm = 0.0d0
  mxm(1,1) = m(1)*m(1)
  mxm(1,2) = m(1)*m(2)
  mxm(2,1) = m(2)*m(1)
  mxm(2,2) = m(2)*m(2)
  
  !dpdC = 0.5d0*MN_Coefs(1)*Gi_r + 2.0d0*MN_Coefs(2)*expQ*(MN_Coefs(3)*(I1-3.0d0)*Gi_r &
  !     + MN_Coefs(4)/sqrtI4*(sqrtI4-1.0d0)**3 * mxm)
  
  !dpdC = 5.0d6*Gi_r + 2.0d0*c0*expQ*(c1*(I1-3.0d0)*Gi_r + c2/sqrtI4*(sqrtI4-1.0d0)**3 * mxm)
  
  !dpdC =  c1*c2*trCm3*exp(c2*trCm3**2)*Gi_r
  !dpdC  = 0.5d0*Coeffa*Gi_r + 0.5d0*Coeffb*Coeffc*exp(Coeffc*trCm3)*Gi_r
  
  !tempI41 = (sqrtI4-1.0d0)**3/sqrtI4
  !tempI42 = (sqrtI4-1.0d0)**3/sqrtI4/I4
  !tempI43 = (sqrtI4-1.0d0)**2/I4
  
  !tempII   = 4.0d0*MN_Coefs(2)*expQ*(MN_Coefs(3)*(I1-3.0d0))**2 + 2.0d0*MN_Coefs(2)*MN_Coefs(3)*expQ
  !tempII   = c1*c2*exp(c2*trCm3**2)*(1.0d0+2.0d0*c2*trCm3**2)
  !tempIMM  = 4.0d0*MN_Coefs(2)*MN_Coefs(3)*MN_Coefs(4)*expQ*(I1-3.0d0)*tempI41
  !tempMMI  = tempIMM
  !tempMMMM = 4.0d0*MN_Coefs(2)*expQ*(MN_Coefs(4)*tempI41)**2 + MN_Coefs(2)*MN_Coefs(4)*expQ*(-tempI42 + 3.0d0*tempI43)
  
  
  dpdC = 0.5d0*MN_Coefs(1)*Gi_r + 2.0d0*MN_Coefs(2)*expQ*(MN_Coefs(3)*(I1-3.0d0)*Gi_r &
       + MN_Coefs(4)*(I4-1.0d0) * mxm)
  
  tempII   = 4.0d0*MN_Coefs(2)*expQ*(MN_Coefs(3)*(I1-3.0d0))**2 + 2.0d0*MN_Coefs(2)*MN_Coefs(3)*expQ
  tempIMM  = 4.0d0*MN_Coefs(2)*MN_Coefs(3)*MN_Coefs(4)*(I1-3.0d0)*(I4-1.0d0)*expQ
  tempMMI  = tempIMM
  tempMMMM = 2.0d0*MN_Coefs(2)*MN_Coefs(4)*(1.0d0+2.0d0*MN_Coefs(4)*(I4-1.0d0)**2)*expQ
  

  do i = 1, 3
    do j = 1, 3
      do k = 1, 3
        do l = 1, 3
		  dpdCC(i,j,k,l) = tempII  *Gi_r(i,j)*Gi_r(k,l) &
		                 + tempIMM *Gi_r(i,j)*mxm (k,l) &
						 + tempMMI *mxm (i,j)*Gi_r(k,l) &
						 + tempMMMM*mxm (i,j)*mxm (k,l)
        end do
      end do
    end do
  end do
  
end subroutine shell_mat_MayNewman_S_C


subroutine shell_mat_Lee_Sacks_S_C(Gi_r, gg_c, gg_c_full, gab_c, C33, m, Cf, &
                             dpdC, dpdCC)
  implicit none  
  
  real(8), intent(in)  :: Gi_r(3,3), gg_c(2,2), C33, m(2), gg_c_full(2,2), gab_c(3)
  real(8), intent(in)  :: Cf(5)
  real(8), intent(out) :: dpdC(3,3), dpdCC(3,3,3,3)
  integer :: i, j, k, l
  real(8) :: I1, I4, gg_r(3,3), Q, mxm(3,3)
  real(8) :: I1m3, I4m1, I1m3sq, I4m1sq, dt, idt, e1, e4
  real(8) :: dpdI1sq, dpdI4sq
  
  
  dpdC = 0.0d0; dpdCC = 0.0d0
  
  I1 = Gi_r(1,1)*gg_c(1,1)+Gi_r(2,2)*gg_c(2,2)+2.0d0*Gi_r(1,2)*gg_c(1,2)+C33
  I4 = m(1)*m(1)*gg_c(1,1) + m(2)*m(2)*gg_c(2,2) + 2.0d0*m(1)*m(2)*gg_c(1,2)
  !I4 = m(1)*m(1)*gg_c_full(1,1) + m(2)*m(2)*gg_c_full(2,2) + 2.0d0*m(1)*m(2)*gg_c_full(1,2)
  !I4 = m(1)*m(1)*gab_c(1) + m(2)*m(2)*gab_c(2) + 2.0d0*m(1)*m(2)*gab_c(3)
  
  I1m3 = I1 - 3.0d0
  I4m1 = I4 - 1.0d0
  
  !I1m3 = 0.0d0
  !I4m1 = 0.0d0
  
  !write(*,*) I4, I4m1
  
  I1m3sq = I1m3*I1m3
  I4m1sq = I4m1*I4m1
  
  e1 = exp(Cf(3)*I1m3sq)
  e4 = exp(Cf(4)*I4m1sq)
  
  !write(*,*) I1, I1m3, e1
  
  mxm = 0.0d0
  mxm(1,1) = m(1)*m(1)
  mxm(1,2) = m(1)*m(2)
  mxm(2,1) = m(2)*m(1)
  mxm(2,2) = m(2)*m(2)
  
  ! dt must be 0 < dt < 1
  dt = Cf(5)
  idt = 1.0d0 - dt
  
  dpdC = 0.5d0*Cf(1)*Gi_r + Cf(2)*( dt*Cf(3)*I1m3*e1*Gi_r + idt*Cf(4)*I4m1*e4*mxm )
  
  dpdI1sq = (1.0d0 + 2.0d0*Cf(3)*I1m3sq) * Cf(2)* dt*Cf(3)*e1
  dpdI4sq = (1.0d0 + 2.0d0*Cf(4)*I4m1sq) * Cf(2)*idt*Cf(4)*e4
  
  do i = 1, 3
    do j = 1, 3
      do k = 1, 3
        do l = 1, 3
		  dpdCC(i,j,k,l) = dpdI1sq *Gi_r(i,j)*Gi_r(k,l) &
						 + dpdI4sq *mxm (i,j)*mxm (k,l)
        end do
      end do
    end do
  end do
  
  !if (idt*e4 > dt*e1) then
  !    write(*,*) I1m3, I4m1, dt*e1, idt*e4
  !end if
end subroutine shell_mat_Lee_Sacks_S_C


subroutine shell_mat_Lee_Sacks_hybrid_S_C(Gi_r, gg_c, gg_c_full, gab_c, C33, m, Cf, &
                             dpdC, dpdCC)
  implicit none  
  
  real(8), intent(in)  :: Gi_r(3,3), gg_c(2,2), C33, m(2), gg_c_full(2,2), gab_c(3)
  real(8), intent(in)  :: Cf(5)
  real(8), intent(out) :: dpdC(3,3), dpdCC(3,3,3,3)
  integer :: i, j, k, l
  real(8) :: I1, I4, gg_r(3,3), Q, mxm(3,3)
  real(8) :: I1m3, I4m1, I1m3sq, I4m1sq, dt, idt, e1, e4
  real(8) :: dpdI1sq, dpdI4sq
  
  real(8) :: E_cf, I1_cf, I4_cf
  real(8) :: I1m3_cf, I4m1_cf, I1m3sq_cf, I4m1sq_cf, e1_cf, e4_cf
  
  real(8) :: dA1dI1, dA4dI4, dA1dI1sq, dA4dI4sq
  
  E_cf  = 0.3d0
  I1_cf = 2.0d0*(2.0d0*E_cf) + 3.0d0
  I4_cf = 2.0d0*(2.0d0*E_cf) + 1.0d0
  
  I1m3_cf = I1_cf - 3.0d0
  I4m1_cf = I4_cf - 1.0d0
  
  I1m3sq_cf = I1m3_cf*I1m3_cf
  I4m1sq_cf = I4m1_cf*I4m1_cf
  
  e1_cf = exp(Cf(3)*I1m3sq_cf)
  e4_cf = exp(Cf(4)*I4m1sq_cf)
  
  
  I1 = Gi_r(1,1)*gg_c(1,1)+Gi_r(2,2)*gg_c(2,2)+2.0d0*Gi_r(1,2)*gg_c(1,2)+C33
  I4 = m(1)*m(1)*gg_c(1,1) + m(2)*m(2)*gg_c(2,2) + 2.0d0*m(1)*m(2)*gg_c(1,2)
  
  I1m3 = I1 - 3.0d0
  I4m1 = I4 - 1.0d0
  
  I1m3sq = I1m3*I1m3
  I4m1sq = I4m1*I4m1
  
  e1 = exp(Cf(3)*I1m3sq)
  e4 = exp(Cf(4)*I4m1sq)
  
  if (I1m3 < I1m3_cf) then ! exp
     dA1dI1   = 2.0d0*Cf(3)*I1m3*e1
     dA1dI1sq = 2.0d0*Cf(3)*(1.0d0 + 2.0d0*Cf(3)*I1m3sq) * e1
  else ! linear 
     dA1dI1   = 2.0d0*Cf(3)*I1m3_cf*e1_cf
     dA1dI1sq = 0.0d0
  end if
  
  if (I4m1 < I4m1_cf) then ! exp
      dA4dI4   = 2.0d0*Cf(4)*I4m1*e4
      dA4dI4sq = 2.0d0*Cf(4)*(1.0d0 + 2.0d0*Cf(4)*I4m1sq) * e4
  else ! linear 
      dA4dI4   = 2.0d0*Cf(4)*I4m1_cf*e4_cf
      dA4dI4sq = 0.0d0
  end if
  
  mxm = 0.0d0
  mxm(1,1) = m(1)*m(1)
  mxm(1,2) = m(1)*m(2)
  mxm(2,1) = m(2)*m(1)
  mxm(2,2) = m(2)*m(2)
  
  ! dt must be 0 < dt < 1
  dt = Cf(5)
  idt = 1.0d0 - dt
  
  dpdC = 0.5d0*Cf(1)*Gi_r + 0.5d0*Cf(2)*( dt*dA1dI1*Gi_r + idt*dA4dI4*mxm )
  
  !dpdI1sq = (1.0d0 + 2.0d0*Cf(3)*I1m3sq) * Cf(2)* dt*Cf(3)*e1
  !dpdI4sq = (1.0d0 + 2.0d0*Cf(4)*I4m1sq) * Cf(2)*idt*Cf(4)*e4
  dpdI1sq = 0.5d0 * Cf(2) * dt * dA1dI1sq
  dpdI4sq = 0.5d0 * Cf(2) *idt * dA4dI4sq
  
  
  do i = 1, 3
    do j = 1, 3
      do k = 1, 3
        do l = 1, 3
		  dpdCC(i,j,k,l) = dpdI1sq *Gi_r(i,j)*Gi_r(k,l) &
						 + dpdI4sq *mxm (i,j)*mxm (k,l)
        end do
      end do
    end do
  end do
  
  
  
  !
!
!   dpdC = 0.0d0; dpdCC = 0.0d0
!
!   I1 = Gi_r(1,1)*gg_c(1,1)+Gi_r(2,2)*gg_c(2,2)+2.0d0*Gi_r(1,2)*gg_c(1,2)+C33
!   I4 = m(1)*m(1)*gg_c(1,1) + m(2)*m(2)*gg_c(2,2) + 2.0d0*m(1)*m(2)*gg_c(1,2)
!   !I4 = m(1)*m(1)*gg_c_full(1,1) + m(2)*m(2)*gg_c_full(2,2) + 2.0d0*m(1)*m(2)*gg_c_full(1,2)
!   !I4 = m(1)*m(1)*gab_c(1) + m(2)*m(2)*gab_c(2) + 2.0d0*m(1)*m(2)*gab_c(3)
!
!   I1m3 = I1 - 3.0d0
!   I4m1 = I4 - 1.0d0
!
!   !I1m3 = 0.0d0
!   !I4m1 = 0.0d0
!
!   !write(*,*) I4, I4m1
!
!   I1m3sq = I1m3*I1m3
!   I4m1sq = I4m1*I4m1
!
!   e1 = exp(Cf(3)*I1m3sq)
!   e4 = exp(Cf(4)*I4m1sq)
!
!   !write(*,*) I1, I1m3, e1
!
!   mxm = 0.0d0
!   mxm(1,1) = m(1)*m(1)
!   mxm(1,2) = m(1)*m(2)
!   mxm(2,1) = m(2)*m(1)
!   mxm(2,2) = m(2)*m(2)
!
!   ! dt must be 0 < dt < 1
!   dt = Cf(5)
!   idt = 1.0d0 - dt
!
!   dpdC = 0.5d0*Cf(1)*Gi_r + Cf(2)*( dt*Cf(3)*I1m3*e1*Gi_r + idt*Cf(4)*I4m1*e4*mxm )
!
!   dpdI1sq = (1.0d0 + 2.0d0*Cf(3)*I1m3sq) * Cf(2)* dt*Cf(3)*e1
!   dpdI4sq = (1.0d0 + 2.0d0*Cf(4)*I4m1sq) * Cf(2)*idt*Cf(4)*e4
!
!   do i = 1, 3
!     do j = 1, 3
!       do k = 1, 3
!         do l = 1, 3
!           dpdCC(i,j,k,l) = dpdI1sq *Gi_r(i,j)*Gi_r(k,l) &
!                          + dpdI4sq *mxm (i,j)*mxm (k,l)
!         end do
!       end do
!     end do
!   end do
  
  !if (idt*e4 > dt*e1) then
  !    write(*,*) I1m3, I4m1, dt*e1, idt*e4
  !end if
end subroutine shell_mat_Lee_Sacks_hybrid_S_C


! covariant -> contravariant 
subroutine cov2contra(g_contra, g_cov)
    implicit none   
    real(8), intent(in) :: g_cov(3,2)
    real(8), intent(out) :: g_contra(3,2)
    
    real(8) :: GG(3), invdetGab, GG_con(3)
    
    GG(1) = sum(g_cov(:,1)*g_cov(:,1))
    GG(2) = sum(g_cov(:,2)*g_cov(:,2))
    GG(3) = sum(g_cov(:,1)*g_cov(:,2))
    
    ! contravariant metric Gab_con and base vectors G_con
    invdetGab = 1.0d0/(GG(1)*GG(2)-GG(3)*GG(3))
    GG_con(1) =  invdetgab*GG(2)
    GG_con(3) = -invdetgab*GG(3)
    GG_con(2) =  invdetgab*GG(1)
    
    ! reference contravariant base vector
    g_contra(:,1) = g_cov(:,1) * GG_con(1) + g_cov(:,2) * GG_con(3)
    g_contra(:,2) = g_cov(:,1) * GG_con(3) + g_cov(:,2) * GG_con(2)
    
    
end subroutine cov2contra 

subroutine eval_tensor(ans, A_in, g)

    implicit none    
    real(8), intent(in)  :: A_in(3), g(3,2)
    real(8), intent(out) :: ans(3,3)
    real(8) :: A(3,3), Agg(3,3)
    integer :: i, j
    
    real(8) :: GGv_r(3), invdetGab, GGv_r_con(3), g_con(3,2), g3(3)
    
    
    A = 0.0d0
    A(1,1) = A_in(1)
    A(2,2) = A_in(2)
    A(1,2) = A_in(3)
    A(2,1) = A_in(3)
    A(3,3) = 1.0d0
    
    GGv_r(1) = sum(g(:,1)*g(:,1))
    GGv_r(2) = sum(g(:,2)*g(:,2))
    GGv_r(3) = sum(g(:,1)*g(:,2))
    
    invdetGab = 1.0d0/(GGv_r(1)*GGv_r(2)-GGv_r(3)*GGv_r(3))
    GGv_r_con(1) =  invdetgab*GGv_r(2)
    GGv_r_con(3) = -invdetgab*GGv_r(3)
    GGv_r_con(2) =  invdetgab*GGv_r(1)
    
    ! contravariant base vector
    g_con(:,1) = g(:,1) * GGv_r_con(1) + g(:,2) * GGv_r_con(3)
    g_con(:,2) = g(:,1) * GGv_r_con(3) + g(:,2) * GGv_r_con(2)
    
    call cross(g_con(:,1), g_con(:,2), g3)
    g3 = g3/sqrt(sum(g3*g3))
    
    ans = 0.0d0
    
    do i = 1, 2
        do j = 1, 2
            call eval_tensor_ij(Agg, A(i,j), g_con(:,i), g_con(:,j))
            ans = ans + Agg
        end do 
    end do
    
    call eval_tensor_ij(Agg, A(3,3), g3, g3)
    ans = ans + Agg
    
    !write(*,*) ans(1,:)
    !write(*,*) ans(2,:)
    !write(*,*) ans(3,:)
    
end subroutine eval_tensor

subroutine eval_tensor_ij(Agg, A, g1, g2)
    implicit none    
    real(8), intent(out) :: Agg(3,3)
    real(8), intent(in) :: A, g1(3), g2(3)

    real(8) :: gg(3,3)
    
    gg(1,:) = (/ g1(1)*g2(1), g1(1)*g2(2), g1(1)*g2(3) /)
    gg(2,:) = (/ g1(2)*g2(1), g1(2)*g2(2), g1(2)*g2(3) /)
    gg(3,:) = (/ g1(3)*g2(1), g1(3)*g2(2), g1(3)*g2(3) /)
    
    Agg = A*gg
    
    !write(*,*) Agg(1,:)
    !write(*,*) Agg(2,:)
    !write(*,*) Agg(3,:)
    
    !write(*,*) gg(1,:)
    !write(*,*) gg(2,:)
    !write(*,*) gg(3,:)

end subroutine eval_tensor_ij

! Transformation of basis for 2x2 tensor 
! b <- a
subroutine Trans_3x3_tensor_2D (Btr_out, b, Atr_in, a)
    implicit none      
    integer, parameter :: n = 2
    real(8), intent(inout) :: Btr_out(3) ! result
    real(8), intent(in) :: Atr_in(3) ! original tensor
    real(8), intent(in) :: b(3,n) ! bases of Btr 
    real(8), intent(in) :: a(3,n) ! bases of Atr
    
    real(8) :: Atr(n,n), Btr(n,n)
    integer :: i, j, k, l
    real(8) :: ab (n,n) ! a dot b
    
    Atr(1,1) = Atr_in(1)
    Atr(2,2) = Atr_in(2)
    Atr(1,2) = Atr_in(3)
    Atr(2,1) = Atr_in(3)
    
    ab = 0.0d0
    do i = 1, n
        do j = 1, n
            ab(i,j) = sum(a(:,i)*b(:,j))
        end do 
    end do
    
    Btr = 0.0d0
    do k = 1, n
        do l = 1, n
            do i = 1, n
                do j = 1, n
                    Btr(k,l) = Btr(k,l) + Atr(i,j) * ab(i,k)*ab(j,l)
                end do 
            end do
        end do 
    end do
    
    Btr_out(1) = Btr(1,1)
    Btr_out(2) = Btr(2,2)
    Btr_out(3) = Btr(1,2)
    
end subroutine Trans_3x3_tensor_2D


! Transformation of basis for 3x3 tensor 
! b_{kl} <- a_{ij}
subroutine Trans_3x3_tensor_3D (Btr, b, Atr, a)
    
    implicit none      
    real(8), intent(inout) :: Btr(3,3) ! result
    real(8), intent(in) :: Atr(3,3) ! original tensor
    real(8), intent(in) :: b(3,3) ! bases of Btr (covariant)
    real(8), intent(in) :: a(3,3) ! bases of Atr (cotravariant)
    
    integer :: i, j, k, l
    real(8) :: ab (3,3) ! a dot b
    
    ab = 0.0d0
    do i = 1, 3
        do j = 1, 3
            ab(i,j) = sum(a(:,i)*b(:,j))
        end do 
    end do
    
    Btr = 0.0d0
    do k = 1, 3
        do l = 1, 3
            do i = 1, 3
                do j = 1,3
                    Btr(l,k) = Btr(l,k) + Atr(i,j) * ab(i,k)*ab(j,l)
                end do 
            end do
        end do 
    end do
    
    
end subroutine Trans_3x3_tensor_3D

subroutine Trans_3x3x3x3_tensor_2D (Btr, b, Atr, a)
    
    implicit none      
    integer, parameter :: n = 2
    real(8), intent(inout) :: Btr(n,n,n,n) ! result
    real(8), intent(in) :: Atr(n,n,n,n) ! original tensor
    real(8), intent(in) :: b(3,n) ! bases of Btr (covariant)
    real(8), intent(in) :: a(3,n) ! bases of Atr (cotravariant)
    
    integer :: i, j, k, l, q, w, e, r
    real(8) :: ab (n,n) ! a dot b
    
    ab = 0.0d0
    do i = 1, n
        do j = 1, n
            ab(i,j) = sum(a(:,i)*b(:,j))
        end do 
    end do
    
    Btr = 0.0d0
    do i = 1, n
        do j = 1, n
            do k = 1, n
                do l = 1, n
                    !
                    do q = 1, n
                        do w = 1, n
                            do e = 1, n
                                do r = 1, n
                                    Btr(i,j,k,l) = Btr(i,j,k,l) + Atr(q,w,e,r) *&
                                    ab(i,q)*ab(j,w)*ab(k,e)*ab(l,r)
                                end do 
                            end do
                        end do
                    end do
                    !
                end do 
            end do
        end do 
    end do
    
    
end subroutine Trans_3x3x3x3_tensor_2D

subroutine Trans_3x3x3x3_tensor_3D (Btr, b, Atr, a)
    
    implicit none      
    real(8), intent(inout) :: Btr(3,3,3,3) ! result
    real(8), intent(in) :: Atr(3,3,3,3) ! original tensor
    real(8), intent(in) :: b(3,3) ! bases of Btr (covariant)
    real(8), intent(in) :: a(3,3) ! bases of Atr (cotravariant)
    
    integer :: i, j, k, l, q, w, e, r
    real(8) :: ab (3,3) ! a dot b
    
    ab = 0.0d0
    do i = 1, 3
        do j = 1, 3
            ab(i,j) = sum(a(:,i)*b(:,j))
        end do 
    end do
    
    Btr = 0.0d0
    do i = 1, 3
        do j = 1, 3
            do k = 1, 3
                do l = 1,3
                    !
                    do q = 1, 3
                        do w = 1, 3
                            do e = 1, 3
                                do r = 1, 3
                                    Btr(i,j,k,l) = Btr(i,j,k,l) + Atr(q,w,e,r) *&
                                    ab(i,q)*ab(j,w)*ab(k,e)*ab(l,r)
                                end do 
                            end do
                        end do
                    end do
                    !
                end do 
            end do
        end do 
    end do
    
    
end subroutine Trans_3x3x3x3_tensor_3D


!======================================================================
! Correct one for exp
!======================================================================
subroutine shell_mat_exp_new(nshl, Gab_r, gab_c, Bv_r, bv_c, &
                                  mu, thi, ngauss, dE_cu, dK_cu, &
                                  N_cu, dN_cu, M_cu, dM_cu, &
								  Coef_a, Coef_b, Coef_c)
  implicit none      

  integer, intent(in) :: nshl, ngauss

  real(8), intent(in) :: Gab_r(3), gab_c(3), Bv_r(3), bv_c(3), &
                         mu, thi, dE_cu(3,3*NSHL), dK_cu(3,3*NSHL)
						 
  real(8), intent(in) :: Coef_a, Coef_b, Coef_c
  
  real(8), intent(out) :: N_cu(3), M_cu(3), dN_cu(3,3*NSHL), &
                          dM_cu(3,3*NSHL)

  real(8) :: GGv_r(3), ggv_c(3), GG_r(2,2), gg_c(2,2), &
             detGG, sdetgg, C33, Gi_r(3,3), gi_c(3,3), trc, trcm3, &
             dpdC(3,3), dpdCC(3,3,3,3), St(2,2), CC(2,2,2,2),&
             Svec(3), Dmat(3,3), Coeffa, Coeffb, Coeffc, &
             zeta

  real(8) :: gp(ngauss), gw(ngauss)
            
  integer :: i, j, k, l, iz

  ! great
!   Coeffa = (mu*0.5d0)*0.5d0
!   Coeffc  = 50.0d0
!   Coeffb  = (mu*0.5d0)*1.0d0/Coeffc
!
!   write(*,*) '1  ', Coeffa, Coeffc, Coeffb
  
  Coeffa  = Coef_a
  Coeffb  = Coef_b
  Coeffc  = Coef_c
  
!   write(*,*) '2  ', Coeffa, Coeffc, Coeffb

  N_cu  = 0.0d0
  M_cu  = 0.0d0
  dN_cu = 0.0d0
  dM_cu = 0.0d0

  GGv_r = 0.0d0
  ggv_c = 0.0d0
  GG_r = 0.0d0
  gg_c = 0.0d0
  Gi_r = 0.0d0
  gi_c = 0.0d0
  dpdC = 0.0d0
  dpdCC = 0.0d0
  St = 0.0d0
  CC = 0.0d0
  Svec = 0.0d0
  Dmat = 0.0d0

  ! thickness integration loop
  call genGPandGW_shell(gp, gw, ngauss) 

  do iz = 1, ngauss
  
    zeta = 0.5d0*thi*GP(iz)

    GGv_r = Gab_r - 2.0d0*zeta*Bv_r
    ggv_c = gab_c - 2.0d0*zeta*bv_c

    detGG  = GGv_r(1)*GGv_r(2) - GGv_r(3)**2
    sdetgg = ggv_c(1)*ggv_c(2) - ggv_c(3)**2

    GG_r(1,:) = (/ GGv_r(1), GGv_r(3) /)
    GG_r(2,:) = (/ GGv_r(3), GGv_r(2) /)

    gg_c(1,:) = (/ ggv_c(1), ggv_c(3) /)
    gg_c(2,:) = (/ ggv_c(3), ggv_c(2) /)

    C33 = detGG/sdetgg
    
    Gi_r(1,:) = 1.0d0/detGG*(/  GG_r(2,2), -GG_r(1,2), 0.0d0 /)
    Gi_r(2,:) = 1.0d0/detGG*(/ -GG_r(2,1),  GG_r(1,1), 0.0d0 /)
    Gi_r(3,:) =             (/      0.0d0,      0.0d0, 1.0d0 /)
    
    gi_c(1,:) = 1.0d0/sdetgg*(/  gg_c(2,2), -gg_c(1,2), 0.0d0     /)
    gi_c(2,:) = 1.0d0/sdetgg*(/ -gg_c(2,1),  gg_c(1,1), 0.0d0     /)
    gi_c(3,:) =              (/      0.0d0,      0.0d0, 1.0d0/C33 /)
    
    trC = Gi_r(1,1)*gg_c(1,1)+Gi_r(2,2)*gg_c(2,2)+2.0d0*Gi_r(1,2)*gg_c(1,2)+C33

    ! Fung    
    trCm3 = trC - 3.0d0
    
    dpdC  = 0.5d0*Coeffa*Gi_r + 0.5d0*Coeffb*Coeffc*exp(Coeffc*trCm3)*Gi_r
    
    dpdCC = 0.0d0
    
    do i = 1, 3
      do j = 1, 3
        do k = 1, 3
          do l = 1, 3
            dpdCC(i,j,k,l) = 0.5d0*Coeffb*Coeffc**2*exp(Coeffc*trCm3)*Gi_r(i,j)*Gi_r(k,l);
          end do
        end do
      end do
    end do
    
    St = 2.0d0*dpdC(1:2,1:2) - 2.0d0*dpdC(3,3)*C33*gi_c(1:2,1:2)
    
    CC = 0.0d0
    
    do i = 1, 2
      do j = 1, 2
        do k = 1, 2
          do l = 1, 2
            CC(i,j,k,l) = 4.0d0*dpdCC(i,j,k,l) &
              +4.0d0*dpdCC(3,3,3,3)*C33**2*gi_c(i,j)*gi_c(k,l) &
              -4.0d0*dpdCC(i,j,3,3)*C33*gi_c(k,l) &
              -4.0d0*dpdCC(k,l,3,3)*C33*gi_c(i,j) &
              +2.0d0*dpdC(3,3)*C33*(2.0d0*gi_c(i,j)*gi_c(k,l)+gi_c(i,k)*gi_c(j,l)+gi_c(i,l)*gi_c(j,k))
          end do
        end do
      end do
    end do
    
    ! Stress vector and material matrix
    Svec = (/ St(1,1), St(2,2), St(1,2) /)

    Dmat(1,:) = (/ CC(1,1,1,1),CC(1,1,2,2),CC(1,1,1,2) /)
    Dmat(2,:) = (/ CC(2,2,1,1),CC(2,2,2,2),CC(2,2,1,2) /)
    Dmat(3,:) = (/ CC(1,2,1,1),CC(1,2,2,2),CC(1,2,1,2) /)

    ! stress resultants
    N_cu  = 0.5d0*thi*Svec*GW(iz) + N_cu
    dN_cu = 0.5d0*thi*matmul(Dmat,(dE_cu+zeta*dK_cu))*GW(iz) + dN_cu
    M_cu  = 0.5d0*thi*Svec*zeta*GW(iz) + M_cu
    dM_cu = 0.5d0*thi*matmul(Dmat,(dE_cu+zeta*dK_cu))*zeta*GW(iz) + dM_cu
  end do

end subroutine shell_mat_exp_new


!======================================================================
! 
!======================================================================
! subroutine shell_mat_exp(nshl, G0_r, G3_r, dG3_r, g0, g3, dg3, &
!                                   mu, thi, ngauss, dE_cu, dK_cu, &
!                                   N_cu, dN_cu, M_cu, dM_cu)
!   implicit none
!
!   integer, intent(in) :: nshl, ngauss
!
!   real(8), intent(in) :: G0_r(3,2), G3_r(3), dG3_r(3,2), &
!                          g0(3,2), g3(3), dg3(3,2), &
!                          mu, thi, dE_cu(3,3*NSHL), &
!                          dK_cu(3,3*NSHL)
!
!   real(8), intent(out) :: N_cu(3), M_cu(3), dN_cu(3,3*NSHL), &
!                           dM_cu(3,3*NSHL)
!
!   real(8) :: G_r(3,3), g(3,3), Gij(3,3), subGC(2,2), Gconij(3,3), &
!              C(3,3), Ci(3,3), CC(3,3,3,3), Dtemp(4,4), D(3,3),    &
!              St(3,3), S(3), GGCi(3,3,3,3), gp(ngauss), gw(ngauss), tmpC(3,3)
!
!   real(8) :: zeta, detJ, rtmp, dC33, subJsq, trC, trCm3, &
!              alpha0, alpha1, alpha2
!
!   integer :: i, j, k, l, iz
!
!   real(8) :: CixCi(3,3,3,3), CipCi(3,3,3,3), IxCi(3,3,3,3), IxI(3,3,3,3)
!
!   real(8) :: Coeff0, Coeff1, Coeff10
!
!   ! great
!   Coeff10 = (mu*0.5d0)*0.5d0
!   Coeff1  = 20.0d0
!   Coeff0  = (mu*0.5d0)*1.0d0/Coeff1
!
! !  Coeff10 = (mu*0.5d0)*0.5d0
! !  Coeff0  = 15.0d0*1.0d4
! !  Coeff1  = 50.0d0
!
!   N_cu  = 0.0d0
!   M_cu  = 0.0d0
!   dN_cu = 0.0d0
!   dM_cu = 0.0d0
!
!   C     = 0.0d0
!   Gij   = 0.0d0
!   subGC = 0.0d0
!   D     = 0.0d0
!   GGCi  = 0.0d0
!
!   ! 4th order tensor products for CC
!   CixCi = 0.0d0
!   CipCi = 0.0d0
!   IxCi  = 0.0d0
!   IxI   = 0.0d0
!
!   St = 0.0d0
!   CC = 0.0d0
!
!   ! thickness integration loop
!   call genGPandGW_shell(gp, gw, ngauss)
!
!   do iz = 1, ngauss
!
!     zeta = 0.5d0*thi*GP(iz)
!
! !     G_r(1:3,1:2) = G0_r(1:3,1:2) + zeta*dG3_r(1:3,1:2)
! !     G_r(1:3,3)   = G3_r(1:3)
! !
! !     g(1:3,1:2) = g0(1:3,1:2) + zeta*dg3(1:3,1:2)
! !     g(1:3,3)   = g3(1:3)
!
!     ! Metric and C tensor
! !    Gij = matmul(transpose(G_r),G_r)
!
!     ! keep the following line, but commented out. it's not clear yet if C
!     ! should be computed like this or like in the next for-loops, both
!     ! cases have justification. if you want, you can try both, in my
!     ! tests it didn't change anything on the results.
! !    C = matmul(transpose(g),g)
!
!     ! linear strain distribution through thickness
!    do i = 1, 2
!      do j = 1, 2
!        C(i,j)   = sum(g0(:,i)*g0(:,j))     + 2.0d0*zeta*sum(g0(:,i)*dg3(:,j))
!        Gij(i,j) = sum(G0_r(:,i)*G0_r(:,j)) + 2.0d0*zeta*sum(G0_r(:,i)*dG3_r(:,j))
!      end do
!    end do
!
!    ! check
!    do i = 1, 2
!      C(i,3) = 0.0d0!sum((g0(:,i)+zeta*dg3(:,i)) * g3(:))   ! should be zero
!      C(3,i) = C(i,3)                                 ! should be zero
!
!      Gij(i,3) = 0.0d0!sum((G0_r(:,i)+zeta*dG3_r(:,i)) * G3_r(:))   ! should be zero
!      Gij(3,i) = Gij(i,3)                                 ! should be zero
!    end do
!
!    C(3,3)   = 1.0d0!sum(g3*g3)      ! C^33, should also be 1 for incompressible
!    Gij(3,3) = 1.0d0!sum(G3_r*G3_r)  ! should be 1...
!
! !    write(*,*) C(1,3), C(2,3), C(3,1), C(3,2)
! !    write(*,*) C(3,3), Gij(3,3)
! !    write(*,*)
!
!     ! contravariant metric
!     call mat_inverse_3x3(Gij, Gconij, rtmp)
!
!     do i = 1, 2
!       do j = 1, 2
!         subGC(i,j) = sum(Gconij(i,:)*C(:,j))
!       end do
!     end do
!
!     ! determinant of GC (C_33 = 1/subJsq)
!     subJsq = subGC(1,1)*subGC(2,2) - subGC(1,2)*subGC(2,1)
!
! 	! get C^-1
!     call mat_inverse_3x3(C, Ci, rtmp)
!
! 	do i = 1, 3
! 	  do j = 1, 3
! 	    do k = 1, 3
! 	      do l = 1, 3
! 	        CixCi(i,j,k,l) = Ci(i,j)*Ci(k,l)
! 	        CipCi(i,j,k,l) = 0.5d0*(Ci(i,k)*Ci(j,l)+Ci(i,l)*Ci(j,k))
!
! 	        GGCi(i,j,k,l)  = Gconij(i,3)*Gconij(j,3)*Ci(k,l) + Gconij(k,3)*Gconij(l,3)*Ci(i,j)
!
! 	        IxCi(i,j,k,l)  = Gconij(i,j)*Ci(k,l) + Ci(i,j)*Gconij(k,l)
!
! 	        IxI(i,j,k,l)   = Gconij(i,j)*Gconij(k,l)
! 	      end do
! 	    end do
! 	  end do
!     end do
!
!     trC = 0.0d0
!     do i = 1, 3
!       do j = 1, 3
! !        trC = trC + (C(i,j)-Gij(i,j))*Gconij(i,j)
!         trC = trC + C(i,j)*Gconij(i,j)
!       end do
!     end do
!
!     trCm3  = trC - 3.0d0
!
! 	alpha0 = exp(coeff1*trcm3)
!
! 	alpha1 = 2.0d0*coeff10 + coeff0*coeff1*alpha0
!
!     St = alpha1*(Gconij-1.0d0/subJsq*Ci)
!
!     alpha2 = coeff0*coeff1**2*alpha0
!
!     CC = 2.0d0*alpha2*IxI &
! 	   + alpha1*1.0d0/subJsq*(2.0d0*CipCi-CixCi) &
! 	   - 2.0d0*alpha1*GGCi &
! 	   - 2.0d0*1.0d0/subJsq*alpha2*IxCi
!
!     ! Stress vector and material matrix
!     S = (/ St(1,1), St(2,2), St(1,2) /)
!
!     Dtemp(1,:) = (/ CC(1,1,1,1),CC(1,1,2,2),CC(1,1,1,2),CC(1,1,3,3) /)
!     Dtemp(2,:) = (/ CC(2,2,1,1),CC(2,2,2,2),CC(2,2,1,2),CC(2,2,3,3) /)
!     Dtemp(3,:) = (/ CC(1,2,1,1),CC(1,2,2,2),CC(1,2,1,2),CC(1,2,3,3) /)
!     Dtemp(4,:) = (/ CC(3,3,1,1),CC(3,3,2,2),CC(3,3,1,2),CC(3,3,3,3) /)
!
!     ! static condensation of S33=0
!     do i = 1, 3
!       do j = 1, 3
!         D(i,j) = Dtemp(i,j)-Dtemp(i,4)*Dtemp(4,j)/Dtemp(4,4)
!       end do
!     end do
!
!     ! updated thickness due to plane stress
!     N_cu  = 0.5d0*thi*S*GW(iz) + N_cu
!     dN_cu = 0.5d0*thi*matmul(D,(dE_cu+zeta*dK_cu))*GW(iz) + dN_cu
!     M_cu  = 0.5d0*thi*S*zeta*GW(iz) + M_cu
!     dM_cu = 0.5d0*thi*matmul(D,(dE_cu+zeta*dK_cu))*zeta*GW(iz) + dM_cu
!   end do
!
! end subroutine shell_mat_exp
!
!
!
! !======================================================================
! !
! !======================================================================
! subroutine shell_mat_exp_2(nshl, G0_r, G3_r, dG3_r, g0, g3, dg3, &
!                                   mu, thi, ngauss, dE_cu, dK_cu, &
!                                   N_cu, dN_cu, M_cu, dM_cu)
!   implicit none
!
!   integer, intent(in) :: nshl, ngauss
!
!   real(8), intent(in) :: G0_r(3,2), G3_r(3), dG3_r(3,2), &
!                          g0(3,2), g3(3), dg3(3,2), &
!                          mu, thi, dE_cu(3,3*NSHL), &
!                          dK_cu(3,3*NSHL)
!
!   real(8), intent(out) :: N_cu(3), M_cu(3), dN_cu(3,3*NSHL), &
!                           dM_cu(3,3*NSHL)
!
!   real(8) :: G_r(3,3), g(3,3), Gij(3,3), subGC(2,2), Gconij(3,3), &
!              C(3,3), Ci(3,3), CC(3,3,3,3), Dtemp(4,4), D(3,3),    &
!              St(3,3), S(3), GGCi(3,3,3,3), gp(ngauss), gw(ngauss)
!
!   real(8) :: zeta, detJ, rtmp, dC33, subJsq, trC, trCm3, &
!              alpha0, alpha1, alpha2
!
!   integer :: i, j, k, l, iz
!
!   real(8) :: CixCi(3,3,3,3), CipCi(3,3,3,3), IxCi(3,3,3,3), IxI(3,3,3,3)
!
!   real(8) :: Coeff0, Coeff1, Coeff10
!
!   ! I think coeff0 can't be too low...
!   ! otherwise too soft, even thought there is exp term.
!
!   Coeff10 = (mu*0.5d0)*0.5d0
!   Coeff0  = (mu*0.5d0)*1.0d0/2.0d0
!   Coeff1  = 2.0d0
!
!   ! this one works...
! !  Coeff10 = mu*0.5d0
! !  Coeff0  = mu*2.0d1!*0.0d0
! !  Coeff1  = 0.01d0!*0.0d0
!
! ! doesn't work
! ! Coeff10 = mu*0.5d0
! ! Coeff0  = mu*1.0d1
! ! Coeff1  = 0.1d0
!
!   N_cu  = 0.0d0
!   M_cu  = 0.0d0
!   dN_cu = 0.0d0
!   dM_cu = 0.0d0
!
!   C     = 0.0d0
!   Gij   = 0.0d0
!   subGC = 0.0d0
!   D     = 0.0d0
!   GGCi  = 0.0d0
!
!   ! 4th order tensor products for CC
!   CixCi = 0.0d0
!   CipCi = 0.0d0
!   IxCi  = 0.0d0
!   IxI   = 0.0d0
!
!   St = 0.0d0
!   CC = 0.0d0
!
!   ! thickness integration loop
!   call genGPandGW_shell(gp, gw, ngauss)
!
!   do iz = 1, ngauss
!
!     zeta = 0.5d0*thi*GP(iz)
!
! !     G_r(1:3,1:2) = G0_r(1:3,1:2) + zeta*dG3_r(1:3,1:2)
! !     G_r(1:3,3)   = G3_r(1:3)
! !
! !     g(1:3,1:2) = g0(1:3,1:2) + zeta*dg3(1:3,1:2)
! !     g(1:3,3)   = g3(1:3)
!
!     ! Metric and C tensor
! !    Gij = matmul(transpose(G_r),G_r)
!
!     ! keep the following line, but commented out. it's not clear yet if C
!     ! should be computed like this or like in the next for-loops, both
!     ! cases have justification. if you want, you can try both, in my
!     ! tests it didn't change anything on the results.
! !    C = matmul(transpose(g),g)
!
!     ! linear strain distribution through thickness
!    do i = 1, 2
!      do j = 1, 2
!        C(i,j)   = sum(g0(:,i)*g0(:,j))     + 2.0d0*zeta*sum(g0(:,i)*dg3(:,j))
!        Gij(i,j) = sum(G0_r(:,i)*G0_r(:,j)) + 2.0d0*zeta*sum(G0_r(:,i)*dG3_r(:,j))
!      end do
!    end do
!
!    ! check
!    do i = 1, 2
!      C(i,3) = sum((g0(:,i)+zeta*dg3(:,i)) * g3(:))   ! should be zero
!      C(3,i) = C(i,3)                                 ! should be zero
!
!      Gij(i,3) = sum((G0_r(:,i)+zeta*dG3_r(:,i)) * G3_r(:))   ! should be zero
!      Gij(3,i) = Gij(i,3)                                 ! should be zero
!    end do
!
!    C(3,3)   = sum(g3*g3)      ! C^33, should also be 1 for incompressible
!    Gij(3,3) = sum(G3_r*G3_r)  ! should be 1...
!
! !!!   write(*,*) C(1,3), C(2,3), C(3,1), C(3,2)
!
!     ! contravariant metric
!     call mat_inverse_3x3(Gij, Gconij, rtmp)
!
!     do i = 1, 2
!       do j = 1, 2
!         subGC(i,j) = sum(Gconij(i,:)*C(:,j))
!       end do
!     end do
!
!     ! determinant of GC (C_33 = 1/subJsq)
!     subJsq = subGC(1,1)*subGC(2,2) - subGC(1,2)*subGC(2,1)
!
! 	! get C^-1
!     call mat_inverse_3x3(C, Ci, rtmp)
!
! 	do i = 1, 3
! 	  do j = 1, 3
! 	    do k = 1, 3
! 	      do l = 1, 3
! 	        CixCi(i,j,k,l) = Ci(i,j)*Ci(k,l)
! 	        CipCi(i,j,k,l) = 0.5d0*(Ci(i,k)*Ci(j,l)+Ci(i,l)*Ci(j,k))
!
! 	        GGCi(i,j,k,l)  = Gconij(i,3)*Gconij(j,3)*Ci(k,l) + Gconij(k,3)*Gconij(l,3)*Ci(i,j)
!
! 	        IxCi(i,j,k,l)  = Gconij(i,j)*Ci(k,l) + Ci(i,j)*Gconij(k,l)
!
! 	        IxI(i,j,k,l)   = Gconij(i,j)*Gconij(k,l)
! 	      end do
! 	    end do
! 	  end do
!     end do
!
!     trC = 0.0d0
!     do i = 1, 3
!       do j = 1, 3
!         trC = trC + C(i,j)*Gconij(j,i)
! !        trC = trC + (C(i,j)-Gconij(i,j))*Gconij(i,j)
!       end do
!     end do
!
!     trCm3  = trC - 3.0d0
!
! 	alpha0 = exp(coeff1*trcm3**2)
!
! 	alpha1 = 2.0d0*(coeff10 + coeff0*coeff1*trCm3*alpha0)
!
!     St = alpha1*(Gconij-1.0d0/subJsq*Ci)
!
!     alpha2 = 2.0d0*coeff0*coeff1*(2.0d0*coeff1*trCm3**2+1.0d0)*alpha0
!
!     CC = 2.0d0*alpha2*IxI &
! 	   + alpha1*1.0d0/subJsq*(2.0d0*CipCi-CixCi) &
! 	   - 2.0d0*alpha1*GGCi &
! 	   - 2.0d0*1.0d0/subJsq*alpha2*IxCi
!
!     ! Stress vector and material matrix
!     S = (/ St(1,1), St(2,2), St(1,2) /)
!
!     Dtemp(1,:) = (/ CC(1,1,1,1),CC(1,1,2,2),CC(1,1,1,2),CC(1,1,3,3) /)
!     Dtemp(2,:) = (/ CC(2,2,1,1),CC(2,2,2,2),CC(2,2,1,2),CC(2,2,3,3) /)
!     Dtemp(3,:) = (/ CC(1,2,1,1),CC(1,2,2,2),CC(1,2,1,2),CC(1,2,3,3) /)
!     Dtemp(4,:) = (/ CC(3,3,1,1),CC(3,3,2,2),CC(3,3,1,2),CC(3,3,3,3) /)
!
!     ! static condensation of S33=0
!     do i = 1, 3
!       do j = 1, 3
!         D(i,j) = Dtemp(i,j)-Dtemp(i,4)*Dtemp(4,j)/Dtemp(4,4)
!       end do
!     end do
!
!     ! updated thickness due to plane stress
!     N_cu  = 0.5d0*thi*S*GW(iz) + N_cu
!     dN_cu = 0.5d0*thi*matmul(D,(dE_cu+zeta*dK_cu))*GW(iz) + dN_cu
!     M_cu  = 0.5d0*thi*S*zeta*GW(iz) + M_cu
!     dM_cu = 0.5d0*thi*matmul(D,(dE_cu+zeta*dK_cu))*zeta*GW(iz) + dM_cu
!   end do
!
! end subroutine shell_mat_exp_2
!
!
!
! !======================================================================
! !
! !======================================================================
! subroutine shell_mat_exp_k(nshl, G0_r, G3_r, dG3_r, g0, g3, dg3, &
!                                   mu, thi, ngauss, dE_cu, dK_cu, &
!                                   N_cu, dN_cu, M_cu, dM_cu)
!   implicit none
!
!   integer, intent(in) :: nshl, ngauss
!
!   real(8), intent(in) :: G0_r(3,2), G3_r(3), dG3_r(3,2), &
!                          g0(3,2), g3(3), dg3(3,2), &
!                          mu, thi, dE_cu(3,3*NSHL), &
!                          dK_cu(3,3*NSHL)
!
!   real(8), intent(out) :: N_cu(3), M_cu(3), dN_cu(3,3*NSHL), &
!                           dM_cu(3,3*NSHL)
!
!   real(8) :: G_r(3,3), g(3,3), Gij(3,3), subGC(2,2), Gconij(3,3), &
!              C(3,3), Ci(3,3), CC(3,3,3,3), Dtemp(4,4), D(3,3),    &
!              St(3,3), S(3), GGCi(3,3,3,3), gp(ngauss), gw(ngauss)
!
!   real(8) :: zeta, detJ, rtmp, dC33, subJ, trC, trCm3, &
!              alpha0, alpha1, alpha2
!
!   integer :: i, j, k, l, iz
!
!   real(8) :: CixCi(3,3,3,3), CipCi(3,3,3,3), IxCi(3,3,3,3), IxI(3,3,3,3)
!
!   real(8) :: Coeff0, Coeff1, Coeff10
!   integer :: coeffk
!
!   Coeff10 = 0.3335557038d0*2.0d0*1.0d7*0.5d0*0.33d0
!   Coeff0  = 0.3335557038d0*2.0d0*1.0d8
!   Coeff1  = 0.001d0
!
!   coeffk = 0
!
!   N_cu  = 0.0d0
!   M_cu  = 0.0d0
!   dN_cu = 0.0d0
!   dM_cu = 0.0d0
!
!   C     = 0.0d0
!   Gij   = 0.0d0
!   subGC = 0.0d0
!   D     = 0.0d0
!   GGCi  = 0.0d0
!
!   ! 4th order tensor products for CC
!   CixCi = 0.0d0
!   CipCi = 0.0d0
!   IxCi  = 0.0d0
!   IxI   = 0.0d0
!
!   St = 0.0d0
!   CC = 0.0d0
!
!   ! thickness integration loop
!   call genGPandGW_shell(gp, gw, ngauss)
!
!   do iz = 1, ngauss
!
!     zeta = 0.5d0*thi*GP(iz)
!
!     G_r(1:3,1:2) = G0_r(1:3,1:2) + zeta*dG3_r(1:3,1:2)
!     G_r(1:3,3)   = G3_r(1:3)
!
!     g(1:3,1:2) = g0(1:3,1:2) + zeta*dg3(1:3,1:2)
!     g(1:3,3)   = g3(1:3)
!
!     ! Metric and C tensor
! !    Gij = matmul(transpose(G_r),G_r)
!
!     ! keep the following line, but commented out. it's not clear yet if C
!     ! should be computed like this or like in the next for-loops, both
!     ! cases have justification. if you want, you can try both, in my
!     ! tests it didn't change anything on the results.
! !    C = matmul(transpose(g),g)
!
!     ! linear strain distribution through thickness
!    do i = 1, 2
!      do j = 1, 2
!        C(i,j)   = sum(g0(:,i)*g0(:,j))     + 2.0d0*zeta*sum(g0(:,i)*dg3(:,j))
!        Gij(i,j) = sum(G0_r(:,i)*G0_r(:,j)) + 2.0d0*zeta*sum(G0_r(:,i)*dG3_r(:,j))
!      end do
!    end do
!
!    do i = 1, 2
!      C(i,3) = sum((g0(:,i)+zeta*dg3(:,i)) * g3(:))   ! should be zero
!      C(3,i) = C(i,3)                                 ! should be zero
!
!      Gij(i,3) = sum((G0_r(:,i)+zeta*dG3_r(:,i)) * G3_r(:))   ! should be zero
!      Gij(3,i) = Gij(i,3)                                 ! should be zero
!    end do
!
!    C(3,3)   = sum(g3*g3)      ! C^33
!    Gij(3,3) = sum(G3_r*G3_r)
!
! !!!   write(*,*) C(1,3), C(2,3), C(3,1), C(3,2)
!
!     ! contravariant metric
!     call mat_inverse_3x3(Gij, Gconij, rtmp)
!
!     do i = 1, 2
!       do j = 1, 2
!         subGC(i,j) = sum(Gconij(i,:)*C(:,j))
!       end do
!     end do
!
!     ! determinant of GC (C_33 = 1/subJ)
!     subJ = subGC(1,1)*subGC(2,2) - subGC(1,2)*subGC(2,1)
!
! 	! get C^-1
!     call mat_inverse_3x3(C, Ci, rtmp)
!
! 	do i = 1, 3
! 	  do j = 1, 3
! 	    do k = 1, 3
! 	      do l = 1, 3
! 	        CixCi(i,j,k,l) = Ci(i,j)*Ci(k,l)
! 	        CipCi(i,j,k,l) = 0.5d0*(Ci(i,k)*Ci(j,l)+Ci(i,l)*Ci(j,k))
! 	        GGCi(i,j,k,l)  = Gconij(i,3)*Gconij(j,3)*Ci(k,l)+Gconij(k,3)*Gconij(l,3)*Ci(i,j)
!
! 	        IxCi(i,j,k,l)  = Gconij(i,j)*Ci(k,l) + Ci(i,j)*Gconij(k,l)
! 	        IxI(i,j,k,l)   = Gconij(i,j)*Gconij(k,l)
! 	      end do
! 	    end do
! 	  end do
!     end do
!
!     trC = 0.0d0
!     do i = 1, 3
!       do j = 1, 3
!         trC = trC + C(i,j)*Gconij(j,i)
!       end do
!     end do
!
!     trCm3  = trC - 3.0d0
!
! 	alpha0 = exp(coeff1*trcm3**coeffk)
!
! 	alpha1 = 2.0d0*coeff10 + coeff0*coeff1*dble(coeffk)*trCm3**(coeffk-1)*alpha0
!
!     St = alpha1*(Gconij-1.0d0/subJ*Ci)
!
!     write(*,*) dble(coeffk)*trCm3**(coeffk-1)
!
!     alpha2 = coeff0*coeff1*dble(coeffk)*trCm3**(coeffk-2)*(dble(coeffk)-1.0d0+coeff1*dble(coeffk)*trCm3**k)*alpha0
!
!     CC = 2.0d0*alpha2*IxI &
! 	   + alpha1*1.0d0/subJ*(2.0d0*CipCi-CixCi) &
! 	   - 2.0d0*alpha1*GGCi &
! 	   - 2.0d0*1.0d0/subJ*alpha2*IxCi
!
!     ! Stress vector and material matrix
!     S = (/ St(1,1), St(2,2), St(1,2) /)
!
!     Dtemp(1,:) = (/ CC(1,1,1,1),CC(1,1,2,2),CC(1,1,1,2),CC(1,1,3,3) /)
!     Dtemp(2,:) = (/ CC(2,2,1,1),CC(2,2,2,2),CC(2,2,1,2),CC(2,2,3,3) /)
!     Dtemp(3,:) = (/ CC(1,2,1,1),CC(1,2,2,2),CC(1,2,1,2),CC(1,2,3,3) /)
!     Dtemp(4,:) = (/ CC(3,3,1,1),CC(3,3,2,2),CC(3,3,1,2),CC(3,3,3,3) /)
!
!     ! static condensation of S33=0
!     do i = 1, 3
!       do j = 1, 3
!         D(i,j) = Dtemp(i,j)-Dtemp(i,4)*Dtemp(4,j)/Dtemp(4,4)
!       end do
!     end do
!
!     ! updated thickness due to plane stress
!     N_cu  = 0.5d0*thi*S*GW(iz) + N_cu
!     dN_cu = 0.5d0*thi*matmul(D,(dE_cu+zeta*dK_cu))*GW(iz) + dN_cu
!     M_cu  = 0.5d0*thi*S*zeta*GW(iz) + M_cu
!     dM_cu = 0.5d0*thi*matmul(D,(dE_cu+zeta*dK_cu))*zeta*GW(iz) + dM_cu
!   end do
!
! end subroutine shell_mat_exp_k
