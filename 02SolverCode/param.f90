!======================================================================
! subroutine to read in the parameters defined in 'param.dat'          
!======================================================================
subroutine getparam(SHL)
  
  use aAdjKeep
  use commonvar
  use commonvar_shell
  use commonvar_contact
  use types_shell
  use mpi

  implicit none

  type(shell),   intent(inout) :: SHL
  
  integer :: inurbs, i, j, bb
  character(len=30) :: fname(2), fnum(3)

  call rread("Damping_a", Damping_a)
  call rread("Damping_b", Damping_b)

  ! Time stepping    
  call rread("Time_Period", Tper)
  call rread("Delt",        Delt)
 
  call iread("Nstep",     Nstep)
  call iread("ifq",       ifq)
  call iread("ifq_tq",    ifq_tq)

  call rread("rhoinf",    rhoinf)
  
  call iread("Mode",    Mode)
  call rread("T_START",    T_START)
  call rread("InitPress",    InitPress)

  ! solver GMRES
  call rread("NS_GMRES_tol",     NS_GMRES_tol)
  call iread("NS_GMRES_itermax", NS_GMRES_itermax)
  call iread("NS_GMRES_itermin", NS_GMRES_itermin)
  
!!!  call rread("NS_NL_Utol",    NS_NL_Utol)
!!!  call rread("NS_NL_Ptol",    NS_NL_Ptol)  
  call iread("NS_NL_itermax", NS_NL_itermax)

  call iread("NS_hessian", NS_hess_flag)
  
  ! flow properfy
  call rread("DENS_AIR",    DENS_AIR)
  call rread("VISC_AIR",    VISC_AIR)

  ! Others
!!!  call rread("WBC_Penalty", WBC_Penalty)
  call rread("WBC_TauNor", WBC_TauNor)
  call rread("WBC_TauTan", WBC_TauTan)

  call iread("Adap_Int_Level", Adap_Int_Level)

  ! input for shell
  call rread("Density_Shell", Density_Shell)
  call rread("Thickness_Shell", Thickness_Shell)
  call rread("E_Shell", E_Shell)
  call rread("nu_Shell", nu_Shell)
  
  call rread("Coef_a", Coef_a)
  call rread("Coef_b", Coef_b)
  call rread("Coef_c", Coef_c)
  
  call rread("MtCoef_1", MtCoefs(1))
  call rread("MtCoef_2", MtCoefs(2))
  call rread("MtCoef_3", MtCoefs(3))
  call rread("MtCoef_4", MtCoefs(4))
  call rread("MtCoef_5", MtCoefs(5))
  call rread("fib_ang", fib_ang)
  
  ! input for stent
  call rread("Density_Stent", Density_Stent)
  call rread("Thickness_Stent", Thickness_Stent)
  call rread("E_Stent", E_Stent)
  call rread("nu_Stent", nu_Stent)
    
  call iread("NGauss_TH_Shell", NGauss_TH_Shell)
  call iread("Material_Shell", Material_Shell)
  call iread("Material_Stent", Material_Stent)
  
  call rread("Shell_CG_Tol", SHL%CGtol)

  call iread("NPatch", SHL%NPB)
  call iread("NBC_set", NBC_set)
  
  !input for contact
  call rread("contact_k1", contact_k1)
  call rread("contact_k2", contact_k2)
  call rread("contact_k3", contact_k3)
  call rread("contact_cutoff", contact_cutoff)
  call iread("contact_pow", contact_pow)
  call rread("contact_lox", contact_lox)
  call rread("contact_loy", contact_loy)
  call rread("contact_loz", contact_loz)
  call rread("contact_hix", contact_hix)
  call rread("contact_hiy", contact_hiy)
  call rread("contact_hiz", contact_hiz)
  call iread("contact_nx", contact_nx)
  call iread("contact_ny", contact_ny)
  call iread("contact_nz", contact_nz)

  SHL%NPT = SHL%NPB
  SHL%NPS = 0

  ! tolerance of converging RHS
  SHL%RHSGtol = 1.0d-3

  ! Include gravity load in each direction
  ! (1.0, 0.0, 0.0) means gravity in +x direction
  SHL%G_fact(1:3) = 0.0d0

  !----------------------------------------
  ! Build time integration parameters
  !----------------------------------------
  if((rhoinf < 0.0d0) .or. (rhoinf > 1.0d0)) then ! backward Euler
    almi = 1.0d0
    alfi = 1.0d0
    gami = 1.0d0
    beti = 1.0d0 
  else    ! generalized-alpha
    almi = (3.0d0-rhoinf)/(1.0d0+rhoinf)/2.0d0
    alfi = 1.0d0/(1.0d0+rhoinf)    
    gami = 0.5d0+almi-alfi
    beti = 0.25d0*(1.0d0+almi-alfi)*(1.0d0+almi-alfi)
  end if

  if (ismaster) write(*,"(a20,x,' = ',x,ES12.4)") "almi", almi
  if (ismaster) write(*,"(a20,x,' = ',x,ES12.4)") "alfi", alfi
  if (ismaster) write(*,"(a20,x,' = ',x,ES12.4)") "gami", gami  
  if (ismaster) write(*,"(a20,x,' = ',x,ES12.4)") "beti", beti 
 
  Dtgl = 1.0d0/Delt     

end subroutine getparam
  


!======================================================================
!
!======================================================================
subroutine rread(vname, val)
  use mpi
  implicit none

  character(len=*), intent(in)  :: vname
  real(8),          intent(out) :: val
  character(len=20) :: buf
  
  call cread(vname, buf)
  read(buf,*) val

  if (ismaster) write(*,"(a20,x,' = ',x,ES12.4)")  vname, val
end subroutine rread


!======================================================================
!
!======================================================================
subroutine iread(vname, val)
  use mpi
  implicit none

  character(len=*), intent(in)  :: vname
  integer,          intent(out) :: val
  character(len=20) :: buf
  
  call cread(vname, buf)
  read(buf,*) val
  
  if (ismaster) write(*,"(a20,x,' = ',x,I12)")  vname, val  
end subroutine iread
  


!======================================================================
!
!======================================================================
subroutine iread3(vname, val)
  use mpi
  implicit none

  character(len=*), intent(in)  :: vname
  integer,          intent(out) :: val(3)
  character(len=20) :: buf
  
  call cread(vname, buf)
  read(buf,*) val(1:3)
  
  if (ismaster) write(*,"(a20,x,' = ',x,3I4)")  vname, val
end subroutine iread3




!======================================================================
! subroutine to read in a variable name, check with input file
! and read in the corresponding value.
!======================================================================
subroutine cread(vname, val)
  use mpi
  implicit none

  character(len=*),  intent(in)  :: vname
  character(len=20), intent(out) :: val
  character(len=40) :: buf, buf1
  integer :: paramf, ios, found, pos
  
  found  = 0
  paramf = 45
  open(paramf, file='param.dat', status='old', iostat=ios)
  
  do while ((ios == 0).and.(found == 0))
    read(paramf,'(A)',iostat=ios) buf
	 
    ! find the position of character "="
    pos  = scan(buf,'=')

    ! string before "=" will be checked with input
    buf1 = buf(1:pos-1)

    ! string after "=" is the output value
    val  = buf(pos+1:)
	   
    if (buf1 == vname) found = 1
  end do

  close(paramf)

  if (found == 0) then
    write(*,*) "Could not find ", vname , " in param.dat"
    call MPI_ABORT(MPI_COMM_WORLD, 911, mpi_err)
!!!    val=''	
  end if        
end subroutine cread

