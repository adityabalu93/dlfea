!=======================================================================
! Module for storing arrays and allocation routines
!=======================================================================
module aAdjKeep

  !...  Knot vectors in u and v directions
  real(8), allocatable :: U_KNOT(:), V_KNOT(:), W_KNOT(:)

  !...  Control Net, Reference Configuration
  real(8), allocatable :: B_NET(:,:,:,:)
  
  !...  Projective Coordinate control net
  real(8), allocatable :: BW(:,:,:,:)

  !...  The Load applied to each edge and element face
  real(8), allocatable :: LD_FACE(:,:), LD_EL(:,:)

  !...  The right hand side load vector G and left hand stiffness matrix K
  real(8), allocatable :: rhsGu(:,:),  rhsGm(:,:), rhsGp(:), &
                          lhsK11(:,:), lhsK12(:,:), &
                          lhsK22(:,:), lhsG(:,:), &
                          lhsD1(:,:),  lhsD2(:,:), lhsM(:)

  !...  IEN matches element number a local node number with global node number
  integer, allocatable :: IEN(:,:)

  !...  INN relate global node number to the (i,j) "NURBS coordinates"
  integer, allocatable :: INN(:,:)

  !...  IPER contains all master/slave relationships.
  integer, allocatable :: IPER(:)
  
  !...  D_Flag and P_Flag - Flags for Solid and Fluid Regions
  integer, allocatable :: D_Flag(:), P_Flag(:)
  
  !...  Matrix relating face number and local node to global node number
  integer, allocatable :: FACE_IEN(:,:)

  !... Mapping between Face element and Volumn element
  integer, allocatable :: F2E(:)

  !...  Face orientation matrix
  integer, allocatable :: FACE_OR(:), EL_TYP(:)

  !...  Boundary condition indicator for global nodes and edges respectively
  integer, allocatable :: IBC(:,:), IBC_FACE(:,:)

  !...  Prescribed displacements of Dirichlet nodes.
  real(8), allocatable :: DIR_BC(:,:)

  !...  Array containing rho, mu and lambda
  real(8), allocatable :: DENS(:), VISC(:), BVISC(:)

  !...  Solution vectors
  real(8), allocatable :: dg(:,:),   dgold(:,:), &
                          ug(:,:),   ugold(:,:), &
                          ugm(:,:),  ugmold(:,:), &
                          acg(:,:),  acgold(:,:), &
                          acgm(:,:), acgmold(:,:), &
                          pg(:),     pgold(:), &
                          fg(:,:), xg(:,:)
  
  !...  For saving forces on all processors
  real(8), allocatable :: force(:,:,:)

  !...  Rotation matrix for symmetry B.C.
  real(8), allocatable :: rot4sym(:,:)

  !...  Array for patch id: four integers representing right or left, w, v, u
  !...  e.g., 1 2 3 4 means: RHS, 2nd patch in W, 3rd in V, 4th in U
  integer, allocatable :: meshID(:,:)

  !...  For controling Krylov subspace
  integer, allocatable :: Kspaceu(:)
 
end module aAdjKeep


!=======================================================================
!
!=======================================================================
module commonvar
  implicit none
  save

  integer, parameter :: NSD = 3

  integer :: Pu, Qu, Ru, MCPu, NCPu, OCPu, NNODZu, &
             NELu, NGAUSSu, NGAUSSv, NGAUSSw, maxNNODZu, &
             Pp, Qp, Rp, MCPp, NCPp, OCPp, NNODZp, NELp, &
             CLOSED_U_flag, CLOSED_V_flag, NGAUSS, NSHLu, &
             NSHLp, NEL_NZA, CLOSED_W_flag, NFACEu, &
             NFACEp, isolver, Nstep, NS_NL_itermax, ifq, ifq_tq, &
             NS_hess_flag, NS_GMRES_itermax, NS_GMRES_itermin, &
             Adap_Int_Level


  ! integer giving the node number of the stick's tip, to be used in outputting
  ! the tip displacement each timestep
  integer :: tipnode

  real(8) :: DetJ, ENRGY, ERROR, beti, alfi, almi, rhoinf, &
             gami, Dtgl, Delt, Tper, NS_GMRES_tol, mu, rho, lambda, &
             DetJinv, theta, thetd, thedd, thetaOld, thetdOld, theddOld, &
             CoefL, coefD, NS_NL_Utol, NS_NL_Ptol, DENS_Air, VISC_Air, &
             WBC_Penalty, WBC_TauTan, WBC_TauNor, InitPress
	
  integer :: Mode
  real(8) :: T_START
end module commonvar



!=======================================================================
!
!=======================================================================
module commonpar
  implicit none
  save

  real(8), parameter :: pi = 3.14159265358979323846264338328d0
end module commonpar


!=======================================================================
!
!=======================================================================
module aaOutBC

  !...  Number of Resistance and Imp. Surfaces
  integer :: NumResSurf, NumImpSurf, NumImpValues

  !...  Location of Surfaces (face orientation)
  integer, allocatable :: Res_Surf_Loc(:), Imp_Surf_Loc(:)

  !...  Res and Imp Values
  real(8), allocatable :: Imp_Surf_Val(:), &
                          lhsBimp(:,:), &
                          qrate(:), qrate_old(:)
end module aaOutBC


!===============================================================
!    1 ~ NP_HUB: Patches for the hub
! NP_HUB+1 ~ NP_SH : Patches for the blade
! NP_SH +1 ~ NP_SH1: Patches for V bending strips
! NP_SH1+1 ~ NP+SH2: Patches for U bending strips
! NP_SH  is the total number of shell patches without strips
! NP_SH2 is the total number of shell patches including strips
!===============================================================
module commonvar_shell
  implicit none
  save

!  ! Solution vectors
!  real(8), allocatable :: dsh(:,:), dshold(:,:), &
!                          ush(:,:), ushold(:,:), &
!                          ash(:,:), ashold(:,:)

  ! verying important mapping
  ! mapping between processors, and mapping between shell and solid
  integer, allocatable :: ProcMap(:), ShSoMap(:,:)
  
  integer :: Material_Shell, NGauss_TH_Shell, NBC_set, Material_Stent
  real(8) :: Torque_SH, Torque_SH_2
  real(8) :: Density_Shell, Thickness_Shell, E_Shell, nu_Shell
  real(8) :: Density_Stent, Thickness_Stent, E_Stent, nu_Stent
  real(8) :: Damping_a, Damping_b
  
  ! for Fung material
  real(8) :: Coef_a, Coef_b, Coef_c 
  real(8) :: MtCoefs(100), fib_ang
  ! fiber direction in contrav.
  real(8), allocatable :: m_contrav(:,:,:)
  
  ! for computing area
  integer :: N_ar
  real(8), allocatable :: Nodes_local(:,:), Nodes_all(:,:)

  integer, parameter :: overflow=400
end module commonvar_shell

! common vars for contact
module commonvar_contact
  implicit none
  save
  
  real(8) :: contact_k1, contact_k2, contact_k3, contact_cutoff
  integer :: contact_pow
  real(8) :: contact_lox, contact_loy, contact_loz
  real(8) :: contact_hix, contact_hiy, contact_hiz
  integer :: contact_nx, contact_ny, contact_nz

end module commonvar_contact



!------------------------------------------------------------------------
! Module for defining shell types
!------------------------------------------------------------------------     
module types_shell

  implicit none

  ! type for gauss point
  type :: gptype
     ! number of actual points (okay to allocate more)
     real(8), allocatable :: xg(:,:,:), nor(:,:,:), param(:,:,:), &
          cparam(:,:,:), backxi(:,:,:), &
          gw(:,:), detJ(:,:), tv1(:,:,:), tv2(:,:,:), &
          ush(:,:,:), ash(:,:,:), &
          traction(:,:,:), contactForce(:,:,:), closestDist(:,:), &
          contactVelForce(:,:,:), contactLhs(:,:,:), contactLhsVel(:,:,:)
     integer, allocatable :: closestPoint(:,:,:), cien(:,:), &
          cien_sgn(:,:), cnshl(:), celem(:), backelem(:,:)
  end type gptype

  ! Declare type mesh
  type :: mesh

     ! gauss points for the nurbs mesh
    type(gptype) :: gp

    ! degree in U and V for each patch
    integer, allocatable :: P(:), Q(:), boundary(:)

    ! patch type
    ! 0-blade; 1-bending strips; 2-shear web; ...
    integer, allocatable :: PTYPE(:)

    ! Knot vectors in u and v directions for each element
    real(8), allocatable :: U_KNOT(:,:), V_KNOT(:,:)

    ! Size of the knot vector for each elements (e.g. NUK=P+MCP+1)
    integer, allocatable :: NUK(:), NVK(:)

    ! Control Net
    real(8), allocatable :: B_NET(:,:), B_NET_U(:,:), B_NET_D(:,:)

    ! Boundary condition indicator for global nodes and edges
    ! respectively
    integer, allocatable :: IBC(:,:)

    real(8), allocatable :: contactarea(:)
    integer, allocatable :: iscontact(:)

    ! array to store force vectors on the wind turbine blade
    real(8), allocatable :: FORCE(:,:)

    ! Global connectivity array
    integer, allocatable :: IEN(:,:), INN(:,:)

    ! array giving the patch number for each element
    integer, allocatable :: elementPatchNumbers(:)

    ! number of shape functions for every element
    integer, allocatable :: NSHL(:)

    ! Bezier extraction operator
    real(8), allocatable :: Ext(:,:,:)

    ! array for neighboring elements
    integer, allocatable :: elementNeighbors(:,:)

    ! Array for closest points
    !integer, allocatable :: CLE(:,:)
    !real(8), allocatable :: CLP(:,:,:)

    integer :: NGAUSS, NNODE, NEL, maxNSHL, nIBC

    ! array for Solution vectors
    real(8), allocatable :: dsh(:,:), dshold(:,:), &
                            ush(:,:), ushold(:,:), &
                            ash(:,:), ashold(:,:)
  end type mesh


  ! Declare type mesh for multi-patch
  type :: mesh_mp
    ! degree in U and V for each patch
    integer, allocatable :: P(:), Q(:)

    ! number of control points in U and V for each patch
    ! (no need for T-spline)
    integer, allocatable :: MCP(:), NCP(:)

    ! Total number of control points and elements for each patch
    integer, allocatable :: NNODE(:), NEL(:)

    ! patch type
    ! 1-blade surface; 0-bending strips; 2-shear web; ...
    integer, allocatable :: PTYPE(:), TPTYPE(:,:)

    ! Knot vectors in u and v directions for each patch
    real(8), allocatable :: U_KNOT(:,:), V_KNOT(:,:)

    ! Control Net
    real(8), allocatable :: B_NET(:,:,:)

    ! Boundary condition indicator for global nodes and edges
    ! respectively
    integer, allocatable :: IBC(:,:,:)

    real(8), allocatable :: contactarea(:)
    integer, allocatable :: iscontact(:)
    
    ! array to store force vectors on the wind turbine blade
    real(8), allocatable :: FORCE(:,:,:)

    ! Mapping between patches and global reduced numbering
    ! e.g., MAP(1,2) = 3 means 1st patch, 2nd node points to 
    !   global reduced node number 3
    integer, allocatable :: MAP(:,:)

    ! array to store indices of neighboring elements
    integer, allocatable :: elementNeighbors(:,:,:)

    ! Define NSHL
    integer, allocatable :: Pb(:,:), Qb(:,:), nIBC(:), &
                            enIBC(:), bound_elm(:,:)
    integer, allocatable :: NSHL(:,:), boundary(:,:)
    integer, allocatable :: IEN(:,:,:)   
    real(8), allocatable :: EXT(:,:,:,:)
  end type mesh_mp


  ! Declare type shell
  type :: shell
    ! number of patches for Blade Surface (S). Matches the fluid mesh
    ! number of patches for blade structure (B). May include shear webs
    ! number of total patches including bending strips (T)
    integer :: NPS, NPB, NPT

    integer :: M_Flag, T_Flag
    real(8) :: RHSGtol, G_fact(3), RHSGNorm, CGTol

    ! row, col, and total of nonzero entries for sparse structure
    integer, allocatable :: row(:), col(:)
    integer :: icnt

    ! The right hand side load vector G and left hand stiffness matrix K
    real(8), allocatable :: RHSG(:,:), LHSK(:,:), &
                            RHSG_EXT(:,:), RHSG_GRA(:,:)

!    ! Solution vectors
!    real(8), allocatable :: yg(:,:), dg(:,:), tg(:), &
!                            mg(:), dl(:,:)

    ! material matrix for composite
    integer :: NMat
    real(8), allocatable :: matA(:,:,:), matB(:,:,:), matD(:,:,:)

    ! number of newton iterations for shell
    integer, allocatable :: Nnewt(:)
  end type shell

  ! type for a coarse background grid for use in contact
  type contactCells

     ! the boundaries in space of the FULL fluid mesh (all processors)
     real(8) :: lowerBounds(3), upperBounds(3)

     ! the number of partitions in each direction
     integer :: nCellsDir(3)
     
     ! an array to store a linked list of gauss point indices.  This is the
     ! size of the number of gauss points, with the same indexing scheme,
     ! and each element stores the next indices for the linked list (with
     ! zero indicating the end of the list)
     integer, allocatable :: gpLinkedList(:,:,:)

     ! an array to store the head of the linked list associated to each 
     ! background cell.  First index ranges over cells, 2nd idnex is of size
     ! 2.  Each such row stores a pair of indices identifying the first
     ! gauss point in the linked list associated to the cell with the index
     ! of the row.  
     integer, allocatable :: hashTable(:,:)

     ! used to speed up iteration over the hash table by giving a
     ! directory of the occupied bins
     logical, allocatable :: isOccupied(:)
     integer, allocatable :: occupiedCells(:)
     integer :: numOccupied

  end type contactCells

  contains
    
    ! routines to convert between ways of indexing into the background 
    ! contact cells

    subroutine contactCells_ijk2cell(cells,i,j,k,cell)
      
      type(contactCells) :: cells
      integer, intent(in) :: i,j,k
      integer, intent(out) :: cell
      
      integer :: M,N

      M = cells%nCellsDir(1)
      N = cells%nCellsDir(2)

      cell = i + (j-1)*M + (k-1)*N*M
      
    end subroutine contactCells_ijk2cell

    subroutine contactCells_cell2ijk(cells,cell,i,j,k)

      type(contactCells) :: cells
      integer, intent(out) :: i,j,k
      integer, intent(in) :: cell
      
      integer :: M,N,ij

      M = cells%nCellsDir(1)
      N = cells%nCellsDir(2)

      k = (cell-1)/(M*N) + 1   ! integer div throws away remainder
      ij = cell - (k-1)*M*N    ! cell number in top k plane

      ! repeat locally in k plane
      j = (ij-1)/M + 1
      i = ij - (j-1)*M

    end subroutine contactCells_cell2ijk

    ! routine to get the i,j, and k coordinates of the cell containing a
    ! point x (assume even spacing of grid for fast checking)
    subroutine contactCells_x2ijk(cells,x,i,j,k)

      type(contactCells) :: cells
      integer, intent(out) :: i,j,k
      real(8), intent(in) :: x(3)

      integer :: M,N,O
      real(8) :: spans(3), dx(3), nx(3)

      M = cells%nCellsDir(1)
      N = cells%nCellsDir(2)
      O = cells%nCellsDir(3)
      
      spans = cells%upperBounds - cells%lowerBounds
      dx = x - cells%lowerBounds
      nx = (cells%nCellsDir)*dx/spans
      
      i = ceiling(nx(1))
      j = ceiling(nx(2))
      k = ceiling(nx(3))
      
    end subroutine contactCells_x2ijk

    ! subroutine to get the cell index for a point (through above)
    subroutine contactCells_x2cell(cells,x,cell)

      type(contactCells) :: cells
      integer, intent(out) :: cell
      real(8), intent(in) :: x(3)

      integer :: i,j,k

      call contactCells_x2ijk(cells,x,i,j,k)
      call contactCells_ijk2cell(cells,i,j,k,cell)
      
    end subroutine contactCells_x2cell

    ! subroutine to build the hash table of cells to gauss points, 
    ! given a gauss point type object in a mesh
    subroutine contactCells_buildHash(cells,sur)

      type(contactCells), intent(inout) :: cells
      type(mesh), intent(in) :: sur

      integer :: jel, igp, oldHead1, oldHead2, cell, i

      ! (assume everything has been allocated correctly)
      
      ! initialize the hash table and linked list array to zero
      !cells%hashTable = 0 ! only accessed for occupied cells; no init needed
      cells%gpLinkedList = 0

      ! intialize occupation info
      do i=1,cells%numOccupied ! (avoid looping over all cells)
         cells%isOccupied(cells%occupiedCells(i)) = .false.
         cells%hashTable(cells%occupiedCells(i),:) = 0
      enddo
      !cells%occupiedCells = 0 ! doesn't need to be initialized; only
                               ! assigned values are accessed
      cells%numOccupied = 0
      !cells%isOccupied = .false. ! not necessary
      
      ! iterate over the gauss points, find the cell index for each
      ! and update the table accordingly
      do jel = 1,sur%nel
         
         ! ignore zero area surface elements
         if(sur%gp%gw(jel,1) == 0) then
            cycle
         endif

         if(sur%elementPatchNumbers(jel) > 3) then
           cycle
         endif

         ! iterate over the surface element's gauss points
         do igp = 1,(sur%ngauss)**2

            ! find the gauss point's cell
            call contactCells_x2cell(cells,sur%gp%xg(jel,igp,:),cell)

            ! update occupation info
            if(.not. cells%isOccupied(cell)) then
               cells%numOccupied = cells%numOccupied + 1
               cells%occupiedCells(cells%numOccupied) = cell
               cells%isOccupied(cell) = .true.
            endif

            ! retrieve the previous head of the linked list
            oldHead1 = cells%hashTable(cell,1)
            oldHead2 = cells%hashTable(cell,2)

            ! fill in the hash table entry
            cells%hashTable(cell,1) = jel
            cells%hashTable(cell,2) = igp

            ! complete the linkage
            cells%gpLinkedList(jel,igp,1) = oldHead1
            cells%gpLinkedList(jel,igp,2) = oldHead2

         enddo
      enddo

      ! DEBUG
      !cells%numOccupied = &
      !     cells%nCellsDir(1)*cells%nCellsDir(2)*cells%nCellsDir(3)
      !do i=1,cells%numOccupied
      !   cells%occupiedCells(i) = i
      !enddo
      !cells%isOccupied = .true.


    end subroutine contactCells_buildHash

    ! subroutine to set up a contactCells instance, given grid parameters
    ! and a shell instance
    subroutine contactCells_init(cells,lowerBounds,upperBounds,nCellsDir,nrb)

      type(contactCells), intent(inout) :: cells
      real(8), intent(in) :: lowerBounds(3), upperBounds(3)
      integer, intent(in) :: nCellsDir(3)
      type(mesh), intent(in) :: nrb

      integer :: numcells

      ! assign the bounds and numbers of cells
      cells%lowerBounds = lowerBounds
      cells%upperBounds = upperBounds
      cells%nCellsDir = nCellsDir

      ! allocate space for the hash table based on the number of cells
      numcells = nCellsDir(1)*nCellsDir(2)*nCellsDir(3)
      allocate(cells%hashTable(numcells,2))
      cells%hashTable = 0

      ! allocate space for the gauss point linked list based on the
      ! maximum number of gauss points in nrb
      allocate(cells%gpLinkedList(NRB%NEL,(NRB%NGAUSS)**2,2))

      ! allocate arrays for occupied cells
      allocate(cells%occupiedCells(numcells))
      allocate(cells%isOccupied(numcells))
      cells%numOccupied = 0
      cells%isOccupied = .false.

    end subroutine contactCells_init

    ! subroutine to allocate a mesh's gptype
    subroutine mesh_allocInitGptype(nrb)
      type(mesh), intent(inout) :: nrb
      integer :: ngpsurf

      ngpsurf = NRB%NGAUSS*NRB%NGAUSS

      allocate(NRB%gp%xg(NRB%NEL,ngpsurf,3),NRB%gp%nor(NRB%NEL,ngpsurf,3), &
           NRB%gp%param(NRB%NEL,ngpsurf,2), &
           NRB%gp%cparam(NRB%NEL,ngpsurf,2), &
           NRB%gp%ush(NRB%NEL,ngpsurf,3),&
           NRB%gp%ash(NRB%NEL,ngpsurf,3),&
           NRB%gp%traction(NRB%NEL,ngpsurf,3), &
           NRB%gp%backxi(NRB%NEL,ngpsurf,3),NRB%gp%gw(NRB%NEL,ngpsurf), &
           NRB%gp%detJ(NRB%NEL,ngpsurf), NRB%gp%backelem(NRB%NEL,ngpsurf), &
           NRB%gp%tv1(NRB%NEL,ngpsurf,3), NRB%gp%tv2(NRB%NEL,ngpsurf,3), &
           NRB%gp%contactForce(NRB%NEL,ngpsurf,3), &
           NRB%gp%contactLhs(NRB%NEL,ngpsurf,9), &
           NRB%gp%contactLhsVel(NRB%NEL,ngpsurf,9), &
           NRB%gp%contactVelForce(NRB%NEL,ngpsurf,3), &
           NRB%gp%closestDist(NRB%NEL,ngpsurf), &
           NRB%gp%closestPoint(NRB%NEL,ngpsurf,2), &
           NRB%gp%cien(NRB%NEL*ngpsurf,2*NRB%maxNSHL), &
           NRB%gp%cien_sgn(NRB%NEL*ngpsurf,2*NRB%maxNSHL), &
           NRB%gp%cnshl(NRB%NEL*ngpsurf),&
           NRB%gp%celem(NRB%NEL*ngpsurf))

      ! fill with zeros as sentinel values
      NRB%gp%xg = 0.0
      NRB%gp%param = 0.0
      NRB%gp%cparam = 0.0
      NRB%gp%gw = 0.0
      NRB%gp%nor = 0.0
      NRB%gp%backxi = 0.0
      NRB%gp%detJ = 0.0
      NRB%gp%backelem = 0
      NRB%gp%tv1 = 0.0
      NRB%gp%tv2 = 0.0
      NRB%gp%ush = 0.0
      NRB%gp%ash = 0.0
      NRB%gp%traction = 0.0
      NRB%gp%contactForce = 0.0
      NRB%gp%contactLhs = 0.0
      NRB%gp%contactLhsVel = 0.0
      NRB%gp%contactVelForce = 0.0
      NRB%gp%closestDist = -1.0
      NRB%gp%closestPoint = 0
      NRB%gp%cien = 0
      NRB%gp%cien_sgn = 0
      NRB%gp%cnshl = 0
      NRB%gp%celem = 0

    end subroutine mesh_allocInitGptype

end module types_shell

!=======================================================================
! Module for defining immersed types
!=======================================================================   
module types_im

  implicit none

  type :: imtype
    real(8), allocatable :: IntPt(:,:,:), EBOUND(:,:,:), PBOUND(:,:)
    !real(8), allocatable :: SGP(:,:,:), SGW(:,:), SDetJ(:,:), Snor(:,:,:)
    integer, allocatable :: EFlag(:), IFlag(:,:), NumInt(:), NEL_dir(:)
    integer :: NEL, NEL_IM, NumInt_Max, NLevel
  end type imtype
end module types_im



!=======================================================================
! Module for defining subcells
!=======================================================================
module types_subcell

  type sctype
    real(8) :: vpu(2), vpv(2)
    real(8) :: mid(2)
    real(8), allocatable :: gpu(:), gpv(:), gwu(:), gwv(:)
    integer, allocatable :: gpflag(:,:)
    integer :: NGAUSSu, NGAUSSv, EFlag
  end type sctype

end module types_subcell


module types_cell

  use types_subcell

  type cetype
    type(sctype) :: subcell(4)
  end type cetype

end module types_cell

!----------------------------------------------------------------------
!     Module for mpi
!----------------------------------------------------------------------
module mpi
  implicit none
  save

  include "mpif.h"

  integer, parameter :: mpi_master = 0
!   integer, parameter :: maxtask    = 50
!   integer, parameter :: maxseg     = 15000
!
  integer :: numnodes, myid, mpi_err
  integer :: status(MPI_STATUS_SIZE)
!   integer :: lstrideu, lstridep, numtask
!   integer :: lfrontu, maxfrontu
!   integer :: lfrontp, maxfrontp
!   integer :: nlworku, nlworkp
  integer :: itag, iacc, iother, numseg, isgbeg, itkbeg, isgend
!   integer :: sevsegtypeu(maxtask,15)
!   integer :: sevsegtypep(maxtask,15)
  logical :: ismaster
            
  !integer, allocatable :: ilworku(:), ilworkp(:)
end module mpi

