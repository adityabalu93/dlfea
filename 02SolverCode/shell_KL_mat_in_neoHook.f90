!======================================================================
! this routine computes the shell stress resultants for a hyperelastic
! material. the specific material law is defined in a subroutine, such that
! different material laws can easily be substituted.
!----------------------
! denomination:
! N_cu,M_cu,dN_cu,dM_cu = normal forces, moments and derivatives
! G0, g0 = mid-plane base vectors
! G3,dG3, g3,dg3 = unit length directors and derivatives G3,alpha
! G.g = 3 base vectors in shell continuum
! mue, km = shear and bulk modulus
! dE_cu,dK_cu = membrane and curvature strain derivatives
! dE = Green-Lagrange strain derivatives
! C = left Cauchy-Green deformation tensor
! Ci = inverse of C
! J = determinant of deformation gradient
! Gij, Gconij = convariant and contravariant metric coefficients
! S,dS = PK2 stress and derivative as vectors
! St = PK2 stress tensor
! CC = material tensor
! D = material matrix
!======================================================================
subroutine shell_mat_in_neoHook(nshl, G0_r, G3_r, dG3_r, g0, g3, dg3, &
                                 mu, thi, ngauss, dE_cu, dK_cu, &
                                 N_cu, dN_cu, M_cu, dM_cu)
  implicit none      

  integer, intent(in) :: nshl, ngauss

  real(8), intent(in) :: G0_r(3,2), G3_r(3), dG3_r(3,2), &
                         g0(3,2), g3(3), dg3(3,2), &
                         mu, thi, dE_cu(3,3*NSHL), &
                         dK_cu(3,3*NSHL)

  real(8), intent(out) :: N_cu(3), M_cu(3), dN_cu(3,3*NSHL), &
                          dM_cu(3,3*NSHL)

  real(8) :: G_r(3,3), g(3,3), Gij(3,3), subGC(2,2), Gconij(3,3),  &
             C(3,3), Ci(3,3), CC(3,3,3,3), Dtemp(4,4), D(3,3),  &
             St(3,3), S(3), GGCi(3,3,3,3), gp(ngauss), gw(ngauss)
             
  real(8) :: zeta, detJ, rtmp, dC33, subJ

  integer :: i, j, k, l, iz

  real(8) :: CixCi(3,3,3,3), CipCi(3,3,3,3)


  N_cu  = 0.0d0
  M_cu  = 0.0d0
  dN_cu = 0.0d0
  dM_cu = 0.0d0

  C    = 0.0d0
  Gij  = 0.0d0 
  subGC   = 0.0d0
  D    = 0.0d0
  GGCi = 0.0d0

  ! 4th order tensor products for CC
  CixCi = 0.0d0
  CipCi = 0.0d0

  St = 0.0d0
  CC = 0.0d0

  ! thickness integration loop
  call genGPandGW_shell(gp, gw, ngauss) 

  do iz = 1, ngauss
  
    zeta = 0.5d0*thi*GP(iz)

    G_r(1:3,1:2) = G0_r(1:3,1:2) + zeta*dG3_r(1:3,1:2)
    G_r(1:3,3)   = G3_r(1:3)

    g(1:3,1:2) = g0(1:3,1:2) + zeta*dg3(1:3,1:2)
    g(1:3,3)   = g3(1:3)

    ! Metric and C tensor
!    Gij = matmul(transpose(G_r),G_r)

    ! keep the following line, but commented out. it's not clear yet if C
    ! should be computed like this or like in the next for-loops, both
    ! cases have justification. if you want, you can try both, in my
    ! tests it didn't change anything on the results.
!    C = matmul(transpose(g),g)

  
    ! linear strain distribution through thickness
   do i = 1, 2
     do j = 1, 2
       C(i,j) = sum(g0(:,i)*g0(:,j)) + 2.0d0*zeta*sum(g0(:,i)*dg3(:,j))
       Gij(i,j) = sum(G0_r(:,i)*G0_r(:,j)) + 2.0d0*zeta*sum(G0_r(:,i)*dG3_r(:,j))
     end do
   end do

   do i = 1, 2
     C(i,3) = sum((g0(:,i)+zeta*dg3(:,i)) * g3(:))   ! should be zero
     C(3,i) = C(i,3)                                 ! should be zero
	 
     Gij(i,3) = sum((G0_r(:,i)+zeta*dG3_r(:,i)) * G3_r(:))   ! should be zero
     Gij(3,i) = Gij(i,3)                                 ! should be zero
   end do
   C(3,3) = sum(g3*g3)
   Gij(3,3) = sum(G3_r*G3_r)
  
!!!   write(*,*) C(1,3), C(2,3), C(3,1), C(3,2)


    ! contravariant metric
    call mat_inverse_3x3(Gij, Gconij, rtmp)

    do i = 1, 2
      do j = 1, 2
        subGC(i,j) = sum(Gconij(i,:)*C(:,j))
      end do
    end do

    ! determinant of GC
    subJ = subGC(1,1)*subGC(2,2) - subGC(1,2)*subGC(2,1)
	  	  
!      if (detJ <= 0.0d0) then
!        write(*,*) "ERROR: zero or negative detJ", detJ
!        stop
!      end if
	  
      call mat_inverse_3x3(C, Ci, rtmp)

	  do i = 1, 3
	    do j = 1, 3
	      do k = 1, 3
	        do l = 1, 3
	          CixCi(i,j,k,l) = Ci(i,j)*Ci(k,l)
	          CipCi(i,j,k,l) = 0.5d0*(Ci(i,k)*Ci(j,l)+Ci(i,l)*Ci(j,k))
	          GGCi(i,j,k,l)  = Gconij(i,3)*Gconij(j,3)*Ci(k,l)+Gconij(k,3)*Gconij(l,3)*Ci(i,j)
	        end do
	      end do
	    end do
	  end do

	  St = mu*(Gconij-1.0d0/subJ*Ci)
      CC = mu*1.0d0/subJ*(2.0d0*CipCi-CixCi)-2.0d0*mu*GGCi
 
    ! Stress vector and material matrix
    S = (/ St(1,1), St(2,2), St(1,2) /)

    Dtemp(1,:) = (/ CC(1,1,1,1),CC(1,1,2,2),CC(1,1,1,2),CC(1,1,3,3) /)
    Dtemp(2,:) = (/ CC(2,2,1,1),CC(2,2,2,2),CC(2,2,1,2),CC(2,2,3,3) /)
    Dtemp(3,:) = (/ CC(1,2,1,1),CC(1,2,2,2),CC(1,2,1,2),CC(1,2,3,3) /)
    Dtemp(4,:) = (/ CC(3,3,1,1),CC(3,3,2,2),CC(3,3,1,2),CC(3,3,3,3) /)

    ! static condensation of S33=0
    do i = 1, 3
      do j = 1, 3
        D(i,j) = Dtemp(i,j)-Dtemp(i,4)*Dtemp(4,j)/Dtemp(4,4)
      end do
    end do

    ! updated thickness due to plane stress
    N_cu  = 0.5d0*thi*S*GW(iz) + N_cu
    dN_cu = 0.5d0*thi*matmul(D,(dE_cu+zeta*dK_cu))*GW(iz) + dN_cu
    M_cu  = 0.5d0*thi*S*zeta*GW(iz) + M_cu
    dM_cu = 0.5d0*thi*matmul(D,(dE_cu+zeta*dK_cu))*zeta*GW(iz) + dM_cu
  end do

end subroutine shell_mat_in_neoHook