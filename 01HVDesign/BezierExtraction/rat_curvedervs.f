      
c     
c     This function computes the first 0-d derivatives of a NURBS
c     curve. It returns CK, a vector whose kth entry is the 
c     derivative of the surface with respect to u k-1 times.
      
c     
c     
c     Algorithm from Piegl, Les. "The NURBS Book". Springer-Verlag: 
c     Berlin 1995; p. 127.
c     
c     
c June 25, 2003
c J. Austin Cottrell
c CES Graduate Student
c Texas Institute for Computational Engineering Science
c University of Texas at Austin



      subroutine rat_curvedervs(i,pl,m,u,u_knotl, Pw,d,ck)

c      use aAdjKeep
      
      include "common.h"

c     --------------VARIABLE DECLARATIONS--------------------------------
c...  span index, degree, number of control points, counters
      integer i, pl, m, k, j,d

c...  integration point, knot vector, projective control points, derivatives
      real(8) u,ck(d+1,NSD), Aders(d+1,NSD), wders(d+1), 
     &     u_knotl(m+pl+1), Pw(m,NSD+1)
      real(8) v(NSD), binn 

c     -------------------------------------------------------------------



c     nonrational curve derivatives in projective plane


      call nonrat_curve_derivs(i, pl, m, u, u_knotl, Pw(:,1:NSD),
     &     NSD,d, Aders)
      call nonrat_curve_derivs(i, pl, m, u, u_knotl, Pw(:,NSD+1),
     &     1,d, wders)
      
      ck = 0d+0
      
      do k = 0,d
         v(:) = Aders(k+1,:)
         do j = 1,k
            call FUBin(k,j,binn)
            v(:) = v(:) - binn*wders(j+1)*CK(k-j+1,:)
         enddo
         ck(k+1,:) = v(:)/wders(1)
      enddo
      
      return
      end

c -----------------------------------------------------------------------             
      subroutine FUBin(n,i,binn)
      
      integer n,i
      real(8) binn,tempn,tempi,tempni

c     helper function to compute binomial coefficients
      call factorial(n, tempn)
      call factorial(i, tempi)
      call factorial(n-i, tempni)
      
      binn = tempn/(tempi*tempni)
      
      return
      end
