c       This subroutine generates the IEN matrix, which relates element numbers
c       and local node numbers to the appropriate global node numbers. The
c       routine also generates the INN matrix, which relates global node
c       number to the "NURBS coordinates" of the node.
	
	  subroutine bezier_num(p_deg, m, knot, nb)


	    use aAdjKeep		! this f90 module contains all allocatables
c       
	    include "common.h"	! common file defines all variables
c       
c...    Local variables
	    integer :: i, j, k, r, p_deg, m, nb, a, b, mult, temp, 
     & s
      real(8) :: knot(m), alphas(p_deg), alpha, numer
      character(len=8) :: fmt
      
      m = m - p_deg - 1

C       write(*,*) "Degree is ", p_deg, ". m is ",m, "knot(:)", knot(1:m)
      a = p_deg+1
      b = a+1
      nb = 1
      
	    do while (b .LE. m)
        
C         Initialize the next txtraction operator
        
C         ount multiplicity of the knot at location b.
        i = b
        do while ((b .LE. m) .AND. (knot(b+1) .EQ. knot(b)))
          b = b + 1
C           write(*,*) "b = ", b
        end do
        mult = b - i +1
C         write(*,*) "mult = ", mult
        
        If (mult .LT. p_deg) Then
          nb = nb+1
          If (b .LE. m) Then
              a = b
              b = b+1
          end If
        Else If (mult .EQ. p_deg) Then
          If (b .LE. m) Then
              nb = nb+1
              a = b
              b = b+1
          end If
        End If
        
      end do
      


      
	  return
	  end
