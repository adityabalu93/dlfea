      
c     
c     This function uses Newton interation to project a point onto
c       a NURBS curve.
c      
c     
c     

c     
c     
c July 23, 2003
c J. Austin Cottrell
c CES Graduate Student
c Texas Institute for Computational Engineering Science
c University of Texas at Austin



      subroutine projection_curve(PT,span,pl,m,u_knotl, Pw,u)

c      use aAdjKeep
      
      include "common.h"

c     --------------VARIABLE DECLARATIONS--------------------------------
      integer i, pl, m, k, j, span

      real(8) u,
     &     u_knotl(m+pl+1), Pw(m,NSD+1), eps1, eps2, u_i, u_i1,
     &     norm_temp, derv_temp, dot_temp,PT(NSD), C(3,NSD),
     &     temp1, temp2, temp3
     

c     -------------------------------------------------------------------

      C  = 0d+0

c...  tolerances
      eps1 = (u_knotl(span + 1) - u_knotl(span))/1000d0
      eps2 = .001

c...  initial guess
      u_i = (u_knotl(span + 1) + u_knotl(span))/2d0;
      u_i1 = u_i;
      call rat_curvedervs(span,pl,m,
     &           u_i,u_knotl, Pw,2,C)

      
      norm_temp = 0d+0
      do i = 1,NSD
         norm_temp = norm_temp + (C(1,i)-PT(i))**2
      enddo
      norm_temp = sqrt(norm_temp)

      dot_temp = 0d+0
      derv_temp = 0d+0
      do i = 1,NSD
         dot_temp = dot_temp + C(2,i)*(C(1,i) - PT(i))
         derv_temp = derv_temp + (C(2,i))**2
      enddo
      dot_temp = abs(dot_temp)
      derv_temp = sqrt(derv_temp)


      do
         if ((norm_temp.le.eps1).or.
     &        ((dot_temp/(derv_temp*norm_temp)).le.eps2)) exit
         

         temp1 = 0d+0
         temp2 = 0d+0
         temp3 = 0d+0
         do i = 1,NSD
            temp1 = temp1 + C(2,i)*(C(1,i) - PT(i))
            temp2 = temp2 + C(3,i)*(C(1,i) - PT(i))
            temp3 = temp3 + C(2,i)**2
         enddo
         

         u_i = u_i1
         u_i1 = u_i - temp1/(temp2 + temp3)

         if (u_i1.lt.u_knotl(span)) then
            u_i1 = u_knotl(span)
         elseif (u_i1.gt.u_knotl(span + 1)) then
            u_i1 = u_knotl(span +1)
         endif
         


         norm_temp = 0d+0
         do i = 1,NSD
            norm_temp = norm_temp + (C(2,i))**2
         enddo
         norm_temp = sqrt(norm_temp)*abs(u_i1 - u_i)


         if (norm_temp.lt.eps1) exit


         call rat_curvedervs(span,pl,m,
     &        u_i1,u_knotl, Pw,2,C)

         

         norm_temp = 0d+0
         do i = 1,NSD
            norm_temp = norm_temp + (C(1,i)-PT(i))**2
         enddo
         norm_temp = sqrt(norm_temp)


         dot_temp = 0d+0
         derv_temp = 0d+0
         do i = 1,NSD
            dot_temp = dot_temp + C(2,i)*(C(1,i) - PT(i))
            derv_temp = derv_temp + (C(2,i))**2
         enddo
         dot_temp = abs(dot_temp)
         derv_temp = sqrt(derv_temp)




      enddo

      u = u_i1

      return
      end
