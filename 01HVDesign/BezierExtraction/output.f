
      subroutine output

c------------------------------------------------------------------------c
c                                                                        c
c        Subroutine to open file and write refined vertex, edge          c
c        and element numbers. Initialze parameters of interest.          c
c                                                                        c
c------------------------------------------------------------------------c

      use aAdjKeep

      include "common.h"

      integer :: i, j, k, meshref
      meshref = 12

c
c...  Write in preliminary information
c
      open (meshref, file='refined_mesh.dat', status='unknown')
      write (meshref,*) NSD       ! number of spatial dimensions
      write (meshref,*) P, Q
      write (meshref,*) MCP, NCP
C       write (meshref,"(I2)", ADVANCE = "NO") P
C       write (meshref,"(I2)", ADVANCE = "YES") Q ! degree of curves in u then v direction
C       write (meshref,"(I3)", ADVANCE = "NO") MCP
C       write (meshref,"(I3)", ADVANCE = "YES") NCP ! number of control points
C                                                   ! in each direction


c
c...  Read knot vectors and control net
c
      do i = 1,MCP+P+1
         if (i < MCP+P+1) then
            write (meshref,"(F13.9)", ADVANCE = "NO") U_KNOT(i) ! write U_KNOT
         else
            write (meshref,"(F13.9)", ADVANCE = "YES") U_KNOT(i) 
         endif
      enddo

      do i = 1,NCP+Q+1
         if (i < NCP+Q+1) then
            write (meshref,"(F13.9)", ADVANCE = "NO") V_KNOT(i) ! write V_KNOT
         else
            write (meshref,"(F13.9)", ADVANCE = "YES") V_KNOT(i) 
         endif
      enddo

      do j = 1,(NCP)
         do i = 1,(MCP)
            do k = 1,NSD+1
               if (k < NSD+1) then
                  write (meshref,"(F13.9)", ADVANCE = "NO")
     &                 B_NET(i,j,k)
               else
                  write (meshref,"(F13.9)", ADVANCE = "YES") 
     &                 B_NET(i,j,k)
               endif
            enddo
         enddo
      enddo
      write (meshref,*) "1"

      

   
c
c...  Write in Master/Slave relationships. These arise from periodic boundary
c       conditions, corners in the geometry, or any other case where two
c       control points must always remain equal.
c
!      do i = 1, NNODZ
!         write (meshref,*) IPER(i) 
!      enddo

c
c...  Write in Boundary/Edge information
c
!      write (meshref,'(I2)', ADVANCE = "NO") CLOSED_U_flag
!      write (meshref,'(I2)', ADVANCE = "YES") CLOSED_V_flag               


c...  Write in boundary condition indicator at each node, in each direction
c     0 - nothing, 1 - Dir., 2 - Neu., 3 - Periodic
!      do i = 1,NNODZ
!         write (meshref,'(I2)', ADVANCE = "NO") IBC(i,1)
!         write (meshref,'(I2)', ADVANCE = "NO") IBC(i,2)
!         write (meshref,'(I2)', ADVANCE = "YES") IBC(i,3)
!      enddo

c...  Write Dirichlet Condition array.
!      do i = 1,NNODZ
!         write (meshref,"(F13.9)", ADVANCE = "NO") DIR_BC(i,1)
!         write (meshref,"(F13.9)", ADVANCE = "NO") DIR_BC(i,2)
!         write (meshref,"(F13.9)", ADVANCE = "YES") DIR_BC(i,3)
!      enddo


c...  Write in boundary condition indicator on each edge, in each direction
c     0 - nothing, 1 - applied load
!      do i = 1,NEDJ
!         write (meshref,'(I2)', ADVANCE = "NO") IBC_EDJ(i,1)
!         write (meshref,'(I2)', ADVANCE = "NO") IBC_EDJ(i,2)
!         write (meshref,'(I2)', ADVANCE = "YES") IBC_EDJ(i,3)
!      enddo


c...  Write in actual load data on each edge
c      This assumes (for the time being) that the load is contant
c      across each edge. This will be modified in the future!
!      do i = 1,NEDJ
!         write (meshref,"(F13.9)", ADVANCE = "NO") LD_EDJ(i,1)
!         write (meshref,"(F13.9)", ADVANCE = "NO") LD_EDJ(i,2)
!         write (meshref,"(F13.9)", ADVANCE = "YES") LD_EDJ(i,3)
!      enddo


     
c
c...  Write in body force loads on elements. This assumes constant
c       force across each element. We can modify this in the future.
!      do i = 1,NEL
!         write (meshref,"(F13.9)", ADVANCE = "NO") LD_FACE(i,1)
!         write (meshref,"(F13.9)", ADVANCE = "NO") LD_FACE(i,2)
!         write (meshref,"(F13.9)", ADVANCE = "YES") LD_FACE(i,3)
!      enddo




      
c
c...  Write in material data
c

c...  Young's Modulus in each element
!      do i = 1,NEL
!         write (meshref,*) EY(i)
!      enddo

c... Poisson's ratio in each element
!      do i = 1,NEL
!         write (meshref,*) NU(i)
!      enddo
         




      return
      end
