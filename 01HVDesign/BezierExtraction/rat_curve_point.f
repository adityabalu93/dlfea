      subroutine rat_curve_point(span,pl,ml,u,u_knotl,Pw,C)

      use aAdjKeep
      include "common.h"

      integer pl, ml, span, j
      real(8) u, u_knotl(pl+ml+1), Pw(ml,NSD+1), C(NSD), 
     &     Cw(NSD+1), N(pl+1)

      


      Cw = 0d+0

      
       call basisfuns(span,pl,ml,u,u_knotl,N)

       do j = 0,pl
          Cw(:) = Cw(:) + N(j+1)*Pw(span-p+j,:);
       enddo

       C(1:NSD) = Cw(1:NSD)/Cw(NSD+1)

       return
       end
