
      subroutine gen_IJ_CON(IJ_CON)

      use aAdjKeep
      include "common.h"

      integer jcount, icount, IJ_CON(MCP,NCP,2), i, j

      
      jcount = 0
      do j= 1,NCP
         icount = 0
         do i = 1,MCP
            IJ_CON(i,j,1) = i+icount
            IJ_CON(i,j,2) = j+jcount
            if (U_KNOT(i).ne.U_KNOT(i+1)) then
               icount = icount+1
            endif
         enddo
         if (V_KNOT(j).ne.V_KNOT(j+1)) then
            jcount = jcount+1
         endif
      enddo

      return
      end
