
      subroutine output_bezier

c------------------------------------------------------------------------c
c                                                                        c
c        Subroutine to open file and write refined vertex, edge          c
c        and element numbers. Initialze parameters of interest.          c
c                                                                        c
c------------------------------------------------------------------------c

      use aAdjKeep

      include "common.h"

      character(len=300) :: pline, fnum(2)
      integer :: i, j, k, meshref, iel, ni, nj, PT(NEL)
      meshref = 12

c
c...  Write in preliminary information
c
      open (meshref, file='tmesh.1.iga', status='unknown')
      write (meshref,*) "type surface"
      write (meshref,*) "nodeN", NNODZ
      write (meshref,*) "elemN", NEL
C       write (meshref,*) "maxNSHL", (P+1)*(Q+1)
	  
C       write (meshref,"(I2)", ADVANCE = "NO") P
C       write (meshref,"(I2)", ADVANCE = "YES") Q ! degree of curves in u then v direction
C       write (meshref,"(I3)", ADVANCE = "NO") MCP
C       write (meshref,"(I3)", ADVANCE = "YES") NCP ! number of control points
C                                                   ! in each direction


c
c...  Write control net

      do j = 1,(NCP)
         do i = 1,(MCP)
            do k = 1,NSD+1
               if (k == 1) then
                 write (meshref,"(A,F13.9)", ADVANCE = "NO")
     &                 "node   ", B_NET(i,j,k)
               else if (k < NSD+1) then
                  write (meshref,"(F13.9)", ADVANCE = "NO")
     &                 B_NET(i,j,k)
               else
                  write (meshref,"(F13.9)", ADVANCE = "YES") 
     &                 B_NET(i,j,k)
               endif
            enddo
         enddo
      enddo

C c...  Write element data
C       IEN = IEN - 1
C       do j = 1, NEL
C          write (meshref,"(A,3I2)")
C      &                 "belem   ", (P+1)*(Q+1), P, Q
C          do i = (P+1)*(Q+1), 1, -1
C            if (i > 1) then
C              write (meshref,"(I5)", ADVANCE = "NO")
C      &                 IEN(j,i)
C            else
C              write (meshref,"(I5)", ADVANCE = "YES")
C      &                 IEN(j,i)
C            end if
C          end do
C
C          do i = 1,(P+1)*(Q+1)
C              write (meshref, *) C_new(i,:,j)
C          end do
C
C
C       enddo   


c...  Write element data
      
      j = 0
      do iel = 1, (MCP-P)*(NCP-Q)
        ! get NURB coordinates
        ni = INN(IEN(iel,1),1)
        nj = INN(IEN(iel,1),2)
C         write (*,*) "ni = ", U_KNOT(ni), "ni+1 = ", U_KNOT(ni+1)
C         write (*,*) "nj = ", V_KNOT(nj), "nj+1 = ", V_KNOT(nj+1)
        if ((U_KNOT(ni) .ne. U_KNOT(ni+1)) .and. 
     &      (V_KNOT(nj) .ne. V_KNOT(nj+1))   ) then   
C         j is for output C matrix
          j = j + 1
          IEN(iel,:) = IEN(iel,:) - 1
         write (meshref,"(A,3I2)")
     &                 "belem   ", (P+1)*(Q+1), P, Q
         do i = (P+1)*(Q+1), 1, -1
           if (i > 1) then
             write (meshref,"(I5)", ADVANCE = "NO")
     &                 IEN(iel,i)
           else
             write (meshref,"(I5)", ADVANCE = "YES")
     &                 IEN(iel,i) 
           end if
         end do
         
         do i = 1,(P+1)*(Q+1)
			 do k = 1, (P+1)*(Q+1)
			 	write(meshref,'(E13.6)',advance='no') C_new(i,k,j)
			 end do
			 write(meshref,*)
             !write (meshref, *) C_new(i,:,j)
         end do
         
         
         end if   
      enddo   
   
      write (*,*) "elemN for output", j
	  
	  
	  do iel = 1, j
		  PT(iel) = iel-1
	  end do

      write(fnum(1),'(I30)') j
      write(fnum(2),'(I30)') Ptype

C       fnum(1) = trim(adjustl(fnum(1)))
C       fnum(2) = trim(adjustl(fnum(2)))
C       Pname   = trim(adjustl(Pname))
      
	  pline = 'set ' 
     &      //trim(adjustl(fnum(1)))//' elem ' 
     &      //trim(adjustl(fnum(2)))//'_' 
     &      //trim(adjustl(Pname))
      
	  !write (meshref,"('set ' I10 ' elem 1_rotor')",advance='no') j
      write (meshref, '(a)',advance='no') trim(adjustl(pline))
	  do i = 1, j
	       write (meshref,'(I10) ',advance='no') PT(i)
	  end do
	  
	  !write (meshref,*) "set ", j, " elem 1_rotor ", PT
	  
	  
c...  Write in Master/Slave relationships. These arise from periodic boundary
c       conditions, corners in the geometry, or any other case where two
c       control points must always remain equal.
c
!      do i = 1, NNODZ
!         write (meshref,*) IPER(i)
!      enddo

c
c...  Write in Boundary/Edge information
c
!      write (meshref,'(I2)', ADVANCE = "NO") CLOSED_U_flag
!      write (meshref,'(I2)', ADVANCE = "YES") CLOSED_V_flag               


c...  Write in boundary condition indicator at each node, in each direction
c     0 - nothing, 1 - Dir., 2 - Neu., 3 - Periodic
!      do i = 1,NNODZ
!         write (meshref,'(I2)', ADVANCE = "NO") IBC(i,1)
!         write (meshref,'(I2)', ADVANCE = "NO") IBC(i,2)
!         write (meshref,'(I2)', ADVANCE = "YES") IBC(i,3)
!      enddo

c...  Write Dirichlet Condition array.
!      do i = 1,NNODZ
!         write (meshref,"(F13.9)", ADVANCE = "NO") DIR_BC(i,1)
!         write (meshref,"(F13.9)", ADVANCE = "NO") DIR_BC(i,2)
!         write (meshref,"(F13.9)", ADVANCE = "YES") DIR_BC(i,3)
!      enddo


c...  Write in boundary condition indicator on each edge, in each direction
c     0 - nothing, 1 - applied load
!      do i = 1,NEDJ
!         write (meshref,'(I2)', ADVANCE = "NO") IBC_EDJ(i,1)
!         write (meshref,'(I2)', ADVANCE = "NO") IBC_EDJ(i,2)
!         write (meshref,'(I2)', ADVANCE = "YES") IBC_EDJ(i,3)
!      enddo


c...  Write in actual load data on each edge
c      This assumes (for the time being) that the load is contant
c      across each edge. This will be modified in the future!
!      do i = 1,NEDJ
!         write (meshref,"(F13.9)", ADVANCE = "NO") LD_EDJ(i,1)
!         write (meshref,"(F13.9)", ADVANCE = "NO") LD_EDJ(i,2)
!         write (meshref,"(F13.9)", ADVANCE = "YES") LD_EDJ(i,3)
!      enddo


     
c
c...  Write in body force loads on elements. This assumes constant
c       force across each element. We can modify this in the future.
!      do i = 1,NEL
!         write (meshref,"(F13.9)", ADVANCE = "NO") LD_FACE(i,1)
!         write (meshref,"(F13.9)", ADVANCE = "NO") LD_FACE(i,2)
!         write (meshref,"(F13.9)", ADVANCE = "YES") LD_FACE(i,3)
!      enddo




      
c
c...  Write in material data
c

c...  Young's Modulus in each element
!      do i = 1,NEL
!         write (meshref,*) EY(i)
!      enddo

c... Poisson's ratio in each element
!      do i = 1,NEL
!         write (meshref,*) NU(i)
!      enddo
         




      return
      end
