      subroutine RefineKnotVectSurf(ml,pl,u_knotl,nl,ql,
     &     v_knotl,Pw,x_knot,r,dir,Qw,Ubar,Vbar)


c
c purpose: This function inserts the knots in x_knot into either u_knot (if dir = 1) or
c           v_knot (dir = 2) and refines the projective control points Pw such that the
c           surface does not change.
c
c
c adapted from algorithm in Piegl, Les. "The NURBS Book". Springer-Verlag: 
c    Berlin 1995; p. 167.
c
c
c July 10, 2003
c J. Austin Cottrell
c CES Graduate Student
c Texas Institute for Computational Engineering Science
c University of Texas at Austin



      include "common.h"
      
      
      integer ml,pl,nl,ql,r,dir,a,b,j,row,col,k,l,ind,i
      real(8) u_knotl(ml+pl+1),u,v,v_knotl(nl+ql+1),Pw(ml,nl,NSD+1), 
     &  x_knot(r), Ubar(ml+pl+1+dir*r), Vbar(nl+ql+1+(1-dir)*r),
     &  Qw(ml+dir*r,nl+(1-dir)*r,NSD+1),alfa
      
      
      
      r = r-1
      if (dir.eq.1) then
        call find_span(pl,ml,x_knot(1),u_knotl,a)
        a = a - 1
        call find_span(pl,ml,x_knot(r+1),u_knotl,b)
        b = b-1
        Ubar = 0d+0 
        do j = 0,a
          Ubar(j+1) = u_knotl(j+1) 
        enddo
        do  j = b+pl,ml+pl
          Ubar(j+r+2) = u_knotl(j+1) 
        enddo
        Vbar = v_knotl 
        
c       save unaltered control points
        do row = 1,nl
          do  k = 0,(a-pl)
            Qw(k+1,row,:) = Pw(k+1,row,:) 
          enddo
          do  k = (b-1),(ml-1)
            Qw(k+r+2,row,:) = Pw(k+1,row,:) 
          enddo
        enddo
        
        i = b+pl-1 
        k = b+pl+r
        
        do  j = r,0,-1
          do
            if ((x_knot(j+1).gt.u_knotl(i+1)).or.(i.le.a)) exit
            Ubar(k+1) = u_knotl(i+1) 
            do row = 1,nl
              Qw(k-pl,row,:) = Pw(i-pl,row,:) 
            enddo
            k = k-1  
            i = i-1 
          enddo
          
          do row = 1,nl
            Qw(k-pl,row,:) = Qw(k-pl+1,row,:) 
          enddo
          do l = 1,pl
            ind = k-pl+l 
            alfa = Ubar(k+l+1) - x_knot(j+1) 
            if (abs(alfa).le.1d-14) then
              do row = 1,nl
                Qw(ind,row,:) = Qw(ind+1,row,:)     
              enddo
            else
              alfa = alfa/(Ubar(k+l+1) - u_knotl(i-pl+l+1)) 
              do row = 1,nl
                Qw(ind,row,:) = alfa*Qw(ind,row,:) +
     &            (1d+0-alfa)*Qw(ind+1,row,:) 
              enddo                
            endif
          enddo
          Ubar(k+1) = x_knot(j+1) 
          k = k-1 
        enddo
      else
        
c       ---------------------------------------------------------------
        
        
        
        
        call find_span(ql,nl,x_knot(1),v_knotl,a)
        a = a-1 
        call find_span(ql,nl,x_knot(r+1),v_knotl,b) 
        b = b-1
        Vbar = 0d+0 
        do j = 0,a
          Vbar(j+1) = v_knotl(j+1) 
        enddo
        do j = b+ql,nl+ql
          Vbar(j+r+2) = v_knotl(j+1) 
        enddo
        Ubar = u_knotl 
        
                                ! save unaltered control points
        do col = 1,ml
          do k = 0,(a-ql)
            Qw(col,k+1,:) = Pw(col,k+1,:) 
          enddo
          do k = (b-1),nl-1
            Qw(col,k+r+2,:) = Pw(col,k+1,:) 
          enddo
        enddo 
        
        i = b+ql-1 
        k = b+ql+r 
        do j = r,0,-1
          do
            if ((x_knot(j+1).gt.v_knotl(i+1)).or.(i.le.a)) exit
            Vbar(k+1) = v_knotl(i+1) 
            do col = 1,ml
              Qw(col,k-ql,:) = Pw(col,i-ql,:) 
            enddo
            k = k-1  
            i = i-1 
          enddo
          
          do col = 1,ml
            Qw(col,k-q,:) = Qw(col,k-q+1,:) 
          enddo
          do l = 1,ql
            ind = k-ql+l 
            alfa = Vbar(k+l+1) - x_knot(j+1) 
            if (abs(alfa).le.1d-14) then
              do col = 1,ml
                Qw(col,ind,:) = Qw(col,ind+1,:)     
              enddo
            else
              alfa = alfa/(Vbar(k+l+1) - v_knotl(i-ql+l+1)) 
              do col = 1,ml
                Qw(col,ind,:) = alfa*Qw(col,ind,:) +
     &            (1d+0-alfa)*Qw(col,ind+1,:) 
              enddo                
            endif
          enddo
          Vbar(k+1) = x_knot(j+1) 
          k = k-1 
        enddo
      endif
      
      return
      end
      
      

