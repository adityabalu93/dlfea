c
c       This subroutine generates the BdryIEN matrix, which relates edge 
c       numbers and local node numbers to the appropriate global node numbers. 
c       It will also generate EDJ_OR which will contain the orientations
c       of a given edge.
c
c
c       June 28, 2003
c
c       J. Austin Cottrell
c       CAM Graduate Student
c       Institute for Computational Engineering Science
c       The University of Texas at Austin

 
	
	subroutine gen_BdryIEN

	use aAdjKeep		! this f90 module contains all allocatables
c       
	include "common.h"	! common file defines all variables
c       
c...    Local variables
	integer :: i, j, edj
	
	allocate(BdryIEN(NEDJ, max(P+1,Q+1)))
	allocate(EDJ_OR(NEDJ))
c       
c...    Initialize matrices and variables
c       
	BdryIEN = 0
	EDJ_OR = 0
        edj = 0
	
c       
c...    Loop through edges assigning numbers and filling out matrices
c         Edge numbers will be assigned sequentially while traversing
c         the boundary in a counter clockwise direction
c       
	if (CLOSED_V_flag.ne.1) then
	   do i = 1,MCP-P
	      edj = edj + 1
	      EDJ_OR(edj) = 1	! 1 indicates edge on "bottom" of grid
	      do j = 0,P
		 BdryIEN(edj,j+1) = edj+P-j
	      enddo
	   enddo
	   if (CLOSED_U_flag.ne.1) then
	      edj = edj+(NCP-Q)
	   endif
	   do i = 1,MCP-P
	      edj = edj+1
	      EDJ_OR(edj) = 3	! 3 indicates edge on "top" of grid
	      do j = 0,P
		 BdryIEN(edj,j+1) = NNODZ - i - j + 1
	      enddo
	   enddo
	   edj = MCP - P
	endif

	if (CLOSED_U_flag.ne.1) then
	   do i = 1,NCP-Q
	      edj = edj+1
	      EDJ_OR(edj) = 2	! 2 indicates edge on "right" side of grid
	      do j = 0,Q
		 BdryIEN(edj,j+1) = (i-j+Q)*MCP 
	      enddo
	   enddo
	   if (CLOSED_V_flag.ne.1) then
	      edj = edj+(MCP-P)
	   endif
	   do i = 1,NCP-Q
	      edj = edj+1
	      EDJ_OR(edj) = 4	! 4 indicates edge on "left" side of grid
	      do j = 0,Q
		 BdryIEN(edj,j+1) = NNODZ-MCP+1 - (i+j-1)*MCP
	      enddo
	   enddo
	endif


	return
	end

