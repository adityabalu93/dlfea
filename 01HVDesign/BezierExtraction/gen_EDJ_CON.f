      subroutine gen_EDJ_CON(EDJ_CON,new_NEDJ)

      use aAdjKeep
      include "common.h"

      integer e, icount,new_NEDJ, ni, nj
      integer  EDJ_CON(new_NEDJ)

      
      icount = 0
      do e = 1,NEDJ
         EDJ_CON(e+icount) = e
         if ((EDJ_OR(e).eq.1).or.(EDJ_OR(e).eq.3)) then
            ni = INN(BdryIEN(e,1),1)
            if (U_KNOT(ni).ne.U_KNOT(ni+1)) then
               icount = icount+1
               EDJ_CON(e+icount) = e
            endif
         endif
         if ((EDJ_OR(e).eq.2).or.(EDJ_OR(e).eq.4)) then
            nj = INN(BdryIEN(e,1),2)
            if (V_KNOT(nj).ne.V_KNOT(nj+1)) then
               icount = icount+1
               EDJ_CON(e+icount) = e
            endif
         endif
      enddo

      return
      end
