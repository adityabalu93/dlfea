c     This function calculates the first 0 to d derivatives of a curve
c     defined by knot vector u_knot and control points P, evaluated
c     at parameter value u.
c     
c     
c     
c     Algorithm from Piegl, Les. "The NURBS Book". Springer-Verlag: 
c     Berlin 1995; p. 82.
c     
c     
c     June 18, 2003
c     J. Austin Cottrell
c     CES Graduate Student
c     Texas Institute for Computational Engineering Science
c     University of Texas at Austin



      subroutine nonrat_curve_derivs(span,pl,m,u,u_knotl,B,dim,d,ck)



c     --------------VARIABLE DECLARATIONS--------------------------------
c...  span index, degree, number of control points, dimension of curve,counters
      integer span, pl,m,dim,k,j,d

c...  integration point, knot vector, control points, derivatives
      real(8) u, u_knotl(m+pl+1), B(m,dim), ck(d+1,dim), nders(d+1,pl+1)
c     -------------------------------------------------------------------  

      

      call dersbasisfuns(span,pl,m,u,u_knotl,d,nders)
      
      do k = 0,d
         ck(k+1,:) = 0d+0
         do j = 0,pl
            ck(k+1,:) = ck(k+1,:) + nders(k+1,j+1)*B(span-pl+j,:)
         enddo
      enddo
      
      return
      end
