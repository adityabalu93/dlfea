c       This subroutine generates the IEN matrix, which relates element numbers
c       and local node numbers to the appropriate global node numbers. The
c       routine also generates the INN matrix, which relates global node
c       number to the "NURBS coordinates" of the node.
	
	  subroutine bezierExtraction(p_deg, m, knot, nb, C_ext)


	    use aAdjKeep		! this f90 module contains all allocatables
c       
	    include "common.h"	! common file defines all variables
c       
c...    Local variables
	    integer :: i, j, k, r, p_deg, m, nb, a, b, mult, temp, 
     & s
      real(8) :: knot(m), alphas(p_deg), alpha, numer
      real(8) :: C_ext(p_deg+1, p_deg+1, nb)
      character(len=8) :: fmt
      
      

C       write(*,*) "C matrix initilization for C(:,:,nb)"
      C_ext = 0
C         Initialize the next txtraction operator
      forall (i=1:p_deg+1) C_ext(i,i,:) = 1
      
      write (fmt, '(A,I2,A)') '(', p_deg+1, 'F6.2)'
        ! if you consider to have used the (row, col) convention, 
        ! the following will print the transposed matrix (col, row)
        ! but I' = I, so it's not important here  
C       write (*, fmt) C_ext(:,:,nb)
      m = m - p_deg - 1
      a = p_deg+1
      b = a+1
      nb = 1
      
	    do while (b .LE. m)
        
C         write(*,*) "C matrix initilization for C_ext(:,:,1)"
C         write (*, fmt) C_ext(:,:,1)
C         ount multiplicity of the knot at location b.
        i = b
        do while ((b .LE. m) .AND. (knot(b+1) .EQ. knot(b)))
          b = b + 1
        end do
        mult = b - i +1
C         write(*,*) "multiplicity is : ", mult
        
        If (mult .LT. p_deg) Then
          numer =  knot(b) - knot(a)
C           write(*,*) "numerator is : ", numer
          do j = p_deg, mult+1, -1
            alphas(j-mult) = numer/(knot(a+j)-knot(a))
          end do
          r = p_deg - mult
          do j = 1, r
            temp = r - j + 1
            s = mult + j
            do k = p_deg+1, s+1, -1
              alpha = alphas(k-s)
              C_ext(:,k,nb) = 
     &        alpha * C_ext(:,k,nb)+(1-alpha)*C_ext(:,k-1,nb)
            end do
            if (b .LE. m) then
              C_ext(temp:temp+j,temp,nb+1) = 
     &        C_ext(p_deg-j+1:p_deg+1, p_deg+1,nb)
            end if
          end do
          
          
          nb = nb+1
          If (b .LE. m) Then
              a = b
              b = b+1
          end If
        Else If (mult .EQ. p_deg) Then
          If (b .LE. m) Then
              nb = nb+1
              a = b
              b = b+1
          end If
        End If
        
      end do
      
C       write(*,*) "C matrix initilization for C_ext(:,:,1)"
C       do i = 1, 3
C         write(*,*) C_ext(i,:,1)
C       end do
      
	  return
	  end
