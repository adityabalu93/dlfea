from __future__ import division
import os
import math
import numpy as np
import geomdl
from geomdl import *
from geomdl.visualization import VisVTK
from geomdl import multi

dim = 3

def ord4p(X, F, n):
    K = [[1, X[0], X[0]**2, X[0]**3, X[0]**4],
        [1, X[1], X[1]**2, X[1]**3, X[1]**4],
        [0, 1, 2*X[0], 3*X[0]**2, 4*X[0]**3],
        [0, 1, 2*X[1], 3*X[1]**2, 4*X[1]**3],
        [1, X[2], X[2]**2, X[2]**3, X[2]**4]]
    C = np.linalg.inv(np.array(K)).dot(np.array(F))
    t = np.linspace(X[0], X[1], n)
    A = C[0] + C[1]*t + C[2]*t**2 + C[3]*t**3 + C[4]*t**4
    return A


def GenLeaflet(ang1, ang2, center, cf_df, cf_dr, cf_dZ, cf_Rfr, cf_HtR, R, Ht, loZ, nPv, nPcn):
    pw = 2
    HtR = Ht * cf_HtR
    Rfr = R * cf_Rfr
    #  Free edge
    coms = [[Rfr*math.cos(ang1), Rfr*math.sin(ang1), Ht],
        [Rfr*math.cos(ang2), Rfr*math.sin(ang2), Ht]]
    psc = [[0 for _ in range(dim)] for _ in range(3)]
    psc[0][:] = coms[0][:]
    psc[-1][:] = coms[-1][:]
    psc[1][:] = center + [Ht+cf_dZ]
    nps = 100
    L3_ps = [[0 for _ in range(dim)] for _ in range(nps)]

    dt = [1 for _ in range(nps - 1)]

    t = [0 for _ in range(nps)]

    for i in range(nps-1):
        t[i+1] = t[i] + dt[i]

    t[:] = [t_i/max(t) for t_i in t]

    angs = [(ang2 - ang1)*t_i + ang1 for t_i in t]

    t = ord4p([0, 1, 0.5], [1, 1., -5., 5., 0.], nps)

    for i in range(nps):
        L3_ps[i][0] = R*math.cos(angs[i])
        L3_ps[i][1] = R*math.sin(angs[i])
        L3_ps[i][2] = t[i]*HtR

    nIns = 1
    nPLc = nIns*2 + 3

    insr1 = [[0 for _ in range(dim)] for _ in range(nIns)]
    insr2 = [[0 for _ in range(dim)] for _ in range(nIns)]

    for i in range(nIns):
        t = (i+1)/(nIns+1)
        insr1[i] = [(1-t)*p1 + t*p2 for p1, p2 in zip(psc[0],psc[1])]
        insr2[i] = [(1-t)*p2 + t*p3 for p2, p3 in zip(psc[1],psc[2])]

    [psc.insert(1 + idx, elem) for idx, elem in enumerate(insr1)]
    [psc.insert(len(insr1)+ idx + 2, elem) for idx, elem in enumerate(insr2)]

    nPsc = len(psc)

    for i in range(nPsc):
        t = (-1)*abs(2/(nPsc-i))*(i-(nPsc+1)/2) + 1
        psc[i][2] = psc[i][2] - loZ*t

    Usc = geomdl.knotvector.generate(pw, nPsc)

    curve_approx = geomdl.fitting.approximate_curve(L3_ps, pw, ctrlpts_size = nPLc)


    curve_psc = geomdl.BSpline.Curve()
    curve_psc.degree = pw
    curve_psc.ctrlpts = psc
    curve_psc.knotvector = Usc

    psLw = []
    psHi = []

    for i in range(nPv):
        t = (i)/(nPv - 1)
        psLw.append(curve_approx.derivatives(t)[0])
        psHi.append(curve_psc.derivatives(t)[0])

    # slight deviation from matlab code in terms of numbers
    t = geomdl.linalg.linspace(0, 1, nPcn)
    ncsP = pw + 1
    CPcn = [[[0.0 for _ in range(dim)] for _ in range(nPcn)] for _ in range(nPv)]
    for i in range(nPcn):
        for d in range(dim):
            CPcn[0][i][d] = t[i]*psLw[0][d] + (1-t[i])*psHi[0][d]
            CPcn[-1][i][d] = t[i]*psLw[-1][d] + (1-t[i])*psHi[-1][d]

    Ucn = geomdl.knotvector.generate(pw, nPcn)
    nUcn = len(Ucn)

    curves = []
    for i in range(1,nPv-1):
        t = i/(nPv-1)
        t = (t-0.5)*1.1 + 0.5
        wt = 4*t*(1-t)
        wt1 = 0.0 if t > 0.5 else (1-wt)
        wt2 = 1.0 - wt if t > 0.5 else 0.0 
        dh = [0.0, 0.0, psHi[i][2] - psLw[i][2]]
        dr = [psHi[i][0] - psLw[i][0], psHi[i][1] - psLw[i][1], 0.0]
        csCPcn = [[0.0 for _ in range(dim)] for _ in range(3)]
        for d in range(dim):
            csCPcn[0][d] = psHi[i][d]
            csCPcn[2][d] = psLw[i][d]
            csCPcn[1][d] = psLw[i][d] + dh[d]*cf_df + dr[d]*cf_dr

        curve_cs = geomdl.BSpline.Curve()
        curve_cs.degree = pw
        curve_cs.ctrlpts = csCPcn
        Ucs = geomdl.knotvector.generate(pw, ncsP)
        curve_cs.knotvector = Ucs
        in_Ucn = Ucn[pw+1:nUcn-pw-1]
        for u in in_Ucn:
            curve_cs.insert_knot(u)
        CPout = curve_cs.ctrlpts

        CPmx = [[0.0 for _ in range(dim)] for _ in range(len(CPout))]
        for j in range(len(CPout)):
            for d in range(dim):
                CPmx[j][d] = CPout[j][d]*wt + CPcn[0][j][d]*wt1 + CPcn[-1][j][d]*wt2

        d1 = [cpout - cpmx for cpout, cpmx in zip(CPout[0], CPmx[0])]
        d2 = [cpout - cpmx for cpout, cpmx in zip(CPout[-1], CPmx[-1])]

        td = geomdl.linalg.linspace(1,0,len(CPmx))
        CPmx2 = [[0.0 for _ in range(dim)] for _ in range(len(CPmx))]

        for idx, d in enumerate(td):
            for idd in range(dim):
                CPmx2[idx][idd] = CPmx[idx][idd] + d*d1[idd] + (1-d)*d2[idd]
        curves.append(curve_cs)
        CPcn[i] = CPmx2

    return CreateSurface(CPcn, curves[0].knotvector, pw)


def CreateSurface(PV, U, p):
    NPP = 150
    nPV = len(PV)
    nPU = len(PV[0])
    P_Sur = [pt for pu in PV for pt in pu]
    V = geomdl.knotvector.generate(p, nPV)
    surf = geomdl.BSpline.Surface()
    surf.degree_u = p
    surf.degree_v = p
    surf.set_ctrlpts(P_Sur, nPV, nPU)
    surf.knotvector_u = V
    surf.knotvector_v = U
    surf.transpose()
    return surf

def HVGen(cf_df = 0.4, cf_dr = 0.8, cf_dZ = -0.295, cf_Rfr = 0.9, cf_HtR = 0.6, R = 1.10634, Ht = 1.1108, loZ = 0.0, render=True, valve_id='geometry01'):
    nU = 17
    nV = 12
    thl = (120)*math.pi/180
    phi = 1.5*math.pi/180
    center = [0, 0]
    angLf = []
    angLf.append([phi/2, thl-phi/2])
    angLf.append([thl + phi/2, 2*thl-phi/2])
    angLf.append([2*thl + phi/2, 3*thl-phi/2])
    leaflet1 = GenLeaflet(angLf[0][0], angLf[0][1], center, cf_df, cf_dr, cf_dZ, cf_Rfr, cf_HtR, R, Ht, loZ, nU, nV)
    leaflet2 = GenLeaflet(angLf[1][0], angLf[1][1], center, cf_df, cf_dr, cf_dZ, cf_Rfr, cf_HtR, R, Ht, loZ, nU, nV)
    leaflet3 = GenLeaflet(angLf[2][0], angLf[2][1], center, cf_df, cf_dr, cf_dZ, cf_Rfr, cf_HtR, R, Ht, loZ, nU, nV)
    heartvalve = multi.SurfaceContainer(leaflet1, leaflet2, leaflet3)
    exchange.export_smesh(heartvalve, valve_id)
    if render:
        vis_config = VisVTK.VisConfig(ctrlpts=False, legend=False)
        heartvalve.vis = VisVTK.VisSurface(vis_config)
        heartvalve.sample_size = 100
        heartvalve.render()
    return heartvalve


def main():
    #  Coeffs
    # cf_dh = -0.1
    # cf_dr = 1.0
    # cf_dZ = -0.5
    # cf_Rfr = 0.9
    # cf_HtR = 0.6
    # R = 1.10634
    # Ht = 1.1108
    loZ = -0.0
    cfdhs = [-0.1, 0.1, 0.3, 0.5]
    cfdrs = [0.25, 0.5, 0.75, 1.0]
    cfdzs = [-0.6, -0.4, -0.2, 0.0]
    cfrfrs = [0.9211, 0.8809, 0.8913, 0.88, 0.8889, 0.8966]
    cfhtrs = [0.8148, 0.8, 0.8438, 0.8823, 0.8857, 0.8]
    rs = [0.95, 1.05, 1.15, 1.25, 1.35, 1.45]
    hts = [1.1, 1.2, 1.35, 1.5, 1.55, 1.6]
    geomnum = 65
    for cf_dh in cfdhs:
        for cf_dr in cfdrs:
            for cf_dZ in cfdzs:
                for R, Ht, cf_Rfr, cf_HtR in zip(rs, hts, cfrfrs, cfhtrs):
                    valve_id = './Geometries/%s/smesh.dat'%geomnum
                    os.makedirs('./Geometries/%s'%geomnum)
                    hv = HVGen(cf_dh, cf_dr, cf_dZ, cf_Rfr, cf_HtR, R, Ht, loZ, render=False, valve_id=valve_id)
                    CreateBezierPatch('./Geometries/%s'%geomnum)
                    MergeBezierPatches('./Geometries/%s'%geomnum, hv., 12)                    
                    geomnum = geomnum + 1


if __name__ == '__main__':
    main()