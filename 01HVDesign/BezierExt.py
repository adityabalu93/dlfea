import os
import shutil

def CreateBezierPatch(foldername):
    files = os.listdir(foldername)
    for file in files:
        if 'smesh' in file:
            shutil.copy(os.path.join(foldername, file), os.path.join(foldername, 'mesh.dat'))
            if os.name == 'nt':
                os.system('bash -c "./bezext.sh %s"'%(foldername))
            else:
                os.system('./bezext.sh %s'%(foldername))
            shutil.move(os.path.join(foldername, 'tmesh.1.iga'), os.path.join(foldername, 'tmesh.leaflet%s.iga'%file.strip().split('.')[1]))

def ReadPatch(filename):
    patch = {}
    with open(filename, 'r') as f:
        elemRead = False
        setRead = False
        elemstart = False
        nodelist = False
        for line in f:
            if 'type' in line and elemRead is False and setRead is False:
                patch['patch type'] = line.strip().split()[1]
            elif 'nodeN' in line and elemRead is False and setRead is False:
                patch['num nodes'] = int(line.strip().split()[1])
                patch['nodes'] = [[0.0 for _ in range(4)] for _ in range(patch['num nodes'])]
                nodeIdx = 0
            elif 'elemN' in line and elemRead is False and setRead is False:
                patch['num elems'] = int(line.strip().split()[1])
                patch['elems'] = [{} for _ in range(patch['num elems'])]
                elemIdx = 0
            elif 'node ' in line and elemRead is False and setRead is False:
                try:
                    patch['nodes'][nodeIdx] = [float(f) for f in line.strip().split()[1:]]
                    nodeIdx = nodeIdx + 1
                except IndexError:
                    raise ValueError('num nodes lesser the nodes')
                if nodeIdx == patch['num nodes']:
                    elemRead = True
            elif elemRead is True and setRead is False:
                if line.strip().split()[0] == 'belem':
                    patch['elems'][elemIdx]['num nodes'] = int(line.strip().split()[1])
                    patch['elems'][elemIdx]['degree'] = [int(line.strip().split()[2]), int(line.strip().split()[2])]
                    elemstart = True
                    nodelist = True
                elif elemstart is True and nodelist is True:
                    assert len(line.strip().split()) == patch['elems'][elemIdx]['num nodes']
                    patch['elems'][elemIdx]['nodes'] = [int(idx) for idx in line.strip().split()]
                    nodelist = False
                    patch['elems'][elemIdx]['connectivity'] = []
                elif elemstart and nodelist is False:
                    assert len(line.strip().split()) == patch['elems'][elemIdx]['num nodes']
                    patch['elems'][elemIdx]['connectivity'].append([float(val) for val in line.strip().split()])
                    if len(patch['elems'][elemIdx]['connectivity']) == (patch['elems'][elemIdx]['degree'][0] + 1)*(patch['elems'][elemIdx]['degree'][0] + 1):
                        elemstart = False
                        elemIdx = elemIdx + 1
                if elemIdx == patch['num elems']:
                    elemRead = False
                    setRead = True
                    patch['sets'] = []
            elif setRead is True and elemRead is False:
                if 'set' in line.strip().split()[0]:
                    Set = {}
                    Set['num idx'] = int(line.strip().split()[1])
                    Set['set type'] = line.strip().split()[2]
                    Set['set name'] = line.strip().split()[3]
                    Set['idx'] = [int(val) for val in line.strip().split()[4:]]
                    assert len(Set['idx']) == Set['num idx']
                    patch['sets'].append(Set)
    return patch


def writeBezierPatch(patch, filename):
    with open(filename, 'w') as f:
        f.write('type %s\n'%patch['patch type'])
        f.write('nodeN %s\n'%patch['num nodes'])
        f.write('elemN %s\n'%patch['num elems'])
        for i in range(patch['num nodes']):
            f.write('node ')
            for j in range(4):
                f.write('%.16e '%patch['nodes'][i][j])
            f.write('\n')
        for i in range(patch['num elems']):
            # print(patch['elems'][i]['num nodes'], patch['elems'][i]['degree'][0])
            f.write('belem %s %s %s\n'%(patch['elems'][i]['num nodes'], patch['elems'][i]['degree'][0], patch['elems'][i]['degree'][1]))
            for j in range(patch['elems'][i]['num nodes']):
                f.write('%s '%patch['elems'][i]['nodes'][j])
            f.write('\n')
            for j in range((patch['elems'][i]['degree'][0]+1)*(patch['elems'][i]['degree'][1]+1)):
                for k in range(patch['elems'][i]['num nodes']):
                    f.write('%.16e '%patch['elems'][i]['connectivity'][j][k])
                f.write('\n')
        for Set in patch['sets']:
            f.write('set %s '%Set['num idx'])
            f.write('%s '%Set['set type'])
            f.write('%s '%Set['set name'])
            for i in range(Set['num idx']):
                f.write('%s '%Set['idx'][i])
            f.write('\n')

def AddBC(patch, npu, npv):
    idx = list(range(0,npv*npu,npu))
    idx += list(range(1,npv*npu,npu))
    idx += list(range(0,2*npu))
    idx += list(range(npv*npu - 2*npu, npv*npu))
    idx = list(set(idx))
    Set = {}
    Set['num idx'] = len(idx)
    Set['set type'] = 'node'
    Set['set name']  = 'fixed'
    Set['idx'] = idx
    patch['sets'].append(Set)
    return patch


def MergeBezierPatches(foldername, npu, npv):
    files = os.listdir(foldername)
    patchfiles = [f for f in files if 'tmesh.leaflet' in f]
    patches = [ReadPatch(os.path.join(foldername,file)) for file in patchfiles if 'tmesh.leaflet' in file]
    patches = [AddBC(patch, npu, npv) for patch in patches]
    patch = {}
    patch['patch type'] = patches[0]['patch type']
    patch['num nodes'] = 0
    patch['num elems'] = 0
    patch['nodes'] = []
    patch['elems'] = []
    patch['sets'] = []
    fixedSet = {}
    fixedSet['set type'] = 'node'
    fixedSet['set name']  = 'fixed'
    fixedSet['idx'] = []
    fixedSet['num idx'] = 0
    for idp, p in enumerate(patches):
        patch['nodes'] += p['nodes']
        for elem in p['elems']:
            elem['nodes'] = [idx+patch['num nodes'] for idx in elem['nodes']]
            patch['elems'].append(elem)
        for Set in p['sets']:
            if Set['set name'] is not 'fixed':
                Set['set name'] = '%s_leaflet'%(idp + 1)
                Set['idx'] = [idx+patch['num elems'] for idx in Set['idx']]
                patch['sets'].append(Set)
            else:
                fixedSet['num idx'] += Set['num idx']
                fixedSet['idx'] += [val+patch['num nodes'] for val in Set['idx']]
        patch['num nodes'] += p['num nodes']
        patch['num elems'] += p['num elems']
    patch['sets'].append(fixedSet)
    assert patch['num nodes'] == len(patch['nodes'])
    assert patch['num elems'] == len(patch['elems'])
    print('number of nodes:',len(patch['nodes']))
    print('number of elems:',len(patch['elems']))
    print('number of sets:',len(patch['sets']))
    writeBezierPatch(patch, os.path.join(foldername,'tmesh.1.iga'))


def main():
    geometries = os.listdir('./Geometries')
    # for geom in geometries:
    CreateBezierPatch(os.path.join('./Geometries/','1'))
    MergeBezierPatches(os.path.join('./Geometries/','1'), 17, 12)

if __name__ == '__main__':
    main()