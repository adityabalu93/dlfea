import os
import shutil
import json
from tqdm import tqdm
datapath = './Data'
successpath = './SuccessfulData'
designlist = sorted(os.listdir(datapath))
successcount = 0
failcount = 0
if not os.path.exists(successpath):
    os.makedirs(successpath)

successlist = []

for design in tqdm(designlist):
    if os.path.isfile(os.path.join(datapath,design,'sh.rest.tsp.160')):
        successcount = successcount + 1
        successlist.append(os.path.abspath(os.path.join(datapath,design)))
        # if not os.path.exists(os.path.join(successpath,'Design%s'%successcount)):
        #     os.mkdir(os.path.join(successpath,'Design%s'%successcount))
        # if not os.path.isfile(os.path.join(successpath,'Design%s'%successcount,'tmesh.1.iga')):
        #     shutil.copy(os.path.join(datapath,design,'tmesh.1.iga'), os.path.join(successpath,'Design%s'%successcount,'tmesh.1.iga'))
        # if not os.path.isfile(os.path.join(successpath,'Design%s'%successcount,'tmesh.0.dat')):
        #     shutil.copy(os.path.join(datapath,design,'tmesh.0.dat'), os.path.join(successpath,'Design%s'%successcount,'tmesh.0.dat'))
        # if not os.path.isfile(os.path.join(successpath,'Design%s'%successcount,'sh.rest.tsp.160')):
        #     shutil.copy(os.path.join(datapath,design,'sh.rest.tsp.160'), os.path.join(successpath,'Design%s'%successcount,'sh.rest.tsp.160'))
        # if not os.path.isfile(os.path.join(successpath,'Design%s'%successcount,'param.dat')):
        #     shutil.copy(os.path.join(datapath,design,'param.dat'), os.path.join(successpath,'Design%s'%successcount,'param.dat'))
        # if not os.path.isfile(os.path.join(successpath,'Design%s'%successcount,'tmesh.leaflet1.iga')):
        #     shutil.copy(os.path.join(datapath,design,'tmesh.leaflet1.iga'), os.path.join(successpath,'Design%s'%successcount, 'tmesh.leaflet1.iga'))
        # if not os.path.isfile(os.path.join(successpath,'Design%s'%successcount,'tmesh.leaflet2.iga')):
        #     shutil.copy(os.path.join(datapath,design,'tmesh.leaflet2.iga'), os.path.join(successpath,'Design%s'%successcount, 'tmesh.leaflet2.iga'))
        # if not os.path.isfile(os.path.join(successpath,'Design%s'%successcount,'tmesh.leaflet3.iga')):
        #     shutil.copy(os.path.join(datapath,design,'tmesh.leaflet3.iga'), os.path.join(successpath,'Design%s'%successcount, 'tmesh.leaflet3.iga'))
        # if not os.path.isfile(os.path.join(successpath,'Design%s'%successcount,'smesh.1.dat')):
        #     shutil.copy(os.path.join(datapath,design,'smesh.1.dat'), os.path.join(successpath,'Design%s'%successcount, 'smesh.1.dat'))
        # if not os.path.isfile(os.path.join(successpath,'Design%s'%successcount,'smesh.2.dat')):
        #     shutil.copy(os.path.join(datapath,design,'smesh.2.dat'), os.path.join(successpath,'Design%s'%successcount, 'smesh.2.dat'))
        # if not os.path.isfile(os.path.join(successpath,'Design%s'%successcount,'smesh.3.dat')):
        #     shutil.copy(os.path.join(datapath,design,'smesh.3.dat'), os.path.join(successpath,'Design%s'%successcount, 'smesh.3.dat'))
        # if not os.path.isfile(os.path.join(successpath,'Design%s'%successcount,'coaptation')):
        #     shutil.copy(os.path.join(datapath,design,'coaptation'), os.path.join(successpath,'Design%s'%successcount, 'coaptation'))
    else:
        failcount = failcount + 1

print('Success count:', successcount)
print('failcount:',failcount)

with open('successlist.json', 'w') as f:
    json.dump(successlist, f, indent=4)
