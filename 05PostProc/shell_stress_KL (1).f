      ! gives back the stresses at local coordinate zeta [-t/2,t/2]
      ! needs the material matrix (no thickness integration!) of the
      ! ! layer where zeta lies.
      ! st_g: Cauchy stress in g1, g2 (curvalinear coord.)
      ! st_e: in local Cartesian coord (laminate coord.)
      ! st_m: in principal material coord (fiber and its transverse)
      subroutine stress_KL(shgradl, shhessl, nshl, q, NSD, nor, 
     &                     B_NET_SH, B_NET_SH_D, lIEN, nnode,
     &                     zeta, st_m)
      implicit none      

      integer, intent(in) :: nshl, q, NSD,
     &                       lIEN(nshl), nnode
      real(8), intent(in) :: shgradl(NSHL,2), shhessl(NSHL,3), nor(3),
     &                       B_NET_SH(nnode,nsd+1),
     &                       B_NET_SH_D(nnode,nsd+1),
     &                       zeta
      real(8), intent(out):: st_m(3)

      real(8) :: dR(NSHL,2), ddR(NSHL,3)
      real(8) :: Gab_r(2,2), Bv_r(3), T_Gcon_E(3,3), T_E_G(3,3), dA_r
      real(8) :: T_sig(3,3), T_eps(3,3)
      real(8) :: gab(2,2), gab_con(2,2), bv(3), T_g_e(3,3), da
      real(8) :: E_cu(3), E_ca(3), K_cu(3), K_ca(3)
      real(8) :: detJk, e_zeta(3)
      real(8) :: st_pk2_ca(3), st_pk2(3), st_cau(3)
      real(8) :: st_g(5), st_e(5), st_vm
      real(8) :: tmp33(3,3), tmp32(3,2), tmp3(3), tmp22(2,2)

      real(8) :: Dmat(3,3), theta

      integer :: i

      Dmat = 0.0d0; theta = 0.0d0

      do i = 1, NSHL
        dR(i,:)  = shgradl(i,:)
        ddR(i,1) = shhessl(i,1)
        ddR(i,2) = shhessl(i,3)
        ddR(i,3) = shhessl(i,2)
      end do
      
      ! reference configuration
      call shell_geo(nshl, nsd, nnode, q, dR, ddR, lIEN, B_NET_SH, nor,
     &               tmp32, tmp3, dA_r, tmp3, Gab_r, tmp22, tmp33, 
     &               Bv_r, T_Gcon_E, T_E_G, tmp33)

      ! actual configuration
      call shell_geo(nshl, nsd, nnode, q, dR, ddR, lIEN, B_NET_SH_D,
     &               nor, tmp32, tmp3, da, tmp3, gab, gab_con, tmp33, 
     &               bv, tmp33, tmp33, T_g_e)

      detJk = da/dA_r

      ! strain vector [E11,E22,E12] referred to curvilinear coor sys
      E_cu(1) = 0.5d0*(gab(1,1)-Gab_r(1,1))
      E_cu(2) = 0.5d0*(gab(2,2)-Gab_r(2,2))
      E_cu(3) = 0.5d0*(gab(1,2)-Gab_r(1,2))

      ! strain coeff. in local cartesian coord. E_ca=[E11 E22 2E12]
      E_ca = MATMUL(T_Gcon_E,E_cu)

      ! curvature vector [K11,K22,K12] referred to curvilinear coor sys
      K_cu = -(bv-Bv_r)
      ! curvature coeff. in local cartesian coord. K_ca=[K11 K22 2K12]
      K_ca = MATMUL(T_Gcon_E,K_cu)
      
      ! Green-Lagrange strain at zeta
      E_zeta = E_ca + zeta*K_ca

      ! PK2 stress in local cartesian E1,E2
      st_pk2_ca = MATMUL(Dmat,E_zeta)
      ! PK2 stress in G1,G2
      st_pk2 = MATMUL(T_E_G,st_pk2_ca)
      ! Cauchy stress in g1,g2
      st_cau(:) = 1.0d0/detJk*st_pk2(:)

      st_g = 0.0d0
      ! Cauchy stress in normalized g1,g2
      st_g(1) = sqrt(gab(1,1)/gab_con(1,1))*st_cau(1)
      st_g(2) = sqrt(gab(2,2)/gab_con(2,2))*st_cau(2)
      st_g(3) = sqrt(gab(1,1)/gab_con(2,2))*st_cau(3)

      ! principal stress
      st_g(4)=0.5d0*(st_g(1)+st_g(2)
     &              +sqrt((st_g(1)-st_g(2))**2+4.0d0*st_g(3)**2))
      st_g(5)=0.5d0*(st_g(1)+st_g(2)
     &              -sqrt((st_g(1)-st_g(2))**2+4.0d0*st_g(3)**2))

      st_e = 0.0d0
      ! Cauchy stress in local cartesian e1,e2
      st_e(1:3) = MATMUL(T_g_e,st_cau)
      ! principal stress
      st_e(4)=0.5d0*(st_e(1)+st_e(2)
     &              +sqrt((st_e(1)-st_e(2))**2+4.0d0*st_e(3)**2))
      st_e(5)=0.5d0*(st_e(1)+st_e(2)
     &              -sqrt((st_e(1)-st_e(2))**2+4.0d0*st_e(3)**2))

      ! von Mises stress
      st_vm = 0.0d0
      st_vm = sqrt(st_e(4)**2-st_e(4)*st_e(5)+st_e(5)**2)
      
      ! rotate the Caucay stress from laminate coordinates (local
      ! Cartesian) to principal material coordinates, which has 
      ! st_m(1) pointting in the fiber direction and 
      ! st_m(2) in the transverse direction
      call composite_rotation_mat(theta, T_sig, T_eps)
      st_m = 0.0d0
      st_m(1:3) = MATMUL(T_sig, st_e(1:3))


      ! HACK
      st_m = E_zeta
          
          
      st_m(1) = 0.5d0*(E_zeta(1)+E_zeta(2)) 
     &  + sqrt((0.5d0*(E_zeta(1)-E_zeta(2)))**2+E_zeta(3)**2)

      st_m(2) = 0.5d0*(E_zeta(1)+E_zeta(2)) 
     &  - sqrt((0.5d0*(E_zeta(1)-E_zeta(2)))**2+E_zeta(3)**2)

      st_m(3) = 0.0d0   
      end subroutine stress_KL
