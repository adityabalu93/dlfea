module commonvar_shell
  implicit none
  save
  
  integer :: NBC_set

end module commonvar_shell

!------------------------------------------------------------------------
!    Module for storing arrays and allocation routines
!------------------------------------------------------------------------     
module types_shell

  implicit none
  
  ! type for gauss point
  type :: gptype
     ! number of actual points (okay to allocate more)
     real(8), allocatable :: xg(:,:,:), nor(:,:,:), param(:,:,:), &
          cparam(:,:,:), backxi(:,:,:), &
          gw(:,:), detJ(:,:), tv1(:,:,:), tv2(:,:,:), &
          ush(:,:,:), ash(:,:,:), &
          traction(:,:,:), lam(:,:), &
          contactForce(:,:,:), closestDist(:,:), &
          contactVelForce(:,:,:), contactLhs(:,:,:), contactLhsVel(:,:,:)
     integer, allocatable :: closestPoint(:,:,:), cien(:,:), &
          cien_sgn(:,:), cnshl(:), celem(:), backelem(:,:)
  end type gptype
  
  ! Declare type mesh
  type :: mesh
    ! gauss points for the nurbs mesh
   type(gptype) :: gp

   ! degree in U and V for each patch
   integer, allocatable :: P(:), Q(:), boundary(:)

   ! patch type
   ! 0-blade; 1-bending strips; 2-shear web; ...
   integer, allocatable :: PTYPE(:)

   ! Knot vectors in u and v directions for each element
   real(8), allocatable :: U_KNOT(:,:), V_KNOT(:,:)

   ! Size of the knot vector for each elements (e.g. NUK=P+MCP+1)
   integer, allocatable :: NUK(:), NVK(:)

   ! Control Net
   real(8), allocatable :: B_NET(:,:), B_NET_U(:,:), B_NET_D(:,:)

   ! Boundary condition indicator for global nodes and edges
   ! respectively
   integer, allocatable :: IBC(:,:)

   ! array to store force vectors on the wind turbine blade
   real(8), allocatable :: FORCE(:,:)

   ! Global connectivity array
   integer, allocatable :: IEN(:,:), INN(:,:)

   ! array giving the patch number for each element
   integer, allocatable :: elementPatchNumbers(:)

   ! number of shape functions for every element
   integer, allocatable :: NSHL(:)

   ! Bezier extraction operator
   real(8), allocatable :: Ext(:,:,:)

   ! array for neighboring elements
   integer, allocatable :: elementNeighbors(:,:)

   ! Array for closest points
   integer, allocatable :: CLE(:)
   real(8), allocatable :: CLP(:,:)

   integer :: NGAUSS, NNODE, NEL, maxNSHL, nIBC

   ! array for Solution vectors
   real(8), allocatable :: dsh(:,:), dshold(:,:), &
                           ush(:,:), ushold(:,:), &
                           ash(:,:), ashold(:,:), &
                           lam(:,:)

    ! Blade rotation. 0 degree is the straight-up position
    real(8) :: BldRot

    ! shell thickness
    real(8) :: thickness_shell, thickness_stent
	
	  real(8) :: E_Shell, nu_Shell, E_Stent, nu_Stent
  end type mesh


  ! Declare type mesh for multi-patch
  type :: mesh_mp
    ! degree in U and V for each patch
    integer, allocatable :: P(:), Q(:)

    ! number of control points in U and V for each patch
    ! (no need for T-spline)
    integer, allocatable :: MCP(:), NCP(:)

    ! Total number of control points and elements for each patch
    integer, allocatable :: NNODE(:), NEL(:)

    ! patch type
    ! 1-blade surface; 0-bending strips; 2-shear web; ...
    integer, allocatable :: PTYPE(:), TPTYPE(:,:)

    ! Knot vectors in u and v directions for each patch
    real(8), allocatable :: U_KNOT(:,:), V_KNOT(:,:)

    ! Control Net
    real(8), allocatable :: B_NET(:,:,:)

    ! Boundary condition indicator for global nodes and edges
    ! respectively
    integer, allocatable :: IBC(:,:,:)

    ! array to store force vectors on the wind turbine blade
    real(8), allocatable :: FORCE(:,:,:)

    ! Mapping between patches and global reduced numbering
    ! e.g., MAP(1,2) = 3 means 1st patch, 2nd node points to 
    !   global reduced node number 3
    integer, allocatable :: MAP(:,:)

    ! array to store indices of neighboring elements
    integer, allocatable :: elementNeighbors(:,:,:)
    
    ! Define NSHL
    integer, allocatable :: Pb(:,:), Qb(:,:), nIBC(:), &
                            enIBC(:), bound_elm(:,:)
    integer, allocatable :: NSHL(:,:), boundary(:,:)
    integer, allocatable :: IEN(:,:,:)   
    real(8), allocatable :: EXT(:,:,:,:)
  end type mesh_mp

  ! Declare type shell
  type :: shell
    ! number of patches for Blade Surface (S). Matches the fluid mesh
    ! number of patches for blade structure (B). May include shear webs
    ! number of total patches including bending strips (T)
    integer :: NPS, NPB, NPT

    integer :: S_Flag, Nnewt, Nincr, Rincr, Nstep, C_Flag, M_Flag, &
               T_Flag, Sol_Flag, FF_Flag
    real(8) :: RHSGtol, DGtol, f_fact, G_fact(3), RHSGNorm, DGNorm, &
               RHSFact, thickness

    ! row, col, and total of nonzero entries for sparse structure
    integer, allocatable :: row(:), col(:)
    integer :: icnt

    integer, allocatable :: ndiag(:)

    ! The right hand side load vector G and left hand stiffness matrix K
    real(8), allocatable :: RHSG(:,:), LHSK(:,:), LHSKV(:), &
                            RHSG_EXT(:,:), RHSG_CEN(:,:), &
                            RHSG_GRA(:,:)

    ! Solution vectors
    real(8), allocatable :: yg(:,:), dg(:,:), tg(:), &
                            mg(:), dl(:,:), ygv(:)

    ! material matrix for composite
    integer :: NMat
    real(8), allocatable :: matA(:,:,:), matB(:,:,:), matD(:,:,:)

    
  end type shell

end module types_shell
