!------------------------------------------------------------------------
!     Main routine to call all the subroutines        
!     This is for the multi-patch elasticity routine
!------------------------------------------------------------------------
  program main

  use types_shell
  implicit none

  type(mesh_mp) :: mNRB, mTSP, mBEZ
  type(mesh)    :: NRB, TSP, BEZ

  ! number of materials
  integer, parameter :: NSD = 3, NVAR1 = 6, NVAR2 = 6

  integer :: so, sf, i, j, k, ip, ier, &
             sumNDZ, sumNEL, nf, NMat, maxPly,  &
             lnod, eloc, eglob, &
             p, q, mcp, ncp, nshl, nel, nnode, kply, ptype, &
             inewt_SH, Nnewt_SH, &
             NPATCH, NEL_NZA_SH, istep, sstep, estep, skip, &
             maxP, maxQ, maxMCP, maxNCP, maxNNODE, maxNEL, tmp

  real(8), allocatable :: strain(:,:), force(:,:), dg_SH(:,:), &
                          tg_SH(:), mg_SH(:), gstress(:,:,:,:)

  character(len=30) :: fname(2), cname(2), fnum
  character(len=10) :: inp

  nf = 99

  ! get main inputs
  i = iargc()
  if (i == 2) then
    call getarg(1,inp); read(inp,*) NPATCH
    call getarg(2,inp); read(inp,*) sstep

    estep = sstep
    skip  = 1

  else if (i == 4) then

    call getarg(1,inp); read(inp,*) NPATCH
    call getarg(2,inp); read(inp,*) sstep
    call getarg(3,inp); read(inp,*) estep
    call getarg(4,inp); read(inp,*) skip

  else
    write(*,*) 'Usage 1: NPatch start'
    write(*,*) 'Usage 2: NPatch start end skip'
    stop
  end if

  ! get shell thickness
!   call getparam(NRB)
  call getparam(TSP)
  write(*,*) "getparam is done!"
  ! get main input
!   call shell_input_nrb_mp(NPATCH, NSD, maxP, maxQ, maxMCP, &
!                           maxNCP, maxNNODE, NRB%maxNSHL, mNRB)
  call shell_input_tsp_mp(NPATCH, NSD, maxP, maxQ, &
                          maxNNODE, TSP%maxNSHL, maxNEL, mTSP, mBEZ)
  ! remove duplcate nodes
!   call reduce_node(NSD, NPATCH, mNRB%NNODE, mNRB%NEL, &
!                    mNRB%B_NET, maxNNODE, mNRB%MAP, &
!                    NRB%NNODE, sumNDZ, sumNEL)
  call reduce_node(NSD, NPATCH, mTSP%NNODE, mTSP%NEL, &
                   mTSP%B_NET, maxNNODE, mTSP%MAP, &
                   TSP%NNODE, sumNDZ, sumNEL)
   
  ! number of total elements (non-overlaping)
!   NRB%NEL = sum(mNRB%NEL)
  TSP%NEL = sum(mTSP%NEL)
  write(*,*) 'total elements number', TSP%NEL
  
!   write(*,*)
!   write(*,*) "total   NNODE = ", sum(mNRB%NNODE)
!   write(*,*) "reduced NNODE = ", NRB%NNODE
!   write(*,*) "total   NEL   = ", NRB%NEL
!   write(*,*)

  !================================
  ! NURBS
  !================================
!   ! remove patch structure for NURBS
!   call shell_input_nrb(mNRB, NRB, NPATCH, NSD, maxP, maxQ, maxMCP, maxNCP)
!
!   NRB%NGAUSS = max(maxP,maxQ)+1
  
  call shell_input_tsp(NSD, NPATCH, mTSP, mBEZ, TSP, BEZ, maxP, maxQ)

  ! allocate space for gauss points
!   NRB%NGAUSS = 4
!  NRB%NGAUSS = 3
  TSP%NGAUSS = 4

  write(*,*) "** SHELL ***************************************"
!     write(*,*) "total   NNODE = ", sum(mNRB%NNODE)
!     write(*,*) "reduced NNODE = ", NRB%NNODE
!     write(*,*) "total   NEL   = ", NRB%NEL
  write(*,*)
  write(*,*) "total T-Spline NNODE = ", TSP%NNODE
  write(*,*) "total T-Spline NEL   = ", TSP%NEL
  write(*,*)
!     write(*,*) "NRB%NGAUSS, TSP%NGAUSS=", NRB%NGAUSS!, TSP%NGAUSS
  write(*,*) "TSP%NGAUSS=", TSP%NGAUSS
  write(*,*) "************************************************"
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Write a iga file for TSP
!     write(*,*) "All patches data are written in tmesh.0.dat"
    !call write_sol_tsp_full(NSD, SHL%NPB, TSP, BEZ, mTSP)
  write(*,*) "input_shell is done!"


  ! allocate array for strain and strain resultant
  ! strain(:,:,:,1:6) are st1, st2, st12 for bottom and top 
  allocate(strain(TSP%NNODE,NVAR1), &
           force(TSP%NNODE,NVAR2), dg_SH(TSP%NNODE,NSD), &
           mg_SH(TSP%NNODE))

  do istep = sstep, estep, skip

    strain = 0.0d0; force = 0.0d0
    dg_SH = 0.0d0; mg_SH = 0.0d0

!     ! read in the displacement patch by patch
!     do ip = 1, NPATCH
!       write(cname(1),'(I8.2)') ip
!       write(cname(2),'(I30)')  istep
!       fname(1) = trim(adjustl(cname(2)))//'.'//trim(adjustl(cname(1)))
!
!       fname(2) = 'sh.rest.'//fname(1)
!       open(nf, file=fname(2), status='old', iostat=ier)
!       read(nf,*) ! skip first line
!       do i = 1, mNRB%NNODE(ip)
!         read(nf,*) (dg_SH(mNRB%MAP(ip,i),k), k = 1, 3)
!       end do
!       close(nf)
!     end do
    write(fname(1),'(I30)') istep
    fnum = 'sh.rest.tsp.'//trim(adjustl(fname(1)))
    open(nf, file=fnum, status='old')                               
    do i = 1, TSP%NNODE
      read(nf,*) (dg_SH(i,k), k = 1, 3)
    end do
    close(nf) 
    
    write(*,*) "Reading data in tsp at step: ", istep
    !================================================================
    ! Begin strain analysis
    !================================================================
    TSP%B_NET_D(:,1:3) = TSP%B_NET(:,1:3) + dg_SH(:,:)
    write(*,*) "TSP%B_NET_D is done"
    
!     call IntElmAss_shell_str(NRB, NSD, NVAR1, strain, mg_SH)
    call IntElmAss_shell_str_tsp(TSP, BEZ, NSD, NVAR1, strain, mg_SH)
    
    ! L2-Projection: divded by the lumped mass (area of influence)
    do k = 1, NVAR1
      strain(:,k) = strain(:,k)/mg_SH(:)
    end do

    !===========================================
    ! for visualization
    !===========================================
!     do ip = 1, NPATCH
!
!         ! output strain resultant and thickness
!         write(cname(1),'(I8.2)') ip
!         write(cname(2),'(I30)')  istep
!
!         fname(1) = trim(adjustl(cname(2)))//'.'//trim(adjustl(cname(1)))
!
!         fname(2) = 'strain.'//fname(1)
!         open(62, file=fname(2), status='replace')
!
!         do i = 1, mNRB%NNODE(ip)
!           write(62,*) (strain(mNRB%MAP(ip,i),k), k = 1, NVAR1)
!         end do
!         close(62)
!
!     end do
    
    
    fnum = 'strain.tsp.'//trim(adjustl(fname(1)))
    open(nf, file=fnum, status='replace')             
                       
    do i = 1, TSP%NNODE
      write(nf,*) (strain(i,j), j = 1, NVAR1)
    end do 
    close(nf)  
    write(*,*) "strain is done for istep :", istep
  end do

  deallocate(force, strain, dg_SH, mg_SH)
  
end program main
