  subroutine IntElmAss_shell_str_tsp(TSP, BEZ, NSD, NVAR, RHSG_SH, mg_SH)

  use types_shell
  
  implicit none

  type(mesh), intent(in) :: TSP, BEZ

  integer, intent(in) :: nsd, NVAR

  real(8), intent(out) :: RHSG_SH(TSP%NNODE,NVAR), &
                          mg_SH(TSP%NNODE)

  !  Local variables
  integer :: p, q, nshl, nshb, nuk, nvk, ptype, iel, igauss, jgauss, &
             i, j, ni, nj, aa, bb, igp, ct, ii

  real(8) :: gp(TSP%NGAUSS), gw(TSP%NGAUSS), gwt, da, VVal, &
             DetJb, zeta, nor(NSD), thi, str(NVAR), &
             xu(NSD), xd(NSD), dxdxi(NSD,2), ddxddxi(nsd,3)
             
  integer, allocatable :: lIEN(:)
  real(8), allocatable :: shl(:), shgradl(:,:), shhessl(:,:), &
                          Rhs(:,:), lmass(:)

  ! get Gaussian points and weights
  gp = 0.0d0; gw = 0.0d0
  call genGPandGW_shell(gp, gw, TSP%NGAUSS) 
  write(*,*) "Gaussian points and weights are done"
  
  ! loop over elements
  do iel = 1, TSP%NEL
    
    p = BEZ%P(iel); nshl = TSP%NSHL(iel); ptype = TSP%PTYPE(iel)
    q = BEZ%Q(iel); nshb = BEZ%NSHL(iel)
         
      allocate(shl(nshl), shgradl(nshl,2), &
               shhessl(nshl,3), lIEN(nshl), &
               Rhs(NVAR,nshl), lmass(nshl))

      lIEN = -1
      do i = 1, nshl
        lIEN(i) = TSP%IEN(iel,i)
      end do      

      ! initialization 

      Rhs     = 0.0d0      ! initialize local load vector
      ct = 0
      lmass = 0.0d0

      ! Loop over integration points (NGAUSS in each direction) 
      do jgauss = 1, TSP%NGAUSS
        do igauss = 1, TSP%NGAUSS
          
          ct = ct + 1
        
          ! Get Element Shape functions and their gradients
          shl = 0.0d0; shgradl = 0.0d0; shhessl = 0.0d0
          xu = 0.0d0; xd = 0.0d0; dxdxi = 0.0d0; ddxddxi = 0.0d0
          nor = 0.0d0; str = 0.0d0

          call eval_SHAPE_bez_sh(gp(igauss), gp(jgauss), &
                                  shl, shgradl, shhessl, nor, &
                                  xu, xd, dxdxi, ddxddxi,  &
                                  p, q, nsd, nshl, nshb,  &
                                  lIEN, TSP%NNODE, &
                                  TSP%B_NET_U, TSP%B_NET_D, DetJb, &
                                  BEZ%Ext(iel,1:nshl,1:nshb))

          gwt = gw(igauss)*gw(jgauss)
          
!           nor = nor/sqrt(sum(nor*nor))

!           ! define the density and thickness of the stick
!           dens = Density_Shell   !0.1d0
!           thi  = Thickness_Shell  !0.06d0
          
          if (ptype == 4) then
            thi  = TSP%Thickness_Stent
          else
            thi  = TSP%Thickness_Shell
          end if
 
           !======================================================
           ! evaluate the stress and resultant force
           !======================================================
           ! stress (bottom)
           zeta = -0.5d0*thi
           call stress_KL(shgradl, shhessl, nshl, q, nsd, nor, &
                          TSP%B_NET, TSP%B_NET_D, lIEN, &
                          TSP%NNODE, zeta, str(1:3))

           ! stress (top)
           zeta = 0.5d0*thi
           call stress_KL(shgradl, shhessl, nshl, q, nsd, nor, &
                          TSP%B_NET, TSP%B_NET_D, lIEN, &
                          TSP%NNODE, zeta, str(4:6))

           !=====================================================
           ! L2-projection
           
           str(1:3) = xu(:)!- xu(:)
           str(4:6) = xu(:)
           
           do ii = 1, nshl
!              Rhs(:,ii) = Rhs(:,ii) + str(:)*shl(ii)*DetJb*gwt
              Rhs(1:3,ii) = Rhs(1:3,ii) + str(1:3)*shl(ii)*DetJb*gwt
              Rhs(4:6,ii) = Rhs(4:6,ii) + str(4:6)*shgradl(ii,1)*DetJb*gwt
             lmass(ii) = lmass(ii) + shl(ii)*DetJb*gwt
	  
           end do

        end do
      end do  ! end loop gauss points

      do ii = 1, NSHL
        ! stress
        RHSG_SH(lIEN(ii),:) = RHSG_SH(lIEN(ii),:) + Rhs(:,ii)
  
        mg_SH(lIEN(ii)) = mg_SH(lIEN(ii)) + lmass(ii)
  
      end do

  
      deallocate(shl, shgradl, shhessl, Rhs, lIEN, lmass)

  end do    ! end loop elements

  end subroutine IntElmAss_shell_str_tsp
