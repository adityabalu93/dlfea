!--------------------------------------------------------------------
! program to extract the shell T-mesh
!--------------------------------------------------------------------
subroutine shell_input_tsp_mp(NP, NSD, maxP, maxQ, &
                              maxNNODE, maxNSHL, maxNEL, mTSP, mBEZ)

  use types_shell
  use commonvar_shell
  implicit none

  type(mesh_mp), intent(out) :: mTSP, mBEZ

  integer, intent(in)  :: NP, NSD
  integer, intent(out) :: maxP, maxQ, maxNNODE, maxNSHL, maxNEL

  integer :: i, j, k, l, mf, ip, ier, tmp, ct, nelu, nelv, &
       iel, niel, nborindex, nIBC, tp

  character(len=30) :: fname, cname, tname, partname, fixed

  allocate(mTSP%P(NP),     mTSP%Q(NP), &
           mTSP%MCP(NP), &
           mTSP%NNODE(NP), mTSP%NEL(NP))

! The degree of the T-spline basis functions in each 
! parametric direction. (not used in the code)
  mTSP%P     = 3
  mTSP%Q     = 3
  
  ! MCP is used for calculating the maxNSHL
  mTSP%MCP   = 0

  mTSP%NNODE = 0
  mTSP%NEL   = 0
  
  ! first loop through all the patches to find the max number of
  ! parameter. This will be used for allocating other arrays.
  do ip = 1, NP

    mf = 11 
    ! Read in preliminary information
    write(cname,'(I8)') ip
    fname = 'tmesh.'//trim(adjustl(cname))//'.iga'
    open(mf, file=fname, status='old')
    ! number of spatial dimensions. Usually NSD = 3
    read(mf,*)   
    ! The number of global T-spline basis functions or control
    ! points in the T-mesh.
    read(mf,*) tname, mTSP%NNODE(ip)

    ! The number of bezier elements which constitute the T-spline.
    read(mf,*) tname, mTSP%NEL(ip)

    ! These two entries are added by me for allocating arrays
    ! The maximum number of global T-spline basis functions which 
    ! are non-zero over eacg element
    read(mf,*) tname, mTSP%MCP(ip)

    close(mf)
  end do ! end loop patches

  
  ! these maximum values incluid bending stripes
  maxP     = maxval(mTSP%P)
  maxQ     = maxval(mTSP%Q)
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!need to change
  maxNSHL   = maxval(mTSP%MCP)

  
  
  maxNNODE = maxval(mTSP%NNODE)
  maxNEL = maxval(mTSP%NEL)
  if (maxNSHL  <= (maxP+1)*(maxQ+1)) then 
    maxNSHL =(maxP+1)*(maxQ+1)
  end if

  ! Allocate arrays for control net
  allocate(mTSP%B_NET(NP,maxNNODE,NSD+1))
  allocate(mTSP%IBC(  NP,maxNNODE,NSD))
  allocate(mTSP%IEN(  NP,maxNEL,maxNSHL))
  allocate(mTSP%NSHL(  NP,maxNEL))
  allocate(mTSP%NIBC(  NP))
  allocate(mTSP%boundary( NP, maxNNODE))
  allocate(mTSP%enIBC(  NP))
  allocate(mTSP%bound_elm( NBC_set, maxNEL)) 
  allocate(mTSP%TPTYPE(NP, maxNEL)) 
  
  mTSP%B_NET  = 0.0d0
  mTSP%IBC    = 0
  mTSP%IEN    = 0
  mTSP%NSHL    = 0
  mTSP%nIBC  = 0 
  mTSP%enIBC  = 0 
  mTSP%boundary = 0
  mTSP%bound_elm = 0
  mTSP%TPTYPE = 0  
         
  allocate(mBEZ%Pb(NP, maxNEL), mBEZ%Qb(NP,maxNEL), &
           mBEZ%NSHL(NP, maxNEL), &
           mBEZ%EXT(NP, maxNEL, maxNSHL, maxNSHL), stat=ier)
  if (ier /= 0) stop 'Allocation Error: TSP%IEN'
  mBEZ%Pb = 0; mBEZ%Qb = 0; mBEZ%NSHL = 0
  mBEZ%EXT = 0.0d0
  
  ! now loop through the patches again to read in the rest of 
  ! the information
  do ip = 1, NP
    mf = 12  

    ! Read in preliminary information
    write(cname,'(I8)') ip
    fname = 'tmesh.'//trim(adjustl(cname))//'.iga'
    open(mf, file=fname, status='old') 
    ! skip "4" lines
    do i = 1, 4
        read(mf,*)  
    end do
    
    ! The control point (x, y, z, w) associated with each global 
    ! T-spline basis function in the T-mesh. The ith T-spline  denoted
    ! control point is by the token "gi". NOTE: These control points
    ! are NOT in homogeneous form i.e in homogeneous form the control
    ! points have the form (xw, yw, zw, w).
    do i = 1, mTSP%NNODE(ip)
      read(mf,*) tname, (mTSP%B_NET(ip, i, j), j = 1, NSD+1)
    end do  
    
    do iel = 1, mTSP%NEL(ip)

      ! The first two integers specify the degree of the bezier 
      ! element in s followed by the degree in t. The third number 
      ! denotes the number of global T-spline basis functions which 
      ! are non-zero over this element. Note that this number often
      ! varies from element to element in a T-spline. 
      ! This is why we need to find maxNSHL_TS in advanced for 
      ! allocating arrays.
      read(mf,*) tname, mTSP%NSHL(ip, iel), mBEZ%Pb(ip, iel), mBEZ%Qb(ip, iel)

      mBEZ%NSHL(ip, iel) = (mBEZ%Pb(ip, iel)+1)*(mBEZ%Qb(ip, iel)+1)
    
      mTSP%TPTYPE(ip, iel) = ip

      ! For each global T-spline basis function which is non-zero
      ! over this element the global index of the basis function 
      ! is listed.
      read(mf,*) (mTSP%IEN(ip,iel,j), j = 1, mTSP%NSHL(ip,iel))
      mTSP%IEN(ip,iel,1:mTSP%NSHL(ip, iel)) = mTSP%IEN(ip,iel,1:mTSP%NSHL(ip,iel))+1

      do j = 1, mTSP%NSHL(ip,iel)
        read(mf,*) (mBEZ%Ext(ip,iel,j,k), k = 1, mBEZ%NSHL(ip, iel))
      end do
      
    end do
    

    ! read IBC flag and give settings.
    do k = 1, NBC_set
      read(mf,*) tname, nIBC, partname, tp, fixed
    
      if (partname == 'node') then
          mTSP%nIBC(ip) = nIBC
          if (mTSP%nIBC(ip)/=0) then
            read(mf,*) (mTSP%boundary(ip,i), i = 1,mTSP%nIBC(ip))
            mTSP%boundary(ip,:)=mTSP%boundary(ip,:)+1
  
            do i = 1, mTSP%nIBC(ip)
              mTSP%IBC(ip,mTSP%boundary(ip,i),:) = 1
            end do  
          end if
        
        else if (partname == 'elem') then
          mTSP%enIBC(ip) = nIBC
          if (mTSP%enIBC(ip)/=0) then
            read(mf,*) (mTSP%bound_elm(k,i), i = 1,mTSP%enIBC(ip))
            mTSP%bound_elm(k,:)=mTSP%bound_elm(k,:)+1
  
            do i = 1, mTSP%enIBC(ip)
              iel = mTSP%bound_elm(k,i)
              mTSP%TPTYPE(ip, iel) = tp
              if (fixed == 'fixed') then
                do j = 1, mTSP%NSHL(ip,iel)
                  mTSP%IBC(ip,mTSP%IEN(ip,iel,j),:) = 1
                end do 
              end if
            end do  
          else
            write(*,*) "No Boundary Element Defined!" 
          end if
        
        else
          write(*,*)  "No Boundary Defined!"
      end if
    end do
    
    close(mf)
    
    do i = 1, mTSP%NNODE(ip)
      if (mTSP%IBC (ip,i,1) /= 0) then
          mTSP%nIBC(ip) = mTSP%nIBC(ip) + 1
      end if
    end do
   
!     if (myid == mpi_master) then
       write(*,*) "Number of BC nodes at Patch", ip, ":", mTSP%nIBC(ip)
    ! end if
  end do ! end loop patches
  
  ! remove duplcate nodes
  ! the reduced node count that counts only master nodes 
  ! in file "input_mp"
  allocate(mTSP%MAP(NP,maxNNODE), stat=ier)
  if (ier /= 0) stop 'Allocation Error: mNRB%MAP'
  mTSP%MAP = -1
  
  
end subroutine shell_input_tsp_mp
