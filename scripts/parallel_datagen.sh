#!/bin/bash

# Copy/paste this job script into a text file and submit with the command:
#    sbatch thefilename
# job standard output will go to the file slurm-%j.out (where %j is the job ID)

#SBATCH --time=72:00:00   # walltime limit (HH:MM:SS)
#SBATCH --nodes=1   # number of nodes
#SBATCH --ntasks-per-node=36   # 36 processor core(s) per node 
#SBATCH --job-name="Closed$1"
#SBATCH --mail-user=baditya@iastate.edu   # email address
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL

module load parallel
module load intel
cd ..

echo "beginning executing all cases..."

parallel -j 36 --controlmaster --delay 0.2 --joblog par_status.log ./scripts/runParallel.sh :::: scripts/geometries${1}.txt :::: scripts/parameters.txt
