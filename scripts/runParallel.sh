#/usr/bin/bash
#ROOT=/work/adarsh/Aditya/NatureMI/GitRepo
OUTLOG=datagen_april2019.log

mkdir Data/$1_$2
cd Data/$1_$2

cp -r ../../03Setups/Params/$2.dat param.dat

cp ../../01HVDesign/Geometries/$1/* .

CODE=../../bin/HV_TSP_MPI.exe

mpirun -n 1 $CODE 2>&1>Solution.log

echo "Run of Geometry $1 and param $2 is completed"> $OUTLOG

cd ../../
