      module commonvar

      implicit none
      
      integer :: NSD, P, Q, R, MCP, NCP, OCP, NNODZ, NEL, 
     &           NEL_NZA, NFACE, 
     &           nnodz_glob_lin, NEL_GLOB_LIN, NVAR,
     &           nnodz_glob, NEL_GLOB, NEL_LOC, rflag

      integer :: ptype
      integer, allocatable :: nnodz_loc(:)
      
      integer, allocatable :: partELE(:,:), ELEpart(:)

      real(8) :: DetJ, DetJb, ENRGY, ERROR, sfact, rot_deg



      integer :: PNSD, P_TS, Q_TS, NNODZ_TS, NEL_TS, maxNSHL_TS,
     &           maxNSHL_BZ, NPats
     
      logical :: if_over_write

      end module commonvar
