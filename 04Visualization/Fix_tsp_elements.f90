subroutine Fix_tsp_elements
  
  
  use aAdjKeep
  use commonvar
  implicit none 
  
  integer :: i, j, ne, j1, j2, nn, k, ne1, ne2, neps, k1, k2, ix, ele1, ele2
  integer, allocatable :: node_table(:,:), node_new(:)
  real(8), allocatable :: eleMINL(:)
  integer :: p2s
  integer :: ix1, ix2
  real(8) :: dist, distMIN, meanD
  logical :: ckn
  
  allocate(node_new(nnodz_glob))
  
  ! ========== reduced nodes ============
  
 !  do i = 1, nnodz_glob
!       node_new(i) = i
!   end do
!
!   distMIN = 3.8e-1
!   !distMIN = 3.8e-3
!
!   allocate(eleMINL(NEL_GLOB))
!   do i = 1, NEL_GLOB
!       eleMINL(i) = 1.0d10
!       do k1 = 1, 3
!           ix1 = IEN_GLOB(i, k1)
!           ix2 = IEN_GLOB(i, k1+1)
!           dist = sum((x_glob(ix1,1:3) - x_glob(ix2,1:3))**2)
!           if (eleMINL(i) > dist) then
!               eleMINL(i) = dist
!           end if
!       end do
!   end do
!
!
!   do i = 1, NEL_GLOB
!       do j = (i+1), NEL_GLOB
!
!           neps = 0
!
!           do k1 = 1, 4
!               do k2 = 1, 4
!                   ix1 = IEN_GLOB(i, k1)
!                   ix2 = IEN_GLOB(j, k2)
!                   if (ix1 == ix2) neps = neps + 1
!               end do
!           end do
!
!           if (neps == 2) CYCLE
!
!           meanD = (eleMINL(i) + eleMINL(j)) * 0.5d0
!
!           do k1 = 1, 4
!               do k2 = 1, 4
!
!                   ix1 = IEN_GLOB(i, k1)
!                   ix2 = IEN_GLOB(j, k2)
!
!                   dist = sum((x_glob(ix1,1:3) - x_glob(ix2,1:3))**2)
!
!                   if (dist < distMIN*distMIN*meanD) then
!                       node_new(ix1) = ix2
!                   end if
!
!               end do
!           end do
!
!       end do
!   end do
!
!   do i = 1, NEL_GLOB
!       do j = 1, 4
!           ix1 = IEN_GLOB(i, j)
!           ix2 = node_new(IEN_GLOB(i, j))
!
!           x_glob(ix2,1:3) = 0.5d0*(x_glob(ix1,1:3) + x_glob(ix2,1:3))
!
!           IEN_GLOB(i, j) = node_new(IEN_GLOB(i, j))
!
!       end do
!   end do
!
!   deallocate(eleMINL)
  
  ! ============================================ 

  p2s = 250
  
  ! node_table(:,1~9) adjacent elements
  ! node_table(:,10~20) adjacent nodes
  allocate(node_table(nnodz_glob, 500))
  node_table = 0
  
  do i = 1, NEL_GLOB
      do j = 1, 4
          
          !if (node_table(IEN_GLOB(i,j),1) > 200) CYCLE
          
          node_table(IEN_GLOB(i,j),1) = node_table(IEN_GLOB(i,j),1) + 1
          
          ne = node_table(IEN_GLOB(i,j),1)
          node_table(IEN_GLOB(i,j), ne + 1) = i
      end do 
  end do
  
  
  !do i = 1, nnodz_glob
  !   write(*,*) node_table(i, 1:6)
  !end do
  
  do i = 1, NEL_GLOB
      do j = 1, 4
          j1 = MODULO(j + 1 - 1, 4) + 1
          j2 = MODULO(j - 1 - 1, 4) + 1
          
          nn = node_table(IEN_GLOB(i,j),p2s)
          ckn = .false.
          do k = 1, nn
              if (node_table(IEN_GLOB(i,j),p2s+k) == IEN_GLOB(i,j1)) then
                  ckn = .true.
              end if
          end do
          
          !if (node_table(IEN_GLOB(i,j),10) > 7) ckn = .true.
          
          if (.not. ckn) then
              node_table(IEN_GLOB(i,j),p2s) = node_table(IEN_GLOB(i,j),p2s) + 1
              nn = node_table(IEN_GLOB(i,j),p2s)
              node_table(IEN_GLOB(i,j),p2s+nn) = IEN_GLOB(i,j1)
          end if
          
          nn = node_table(IEN_GLOB(i,j),p2s)
          ckn = .false.
          do k = 1, nn
              if (node_table(IEN_GLOB(i,j),p2s+k) == IEN_GLOB(i,j2)) then
                  ckn = .true.
              end if
          end do
          
          !if (node_table(IEN_GLOB(i,j),10) > 7) ckn = .true.
          
          if (.not. ckn) then
              node_table(IEN_GLOB(i,j),p2s) = node_table(IEN_GLOB(i,j),p2s) + 1
              nn = node_table(IEN_GLOB(i,j),p2s)
              node_table(IEN_GLOB(i,j),p2s+nn) = IEN_GLOB(i,j2)
          end if
          
      end do 
  end do
  
  ! do i = 1, nnodz_glob
!        write(*,*) node_table(i, 10:18)
!     end do
  
  do i = 1, nnodz_glob
      
      node_new(i) = i
      
      ne1 = node_table(i,1)
      if (ne1 == 2) then
          nn = node_table(i,p2s)
          
          neps = 0
          do j = 1, nn
              ix = node_table(i,p2s + j)
              if (node_table(ix,1) == 1) then
                  neps = neps + 10
              end if
              if (node_table(ix,1) == 2) then
                  neps = neps + 1
              end if
          end do
          
          if (neps >= 2) CYCLE
          
          do j = 1, nn
              ix = node_table(i,p2s + j)
              ne2 = node_table(ix,1)
              
              neps = 0
              
              do k1 = 1, ne1
                  do k2 = 1, ne2
                      ele1 = node_table(i,1+k1)
                      ele2 = node_table(ix,1+k2)
                      if (ele1 == ele2) then
                          neps = neps + 1
                      end if
                  end do
              end do
              
              if (neps == 1) then
                  !write(*,*) x_glob(ix,1:3)+y_glob(ix,1:3)
                  !x_glob(i,1:3) = x_glob(ix,1:3)
                  !y_glob(i,1:3) = y_glob(ix,1:3)
                  node_new(i) = ix
                  EXIT
              end if
              
          end do
          
      end if
  end do
  
  do i = 1, NEL_GLOB
      do j = 1, 4
          IEN_GLOB(i, j) = node_new(IEN_GLOB(i, j))
      end do
  end do
  
  
  
  
  !do i = 1, nnodz_glob
  !    write(*,*) node_table(i, 1:6), node_table(i, 10:16)
  !end do
  


  
  
  deallocate(node_table, node_new)
  
  
end subroutine Fix_tsp_elements