!--------------------------------------------------------------------
! program to extract the shell mesh
!--------------------------------------------------------------------

subroutine shell_input_tspline

    use aAdjKeep
    use commonvar
    implicit none

    integer :: i, j, ier, mf, iel, iNSHL, Reason, mp_nel, pos, ps
  
    character(len=80) :: tmp
    character(len=30) :: fname, tname, tname2, tname3, partname
    
    integer, allocatable :: tempArray(:)
    
    NSD = 3

    mf = 12
    fname = trim('tmesh.1.iga')
    open(mf, file = fname, status = 'old')
    
    ! number of spatial dimensions. Usually NSD = 3
    read(mf,*)   
    ! The number of global T-spline basis functions or control
    ! points in the T-mesh.
    read(mf,*) tname, NNODZ_TS
    
    ! The number of bezier elements which constitute the T-spline.
    read(mf,*) tname, NEL_TS
    
	! skip nodes
    do i = 1, NNODZ_TS
      read(mf,*) 
    end do 
    
    maxNSHL_TS = 0
    
    do iel = 1, NEL_TS
		
	  do
       	 read(mf,*) tname, tname2
		 if (tname == 'belem') then
			 read(tname2,*) iNSHL
			 !write(*,*) tname2, iNSHL
			 if ( maxNSHL_TS < iNSHL ) then
				 maxNSHL_TS = iNSHL
			 end if
			 exit
		 end if
	  end do
      
    end do
    
    write(*,*) fname, ": maxNSHL = ", maxNSHL_TS
    
    Npats = 0
    
	do 
		read(mf,*, IOSTAT=Reason) tname
		if (Reason == 0) then
			if (tname == 'set') then
				Npats = Npats + 1
			end if
		else
			exit
		end if
	end do
	
	write(*,*) fname, ": N Sets = ", Npats
    
    close(mf)
    
    P_TS = 3; Q_TS = 3
    maxNSHL_BZ = (P_TS+1)*(Q_TS+1)
    
    
    open(mf, file = fname, status = 'old')
    ! skip "3" lines
	do i = 1, 3
        read(mf,*)  
    end do
    
    allocate(B_NET_TS(NNODZ_TS,NSD+1))
    B_NET_TS   = 0.0d0
    do i = 1, NNODZ_TS
      read(mf,*) tmp, B_NET_TS(i,1:NSD+1)
    end do
    
    allocate(P_BZ(NEL_TS), Q_BZ(NEL_TS), &
             NSHL_BZ(NEL_TS), NSHL_TS(NEL_TS), &
             IEN_TS(NEL_TS,maxNSHL_TS))
    P_BZ = 0; Q_BZ = 0; NSHL_BZ = 0; NSHL_TS = 0; IEN_TS = 0
    
    allocate(BZext(NEL_TS,maxNSHL_TS,maxNSHL_BZ))
    BZext = 0.0d0
    
    do i = 1, NEL_TS
      read(mf,*) tmp, NSHL_TS(i), P_BZ(i), Q_BZ(i)

      NSHL_BZ(i) = (P_BZ(i)+1)*(Q_BZ(i)+1)

      ! check maxNSHL_TS and maxNSHL_BZ
      if (NSHL_TS(i) > maxNSHL_TS) then
        write(*,*) "ERROR: maxNSHL_TS is wrong!!! Should be:", &
                    NSHL_TS(i), maxNSHL_TS
        stop
      end if

      if (NSHL_BZ(i) > maxNSHL_BZ) then
        write(*,*) "ERROR: maxNSHL_BZ is wrong!!! Should be:", &
                    NSHL_BZ(i), maxNSHL_BZ
        stop
      end if

      read(mf,*) IEN_TS(i,1:NSHL_TS(i))
      IEN_TS(i,1:NSHL_TS(i)) = IEN_TS(i,1:NSHL_TS(i)) + 1

      do j = 1, NSHL_TS(i)
        read(mf,*) BZext(i,j,1:NSHL_BZ(i))
      end do

    end do
    
    ! =========== Reading Parts =============
    
    if (NNODZ_TS > NEL_TS) then
  	  allocate(tempArray(NNODZ_TS))
    else
  	  allocate(tempArray(NEL_TS))
    end if
    
    allocate(ELEpart(NEL_TS))
    allocate(partELE(Npats, NEL_TS+2))
    ELEpart = 0
    partELE = 0
    
    
    if (Npats > 0) then
        do i = 1, Npats
	        ! read element type flag and give settings.
	        ! TPTYPE is for part
	        read(mf,*) tname, mp_nel, tname2, tname3, (tempArray(j), j = 1, mp_nel)
            
		    ! find the position of character "_"
		    pos  = scan(tname3,'_')
            
			! if "_" was not found
			if (pos == 0) then
				tname = tname3 
			else
		    	tname = tname3(1:pos-1)
			end if
            
            if (tname == 'fixed' .or. tname == 'fix') then
                partELE(i, 1) = mp_nel
                partELE(i, 2) = -1
            else
                read(tname,*) ptype
                
                tempArray = tempArray + 1
                
                partELE(i, 1) = mp_nel
                partELE(i, 2) = ptype
                do j = 1, mp_nel
                    ELEpart( tempArray(j) ) = ptype
                    partELE(i, j+2) = tempArray(j)
                end do
            end if
            
            !write(*,*) mp_nel
            
        end do
    end if
    
    
    
    deallocate(tempArray)
    
      
end subroutine shell_input_tspline