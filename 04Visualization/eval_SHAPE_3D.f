

      subroutine eval_SHAPE_shell(ifac,gpu,gpv,nshl,shl,shgradl,
     &                            shhessl,nor)

!      use aAdjKeep_shell
!      use commonvar_shell

      use aAdjKeep
      use commonvar

      implicit none


      integer, intent(in)  :: ifac, nshl
      real(8), intent(in)  :: gpu, gpv
      real(8), intent(out) :: nor(NSD)
      real(8), intent(out) :: shl(nshl), shgradl(nshl,2), 
     &                        shhessl(nshl,3)
      real(8) :: M(3,4), N(3,4), Bern(16), dBern(16,2), ddBern(16,3)
      real(8) :: ee, ff, gg, we
      real(8) :: dxdxi(NSD,2)
      integer :: i, j, k, ii, ct, kk, aa, bb

      real(8) :: tmpshl(nshl), tmpshgradl(nshl,2), tmpshhessl(nshl,3)
      real(8) :: shl_sum, shgradl_sum(2), shhessl_sum(3)

      M = 0.0d0
      N = 0.0d0

      shl = 0.0d0
      shgradl = 0.0d0
      shhessl = 0.0d0

      tmpshl = 0.0d0
      tmpshgradl = 0.0d0
      tmpshhessl = 0.0d0


      ! M(1,4), M(2,4) and M(3,4) are the Bernstein basis and
      ! its first and second derivatives, respectively.
      call Bernstein_p3(gpu, M(1,:), M(2,:), M(3,:))
      call Bernstein_p3(gpv, N(1,:), N(2,:), N(3,:))


C       ct = 0
C       do j = 1, 4
C         do i = 1, 4
C           ct = ct + 1
C
C           kk = IEN_TS(ifac,ct)
C
C           Bern(ct)    = M(1,i)*N(1,j)*B_NET_TS(kk,NSD+1)
C
C           dBern(ct,1) = M(2,i)*N(1,j)*B_NET_TS(kk,NSD+1)
C           dBern(ct,2) = M(1,i)*N(2,j)*B_NET_TS(kk,NSD+1)
C
C           ddBern(ct,1) = M(3,i)*N(1,j)*B_NET_TS(kk,NSD+1)
C           ddBern(ct,2) = M(2,i)*N(2,j)*B_NET_TS(kk,NSD+1)
C           ddBern(ct,3) = M(1,i)*N(3,j)*B_NET_TS(kk,NSD+1)
C         end do
C       end do

      ct = 0
      do j = 1, 4
        do i = 1, 4
          ct = ct + 1

          !kk = IEN_TS(ifac,ct)

          Bern(ct)    = M(1,i)*N(1,j)

          dBern(ct,1) = M(2,i)*N(1,j)
          dBern(ct,2) = M(1,i)*N(2,j)

          ddBern(ct,1) = M(3,i)*N(1,j)
          ddBern(ct,2) = M(2,i)*N(2,j)
          ddBern(ct,3) = M(1,i)*N(3,j)
        end do
      end do


      do aa = 1, nshl
        do bb = 1, 16
          tmpshl(aa) = tmpshl(aa) + BZext(ifac,aa,bb)*Bern(bb)

          tmpshgradl(aa,1) = tmpshgradl(aa,1) + 
     &                       BZext(ifac,aa,bb)*dBern(bb,1)
          tmpshgradl(aa,2) = tmpshgradl(aa,2) +
     &                       BZext(ifac,aa,bb)*dBern(bb,2)

          tmpshhessl(aa,1) = tmpshhessl(aa,1) +
     &                       BZext(ifac,aa,bb)*ddBern(bb,1)
          tmpshhessl(aa,2) = tmpshhessl(aa,2) +
     &                       BZext(ifac,aa,bb)*ddBern(bb,2)
          tmpshhessl(aa,3) = tmpshhessl(aa,3) +
     &                       BZext(ifac,aa,bb)*ddBern(bb,3)
        end do
      end do
      
      ! multiply the functions with the weight
      do i = 1, nshl
          we = B_NET_TS(IEN_TS(ifac, i),NSD+1)
          
          tmpshl(i)   = tmpshl(i)*we
          tmpshgradl(i,:) = tmpshgradl(i,:)*we
          tmpshhessl(i,:) = tmpshhessl(i,:)*we
      end do
      
      

      ! for some reason matmul doesn't work for gfortran...
!      tmpshl = MATMUL(BZext(ifac,:,:),Bern)
!      tmpshgradl(:,1) = MATMUL(BZext(ifac,:,:),dBern(:,1)) 
!      tmpshgradl(:,2) = MATMUL(BZext(ifac,:,:),dBern(:,2))
!      tmpshhessl(:,1) = MATMUL(BZext(ifac,:,:),ddBern(:,1))
!      tmpshhessl(:,2) = MATMUL(BZext(ifac,:,:),ddBern(:,2))
!      tmpshhessl(:,3) = MATMUL(BZext(ifac,:,:),ddBern(:,3))

      shl_sum = sum(tmpshl)

      shgradl_sum(1) = sum(tmpshgradl(:,1))
      shgradl_sum(2) = sum(tmpshgradl(:,2))

      shhessl_sum(1) = sum(tmpshhessl(:,1))
      shhessl_sum(2) = sum(tmpshhessl(:,2))
      shhessl_sum(3) = sum(tmpshhessl(:,3))


      shl = tmpshl/shl_sum

      shgradl(:,1) = tmpshgradl(:,1)/shl_sum -
     &               (tmpshl(:)*shgradl_sum(1))/(shl_sum**2)
      shgradl(:,2) = tmpshgradl(:,2)/shl_sum -
     &               (tmpshl(:)*shgradl_sum(2))/(shl_sum**2)

      shhessl(:,1) = tmpshhessl(:,1)/shl_sum -
     &       tmpshgradl(:,1)*shgradl_sum(1)/(shl_sum**2) -
     &       ((tmpshgradl(:,1)*shgradl_sum(1)+tmpshl(:)*shhessl_sum(1))/
     &       (shl_sum**2) 
     &        -2d+0*tmpshl(:)*shgradl_sum(1)*shgradl_sum(1)/
     &       (shl_sum**3))

      shhessl(:,2) = tmpshhessl(:,2)/shl_sum -
     &       tmpshgradl(:,1)*shgradl_sum(2)/(shl_sum**2) -
     &       ((tmpshgradl(:,2)*shgradl_sum(1)+tmpshl(:)*shhessl_sum(2))/
     &       (shl_sum**2) 
     &        -2d+0*tmpshl(:)*shgradl_sum(1)*shgradl_sum(2)/
     &       (shl_sum**3))

      shhessl(:,3) = tmpshhessl(:,3)/shl_sum -
     &       tmpshgradl(:,2)*shgradl_sum(2)/(shl_sum**2) -
     &       ((tmpshgradl(:,2)*shgradl_sum(2)+tmpshl(:)*shhessl_sum(3))/
     &       (shl_sum**2) 
     &        - 2d+0*tmpshl(:)*shgradl_sum(2)*shgradl_sum(2)/
     &       (shl_sum**3))










      ! Now we calculate the face Jacobian
      dxdxi = 0.0d0

      do i = 1, nshl

        k = IEN_TS(ifac,i)

        do ii = 1, 3

!          xi(ii) = xi(ii) + B_NET_TS_REF(k,ii)*shl(i)

          dxdxi(ii,1:2) = dxdxi(ii,1:2) + B_NET_TS(k,ii)*
     &                                    shgradl(i,1:2)
        end do

      end do

      ee = dxdxi(1,1)**2+dxdxi(2,1)**2+dxdxi(3,1)**2
      ff = dxdxi(1,1)*dxdxi(1,2)+dxdxi(2,1)*dxdxi(2,2)+
     &     dxdxi(3,1)*dxdxi(3,2)
      gg = dxdxi(1,2)**2+dxdxi(2,2)**2+dxdxi(3,2)**2

      DetJb = sqrt(ee*gg-ff**2) ! Jacobian of face mapping

      nor(1) = dxdxi(2,1)*dxdxi(3,2) - dxdxi(3,1)*dxdxi(2,2)
      nor(2) = dxdxi(3,1)*dxdxi(1,2) - dxdxi(1,1)*dxdxi(3,2)
      nor(3) = dxdxi(1,1)*dxdxi(2,2) - dxdxi(2,1)*dxdxi(1,2)

      end subroutine eval_SHAPE_shell
      





      subroutine Bernstein_p3(xi, B, dB, ddB)

      implicit none

      real(8), intent(in)  :: xi
      real(8), intent(out) :: B(4), dB(4), ddB(4)

      real(8) :: x

      x = 0.5d0*(1.0d0+xi)

      B(1) = (1.0d0-x)**3
      B(2) = 3.0d0*x*(1.0d0-x)**2
      B(3) = 3.0d0*(x**2)*(1.0d0-x)
      B(4) = x**3
      
      dB(1) = -3.0d0*(1.0d0-x)**2
      dB(2) = 3.0d0*(1.0d0-x)*(1.0d0-3.0d0*x)
      dB(3) = 3.0d0*x*(2.0d0-3.0d0*x)
      dB(4) = 3.0d0*x**2

      dB = dB*0.5d0

      ddB(1) = 6.0d0*(1.0d0-x)
      ddB(2) = 6.0d0*(3.0d0*x-2.0d0)
      ddB(3) = 6.0d0*(1.0d0-3.0d0*x)
      ddB(4) = 6.0d0*x

      ddB = ddB*0.25d0

      end subroutine Bernstein_p3
      
      



      


