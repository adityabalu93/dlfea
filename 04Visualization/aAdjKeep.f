      module aAdjKeep

!...  IEN matches element number a local node number with global node number
      integer, allocatable :: IEN_GLOB(:,:)

!...  Solution vector
      real(8), allocatable :: yg(:,:), x_glob(:,:), y_glob(:,:)

!...  Interpolated solution 
      real(8), allocatable :: QuantLin(:,:,:)

!...  Patch boundary index
      integer, allocatable :: GPBC(:)
      integer :: GPBCount


!...  Added for Bezier extraction
      ! Control Net
      real(8), allocatable :: B_NET_TS(:,:)

      integer, allocatable :: P_BZ(:), Q_BZ(:), NSHL_BZ(:), NSHL_TS(:),
     &                        IEN_TS(:,:)

      ! Bezier extraction operator
      real(8), allocatable :: BZext(:,:,:)
      
!...  Added for Bezier extraction ALL
      ! Control Net
      real(8), allocatable :: B_NET_TS_ALL(:,:)

      integer, allocatable :: P_BZ_ALL(:), Q_BZ_ALL(:), NSHL_BZ_ALL(:),
     &                        NSHL_TS_ALL(:), IEN_TS_ALL(:,:)
     
      real(8), allocatable :: yg_ALL(:,:)

      ! Bezier extraction operator ALL
      real(8), allocatable :: BZext_ALL(:,:,:)

      end module

c-------------------------------------------------------------------------c







