subroutine results_3D(istep)
  
  use aAdjKeep
  use commonvar     
  implicit none
  
  integer, intent(in) :: istep
  integer :: i, j, k, kk, g, sol_file, ierr
  character(len=30) :: fname, cname(2)

  yg = 0.0d0
  
  sol_file = 12
!  fname = trim('restart' // cname(istep)) // cname (iproc)

  ! disp
  write(cname(2),'(I8)')   istep
  fname = 'sh.rest.tsp.'//trim(adjustl(cname(2)))
  open(sol_file, file=fname, status='old', iostat=ierr)

  if (ierr == 0) then
    ! read in the solution data
    do i = 1, NNODZ
      read(sol_file,*) yg(i,1:3)
    end do
    yg(:,1:3) = yg(:,1:3)*sfact
    
    ! fiber as velocity vector 
!     do i = 1, NNODZ
!       read(sol_file,*) yg(i,10:12)
!     end do
    
  end if

  close(sol_file)
  
  ! strain
  write(cname(2),'(I8)')   istep
  fname = 'strain.tsp.'//trim(adjustl(cname(2)))
  open(sol_file, file=fname, status='old', iostat=ierr)

  if (ierr == 0) then
    ! read in the solution data
    do i = 1, NNODZ
      read(sol_file,*) yg(i,4:9)
    end do
  else
    write(*,*) "WARNING: No stress files: "
  end if

  close(sol_file)
  
  ! infos
  fname = 'sh.info.'//trim(adjustl(cname(2)))
  open(sol_file, file=fname, status='old', iostat=ierr)

  if (ierr == 0) then
    ! read in the solution data
    do i = 1, NNODZ
      read(sol_file,*) yg(i,11:16)
    end do
  else
    write(*,*) "WARNING: No sh.info files: "
  end if

  close(sol_file)

!$$$  ! force
!$$$  fname = 'output/for.'//trim(adjustl(cname(1)))//'.dat'
!$$$  open(sol_file, file=fname, status='old', iostat=ierr)
!$$$
!$$$  if (ierr == 0) then
!$$$    ! read in the solution data
!$$$    do i = 1, NNODZ
!$$$      read(sol_file,*) yg(i,4:6)
!$$$    end do
!$$$  end if
!$$$  close(sol_file)
!$$$
!$$$  ! thickness
!$$$  fname = 'output/thi.'//trim(adjustl(cname(1)))//'.dat'
!$$$  open(sol_file, file=fname, status='old', iostat=ierr)
!$$$
!$$$  if (ierr == 0) then
!$$$    do i = 1, NNODZ
!$$$      read(sol_file,*) yg(i,7)
!$$$    end do
!$$$  end if
!$$$  close(sol_file)
!$$$
!$$$  ! stresses
!$$$  write(cname(2),'(I8.2)') kthply
!$$$  fname = 'output/str.'//trim(adjustl(cname(1)))//'.'
!$$$     &    //trim(adjustl(cname(2)))//'.dat'
!$$$  open(sol_file, file=fname, status='old', iostat=ierr)
!$$$
!$$$  if (ierr == 0) then
!$$$    ! read in the solution data
!$$$    do i = 1, NNODZ
!$$$      read(sol_file,*) yg(i,8:13)
!$$$    end do
!$$$  end if
!$$$
!$$$  close(sol_file)
!$$$      
!$$$  ! stress resultant
!$$$  fname = 'output/res.'//trim(adjustl(cname(1)))//'.dat'
!$$$  open(sol_file, file=fname, status='old', iostat=ierr)
!$$$
!$$$  if (ierr == 0) then
!$$$    ! read in the solution data
!$$$    do i = 1, NNODZ
!$$$      read(sol_file,*) yg(i,14:19)
!$$$    end do
!$$$  end if
!$$$
!$$$  close(sol_file)
 
end subroutine results_3D
