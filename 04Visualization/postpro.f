
      subroutine postpro
      
      use aAdjKeep
      use commonvar
      implicit none 
       
c...  Local variables
      integer :: igauss, jgauss, i, j, k, kgauss, aa,
     &           ct, iln, iel, nshl, NGAUSS
      integer :: ni, nj, nk, kk
      real(8) :: gp(2), gw(2)
      real(8) :: dist, distmin, summ, nor(NSD)
      real(8) :: tmp1, tmp2

      real(8), allocatable :: shl(:), shg(:), shgradl(:,:), shhessl(:,:)
      real(8), allocatable :: QuantQuad(:,:), yl(:,:)

c     get Gaussian points and weights
      NEL_LOC = 0 
      NGAUSS  = 2
      gp(1)   = -1.0d0
      gp(2)   =  1.0d0

c      write(*,*) NEL_NZA

      allocate(QuantQuad(4,NVAR+3))

      do iel = 1, NEL

        nshl = NSHL_TS(iel)

        allocate(shl(nshl), shg(nshl),
     &           shgradl(nshl,2), shhessl(nshl,3),
     &           yl(NSHL,NVAR+3))
        
          
          NEL_LOC = NEL_LOC + 1
          
c...      Set up element parameters
  
          ct = 0
          QuantQuad = 0d0
          
c...      Loop over integration points (NGAUSS in each direction)
          
          do jgauss = 1, NGAUSS
             do igauss = 1, NGAUSS
                
                ct = ct+1
                
c...  Get Element Shape functions and their gradients
                
                shl = 0d+0      ! initialize
                shg = 0d+0
                shgradl = 0d+0
                shhessl = 0d+0
                
                nor = 0d+0
                
                call eval_SHAPE_shell(iel, gp(igauss), gp(jgauss),
     &                             nshl, shl, shgradl, shhessl, nor)

                do aa = 1, NSHL
                  yl(aa,1:3) = B_NET_TS(IEN_TS(iel,aa),1:3)
                  yl(aa,4:NVAR+3) = yg(IEN_TS(iel,aa),1:NVAR)
                end do

                do aa = 1, NSHL
                  QuantQuad(ct,:) = QuantQuad(ct,:) + yl(aa,:)*shl(aa)
                  !write(*,*) iel, jgauss, igauss, aa, shl(aa)
                end do
                
!                do kk = 11, 15
!                  if (isnan(QuantQuad(ct,kk))) then
!                    QuantQuad(ct,kk) = 0.0d0
!                  end if
!                end do
                             
             end do
          end do
          
          ! Collect Local Arrays for "linear" elements         
          QuantLin(NEL_LOC,1,:) = QuantQuad(1,:)
          QuantLin(NEL_LOC,2,:) = QuantQuad(2,:)
          QuantLin(NEL_LOC,3,:) = QuantQuad(4,:)
          QuantLin(NEL_LOC,4,:) = QuantQuad(3,:)

        deallocate(shl, shg, shgradl, shhessl, yl)

                  
      end do

      deallocate(QuantQuad)

      end subroutine postpro



      subroutine postpro_int
      
      use aAdjKeep
      use commonvar
      implicit none 
       
c...  Local variables
      integer :: igauss, jgauss, i, j, k, aa,
     &           ct, iln, iel, nshl, NGAUSS
      integer :: ni, nj, nk, kk
      real(8) :: gp(3), gw(3)
      real(8) :: dist, distmin, summ, nor(NSD)
      real(8) :: tmp1, tmp2

      real(8), allocatable :: shl(:), shg(:), shgradl(:,:), shhessl(:,:)
      real(8), allocatable :: QuantQuad(:,:), yl(:,:)

c     get Gaussian points and weights
      NEL_LOC = 0 
      NGAUSS  = 3
      gp(1)   = -1.0d0
      gp(2)   =  0.0d0
      gp(3)   =  1.0d0

      write (*,*) "Interpolating..."

      allocate(QuantQuad(9,NVAR+3))

      do iel = 1, NEL

        nshl = NSHL_TS(iel)

        allocate(shl(nshl), shg(nshl),
     &           shgradl(nshl,2), shhessl(nshl,3),
     &           yl(NSHL,NVAR+3))        
 
          NEL_LOC = NEL_LOC + 4
          
c...      Set up element parameters
  
          ct = 0
          QuantQuad = 0d0
          
c...      Loop over integration points (NGAUSS in each direction)
          
          do jgauss = 1, NGAUSS
             do igauss = 1, NGAUSS
                
                ct = ct+1
                
c...  Get Element Shape functions and their gradients
                
                shl = 0d+0      ! initialize
                shg = 0d+0
                shgradl = 0d+0
                shhessl = 0d+0
                
                nor = 0d+0
                
                call eval_SHAPE_shell(iel, gp(igauss), gp(jgauss),
     &                             nshl, shl, shgradl, shhessl, nor)

                do aa = 1, NSHL
                  yl(aa,1:3) = B_NET_TS(IEN_TS(iel,aa),1:3)
                  yl(aa,4:NVAR+3) = yg(IEN_TS(iel,aa),1:NVAR)
                end do

                do aa = 1, NSHL
                  QuantQuad(ct,:) = QuantQuad(ct,:) + yl(aa,:)*shl(aa)
                end do
                
!                do kk = 11, 15
!                  if (isnan(QuantQuad(ct,kk))) then
!                    QuantQuad(ct,kk) = 0.0d0
!                  end if
!                end do
                             
             end do
          end do
          
          ! Collect Local Arrays for "linear" elements         
          QuantLin(NEL_LOC-3,1,:) = QuantQuad(1,:)
          QuantLin(NEL_LOC-3,2,:) = QuantQuad(2,:)
          QuantLin(NEL_LOC-3,3,:) = QuantQuad(5,:)
          QuantLin(NEL_LOC-3,4,:) = QuantQuad(4,:)
          
          QuantLin(NEL_LOC-2,1,:) = QuantQuad(2,:)
          QuantLin(NEL_LOC-2,2,:) = QuantQuad(3,:)
          QuantLin(NEL_LOC-2,3,:) = QuantQuad(6,:)
          QuantLin(NEL_LOC-2,4,:) = QuantQuad(5,:)
          
          QuantLin(NEL_LOC-1,1,:) = QuantQuad(4,:)
          QuantLin(NEL_LOC-1,2,:) = QuantQuad(5,:)
          QuantLin(NEL_LOC-1,3,:) = QuantQuad(8,:)
          QuantLin(NEL_LOC-1,4,:) = QuantQuad(7,:)
           
          QuantLin(NEL_LOC,1,:) = QuantQuad(5,:)
          QuantLin(NEL_LOC,2,:) = QuantQuad(6,:)
          QuantLin(NEL_LOC,3,:) = QuantQuad(9,:)
          QuantLin(NEL_LOC,4,:) = QuantQuad(8,:)
                  
        deallocate(shl, shg, shgradl, shhessl, yl)

      end do

      deallocate(QuantQuad)

      end subroutine postpro_int
      
      
      subroutine postpro_int_n(NGAUSS)
      
      use aAdjKeep
      use commonvar
      implicit none 
      
      integer, intent(in) :: NGAUSS
       
c...  Local variables
      integer :: igauss, jgauss, i, j, k, aa,
     &           ct, iln, iel, nshl
      integer :: ni, nj, nk, kk
      integer :: INTele
      real(8) :: gp(NGAUSS), gw(NGAUSS)
      real(8) :: dist, distmin, summ, nor(NSD)
      real(8) :: tmp1, tmp2
      real(8) :: intv

      real(8), allocatable :: shl(:), shg(:), shgradl(:,:), shhessl(:,:)
      real(8), allocatable :: QuantQuad(:,:), yl(:,:)

c     get Gaussian points and weights
      NEL_LOC = 0 
      !NGAUSS  = 3
      intv = 2.0d0/(real(NGAUSS,8)-1.0d0)
      do i = 1, NGAUSS
          gp (i) = -1.0d0 + intv*(real(i,8) - 1.0d0)
      end do
      
      INTele = (NGAUSS-1) ** 2

      write (*,*) "Interpolating...", INTele

      allocate(QuantQuad(NGAUSS**2,NVAR+3))

      do iel = 1, NEL

        nshl = NSHL_TS(iel)

        allocate(shl(nshl), shg(nshl),
     &           shgradl(nshl,2), shhessl(nshl,3),
     &           yl(NSHL,NVAR+3))        
 
          NEL_LOC = NEL_LOC + INTele
          
c...      Set up element parameters
  
          ct = 0
          QuantQuad = 0d0
          
c...      Loop over integration points (NGAUSS in each direction)
          
          do jgauss = 1, NGAUSS
             do igauss = 1, NGAUSS
                
                ct = ct+1
                
c...  Get Element Shape functions and their gradients
                
                shl = 0d+0      ! initialize
                shg = 0d+0
                shgradl = 0d+0
                shhessl = 0d+0
                
                nor = 0d+0
                
                call eval_SHAPE_shell(iel, gp(igauss), gp(jgauss),
     &                             nshl, shl, shgradl, shhessl, nor)

                do aa = 1, NSHL
                  yl(aa,1:3) = B_NET_TS(IEN_TS(iel,aa),1:3)
                  yl(aa,4:NVAR+3) = yg(IEN_TS(iel,aa),1:NVAR)
                end do

                do aa = 1, NSHL
                  QuantQuad(ct,:) = QuantQuad(ct,:) + yl(aa,:)*shl(aa)
                end do
                
!                do kk = 11, 15
!                  if (isnan(QuantQuad(ct,kk))) then
!                    QuantQuad(ct,kk) = 0.0d0
!                  end if
!                end do
                             
             end do
          end do
          
          ! Collect Local Arrays for "linear" elements         
          ct = 0
          do i = 1, NGAUSS-1
              do j = 1, NGAUSS-1
                  k = NEL_LOC-ct
                  QuantLin(k,1,:) = QuantQuad(i   +(j-1)*NGAUSS,:)
                  QuantLin(k,2,:) = QuantQuad(i+1 +(j-1)*NGAUSS,:)
                  QuantLin(k,3,:) = QuantQuad(i+1 + j*NGAUSS,:)
                  QuantLin(k,4,:) = QuantQuad(i   + j*NGAUSS,:)
                  ct = ct+1
              end do 
          end do
          
                  
        deallocate(shl, shg, shgradl, shhessl, yl)

      end do

      deallocate(QuantQuad)

      end subroutine postpro_int_n
