      subroutine genstruct(iproc,numproc,intep)
      
      use aAdjKeep
      use commonvar
      implicit none

!...  Arguments
      integer :: iproc,numproc,intep
      
!...  Local variables
      integer,allocatable :: PBC(:)
      integer :: iel,iln, ni, nj, nk, k, kk, ieg
      real(8) :: x_loc(4*NEL_LOC,NSD),distmin,dist
      integer :: GPBPrev,  pcnt 
 
!...  Initialize global patch boundary index

c$$$      if ((icount.ne.1).and.(iproc.eq.1)) then
c$$$        deallocate(GPBC) 
c$$$      endif

      if (iproc.eq.1) then  
        if (intep == 0) then
          allocate(GPBC(NNODZ*numproc)) !! Overestimated!!
        else if (intep == 1) then
          allocate(GPBC(4*NNODZ*numproc)) !! Overestimated!!
        else if (intep > 1) then
          allocate(GPBC((intep-1)**2*NNODZ*numproc)) !! Overestimated!!   	
        end if
        GPBC = 0
        GPBCount = 0
      end if  
                
!...  Initialize local patch boundary index

      if (intep == 0) then
        allocate(PBC(NEL))
      else if (intep == 1) then
        allocate(PBC(NEL*4))
      else if (intep > 1) then
        allocate(PBC(NEL*(intep-1)**2))  
      end if     
      PBC = 0

!... Loop over elements
      
      NEL_LOC      = 0

      do iel = 1, NEL
      
!...    Check to see if current element has nonzero area
!        ni = INN(IEN(iel,1),1)  ! get NURB coordinates
!        nj = INN(IEN(iel,1),2)
        
!        if ((U_KNOT(ni).ne.U_KNOT(ni+1)).and.
!     &      (V_KNOT(nj).ne.V_KNOT(nj+1))) then

!	  if (((ni.eq.P+1).or.(ni.eq.MCP)).or.   
!     &	      ((nj.eq.Q+1).or.(nj.eq.NCP))) then

     
!            if (intep == 0) then
!              PBC(NEL_LOC+1) = 1 
!            else
!              PBC(NEL_LOC+1:NEL_LOC+4) = 1 
!!            endif
     
!          endif

          if (intep == 0) then
            NEL_LOC = NEL_LOC + 1
          else if (intep == 1) then
            NEL_LOC = NEL_LOC + 4
          else if (intep > 1) then
            NEL_LOC = NEL_LOC + (intep-1)**2 
          endif
          
!        endif       
      enddo
      != end of looping through elements ============================



      ! Build Linear IEN Based on Spatial Location   
         
c$$$      if ((icount.ne.1).and.(iproc.eq.1)) then
c$$$        deallocate(IEN_GLOB) 
c$$$      endif

      if (iproc.eq.1) then
        allocate(IEN_GLOB(NEL_LOC*20*numproc,4))
      endif
      
      GPBPrev    = GPBCount 
      nnodz_loc(iproc)  = 0    
      distmin    = 5.0d-8
      
      pcnt    = 1
      
      write (*,*) "Building linear structure..." 
      
      do iel = 1, NEL_LOC
        ieg = iel + NEL_GLOB
        if (iel.eq.pcnt*NEL_LOC/5) then     
          write(*,*) ieg,"/",NEL_LOC+NEL_GLOB
          pcnt = pcnt + 1
        end if
               
        do iln = 1, 4

         ! If node is on patch boundary 
         ! check for corresponding global node
         if (PBC(iel).eq.1) then
           do k = GPBPrev, 1,-1
             kk = GPBC(k)

            dist = (x_glob(kk,1) - QuantLin(iel,iln,1))**2 +
     &             (x_glob(kk,2) - QuantLin(iel,iln,2))**2 +
     &             (x_glob(kk,3) - QuantLin(iel,iln,3))**2

            dist = sqrt(dist)
      
            ! Repeated Global Node
            if (dist <= distmin) then
              IEN_GLOB(ieg,iln) = kk 
              goto 1000
            endif
          enddo
        endif  ! PBC endif

         ! If not global node search locally
         ! Check for repeated nodes
         do k = nnodz_loc(iproc),1,-1
            
           ! Distance to existing Global Nodes
           dist = (x_loc(k,1) - QuantLin(iel,iln,1))**2 +
     &            (x_loc(k,2) - QuantLin(iel,iln,2))**2 +
     &            (x_loc(k,3) - QuantLin(iel,iln,3))**2
            
           dist = sqrt(dist)

           ! Repeated local Node 
           if (dist <= distmin) then
             IEN_GLOB(ieg,iln) = k + nnodz_glob
             goto 1000
           end if
         end do
         
         ! If not local node
         ! Add New Global Node
         nnodz_loc(iproc) = nnodz_loc(iproc) + 1
         IEN_GLOB(ieg,iln) = nnodz_loc(iproc) + nnodz_glob
         
         if (PBC(iel).eq.1) then
           GPBCount = GPBCount + 1
           GPBC(GPBCount) = IEN_GLOB(ieg,iln)
         end if
         x_loc(nnodz_loc(iproc),:) = 
     &         QuantLin(iel,iln,1:3)
         
 1000     continue
          
        end do
      end do

      deallocate(PBC) 
      deallocate(GPBC)

      end subroutine genstruct
      
c$$$!=============================
c$$$
c$$$      subroutine readstruct(numproc)
c$$$      
c$$$      use aAdjKeep
c$$$      use commonvar
c$$$      implicit none    
c$$$      
c$$$      integer :: numproc, sfile, ierr , ieg, npstruct, iproc       
c$$$      character(len=30) :: fname
c$$$      character(len=10) :: cname
c$$$      
c$$$      
c$$$      allocate(nnodz_loc(numproc))
c$$$      
c$$$      linstruct = 0    
c$$$    
c$$$      sfile = 14          
c$$$      fname = 'struct'// trim(cname(intep))//'.dat'
c$$$      open(sfile, file=fname, status='old', iostat=ierr)
c$$$     
c$$$      if (ierr == 0) then
c$$$        write (*,*) "Reading linear structure..."
c$$$     
c$$$        read(sfile,'(2I8)') npstruct, NEL_GLOB
c$$$
c$$$        if (npstruct /= numproc) then
c$$$          write(*,*)  "Numprocs does not coincide with struct file"
c$$$          call exit
c$$$        end if
c$$$
c$$$        do iproc = 1, numproc
c$$$          read(sfile,'(I8)') nnodz_loc(iproc) 
c$$$        end do
c$$$        
c$$$        allocate(IEN_GLOB(NEL_GLOB,8))
c$$$        do ieg = 1, NEL_GLOB        
c$$$          read(sfile,'(8I8)') IEN_GLOB(ieg,:) 
c$$$        end do
c$$$
c$$$        close(sfile)
c$$$   
c$$$      else
c$$$        write (*,*) "Linear structure: ", trim(fname), 
c$$$     &              " not found :: will build ..."
c$$$        linstruct = 1
c$$$      end if
c$$$      
c$$$      end subroutine readstruct
c$$$
c$$$!=============================
c$$$
c$$$      subroutine writestruct(numproc)
c$$$      
c$$$      use aAdjKeep
c$$$      use commonvar
c$$$      implicit none   
c$$$      
c$$$      integer :: numproc, sfile, ieg, iproc  
c$$$      character(len=30) :: fname
c$$$      character(len=10) :: cname
c$$$
c$$$      write (*,*) 'Writing linear structure...'
c$$$
c$$$      sfile = 15
c$$$      fname = trim('struct'// trim(cname(intep)) //'.dat') 
c$$$      open(sfile,name=fname, status='replace')     
c$$$ 
c$$$      write(sfile,'(2I8)') numproc,NEL_GLOB
c$$$      
c$$$      do iproc = 1, numproc
c$$$        write(sfile,'(I8)') nnodz_loc(iproc) 
c$$$      end do
c$$$      
c$$$      do ieg = 1, NEL_GLOB        
c$$$        write(sfile,'(8I8)') IEN_GLOB(ieg,:) 
c$$$      end do
c$$$
c$$$      close(sfile)
c$$$            
c$$$      end subroutine writestruct      
c$$$
!=============================

      subroutine applystruct(iproc, numproc)
        
      use aAdjKeep
      use commonvar
      implicit none
      
!...  Arguments
      integer :: iproc, numproc
      
!...  Local variables
      integer :: iel, ieg, iln, idx, i
      integer :: ielA(4*NEL_LOC),ilnA(4*NEL_LOC)
         
      write (*,*) "Applying linear structure..." 
        
      ! Allocate
c$$$      if ((icount /= 1).and.(iproc == 1)) then
c$$$        deallocate(x_glob) 
c$$$        if (NVAR >= 1) deallocate(y_glob) 
c$$$      end if
     
      if (iproc == 1) then      
        allocate(x_glob(nnodz_loc(iproc)*20*numproc,NSD)) 
        allocate(y_glob(nnodz_loc(iproc)*20*numproc,NVAR))
      end if


      ! Invert map 
      do iel = 1, NEL_LOC
        ieg = iel + NEL_GLOB
        do iln = 1, 4
   
          idx = IEN_GLOB(ieg,iln) 
  
          x_glob(idx,:) = 
     &       QuantLin(iel,iln,1:3)

          if (NVAR >= 1) then
            y_glob(idx,:) = 
     &          QuantLin(iel,iln,4:NVAR+3)     
          endif
  
        end do
      end do
     
      end subroutine applystruct
