import os
import keras
import numpy as np
from IO.reader import readfile, readevalfile

np.random.seed(0)

class DataProcessor(keras.utils.Sequence):
    """
    'Generator for data'
    """
    def __init__(self, data_list, batch_size=32, train = False):
        self.data_list = data_list
        self.train = train
        self.indexes = np.arange(len(self.data_list))
        self.batch_size = batch_size
        self.labelled = True
        self._u = 17
        self._v = 12
        self._numleaflets = 3
        if self.train:
            np.random.shuffle(self.indexes)

    def __len__(self):
        """'Denotes the number of batches per epoch'"""
        return int(len(self.data_list) / self.batch_size)

    def __getitem__(self, index):
        """'Generate one batch of data'"""
        # Generate indexes of the batch
        if index == 'all':
            indexes = self.indexes
        else:
            indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Generate data
        if self.labelled == True:
            X, y = self._data_process(indexes)
            return X, y
        else:
            X = self._data_process(indexes)
            return X

    def on_epoch_end(self):
        """'Updates indexes after each epoch'"""
        if self.train == True:
            np.random.shuffle(self.indexes)


    def _data_process(self, indexes):
        """'Generates data containing batch_size samples'"""
        inputs = [[None for _ in range(len(indexes))] for _ in range(self._numleaflets + 2)]
        if self.labelled:
            outputs = [[None for _ in range(len(indexes))] for _ in range(self._numleaflets + 1)]
        for i, index in enumerate(indexes):
            if self.train:
                shiftval = np.random.normal(loc = 0.0 , scale = 0.2)
            else:
                shiftval = 0.0

            if self.labelled:
                input_sample, output_sample = readfile(self.data_list[index], self._u, self._v, self._numleaflets, self.labelled)
            else:
                input_sample = readfile(self.data_list[index], self._u, self._v, self._numleaflets, self.labelled)

            for j in range(self._numleaflets + 2):
                if j < self._numleaflets:
                    inputs[j][i] = input_sample[j] + shiftval
                else:
                    inputs[j][i] = input_sample[j]

            if self.labelled:
                for j in range(self._numleaflets + 1):
                    if j < self._numleaflets:
                        outputs[j][i] = output_sample[j] + shiftval
                    else:
                        outputs[j][i] = output_sample[j]

        if self.labelled is True:
            inputs = [np.array(ip) for ip in inputs]
            outputs = [np.array(op) for op in outputs]
            return inputs, outputs
        else:
            inputs = [np.array(ip) for ip in inputs]
            return inputs


def split_data(data_list):
    num_data_points = len(data_list)
    print(num_data_points)
    indices = np.arange(num_data_points)
    np.random.shuffle(indices)
    train_split = 0.6
    val_split = 0.2
    test_split = 1 - train_split - val_split

    train_indices = indices[:int(num_data_points*train_split)]
    val_indices = indices[int(num_data_points*train_split):int(num_data_points*(train_split+val_split))]
    test_indices = indices[int(num_data_points*(train_split+val_split)):]
    return [data_list[idx] for idx in train_indices], [data_list[idx] for idx in val_indices] ,[data_list[idx] for idx in test_indices]



