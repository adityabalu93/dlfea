import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='3'
os.environ['KERAS_BACKEND']="tensorflow"
os.environ['CUDA_VISIBLE_DEVICES']='0'
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
from keras import backend as K
K.set_session(sess)
K.set_image_data_format('channels_last')
from keras import backend as K
from keras.models import Model
from keras import layers
from keras.layers.core import Activation, Flatten, Dense, Dropout
from keras.layers import Conv2D, SeparableConv2D, Conv2DTranspose, concatenate, Add
from keras.layers.pooling import MaxPooling2D
from keras.layers import Input, Reshape, RepeatVector
from keras.layers.normalization import BatchNormalization
from keras.regularizers import l2
import numpy as np
from keras.models import load_model
from keras.losses import logcosh

def conv_filters(x,
              filters,
              num_row,
              num_col,
              padding='valid',
              strides=(1, 1),
              name=None):
    if name is not None:
        bn_name = name + '_bn'
        conv_name = name + '_conv'
    else:
        bn_name = None
        conv_name = None
    if K.image_data_format() == 'channels_first':
        bn_axis = 1
    else:
        bn_axis = 3
    x = SeparableConv2D(
        filters, (num_row, num_col),
        strides=strides,
        padding=padding,
        name=conv_name)(x)
    x = BatchNormalization(axis=bn_axis, scale=False, name=bn_name)(x)
    x = Activation('relu', name=name)(x)
    return x

def encoder(x):
    x1 = conv_filters(x, 3, 3, 3)
    x2 = conv_filters(x1, 6, 3, 3)
    x3 = conv_filters(x2, 12, 3, 3)
    x4 = conv_filters(x3, 12, 3, 3)
    # x6 = conv_filters(x5, 64, 3, 3)
    x5 = MaxPooling2D((2, 2))(x4)
    x6 = conv_filters(x5, 12, 1, 1)
    x7 = Flatten()(x6)
    return x7


def loadmodel(batch_size = 32):
    input_shape = (17,12,3)
    x1 = Input(shape = input_shape, name = 'leaflet1')
    x2 = Input(shape = input_shape, name = 'leaflet2')
    x3 = Input(shape = input_shape, name = 'leaflet3')
    x4 = Input(shape = (1,), name = 'pressure')
    x5 = Input(shape = (4,), name = 'params')
    
    branch1 = encoder(x1)
    branch2 = encoder(x2)
    branch3 = encoder(x3)

    x41 = RepeatVector(15)(x4)
    x41 = Flatten()(x41)

    x51 = Dense(32)(x5)

    FC1 = concatenate([branch1, branch2, branch3, x41, x51])
    FC2 = Dense(64)(FC1)
    FC2 = BatchNormalization(axis=-1)(FC2)
    FC2 = Activation('relu')(FC2)
    FC3 = concatenate([FC2, x4])
    FC3 = Dense(64, activation='relu')(FC2)

    code_layer = Dense(64, activation='relu')(FC3)
    coaptation_layer = Dense(8, activation='relu')(code_layer)

    coaptation_layer = Dense(1, activation='relu')(coaptation_layer)

    dense_layer1 = Dense(64, activation='relu')(code_layer)
    dense_layer2 = Dense(64, activation='relu')(code_layer)
    dense_layer3 = Dense(64, activation='relu')(code_layer)


    dense_layer1 = Dense(8 * 9 * 4, activation='relu')(dense_layer1)
    dense_layer2 = Dense(8 * 9 * 4, activation='relu')(dense_layer2)
    dense_layer3 = Dense(8 * 9 * 4, activation='relu')(dense_layer3)


    if K.image_data_format() == 'channels_first':
        output_shape = (batch_size, 8, 9, 4)
    else:
        output_shape = (batch_size, 9, 4, 8)


    disp1 = Reshape(output_shape[1:])(dense_layer1)
    disp2 = Reshape(output_shape[1:])(dense_layer2)
    disp3 = Reshape(output_shape[1:])(dense_layer3)

    # deconv_layer1 = Conv2DTranspose(16, (5, 5), strides=(1, 1))
    # deconv_layer2 = Conv2DTranspose(3, (3, 3), strides=(1, 1))
    # deconv_layer3 = Conv2DTranspose(3, (3, 3), strides=(1, 1))

    disp1 = Conv2DTranspose(8, (5, 5), strides=(1, 1))(disp1)
    disp2 = Conv2DTranspose(8, (5, 5), strides=(1, 1))(disp2)
    disp3 = Conv2DTranspose(8, (5, 5), strides=(1, 1))(disp3)


    disp1 = Conv2DTranspose(4, (3, 3), strides=(1, 1))(disp1)
    disp2 = Conv2DTranspose(4, (3, 3), strides=(1, 1))(disp2)
    disp3 = Conv2DTranspose(4, (3, 3), strides=(1, 1))(disp3)

    disp1 = Conv2DTranspose(3, (3, 3), strides=(1, 1))(disp1)
    disp2 = Conv2DTranspose(3, (3, 3), strides=(1, 1))(disp2)
    disp3 = Conv2DTranspose(3, (3, 3), strides=(1, 1))(disp3)


    disp1 = Conv2DTranspose(3, (1, 1), strides=(1, 1))(disp1)
    disp2 = Conv2DTranspose(3, (1, 1), strides=(1, 1))(disp2)
    disp3 = Conv2DTranspose(3, (1, 1), strides=(1, 1))(disp3)


    K.set_epsilon(1e-09)
    def disp_loss(y_true, y_pred, clip_delta=1.0):
        se = K.mean(K.square(y_pred - y_true), axis=-1)
        # ae = K.mean(K.square(y_pred - y_true)*y_true, axis=-1)
        # clipper_max = K.max(K.square(y_pred - y_true), axis=-1)
        # clipper_min = K.min(K.square(y_pred - y_true))
        # y_true = K.clip((y_true - K.min(y_true))/K.max(y_true), K.epsilon(), 1)
        # y_pred = K.clip((y_pred - K.min(y_pred))/K.max(y_pred), K.epsilon(), 1)
        # kld = K.sum(y_true * K.log(y_true / y_pred), axis=-1)
        # boundary_cond = 
        return se


    model=Model(inputs=[x1, x2, x3, x4, x5], outputs=[disp1, disp2, disp3, coaptation_layer])

    model.compile(loss=[disp_loss, disp_loss, disp_loss,'mse'], optimizer='adam')
    model.summary()
    return model
