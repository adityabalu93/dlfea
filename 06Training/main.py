import os
import json
import argparse
import numpy as np
parser = argparse.ArgumentParser(description='Experiments for optimizer')
parser.add_argument('-g','--gpu', default='0', type=str,
                help='gpu id to use from 0 to 3')
parser.add_argument('-w','--write', default=False, type=bool,
                help='writing the validation and test data')
parser.add_argument('-t','--train', default=True, type=bool,
                help='performing training or loading the model')
parser.add_argument('-d','--data',type=str,
                help='Provide the path of the data you want to load and predict')
parser.add_argument('-tp','--test_pressure',type=float,
                help='Provide test pressure applied on the heart valve')
parser.add_argument('-tt','--test_thickness',type=float,
                help='Provide the thickness of the heart valve')
args = parser.parse_args()
os.environ["CUDA_VISIBLE_DEVICES"]=args.gpu

from IO.reader import readfiles, readevalfile
from eval import evaluatemodel, predictevaldata
from preprocess import split_data, DataProcessor
from train import loadmodel
from keras.callbacks import EarlyStopping, TensorBoard
from keras.models import model_from_json
from modelcheckpoint import ModelCheckpointLight, load_weight_light

def main():
    if not args.data:
        filepath = os.path.dirname(os.path.realpath(__file__))
        parent, _ = os.path.split(filepath)
        resultspath = os.path.join(parent,"PredData")
        with open('successlist.json', 'r') as f:
            data_list = json.load(f)

        print('************** Splitting data *****************')
        train_list, val_list, test_list = split_data(data_list)

        if not os.path.exists(resultspath):
            os.makedirs(resultspath)
        if args.train:
            print('************** Training data loader initialize *****************')
            train_data = DataProcessor(train_list, train=True, batch_size=32)
            val_data = DataProcessor(val_list, train=False, batch_size=32)
            test_data = DataProcessor(test_list, train=False, batch_size=32)
            
            print('************** Loading and setting up model *****************')
            feamodel = loadmodel(batch_size = 32)
            json_string = feamodel.to_json()
            with open((os.path.join(resultspath,'model.json')), 'w') as f:
                f.write(json_string)

            early_stopping = EarlyStopping(monitor='val_loss', patience=30)
            model_checkpoint = ModelCheckpointLight(os.path.join(resultspath,'weights.h5'),
                                     monitor='val_loss', verbose=1, save_best_only=True)

            print('************** Training data *****************')
            feamodel.fit_generator(train_data,  epochs = 200, validation_data = val_data,
                callbacks = [model_checkpoint, early_stopping], verbose = 1, use_multiprocessing = True,
                workers = 36)

            # Loading the best weights
            feamodel.load_weights(os.path.join(resultspath,'weights.h5'))
        else:
            print('************** Loading and setting up model *****************')
            with open((os.path.join(resultspath, 'model.json')), 'r') as f:
                loaded_model_json = f.read()
            feamodel = model_from_json(loaded_model_json)

            # Loading the best weights
            load_weight_light(feamodel, os.path.join(resultspath, 'weights.h5'))
            
            # compile the model to perform inference
            feamodel.compile(loss='mse', optimizer='adam')

        evaluatemodel(feamodel, val_data, test_data, resultspath, args.write)
    else:
        print('************** Loading and setting up model *****************')
        try:
            with open((os.path.join(resultspath, 'model.json')), 'r') as f:
                loaded_model_json = f.read()
        except:
            raise IOError('Check the path for the model!')
        feamodel = model_from_json(loaded_model_json)
        # Loading the best weights
        try:
            feamodel.load_weights(os.path.join(resultspath, 'weights.h5'))
        except:
            raise IOError('Check the existence of the weights or the correctness of weights')
        # compile the model to perform inference
        feamodel.compile(loss='mse', optimizer='adam')

        data = readevalfile(args.data,args.test_pressure,args.test_thickness)

        predictevaldata(feamodel,data,args.data,args.write)


if __name__ == '__main__':
    main()
