import os

def ReadToutput(filename):
    with open(filename,'r') as f:
        pts = []
        for line in f:
            pt = [float(val) for val in  line.strip().split()]
            pts.append(pt)
        assert len(pts)%3 == 1 # since there are displacements, velocity and acceleration for each ctrl point
    # print(len(pts))
    return pts[:int((len(pts) -1)/3)], pts[-1] 

def ReadOutput(nmesh, foldername):
    tOutput, time_stamp = ReadToutput(os.path.join(foldername, 'sh.rest.tsp.160'))
    tmesh = ReadTmeshDat(os.path.join(foldername, 'tmesh.0.dat'))
    return ConvertOutput(nmesh, tmesh, tOutput)

def ReadTmeshDat(filename):
    patch = {}
    with open(filename, 'r') as f:
        elemRead = False
        elemstart = False
        nodelist = False
        for line in f:
            if 'type' in line and elemRead is False:
                patch['patch type'] = line.strip().split()[1]
            elif 'nodeN' in line and elemRead is False:
                patch['num nodes'] = int(line.strip().split()[1])
                patch['nodes'] = [[0.0 for _ in range(4)] for _ in range(patch['num nodes'])]
                nodeIdx = 0
            elif 'elemN' in line and elemRead is False:
                patch['num elems'] = int(line.strip().split()[1])
                patch['elems'] = [{} for _ in range(patch['num elems'])]
                elemIdx = 0
            elif 'node ' in line and elemRead is False:
                try:
                    words = line.strip().split()
                    if words[0] in 'node':
                        patch['nodes'][nodeIdx] = [float(f) for f in words[1:]]
                    nodeIdx = nodeIdx + 1
                except IndexError:
                    raise ValueError('num nodes lesser the nodes')
                if nodeIdx == patch['num nodes']:
                    elemRead = True
            elif elemRead is True:
                if line.strip().split()[0] == 'belem':
                    patch['elems'][elemIdx]['num nodes'] = int(line.strip().split()[1])
                    patch['elems'][elemIdx]['degree'] = [int(line.strip().split()[2]), int(line.strip().split()[2])]
                    elemstart = True
                    nodelist = True
                elif elemstart is True and nodelist is True:
                    assert len(line.strip().split()) == patch['elems'][elemIdx]['num nodes']
                    patch['elems'][elemIdx]['nodes'] = [int(idx) for idx in line.strip().split()]
                    nodelist = False
                    patch['elems'][elemIdx]['connectivity'] = []
                elif elemstart and nodelist is False:
                    assert len(line.strip().split()) == patch['elems'][elemIdx]['num nodes']
                    patch['elems'][elemIdx]['connectivity'].append([float(val) for val in line.strip().split()])
                    if len(patch['elems'][elemIdx]['connectivity']) == (patch['elems'][elemIdx]['degree'][0] + 1)*(patch['elems'][elemIdx]['degree'][0] + 1):
                        elemstart = False
                        elemIdx = elemIdx + 1
                if elemIdx == patch['num elems']:
                    elemRead = False
                    patch['sets'] = []
    return patch

def ConvertOutput(nmesh, tmesh, toutput):
    noutput = [None for _ in range(nmesh['num nodes'])]
    for eid, (telem, nelem) in enumerate(zip(tmesh['elems'], nmesh['elems'])) :
        for idx, tnode in enumerate(telem['nodes']):
            output = toutput[tnode]
            noutput[nelem['nodes'][idx]] = output

    for output in noutput:
        if output is None:
            raise ValueError('mapping Failed')
    return noutput


def GetCoaptation(filename):
    with open(filename,'r') as f:
        lines = f.readlines()
    last_line = lines[-1]
    last_line = last_line.strip().split()
    coaptation_val = float(last_line[0])
    return coaptation_val