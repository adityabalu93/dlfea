import numpy as np
import os
from .mesh import ReadPatch
from .params import ReadParam
from .SimOutputs import ReadOutput, GetCoaptation
import zipfile
import io
import joblib
from tqdm import tqdm
import concurrent.futures


def saveCompressed(fh, **namedict):
     with zipfile.ZipFile(fh,
                          mode="w",
                          compression=zipfile.ZIP_DEFLATED,
                          allowZip64=True) as zf:
         for k, v in namedict.items():
             buf = io.BytesIO()
             np.lib.npyio.format.write_array(buf,
                                             np.asanyarray(v),
                                             allow_pickle=True)
             zf.writestr(k + '.npy',
                         buf.getvalue())

def readfile(datapath, u, v, numleaflets, labelled):
    if labelled:
        if os.path.isfile(os.path.join(datapath,'target.npz')):
            data = np.load(os.path.join(datapath,'target.npz'))
            inputs, outputs = data['inputs'], data['outputs']
            return data['inputs'], data['outputs']
        else:
            if os.path.isfile(os.path.join(datapath,'sh.rest.tsp.160')):
                nmesh = ReadPatch(os.path.join(datapath,'tmesh.1.iga'))
                params = ReadParam(os.path.join(datapath,'param.dat'))
                patch = np.array(nmesh['nodes'])
                assert len(patch) == u*v*numleaflets
                leaflet1 = ((patch[:u*v]).reshape(u, v, 4))[:,:,:3]
                leaflet2 = ((patch[u*v:2*u*v]).reshape(u, v, 4))[:,:,:3]
                leaflet3 = ((patch[2*u*v:]).reshape(u, v, 4))[:,:,:3]
                pressure = params['InitPress']
                matparams = np.array([params['Thickness_Shell'], params['MtCoef_1'], params['MtCoef_2'], params['MtCoef_3']])
                disps = ReadOutput(nmesh, datapath)
                cpval = GetCoaptation(os.path.join(datapath,'coaptation'))
                disp1 = (np.array(disps[:u*v]).reshape(u, v, 3))
                disp2 = (np.array(disps[u*v:2*u*v]).reshape(u, v, 3))
                disp3 = (np.array(disps[2*u*v:]).reshape(u, v, 3))
                data = {}
                data['inputs'] = [leaflet1, leaflet2, leaflet3, pressure, matparams]
                data['outputs'] = [disp1, disp2, disp3, cpval]
                saveCompressed(open(os.path.join(datapath,'target.npz'), 'wb'), **data)
                return data['inputs'], data['outputs']
            else:
                raise ValueError('Target not available')
    else:
        if os.path.isfile(os.path.join(datapath,'input.npz')):
            data = np.load(os.path.join(datapath,'input.npz'))
            return data['inputs']
        else:
            if os.path.isfile(os.path.join(datapath,'tmesh.1.iga')):
                nmesh = ReadPatch(os.path.join(datapath,'tmesh.1.iga'))
                params = ReadParam(os.path.join(datapath,'param.dat'))
                patch = np.array(nmesh['nodes'])
                assert len(patch) == u*v*numleaflets
                leaflet1 = ((patch[:u*v]).reshape(u, v, 4))[:,:,:3]
                leaflet2 = ((patch[u*v:2*u*v]).reshape(u, v, 4))[:,:,:3]
                leaflet3 = ((patch[2*u*v:]).reshape(u, v, 4))[:,:,:3]
                pressure = params['InitPress']
                matparams = np.array([params['Thickness_Shell'], params['MtCoef_1'], params['MtCoef_2'], params['MtCoef_3']])
                disps = ReadOutput(nmesh, datapath)
                cpval = GetCoaptation(os.path.join(datapath,'coaptation'))
                disp1 = (np.array(disps[:u*v]).reshape(u, v, 3))
                disp2 = (np.array(disps[u*v:2*u*v]).reshape(u, v, 3))
                disp3 = (np.array(disps[2*u*v:]).reshape(u, v, 3))
                data = {}
                data['inputs'] = [leaflet1, leaflet2, leaflet3, pressure, matparams]
                saveCompressed(open(os.path.join(datapath,'input.npz'), 'wb'), **data)
                return data['inputs']
            else:
                raise ValueError('Param not available')



def _readfiles(folderpath):
    data_list = os.listdir(folderpath)
    data_size = len(data_list)
    data = []
    _u = 17
    _v = 12
    _numleaflets = 3
    data = [None for _ in range(data_size)]

    with concurrent.futures.ThreadPoolExecutor() as executor:
        futures = [executor.submit(__readfile, folderpath, datapath) for datapath in tqdm(data_list[:int(0.01*data_size)])]
        data = [fut.result() for fut in futures]

    data_dict = {'data':data}
    saveCompressed(open(os.path.join(folderpath,'data.npz'), 'wb'), **data_dict)
    return data


def readfiles(folderpath):
    try:
        data = np.load(os.path.join(folderpath,'data.npz'))
        data = data['data']
    except FileNotFoundError:
        print('binary files not found, loading raw files and generating them')
        data = _readfiles(folderpath)
    return data


def readevalfile(datapath):
    data = []
    params = ReadParam(os.path.join(datapath,'param.dat'))
    patch = ReadPatch(os.path.join(datapath,'tmesh.1.iga'))
    data.append([patch, params])
    return data

