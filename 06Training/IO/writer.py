import numpy as np
import os
import scipy.io as io

def writemeshfile(filename, data):
    u_ctrl_pts = 17
    v_ctrl_pts = 12
    with open(filename,'w') as f:
        f.write('3\r\n')
        f.write('3 3\r\n')
        f.write('17 12\r\n')
        f.write('0 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 14 14 14\r\n')
        f.write('0 0 0 0 1 2 3 4 5 6 7 8 9 9 9 9\r\n')
        for u in range(u_ctrl_pts):
            for v in range(v_ctrl_pts):
                f.write('{} {} {} {}\r\n'.format(data[u,v,0],data[u,v,1], data[u,v,2], 1))


def writeresultfile(filename,data):
    u_ctrl_pts = 17
    v_ctrl_pts = 12
    with open(filename,'w') as f:
        f.write('         160  1.599999999999997E-002  1.599999999999997E-002\r\n')
        for u in range(u_ctrl_pts):
            for v in range(v_ctrl_pts):
                f.write('{} {} {}\r\n'.format(data[u,v,0],data[u,v,1], data[u,v,2]))

def write_coaptation_val(filename, true_value, pred_value):
    with open(filename,'w') as f:
        f.write('True Coaptation Area: {} Predicted Coaptation Area: {}\r\n'.format(true_value, pred_value))


def write_data(path,data,predictions):
# This method assumes that the final output geometry is the deformed positions
# Ensure that the displacements are added with the input geometry before this method
    folderpath = os.path.join(path,'runs')
    if not os.path.exists(folderpath):
        os.makedirs(folderpath)

    data_size = predictions[0].shape[0]

    for i in range(data_size):
        geom_num = data[0][-2][i][0]
        thickness_fn = data[0][-1][i][0]
        pressure = data[0][3][i][0]
        datapoint_path = os.path.join(folderpath,'%s_%s_%d'%(str(geom_num),str(thickness_fn),pressure),'setup')
        if not os.path.exists(datapoint_path):
            os.makedirs(datapoint_path)
        for j in range(1,4):
            writeresultfile(os.path.join(datapoint_path,'sh.rest.%s.0%s'%(140, j)), predictions[j-1][i])
        for j in range(1,4):
            writeresultfile(os.path.join(datapoint_path,'sh.rest.%s.0%s_original'%(140, j)), data[1][j-1][i])
        for j in range(1,4):
            writemeshfile(os.path.join(datapoint_path,'smesh.%s.dat'%(j)), data[0][j-1][i])
        write_coaptation_val(os.path.join(datapoint_path,'coaptation.txt'), data[1][-1][i], predictions[-1][i])

def writematfile(path,data,predictions,data_type):

    data = list(data)
    datanames = ['inputs_', 'outputs_']
    data_dict = {}
    for i, datatype in enumerate(data):
        for j, array in enumerate(datatype):
            varname = datanames[i]+str(j)+'_original'
            data_dict[varname] = array

    for j, array in enumerate(predictions):
        varname = datanames[1]+str(j)+'_predicted'
        data_dict[varname] = array

    io.savemat(os.path.join(path,data_type+'_data'),data_dict)

def write_pred_data(datapoint_path,data,predictions):
# This method assumes that the final output geometry is the deformed positions
# Ensure that the displacements are added with the input geometry before this method
    print(predictions[1].shape)
    for j in range(1,4):
        writeresultfile(os.path.join(datapoint_path,'sh.rest.%s.0%s_predicted'%(140, j)), predictions[j-1][0])
    for j in range(1,4):
        writeresultfile(os.path.join(datapoint_path,'sh.rest.%s.0%s_original'%(140, j)), data[1][j-1][0])
    write_coaptation_val(os.path.join(datapoint_path,'coaptation.txt'), data[1][-1][0], predictions[-1][0])
