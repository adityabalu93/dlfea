from keras.models import model_from_json
from IO.writer import write_data, writematfile, write_pred_data
import numpy as np
import scipy.io
from keras import backend as K

def evaluatemodel(model,val,test, path, write=False):
    val.labelled = False
    val_pred = model.predict_generator(val)
    val.labelled = True
    validation_metrics = model.evaluate_generator(val)
    test.labelled = False
    test_pred = model.predict_generator(test)
    test.labelled = True
    test_metrics = model.evaluate_generator(test)
    print("validation_metrics:", validation_metrics)
    print("test_metrics:", test_metrics)
    if write:
        write_data(path, val, val_pred)
        write_data(path, test, test_pred)


def predictevaldata(model,data, path, write=False):
    pred = model.predict_generator(data)
    metrics = model.evaluate_generator(data)
    print("metrics:", metrics)
    if write:
        write_pred_data(path, data, pred)
